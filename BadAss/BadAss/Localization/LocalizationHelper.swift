//
//  LocalizationHelper.swift
//  Obsidiam
//
//  Created by DAY on 26/04/20.
//  Copyright © 2020 DAY. All rights reserved.
//


import UIKit


let kEnglish = "en"
let kHindi = "hi"

let CurrentLanguageKey = "CurrentLanguageKey"
let DefaultLanguage = kEnglish
let BaseBundle = "Base"

/// Name for language change notification
public let LanguageChangeNotification = "LanguageChangeNotification"

// MARK: Localization Syntax
public func Localized(_ string: String) -> String {
    return string.localized()
}


public extension String {
    /**
     friendly localization syntax, replaces NSLocalizedString
     - Returns: The localized string.
     */
    func localized() -> String {
        let bundle: Bundle = .main
        if let path = bundle.path(forResource: LocalizationHelper.currentLanguage(), ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        } else if let path = bundle.path(forResource: BaseBundle, ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        } else {
            return self
        }
    }
}

class LocalizationHelper: NSObject {
    /**
     List available languages
     - Returns: Array of available languages.
     */
    open class func availableLanguages(_ excludeBase: Bool = false) -> [String] {
        var availableLanguages = Bundle.main.localizations
        // If excludeBase = true, don't include "Base" in available languages
        if let indexOfBase = availableLanguages.firstIndex(of: "Base"), excludeBase == true {
            availableLanguages.remove(at: indexOfBase)
        }
        return availableLanguages
    }
    
    /**
     Current language
     - Returns: The current language. String.
     */
    open class func currentLanguage() -> String {
        if let currentLanguage = UserDefaults.standard.object(forKey: CurrentLanguageKey) as? String {
            return currentLanguage
        }
        return DefaultLanguage
    }
    
    /**
     Change the current language
     - Parameter language: Desired language.
     */
    open class func setCurrentLanguage(_ language: String) {
        let selectedLanguage = availableLanguages().contains(language) ? language : DefaultLanguage
        UserDefaults.standard.set(selectedLanguage, forKey: CurrentLanguageKey)
        UserDefaults.languageCode = selectedLanguage
        UserDefaults.standard.synchronize()
    }
    
    /**
     Default language
     - Returns: The app's default language. String.
     */
    open class func defaultLanguage() -> String {
        var defaultLanguage: String = String()
        guard let preferredLanguage = Bundle.main.preferredLocalizations.first else {
            return DefaultLanguage
        }
        let availableLanguages: [String] = self.availableLanguages()
        if availableLanguages.contains(preferredLanguage) {
            defaultLanguage = preferredLanguage
        } else {
            defaultLanguage = DefaultLanguage
        }
        return defaultLanguage
    }
    
    /**
     Resets the current language to the default
     */
    open class func resetCurrentLanguageToDefault() {
        setCurrentLanguage(defaultLanguage())
    }
    
    /**
     Get the current language's display name for a language.
     - Parameter language: Desired language.
     - Returns: The localized string.
     */
    open class func displayNameForLanguage(_ language: String) -> String {
        let locale: NSLocale = NSLocale(localeIdentifier: currentLanguage())
        if let displayName = locale.displayName(forKey: NSLocale.Key.identifier, value: language) {
            return displayName
        }
        return String()
    }
}



class LocalisedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitle((titleLabel?.text ?? "").localized(), for: .normal)
    }
}


class LocalisedLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        text = (text ?? "").localized()
    }
}
