//
//  QBManager.swift
//  iTouch
//
//  Created by Day User on 16/10/19.
//  Copyright © 2019 saffron. All rights reserved.
//

import Foundation
import Quickblox

var unreadCount = 0
var deafultPageSize = 30

struct QBCredentials {
    static let applicationId: UInt      = 86086
    static let authorizationKey         = "rZ7y4EzDePKE4cF"
    static let authorizationSecret      = "DzEn944ywLzZnQ7"
    static let accountKey               = "JhWyQzSNDSb5pTzURsbW"
    static let accountId                = 116195
    static let password                 = "badass@123"
}


protocol QBManagerDelegate {
    func connectionEstablished()
    func connectionFailed()
    func connectionEstablishing()
}

extension QBManagerDelegate {
    func connectionEstablished() {}
    func connectionFailed() {}
    func connectionEstablishing() {}
}

enum ChatConnection: Int {
    case Connecting = 0
    case Connected
    case Failed
    case None
}

class QBManager: NSObject {
    
    //Shared Instance
    static let shared: QBManager = {
        let shared = QBManager()
        return shared
    }()
    
    private override init() {}
    
    var connectionStatus: ChatConnection = .None
    var currentUserId: UInt = 0
    var currentUser: QBUUser?
    
    var connectionDelegates: [QBManagerDelegate] = []
    
    // MARK:- Initialization Methods
    func setupQB() {
        QBSettings.applicationID = QBCredentials.applicationId
        QBSettings.authKey = QBCredentials.authorizationKey
        QBSettings.authSecret = QBCredentials.authorizationSecret
        QBSettings.accountKey = QBCredentials.accountKey
        QBSettings.keepAliveInterval = 30
        QBSettings.autoReconnectEnabled = true
        QBSettings.reconnectTimerInterval = 3
        QBSettings.carbonsEnabled = true
        QBSettings.logLevel = .errors
    }
    
    //MARK:- Authorization Methods
    
    func manageUserAuthorization() {
        
        connectionStatus = .Connecting
        connectionDelegates.forEach({ (delegate) in
            delegate.connectionEstablishing()
        })
        
        guard let loggedInUser = AuthManager.shared.loggedInUser else {
            connectionStatus = .Failed
            connectionDelegates.forEach({ (delegate) in
                delegate.connectionFailed()
            })
            return
        }
        guard let username = loggedInUser.username  else {
            connectionStatus = .Failed
            connectionDelegates.forEach({ (delegate) in
                delegate.connectionFailed()
            })
            return
        }
        self.login(login: username)
    }
    
    
    func signup() {
        guard let loggedInUser = AuthManager.shared.loggedInUser else {
            self.connectionStatus = .Failed
            self.connectionDelegates.forEach({ (delegate) in
                delegate.connectionFailed()
            })
            return
        }
        guard let username = loggedInUser.username  else {
            self.connectionStatus = .Failed
            self.connectionDelegates.forEach({ (delegate) in
                delegate.connectionFailed()
            })
            return
        }
                
        let user = QBUUser()
        user.externalUserID = UInt(loggedInUser.user_id ?? 0)
        user.login = username
        user.password = QBCredentials.password
        user.fullName = loggedInUser.fullname
        user.email = loggedInUser.email
        user.customData = loggedInUser.image
        
        QBRequest.signUp(user, successBlock: { [weak self] response, user in
            guard let selfff = self else {
                self?.connectionStatus = .Failed
                self?.connectionDelegates.forEach({ (delegate) in
                    delegate.connectionFailed()
                })
                return
            }
            selfff.login(login: username)
            }, errorBlock: { [weak self] response in
                if response.status == QBResponseStatusCode.validationFailed {
                    // The user with existent login was created earlier
                    self?.login(login: username)
                    return
                } else {
                    self?.connectionStatus = .Failed
                    self?.connectionDelegates.forEach({ (delegate) in
                        delegate.connectionFailed()
                    })
                }
        })
    }
    
    
    func login(login: String, password: String = QBCredentials.password) {
        QBRequest.logIn(withUserLogin: login,
                        password: password,
                        successBlock: { [weak self] response, user in
                            guard let selfff = self else {
                                self?.connectionStatus = .Failed
                                self?.connectionDelegates.forEach({ (delegate) in
                                    delegate.connectionFailed()
                                })
                                return
                            }
                            selfff.currentUser = user
                            selfff.currentUserId = user.id
                            selfff.loginToChat()
                            selfff.updateQBIdOnServer(qbId: user.id)
            }, errorBlock: { response in
                if response.status == QBResponseStatusCode.unAuthorized {
                    // Clean profile
                    self.signup()
                } else {
                    self.connectionStatus = .Failed
                    self.connectionDelegates.forEach({ (delegate) in
                        delegate.connectionFailed()
                    })
                }
        })
    }
    
    func updateQBIdOnServer(qbId: UInt) {
        
        guard let userId = AuthManager.shared.loggedInUser?.user_id else {
            return
        }
        
        var requestDict = Dictionary<String,Any>()
        requestDict[kQbId] = "\(qbId)"
        requestDict[kUserId] = userId
        WebServices.updateUserSettings(params: requestDict, loader: false) { (response) in
            if response == nil {
                Debug.log("Error In Update of QB_Id")
            }
        }
    }
    
    func uploadProfilePic() {
        if let urlString = AuthManager.shared.loggedInUser?.image {
            let updateParam = QBUpdateUserParameters()
            updateParam.customData = urlString
            QBRequest.updateCurrentUser(updateParam, successBlock: { (response , user) in
                Debug.log("\(user)")
            }, errorBlock: nil)
        }
    }
    
    
    func updateProfileName() {
        if let fullName = AuthManager.shared.loggedInUser?.fullname {
            let updateParam = QBUpdateUserParameters()
            updateParam.fullName = fullName
            QBRequest.updateCurrentUser(updateParam, successBlock: { (response , user) in
                Debug.log("\(user)")
            }, errorBlock: nil)
        }
    }

    //MARK:- Chat Initialization
    func loginToChat() {
        self.connectionStatus = .Connecting
        self.connectionDelegates.forEach({ (delegate) in
            delegate.connectionEstablishing()
        })
        QBChat.instance.connect(withUserID: currentUserId, password: QBCredentials.password) { (error) in
            if let err = error {
                Debug.log("\(err)")
                self.connectionStatus = .Failed
                self.connectionDelegates.forEach({ (delegate) in
                    delegate.connectionFailed()
                })
            }
            self.connectionStatus = .Connected
            self.connectionDelegates.forEach({ (delegate) in
                delegate.connectionEstablished()
            })
            AppDelegate.shared.registerForRemoteNotification()
        }
    }
    
    func disconnectChat() {
        if QBChat.instance.isConnected {
            QBChat.instance.disconnect { (error) in
            }
        }
    }
    
    func logout() {
        QBRequest.logOut(successBlock: { (response) in
            self.disconnectChat()
        }) { (error) in
            
        }
    }
    
    func isConnected() -> Bool {
        return QBChat.instance.isConnected
    }
    
    
    //MARK:- Notifications Related Methods
    
    func registerPushToken(token: Data) {
        
        guard QBChat.instance.isConnected else { return }
        let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
        let qbSubscription = QBMSubscription()
        qbSubscription.notificationChannel = .APNS
        qbSubscription.deviceUDID = deviceIdentifier
        qbSubscription.deviceToken = token
        qbSubscription.devicePlatform = "iOS"
        QBRequest.createSubscription(qbSubscription, successBlock: { (response, subscriptions) in
            Debug.log("\(response)")
        }) { (response) in
            Debug.log("\(response)")
        }
        
        
        guard let userId = AuthManager.shared.loggedInUser?.user_id else {
            return
        }
        
        var requestDict = Dictionary<String,Any>()
        requestDict[kDeviceToken] = UserDefaults.fcmToken
        requestDict[kUserId] = userId

        WebServices.updateUserSettings(params: requestDict, loader: false) { (response) in
            if response == nil {
                Debug.log("Error In Update of FCM Token")
            }
        }
    }

    
    func removePushToken() {
        let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString ?? ""
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response) in
            Debug.log("\(response)")
        }) { (error) in
            Debug.log("\(error)")
        }
        
        
        guard let userId = AuthManager.shared.loggedInUser?.user_id else {
            return
        }
        
        var requestDict = Dictionary<String,Any>()
        requestDict[kDeviceToken] = "No Device Token"
        requestDict[kUserId] = userId
        WebServices.updateUserSettings(params: requestDict, loader: false) { (response) in
            if response == nil {
                Debug.log("Error In Update of FCM Token")
            }
        }
    }
    
    
    func deleteUser() {
        QBRequest.deleteCurrentUser(successBlock: { (response) in
            Debug.log("\(response)")
        }) { (response) in
            Debug.log("\(response)")
        }
    }
    
    
    func sendPush(toIds: [UInt], payload: [String: Any]) {
        // Send push to users with ids 292,300,1295
 
        var data: Data? = nil
        do {
            data = try JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
        } catch {
        }
        var message: String? = nil
        if let data = data {
            message = String(data: data, encoding: .utf8)
        }

        let event = QBMEvent()
        event.notificationType = .push
        event.usersIDs = toIds.map({"\($0)"}).joined(separator: ",")
        event.type = .oneShot
        event.message = message
        event.prepareMessage()

        QBRequest.createEvent(event, successBlock: { response, events in
            // Successful response with event
            Debug.log("Response ------->>>>> \(response)  Events ------>>>>> \(String(describing: events))")
        }, errorBlock: { response in
            // Handle error
            Debug.log("Response ------->>>>> \(response)")
        })
        
    }
    
    
    //MARK:- Manage Contacts List
    
    func sendContactRequest(qbId: String, onCompletion: @escaping (_ status: Bool, _ error: NSError?) -> Void) {
        let userID: UInt = UInt(qbId) ?? 0
        QBChat.instance.addUser(toContactListRequest: userID) { (error) in
            if let error = error {
                onCompletion(false, error as NSError)
            } else {
                onCompletion(true, nil)
            }
        }
    }
    
    func acceptContactRequest(qbId: String, onCompletion: @escaping (_ status: Bool, _ error: NSError?) -> Void) {
        let userID: UInt = UInt(qbId) ?? 0
        QBChat.instance.confirmAddContactRequest(userID) { (error) in
            if let error = error {
                onCompletion(false, error as NSError)
            } else {
                onCompletion(true, nil)
            }
        }
    }
    
    //MARK:- Chat Dialogs Methods
    
    func getAllDialogs(pageSize: Int = deafultPageSize, page: Int = 0, onCompletion: @escaping ([QBChatDialog]) -> Void ) {
        let extendedRequest = ["sort_desc" : "last_message_date_sent"]
        let page = QBResponsePage(limit: pageSize, skip: (page * pageSize))
        QBRequest.dialogs(for: page, extendedRequest: extendedRequest, successBlock: { (response: QBResponse, dialogs: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, page: QBResponsePage?) -> Void in
            let filteredDialogs = dialogs?.filter({ $0.data?["is_blocked"] as? Bool == false }) ?? []
            onCompletion(filteredDialogs)
        }) { (response: QBResponse) -> Void in
            onCompletion([])
        }
    }
    
    
    func getCountOfDialogs(onCompletion: @escaping (Int?) -> Void ) {
        QBRequest.countOfDialogs(withExtendedRequest: nil, successBlock: { (response : QBResponse!, count : UInt) -> Void in
            onCompletion(Int(count))
        }) { (response : QBResponse!) -> Void in
            onCompletion(nil)
        }
    }
    
    
    func getDialogWithUser(qbId: String, onCompletion: @escaping (Bool, QBChatDialog?) -> Void) {
        let qb_ids = [qbId, "\(self.currentUserId)"].sorted(by: { $0 < $1 }).joined(separator: ",")
        let extendedRequest: [String: String] = [
            "data[class_name]" : "Request",
            "data[qb_ids]" : qb_ids,
        ]
        
        QBRequest.dialogs(for: QBResponsePage(limit: 10, skip: 0), extendedRequest: extendedRequest, successBlock: { (response: QBResponse, dialogs: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, page: QBResponsePage?) in
            if response.isSuccess {
                if let dialogs = dialogs, dialogs.count > 0 {
                    onCompletion(true, dialogs[0])
                } else {
                    onCompletion(false, nil)
                }
            } else {
                onCompletion(false, nil)
            }
        }) { (response) in
            onCompletion(false, nil)
        }
    }
    
    
    func checkForDialog(otherUserId: String, withAdvisor: Bool = false, onCompletion: @escaping (Bool, QBChatDialog?, _ isNewDialog: Bool) -> Void) {
            
        let qb_ids = [otherUserId, "\(self.currentUserId)"].sorted(by: { $0 < $1 }).joined(separator: ",")
        let extendedRequest: [String: String] = [
            "data[class_name]" : "Request",
            "data[qb_ids]" : qb_ids,
        ]
        
        QBRequest.dialogs(for: QBResponsePage(limit: 10, skip: 0), extendedRequest: extendedRequest, successBlock: { (response: QBResponse, dialogs: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, page: QBResponsePage?) in
            if response.isSuccess {
                if let dialogs = dialogs, dialogs.count > 0 {
                    onCompletion(true, dialogs[0], false)
                } else {
                    self.createNewDialog(otherUserId: otherUserId, withAdvisor: withAdvisor) { (success, dialog) in
                        if success , let dialog = dialog {
                            onCompletion(true, dialog, true)
                        } else {
                            onCompletion(false, nil, false)
                        }
                    }
                }
            } else {
                onCompletion(false, nil, false)
            }
        }) { (response) in
            onCompletion(false, nil, false)
        }
    }
    
    
    func createNewDialog(otherUserId: String, withAdvisor: Bool = false, onCompletion: @escaping (Bool, QBChatDialog?) -> Void) {
        let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: .private)
        chatDialog.occupantIDs = [NSNumber(integerLiteral: Int(otherUserId) ?? 0)]
        
        let qb_ids = [otherUserId, "\(self.currentUserId)"].sorted(by: { $0 < $1 }).joined(separator: ",")
        chatDialog.data = [
            "class_name": "Request",
            "qb_ids": qb_ids,
            "is_blocked": false,
            "requester_id": "\(self.currentUserId)",
            "is_requested": withAdvisor ? false : true,
            "is_declined": false
        ]
        
        QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
            onCompletion(true, createdDialog)
        }, errorBlock: {(response: QBResponse!) in
            onCompletion(false, nil)
        })
    }
    
    func deleteDialog(roomId: String, onCompletion: @escaping (Bool) -> Void) {
        QBRequest.deleteDialogs(withIDs: Set(arrayLiteral: roomId), forAllUsers: false,
                                       successBlock: { (response: QBResponse!, deletedObjectsIDs: [String]?, notFoundObjectsIDs: [String]?, wrongPermissionsObjectsIDs: [String]?) -> Void in
                                        onCompletion(true)
        }) { (response: QBResponse!) -> Void in
            onCompletion(false)
        }
    }
    
    
    func loadDialog(withID dialogID: String, completion: @escaping (_ loadedDialog: QBChatDialog?) -> Void) {
        let responsePage = QBResponsePage(limit: 1, skip: 0)
        let extendedRequest = ["_id": dialogID]
        QBRequest.dialogs(for: responsePage, extendedRequest: extendedRequest,
                          successBlock: { response, dialogs, dialogsUsersIDs, page in
                            guard let chatDialog = dialogs.first else {
                                completion(nil)
                                return
                            }
                            completion(chatDialog)
        }, errorBlock: { response in
            completion(nil)
            Debug.log("[ChatManager] loadDialog error: \(response)")
        })
    }
    
    
    //MARK:- Chat methods
    
    func getChatHistory(pageSize: Int = 50, page: Int = 0, dialogId: String, search: String? = "", onCompletion: @escaping ([QBChatMessage]) -> Void) {
        let resPage = QBResponsePage(limit: pageSize, skip: (page * pageSize))
        
        var request: [String: String] = ["sort_desc" : "date_sent"]
        if let searchText = search, searchText.length > 0 {
            request["message[ctn]"] = searchText
        }
        
        QBRequest.messages(withDialogID: dialogId, extendedRequest: request, for: resPage, successBlock: {(response: QBResponse, messages: [QBChatMessage]?, responcePage: QBResponsePage?) in
            onCompletion(messages ?? [])
        }, errorBlock: {(response: QBResponse!) in
            onCompletion([])
        })
    }
    
    func getUnreadMessageCounts(onCompletion: @escaping (UInt?, Bool) -> Void) {
        let dialogsIDs: NSSet = NSSet(array: [])
        QBRequest.totalUnreadMessageCountForDialogs(withIDs: dialogsIDs as! Set<String>, successBlock: { (response, count, data) in
            onCompletion(count, true)
            unreadCount = Int(count)
        }) { (response) in
            onCompletion(nil, false)
        }
    }
    
    func getMessagesCount(dialogId: String, onCompletion: @escaping (Int?) -> Void) {
        QBRequest.countOfMessages(forDialogID: dialogId, extendedRequest: nil, successBlock: { (res, count) in
            onCompletion(Int(count))
        }) { (error) in
             onCompletion(nil)
        }
    }
    
    func deleteChatMessage(messageId: String, onCompletion: @escaping (Bool) -> Void) {
        QBRequest.deleteMessages(withIDs: Set(arrayLiteral:messageId), forAllUsers: false, successBlock: { (response: QBResponse!) -> Void in
            onCompletion(true)
        }) { (response: QBResponse!) -> Void in
            onCompletion(false)
        }
    }
    
    func readMessage(message: QBChatMessage) {
        if message.markable {
            QBChat.instance.read(message) { (err) in
                
            }
        }
    }
    
    
    
    //MARK:- Block Unblock Using Dialog Custom Parameters
    
    func block(qbId: String, onCompletion: @escaping (Bool) -> Void) {
        getDialogWithUser(qbId: qbId) { (success, dialog) in
            if let dialog = dialog {
                var data = dialog.data
                data?["class_name"] = "Request"
                data?["is_blocked"] = true
                
                let chatDialog = QBChatDialog(dialogID: dialog.id, type: .private)
                chatDialog.data = data
                
                QBRequest.update(chatDialog, successBlock: { (response, dialog) in
                    if response.isSuccess {
                        onCompletion(true)
                    } else {
                        onCompletion(false)
                    }
                }) { (response) in
                    onCompletion(false)
                }
            } else {
                onCompletion(false)
            }
        }
    }
    
    
    func unblock(qbId: String, onCompletion: @escaping (Bool) -> Void) {
        getDialogWithUser(qbId: qbId) { (success, dialog) in
            if let dialog = dialog {
                
                var data = dialog.data
                data?["class_name"] = "Request"
                data?["is_blocked"] = false

                let chatDialog = QBChatDialog(dialogID: dialog.id, type: .private)
                chatDialog.data = data
                
                QBRequest.update(chatDialog, successBlock: { (response, dialog) in
                    if response.isSuccess {
                        onCompletion(true)
                    } else {
                        onCompletion(false)
                    }
                }) { (response) in
                    onCompletion(false)
                }
            } else {
                onCompletion(false)
            }
        }
    }
    
    
}



//MARK:- Chat Custom Image View
var chatImageCache = NSCache<NSString, UIImage>()

class ChatImageView: UIImageView {
    
    var attachmentId: String = ""
    var userImageKeyId: String?

    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    func setImageFromQBAttachament(attachment: QBChatAttachment) {
        
        if let image = chatImageCache.object(forKey: (attachment.id ?? "") as NSString) {
            if self.attachmentId == attachment.id {
                self.image = image
            }
            return
        }
        
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
        activityIndicator.center = self.center
        
        if let id = UInt(attachment.id!) {
            QBRequest.downloadFile(withID: id, successBlock: { (response, data) in
                self.activityIndicator.removeFromSuperview()
                
                let image: UIImage? = UIImage(data: data)
                if let img = image {
                    chatImageCache.setObject(img, forKey: (attachment.id ?? "") as NSString)
                }
                if self.attachmentId == attachment.id {
                    self.image = image
                }
                
            }, statusBlock: { (request, status) in
                
            }) { (response) in
                self.activityIndicator.removeFromSuperview()
            }
        } else {
            self.activityIndicator.removeFromSuperview()
        }
    }
    
    
    func setImageFromDialog(dialog: QBChatDialog, OnCompletion: @escaping () -> Void) {

        if let otherUserId = dialog.occupantIDs?.filter({$0.uintValue != QBManager.shared.currentUserId}).first {
            
            if let image = chatImageCache.object(forKey: ("UserImage:\(otherUserId)") as NSString) {
                if self.userImageKeyId == "\(otherUserId)" {
                    self.image = image
                }
                return
            }
            
            DispatchQueue.global(qos: .background).async {
                
                QBRequest.user(withID: UInt(truncating: otherUserId), successBlock: { (response, user) in
                    if let url = user.customData {
                        if let image = chatImageCache.object(forKey: url as NSString) {
                            if self.userImageKeyId == "\(otherUserId)" {
                                DispatchQueue.main.async {
                                    self.image = image
                                }
                            }
                            return
                        }
                        if let imageURL = URL(string: url) {
                            DispatchQueue.global(qos: .background).async {
                                do {
                                    let imgData = try Data(contentsOf: imageURL)
                                    let image = UIImage(data: imgData)
                                    
                                    if let img = image {
                                        chatImageCache.setObject(img, forKey: ("UserImage:\(otherUserId)") as NSString)
                                    }
                                    if self.userImageKeyId == "\(otherUserId)" {
                                        DispatchQueue.main.async {
                                            self.image = image
                                        }
                                    }
                                } catch {
                                    
                                }
                            }
                        }
                    }
                }, errorBlock: nil)
            }
        }
    }
    
}



