//
//  QuestionCount.swift
//  BadAss
//
//  Created by Dharmendra Sinha on 15/11/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper

class QuestionCount : Mappable {

    var count_id: Int?
    var free_count: String?
    var free_count_status: String?
    var paid_count: String?
    var paid_count_status: String?

    init() {
    }
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        count_id <- map["count_id"]
        free_count <- map["free_count_questions"]
        free_count_status <- map["free_count_status"]
        paid_count <- map["paid_count_questions"]
        paid_count_status <- map["paid_count_status"]
    }
}
