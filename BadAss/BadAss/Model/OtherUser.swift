//
//  OtherUser.swift
//  BadAss
//
//  Created by DAY on 03/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper

struct OtherUser : Mappable {
    var qb_id : String?
    var phone : String?
    var email : String?
    var votes_count : Int?
    var show_dob : String?
    var fan_count : String?
    var chat_privacy : String?
    var follow_status : String?
    var preferred_languages : [String]?
    var country : Country?
    var show_my_profile : String?
    var blocked_status : String?
    var country_id : String?
    var name : String?
    var show_gender : String?
    var social_id : String?
    var user_id : Int?
    var followings_count : String?
    var language : String?
    var username : String?
    var age : String?
    var about_me : String?
    var dob : String?
    var profile_pic : String?
    var access_token : String?
    var phone_code : String?
    var gender : String?
    var device_token : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        qb_id <- map["qb_id"]
        phone <- map["phone"]
        email <- map["email"]
        votes_count <- map["votes_count"]
        show_dob <- map["show_dob"]
        fan_count <- map["fan_count"]
        chat_privacy <- map["chat_privacy"]
        follow_status <- map["follow_status"]
        preferred_languages <- map["preferred_languages"]
        country <- map["country"]
        show_my_profile <- map["show_my_profile"]
        blocked_status <- map["blocked_status"]
        country_id <- map["country_id"]
        name <- map["name"]
        show_gender <- map["show_gender"]
        social_id <- map["social_id"]
        user_id <- map["user_id"]
        followings_count <- map["followings_count"]
        language <- map["language"]
        username <- map["username"]
        age <- map["age"]
        about_me <- map["about_me"]
        dob <- map["dob"]
        profile_pic <- map["profile_pic"]
        access_token <- map["access_token"]
        phone_code <- map["phone_code"]
        gender <- map["gender"]
        device_token <- map["device_token"]
    }

}
