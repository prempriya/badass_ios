//
//  Page.swift
//  BadAss
//
//  Created by DAY on 06/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation

struct Page {
    
    var page: Int = 1
    var pageSize: Int = 10
    var totalPages: Int = 1
    var totalRecords: Int = 0
    
    var isExecuting: Bool = false
    var hasNext: Bool = false

    
    func hasNextPage() -> Bool {
        return page < totalPages
    }
    
}
