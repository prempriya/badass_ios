//
//  Country.swift
//  BadAss
//
//  Created by DAY on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class Country: Mappable {
    
    var id: String?
    var name: String?
    
    required init?(map: Map){
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    
    func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
    }
    
}
