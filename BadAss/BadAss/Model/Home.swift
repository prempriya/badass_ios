import Foundation
import ObjectMapper

class HomeModel : Mappable {
    var video_url : String?
    var video_id : Int?
    var vote_count : String?
    var creator : Creator?
    var thumbnail: String?
    var comments_count: String?
    var reply_video_count: String?

    var vote_status: String?
    var reply_videos = [HomeModel]()
    
    
    var challengeName: String?
    var url: String?
    var player: AVPlayer?
    var playerItem: CachingPlayerItem?
    var isPlayed = false
    var timer = Timer()
    var counter = 0
    var isEnded = false
    
    init() {
    }
    
    required init?(map: Map){
    }
    
    
    func addObserver() {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue:
        OperationQueue.main) {  notification in //1
            self.player?.seek(to: CMTime.zero)
            if self.isPlayed {
            }
        }
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func mapping(map: Map) {
        video_url <- map["video_url"]
        video_id <- map["video_id"]
        vote_count <- map["vote_count"]
        creator <- map["creator"]
        thumbnail <- map["thumbnail"]
        vote_status <- map["vote_status"]
        comments_count <- map["comments_count"]
        reply_video_count <- map["reply_video_count"]
        reply_videos <- map["reply_videos"]
    }
    
    
    func initialisePlayer() {
        if let urlString = video_url, let url = URL(string: urlString) {
            Debug.log("Initialized")
            self.player = AVPlayer(url: url)
        }
    }
    
    func deinitialisePlayer() {
        Debug.log("De-Initialized")
        self.player = nil
    }
    
}

class Creator : Mappable {
    var user_id : Int?
    var name : String?
    var profile_pic : String?
    var fan_count : String?
    var show_profile : String?
    var chat_privacy : String?
    var follow_status : String?
    var username: String?
    var qb_id: String?
    
    init() {
    }
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        user_id <- map["user_id"]
        name <- map["name"]
        profile_pic <- map["profile_pic"]
        fan_count <- map["fan_count"]
        show_profile <- map["show_profile"]
        chat_privacy <- map["chat_privacy"]
        follow_status <- map["follow_status"]
        username  <- map["username"]
        qb_id <- map["qb_id"]
    }
    
}

class SearchModel : Mappable {
    var videos = [HomeModel]()
    var users = [SuggestedUser]()
    
    init() {
    }
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        videos <- map["videos"]
        users <- map["users"]
       
    }
    
}




