//
//  Filter.swift
//  BadAss
//
//  Created by DAY on 28/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation

class Filter: NSObject, NSCoding {

    static let shared: Filter = Filter()
    
    var category : String = "0"
    var isLatest : Bool = false
    var languages : [String] = []
    var trending : Bool = true

    private override init() {
        
    }
    
    @objc required init(coder aDecoder: NSCoder) {
        category = aDecoder.decodeObject(forKey: "category") as! String
        isLatest = aDecoder.decodeObject(forKey: "isLatest") as! Bool
        languages = aDecoder.decodeObject(forKey: "languages") as! [String]
        trending = aDecoder.decodeObject(forKey: "trending") as! Bool
    }

    @objc func encode(with aCoder: NSCoder) {
        aCoder.encode(category, forKey: "category")
        aCoder.encode(isLatest, forKey: "isLatest")
        aCoder.encode(languages, forKey: "languages")
        aCoder.encode(trending, forKey: "trending")
    }
    
}
