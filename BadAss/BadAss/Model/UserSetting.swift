//
//  UserSetting.swift
//  BadAss
//
//  Created by DAY on 19/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class UserSetting: Mappable {
    
    var qb_id: String?
    var deviceToken: String?
    var language: String?
    var chatPrivacy: String?
    var showMyProfile: String?

    init() {
        
    }
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        qb_id <- map["qb_id"]
        deviceToken <- map["device_token"]
        language <- map["language"]
        showMyProfile <- map["show_my_profile"]
        chatPrivacy <- map["chat_privacy"]
    }
    
}
