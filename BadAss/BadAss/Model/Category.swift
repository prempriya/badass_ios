//
//  Category.swift
//  BadAss
//
//  Created by DAY on 11/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class Category: Mappable {
    
    var id: String?
    var category: String?
    var categoryHindi: String?
    var status: String?
    var isSelected: Bool = false
    var image: String?
    
    required init?(map: Map){
    }
    
    init(id: String, category: String, categoryHindi: String, status: String) {
        self.id = id
        self.category = category
        self.status = status
        self.categoryHindi = categoryHindi
    }
    
    
    func mapping(map: Map){
        id <- map["id"]
        category <- map["category"]
        categoryHindi <- map["category_hindi"]
        status <- map["status"]
        image <- map["image"]
    }
    
}
