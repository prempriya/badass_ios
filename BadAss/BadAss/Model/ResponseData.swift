//
//  ResponseData.swift
//  SafeHouse
//
//  Created by DEEPAK on 07/05/19.
//  Copyright © 2019 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class ResponseData < T: Mappable >: Mappable{
    var message: String?
    var object: T?
    var array : [T]?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        message <- map[Constants.kMessage]
        object <- map[Constants.kData]
        array <- map[Constants.kData]
        statusCode <- map[Constants.Code]
    }
}

class Initial: Mappable {
    
    var otp: Int?
    var authToken: String?
    var profile_picture: String?
    var comment_id: Int?
    var qb_id: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        otp <- map[Constants.Code]
        authToken <- map[Constants.AUTH_TOKEN]
        profile_picture <- map[Constants.kProfile_Picture]
        comment_id <- map["comment_id"]
        qb_id <- map["qb_id"]
    }
}

class NotificationSetting: Mappable{
    var receive_notification: String?
    var status = false
    
    init(){
        
    }
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        receive_notification <- map["receive_notification"]
        status = receive_notification == "A"
    }
}




/// --- Multipart handling modal (upload image and upload video)---
class VideoModal: Mappable {
    var video_id: Int?
    var video_url: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        video_id <- map[Constants.Video_ID]
        video_url <- map[Constants.Video_URL]
    }
}

class ImageModal: Mappable {
    var image_id: Int?
    var image_url: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        image_url <- map[Constants.kImage_URL]
    }
}
