//
//  Language.swift
//  BadAss
//
//  Created by DAY on 11/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class Language: Mappable {
    
    var id: String?
    var language: String?
    var status: String?
    var isSelected: Bool = false
    
    init(id: String) {
        self.id = id
    }
    
    required init?(map: Map){
    }
    
    func mapping(map: Map){
        id <- map["id"]
        language <- map["language"]
        status <- map["status"]
    }
    
}

