//
//  SuggestedUSer.swift
//  BadAss
//
//  Created by DAY on 02/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper

class SuggestedUser : Mappable {
    
    var user_id : Int?
    var name : String?
    var username: String?
    var email : String?
    var qb_id : String?
    var profile_pic : String?
    var show_my_profile : String?
    var follow_status: String?

    init() {
    }
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        user_id <- map["user_id"]
        name <- map["name"]
        username <- map["username"]
        email <- map["email"]
        qb_id <- map["qb_id"]
        profile_pic <- map["profile_pic"]
        show_my_profile <- map["show_my_profile"]
        follow_status <- map["follow_status"]
    }

}
