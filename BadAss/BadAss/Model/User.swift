//
//  User.swift
//  GreenEntertainment
//
//  Created by Prateek Keshari on 04/06/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class User: Mappable {
    
    var user_id: Int?
    var socialId: String?
    var qbId: String?
    var socialIdString: String?
    var socialType: String?
    var authToken: String?
    var age: String?
    var email: String?
    var first_name: String?
    var image: String?
    var last_name: String?
    var phone_number: String?
    var phoneCode: String?
    var errorIndex = -1
    var errorMessage: String?
    var countryFlag: UIImage?
    var auth_token: String?
    var gender: String?
    var password: String?
    var newPassword: String?
    var confirm_password: String?
    var isAgreeToTnC: Bool?
    var hasEmailVerified: Bool?
    var country: Country?
    var countryId: String?
    var state: String?
    var dob: String?

    var preferredLanguages = [Language]()
    var preferredLanguagesIDs = [String]()
    
    var about: String?
    var fullname:String?
    var userName: String?
    var isNotification: Bool?
    var zip_code: String?
    var username: String?

    var language: String?
    var videoTitle: String?
    var videoCategory: String?
    var videoCategoryID: String?
    var languageID: String?

    
    var showGender: String?
    var showDob: String?
    
    var fanCount: String?
    var voteCount: Int?
    var followingCount: String?
    
    var paidPoints: String?
    var freePoints: String?
    
    var unreadNotificationCount: Int?
    
    init() {
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        user_id <- map["user_id"]
        image <- map["profile_pic"]
        phoneCode <- map["phone_code"]
        authToken <- map["access_token"]
        age <- map["age"]
        email <- map["email"]
        username <- map["username"]
        fullname <- map["name"]
        phone_number <- map["phone"]
        gender <- map["gender"]
        language <- map["language"]
        qbId <- map["qb_id"]
        
        dob <- map["dob"]
        about <- map["about_me"]
        country <- map["country"]
        showGender <- map["show_gender"]
        showDob <- map["show_dob"]
        socialId <- map["social_id"]
        preferredLanguagesIDs <- map["preferred_languages"]
        
        
        fanCount <- map["fan_count"]
        voteCount <- map["votes_count"]
        followingCount <- map["followings_count"]
        
        freePoints <- map["free_count_questions"]
        paidPoints <- map["paid_count_questions"]
        
        unreadNotificationCount <- map["unread_notification_count"]
    }
    
}


