//
//  Comment.swift
//  BadAss
//
//  Created by Prateek Keshari on 19/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment : Mappable {
    
    var comment_id : Int?
    var reply_id : Int?
    
    var comment : String?
    var created_at: Int?
    var likes_count : String?
    var replies_count : String?
    var is_liked : String?
    var creator =  Creator()
    var reply_user_data = [ReplyUserData]()
    
    init() {
    }
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        comment_id <- map["comment_id"]
        comment <- map["comment"]
        created_at <- map["created_date"]
        likes_count <- map["likes_count"]
        replies_count <- map["replies_count"]
        is_liked <- map["is_liked"]
        creator <- map["creator"]
        reply_user_data <- map["reply_user_data"]
        reply_id <- map["reply_id"]
        
    }
    
}




class ReplyUserData : Mappable {
    var user_id : Int?
    var created_date : Int?
    var name : String?
    var profile_pic : String?
    var qb_id : String?
    var chat_privacy : String?
    var replies_count : String?
    var username : String?
    var comment_id : Int?
    var comment : String?
    var is_liked : String?
    var show_profile : String?
    var likes_count : String?
    var fan_count : String?
    
    init() {
    }
    
    required init?(map: Map){
    }
    func mapping(map: Map) {
        
        user_id <- map["user_id"]
        created_date <- map["created_date"]
        name <- map["name"]
        profile_pic <- map["profile_pic"]
        qb_id <- map["qb_id"]
        chat_privacy <- map["chat_privacy"]
        replies_count <- map["replies_count"]
        username <- map["username"]
        comment_id <- map["comment_id"]
        comment <- map["comment"]
        is_liked <- map["is_liked"]
        show_profile <- map["show_profile"]
        likes_count <- map["likes_count"]
        fan_count <- map["fan_count"]
    }
    
}
