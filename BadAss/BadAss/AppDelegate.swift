//
//  AppDelegate.swift
//  BadAss
//
//  Created by Prateek Keshari on 06/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKCoreKit
import Firebase
import Firebase
import Cloudinary

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController = UINavigationController()
    var tabbar: TabbarViewController?
    var currentDialogId: String?
    
    var cloudinary: CLDCloudinary?
    
    static let shared = UIApplication.shared.delegate as! AppDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(MessageViewController.self)
        
        // Language Setting
        if UserDefaults.standard.value(forKey: CurrentLanguageKey) == nil {
            UserDefaults.languageCode = kEnglish
            UserDefaults.standard.set(kEnglish, forKey: CurrentLanguageKey)
        }
        
        let config = CLDConfiguration(cloudName: "dsinha6193", apiKey: "292889121746382")
        cloudinary = CLDCloudinary(configuration: config)
        
        
        FirebaseApp.configure()
        QBManager.shared.setupQB()
        SocialLoginHelper.shared.initialiseGoogleSignin()
        SocialLoginHelper.shared.initialiseFacebookLogin(application, didFinishLaunchingWithOptions: launchOptions)
        
        UINavigationBar.appearance().barTintColor = AppThemeColors.navBackground
        UINavigationBar.appearance().tintColor = AppThemeColors.navBackground
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.getLanguagesAndCategories()
        }
        
        loadInitialSetup()
        
        
        // Check if launched from notification
        let notificationOption = launchOptions?[.remoteNotification]
        if let notification = notificationOption as? [String: AnyObject] {
            handlePushNotification(dict: notification as [String: Any])
        }
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        QBManager.shared.loginToChat()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        QBManager.shared.disconnectChat()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        QBManager.shared.disconnectChat()
    }
    
    
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance()?.handle(url) ?? false
    }
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
    }

}

extension AppDelegate {
    
    func loadInitialSetup(){
        if let token = AuthManager.shared.loggedInUser?.authToken, token.length > 0, let vc = UIStoryboard.tabbar.get(TabbarViewController.self) {
            tabbar = vc
            navigationController = UINavigationController(rootViewController: vc)
            navigationController.navigationBar.isHidden = true
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        } else if let vc =  UIStoryboard.auth.get(LoginViewController.self) {
            navigationController = UINavigationController(rootViewController: vc)
            navigationController.navigationBar.isHidden = true
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
    }
    
}


extension AppDelegate {
    
    fileprivate func getLanguagesAndCategories() {
        
        WebServices.getLanguages { (response) in
            if let languages = response?.array {
                languagesArray = languages
            }
        }
       
        WebServices.getCategories { (response) in
            if let categories = response?.array {
                categoriesArray = categories
                DispatchQueue.main.async {
                    if let home = (APPDELEGATE.tabbar?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? HomeViewController {
                        home.refreshCategories()
                    }
                }
            }
        }
        
    }
    
    
}



