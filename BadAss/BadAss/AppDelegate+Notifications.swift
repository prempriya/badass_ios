//
//  AppDelegate+Notifications.swift
//  BadAss
//
//  Created by DAY on 13/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import Foundation
import UserNotifications
import Firebase
import FirebaseMessaging

extension AppDelegate {
    
    func registerForRemoteNotification() {

        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                Debug.log("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                Debug.log("Remote instance ID token: \(result.token)")
                UserDefaults.fcmToken = result.token
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization (
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }

}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        UserDefaults.apnsToken = token
        Messaging.messaging().apnsToken = deviceToken
        QBManager.shared.registerPushToken(token: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserDefaults.apnsToken = "No Token Fetched"
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let dataDict = notification.request.content.userInfo
        Debug.log("\(dataDict.toJsonString())")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "profile"), object: nil, userInfo: nil)
        
        if let dict = notification.request.content.userInfo as? [String: Any], let notification_type = dict["type"] as? String  {
            switch notification_type {
            case NotificationType.FOLLOW_TYPE:
                break
            case NotificationType.COMMENT_REPLY_TYPE, NotificationType.COMMENT_TYPE:
                NotificationCenter.default.post(name: NSNotification.Name.videoComment, object: nil)
                break
            case NotificationType.VIDEO_REPLY_TYPE :
                break
            case NotificationType.CHAT_REQUEST :
                NotificationCenter.default.post(name: NSNotification.Name.chatRequest, object: nil)
                break
            case NotificationType.CHAT_MESSAGE :
                NotificationCenter.default.post(name: NSNotification.Name.chatMessage, object: nil)
//                if let id = dict[kId] as? String, id == self.currentDialogId {
////                    completionHandler([])
//                    return
//                }
                break
            default:
                break
            }
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let dataDict = response.notification.request.content.userInfo as! Dictionary<String, Any>
        Debug.log("\(dataDict.toJsonString())")
         if let dict = response.notification.request.content.userInfo as? [String: Any] {
             print(dict.debugDescription)
             handlePushNotification(dict: dict)
         }
        completionHandler()
    }
    
    func handlePushNotification(dict: [String: Any]) {
        if let notification_type = dict["type"] as? String  {
            switch notification_type {
            case NotificationType.FOLLOW_TYPE:
                if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
                    vc.userId = dict[kFollowerId] as? String ?? ""
                    AppDelegate.shared.navigationController.pushViewController(vc, animated: true)
                }
                break
            case NotificationType.COMMENT_REPLY_TYPE, NotificationType.COMMENT_TYPE:
                if let vc = UIStoryboard.main.get(CommentViewController.self) {
                    vc.videoId = Int(dict[kId] as? String ?? "")
                    vc.isFromNotification = true
                    AppDelegate.shared.navigationController.pushViewController(vc, animated: true)
                }
                break
            case NotificationType.VIDEO_REPLY_TYPE :
                if let vc = UIStoryboard.main.get(PostViewController.self) {
                    vc.videoId = Int(dict[kId] as? String ?? "") ?? 0
                    vc.isFromNotification = true
                    AppDelegate.shared.navigationController.pushViewController(vc, animated: true)
                }
                break
            case NotificationType.VIDEO_UPLOAD :
                if let vc = UIStoryboard.main.get(PostViewController.self) {
                    vc.videoId = Int(dict[kId] as? String ?? "") ?? 0
                    vc.isFromNotification = true
                    AppDelegate.shared.navigationController.pushViewController(vc, animated: true)
                }
                break
            case NotificationType.CHAT_REQUEST :
                if QBManager.shared.isConnected() {
                    AppDelegate.shared.navigationController.popToRootViewController(animated: true)
                    AppDelegate.shared.tabbar?.selectedIndex = 3
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        AppDelegate.shared.navigationController.popToRootViewController(animated: true)
                        AppDelegate.shared.tabbar?.selectedIndex = 3
                    }
                }
                break
            case NotificationType.CHAT_MESSAGE :
                if let dialog_id = dict[kId] as? String {
                    if QBManager.shared.isConnected() {
                        ProgressHud.showActivityLoader()
                        QBManager.shared.loadDialog(withID: dialog_id) { (dialog) in
                            ProgressHud.hideActivityLoader()
                            if let vc = UIStoryboard.main.get(MessageViewController.self), let dialog = dialog {
                                vc.chatDialog = dialog
                                AppDelegate.shared.navigationController.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        ProgressHud.showActivityLoader()
                        QBManager.shared.loadDialog(withID: dialog_id) { (dialog) in
                            ProgressHud.hideActivityLoader()
                            if let vc = UIStoryboard.main.get(MessageViewController.self), let dialog = dialog {
                                vc.chatDialog = dialog
                                AppDelegate.shared.navigationController.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
                break
            case NotificationType.ADMIN_NOTIFICATION:
                    AppDelegate.shared.navigationController.popToRootViewController(animated: true)
                    AppDelegate.shared.tabbar?.selectedIndex = 1
                break
            default:
                break
            }
        }
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        Debug.log("Firebase registration token: \(fcmToken)")
        UserDefaults.fcmToken = fcmToken
    }
}



