//
//  Webservice+EndPoints.swift
//  GreenEntertainment
//
//  Created by MacMini-iOS on 18/07/19.
//  Copyright © 2019 Quytech. All rights reserved.
//



extension WebServices {
    
    enum EndPoint : String {
        case registration = "registration"
        case login = "login"
        case sendOTP = "otp"
        case verifyOTP = "verify-otp"
        case resendOtp = "resend_otp"
        case languages = "languages"
        case categories = "categories"
        case countries = "countries"
        case uploadImage = "upload-image"
        case forget_password = "forget-password"
        case reset_password = "reset-password"
        case change_password = "change-password"
        
        case updateUserSetting = "update-user-setting"
        case getUserSetting = "get-user-setting"
        case getProfile = "profile"
        case getOtherProfile = "other-user-profile"
        case reply_video = "reply-video"
        case comment = "comment"
        case comment_like = "comment-like"
        case comment_list = "comment-list"
        case comment_reply = "comment-reply"
        case recently_viewed_videos = "recently-viewed-videos"
        case my_videos = "my-videos"
        case get_reply_videos = "get-reply-videos"
        case my_reply_videos = "my-reply-videos"

        
        case updateProfile = "update-profile"
        case userSuggestions = "user-suggestions"
        case follow = "action-following"
        case followings = "followings"
        case fans = "fans"
        case blocked_user = "blocked-users"
        case block_unblock = "block-unblock"
        case edit_user_profile = "edit_user_profile"
        case term_condition = "term"
        case about_us = "about"
        case logout = "logout"
        case get_videos = "get-videos"
        case fetch_content = "fetch_content"
        case update_existing_email = "update_existing_email"
        case view_user_profile = "other_user_profile"
        case get_followers_followings = "get_followers_followings"
        case my_rewards = "my_rewards"
        case claim_reward = "claim_reward"
        case notification_setting = "notification/settings"
        case update_notification_setting = "update/notification/settings"
        case contact_us = "contact_us"
        case users = "list_users_except_me"
        case send_invite = "send_invite"
        case follow_user = "follow_unfollow"
        case app_gallery = "app_gallery"
        case get_challenge_list = "challenges"
        case donate_money_to_user = "donate_money_to_user"
        case send_message = "send_message"
        case view_users_list = "view_users_list"
        case delete_chat_messages = "delete_chat_messages"
        case view_message = "view_message"
        case share_video = "share_video"
        case increase_view_count_video = "video-viewed"
        case find_friends = "find_user"
        case save_video_file = "add-video"
        case challenge_name_list = "challenge_name_list"
        case post_video_challenge = "post_video_challenge"
        case participate_challenge = "participate_challenge"
        case filter_transaction = "filter_transaction"
        case save_video_to_gallery = "post_video_to_gallery"
        case notifications = "notifications"
        case vote_video = "vote-unvote"
        case delete_notification = "delete-notification"
        case video_detail = "get-video-detail"
        case search = "search"


        case trending_general_following = "trending_general_following"
        case get_questions_count = "get-count-questions"
        case get_qb_id_using_email = "get-qb-id"
        case update_questions_count = "update-count-questions"
        
        var path : String {
            let url = Environment.baseURL()
            return url + self.rawValue
        }
    }

}

enum Response {
    case success(Any?)
    case failure(String?)
}

internal struct APIConstants {
    
    static let code = "code"
    static let success = "status"
    static let message = "message"
    
}

enum Validate : String {
    case none
    case success = "200"
    case failure = "400"
    case invalidAccessToken = "240"
    case adminBlocked = "402"
    
    static func link(code: String) -> Validate {
        switch code {
        case "200", "409","402", "201", "202", "203","0":
            return .success
        default:
            return .failure
        }
    }
}

