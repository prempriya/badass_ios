//
//  Webservices.swift
//  FutureNow
//
//  Created by MacMini-iOS on 18/07/19.
//  Copyright © 2019 Quytech. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import ObjectMapper

enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}

// MARK:
// --- User Management APIs --- //
extension WebServices {
    
    static func registration( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?) {
        AppNetworking.POST(endPoint: EndPoint.registration.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    
    
    static func login( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?) {
        AppNetworking.POST(endPoint: EndPoint.login.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }

    static func sendOTP( params: [String: Any], successCompletion: ((ResponseData<Initial>?)-> Void)?){
        AppNetworking.POST(endPoint: EndPoint.sendOTP.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    static func verifyOTP( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
        AppNetworking.POST(endPoint: EndPoint.verifyOTP.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    

    static func forget_password( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
        AppNetworking.POST(endPoint: EndPoint.forget_password.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    static func reset_password( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
        AppNetworking.POST(endPoint: EndPoint.reset_password.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    
    static func change_password( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
        AppNetworking.POST(endPoint: EndPoint.change_password.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    static func getLanguages( successCompletion: ((ResponseData<Language>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.languages.path, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Language>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    static func getCategories( successCompletion: ((ResponseData<Category>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.categories.path, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Category>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    
    static func getCountries( successCompletion: ((ResponseData<Country>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.countries.path, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Country>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    static func uploadImage( params: [String: Any], files: [AttachmentInfo], successCompletion: ((ResponseData<Initial>?)-> Void)? ) {
        AppNetworking.UPLOAD(endPoint: EndPoint.uploadImage.path, parameters: params, headers: [:], files: files, loader: true, completion: {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        })
    }
    
    
    static func getUserSettings(successCompletion: ((ResponseData<UserSetting>?)-> Void)?) {
        
        let userId = AuthManager.shared.loggedInUser?.user_id ?? 0
        
        AppNetworking.GET(endPoint: EndPoint.getUserSetting.path, parameters: [kUserId: userId], loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<UserSetting>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    
    static func getMyProfile(successCompletion: ((ResponseData<User>?)-> Void)?) {
        
        let userId = AuthManager.shared.loggedInUser?.user_id ?? 0
        
        AppNetworking.GET(endPoint: EndPoint.getProfile.path, parameters: [kUserId: userId], loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    
    static func getOtherUserProfile(otherUserId: String, successCompletion: ((ResponseData<OtherUser>?)-> Void)?) {
        
        var params = [String: Any]()
        params[kOtherUserId] = otherUserId
        params[kMyUserId] = AuthManager.shared.loggedInUser?.user_id
        
        AppNetworking.GET(endPoint: EndPoint.getOtherProfile.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<OtherUser>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    static func updateUserSettings(params: [String: Any], loader: Bool = true, successCompletion: ((ResponseData<UserSetting>?)-> Void)?) {
        AppNetworking.POST(endPoint: EndPoint.updateUserSetting.path, parameters: params, loader: loader) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<UserSetting>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    
    
    
    
    
//
//   static func reset_password( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.reset_password.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//
//                break
//            }
//        }
//    }
//    static func change_password( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//           AppNetworking.POST(endPoint: EndPoint.change_password.path, parameters: params, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   print(value ?? "")
//                   let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//
//                   AlertController.alert(title: "", message:  message ?? "")
//
//
//                   break
//               }
//           }
//       }
//
//
//
//    static func resendOTP(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.resendOtp.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
//
//
    static func updateProfile( params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)? ) {
        AppNetworking.POST(endPoint: EndPoint.updateProfile.path, parameters: params, headers: [:], loader: true) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    
    static func getUserSuggestions(params: [String: Any], successCompletion: ((ResponseData<SuggestedUser>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.userSuggestions.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<SuggestedUser>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    
    static func getFollowingList(params: [String: Any], successCompletion: ((ResponseData<FollowedUser>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.followings.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<FollowedUser>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    
    static func getFansList(params: [String: Any], successCompletion: ((ResponseData<FollowedUser>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.fans.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<FollowedUser>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    
    static func followUser(params: [String: Any], successCompletion: ((ResponseData<Initial>?)-> Void)?) {
        AppNetworking.POST(endPoint: EndPoint.follow.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    static func blockUnblock(params: [String: Any], successCompletion: ((ResponseData<Initial>?)-> Void)?) {
         AppNetworking.POST(endPoint: EndPoint.block_unblock.path, parameters: params, loader: false) {  (response) in
             switch response {
             case .success(let value):
                 let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                 successCompletion?(response)
                 break
             case .failure(let message):
                 AlertController.alert(title: "", message: message ?? "")
                 successCompletion?(nil)
                 break
             }
         }
     }
    
    
    
    static func getBlockList(params: [String: Any], successCompletion: ((ResponseData<FollowedUser>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.blocked_user.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<FollowedUser>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
//
//    static func getStaticContent(params: [String: Any], successCompletion: ((ResponseData<StaticModel>?)-> Void)?){
//           AppNetworking.POST(endPoint: EndPoint.fetch_content.path, parameters: params, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   print(value ?? "")
//                   let response = Mapper<ResponseData<StaticModel>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//
//                   AlertController.alert(title: "", message:  message ?? "")
//
//                   break
//               }
//           }
//       }
//
//    static func updateExistingEmail(params: [String: Any], successCompletion: ((ResponseData<Initial>?)-> Void)?){
//              AppNetworking.POST(endPoint: EndPoint.update_existing_email.path, parameters: params, loader: true) {  (response) in
//                  switch response {
//                  case .success(let value):
//                      print(value ?? "")
//                      let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
//                      successCompletion?(response)
//                      break
//                  case .failure(let message):
//
//                      AlertController.alert(title: "", message:  message ?? "")
//
//                      break
//                  }
//              }
//          }
//
//
//
//    static func logout(successCompletion: ((ResponseData<Initial>?)-> Void)?){
//         AppNetworking.GET(endPoint: EndPoint.logout.path, loader: true) {  (response) in
//             switch response {
//             case .success(let value):
//                 print(value ?? "")
//                 let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
//                 successCompletion?(response)
//                 break
//             case .failure(let message):
//                 AlertController.alert(title: "", message:  message ?? "")
//                 break
//             }
//         }
//     }
//
//
//
//    static func getUserProfile(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//          AppNetworking.POST(endPoint: EndPoint.view_user_profile.path, parameters: params, loader: true) {  (response) in
//              switch response {
//              case .success(let value):
//                  print(value ?? "")
//                  let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                  successCompletion?(response)
//                  break
//              case .failure(let message):
//
//                  AlertController.alert(title: "", message:  message ?? "")
//
//                  break
//              }
//          }
//      }
//
//    static func getFollowerFollowing(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.get_followers_followings.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
//
//    static func getMyRewards(params: [String: Any], successCompletion: ((ResponseData<RewardsModel>?)-> Void)?){
//           AppNetworking.POST(endPoint: EndPoint.my_rewards.path, parameters: params, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   print(value ?? "")
//                   let response = Mapper<ResponseData<RewardsModel>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//
//                   AlertController.alert(title: "", message:  message ?? "")
//
//                   break
//               }
//           }
//       }
//
//    static func claimRewards(params: [String: Any], successCompletion: ((ResponseData<RewardsModel>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.claim_reward.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<RewardsModel>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//                           }
//                       }
//        }
//
//    static func getChallengesList( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
//        AppNetworking.POST(endPoint: EndPoint.get_challenge_list.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//                AlertController.alert(title: "", message:  message ?? "")
//                break
//            }
//        }
//    }
//
//    static func getNotificationSetting(successCompletion: ((ResponseData<NotificationSetting>?)-> Void)?){
//        AppNetworking.GET(endPoint: EndPoint.notification_setting.path, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<NotificationSetting>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//                AlertController.alert(title: "", message:  message ?? "")
//                break
//            }
//        }
//    }
//
//    static func updateNotificationSetting(params: [String: Any], successCompletion: ((ResponseData<NotificationSetting>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.update_notification_setting.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<NotificationSetting>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
//
//    static func submitFeedback(params: [String: Any], successCompletion: ((ResponseData<NotificationSetting>?)-> Void)?){
//           AppNetworking.POST(endPoint: EndPoint.contact_us.path, parameters: params, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   print(value ?? "")
//                   let response = Mapper<ResponseData<NotificationSetting>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//
//                   AlertController.alert(title: "", message:  message ?? "")
//
//                   break
//               }
//           }
//       }
//
//
//    static func getUsersList(successCompletion: ((ResponseData<User>?)-> Void)?){
//           AppNetworking.GET(endPoint: EndPoint.users.path, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   print(value ?? "")
//                   let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//                   AlertController.alert(title: "", message:  message ?? "")
//                   break
//               }
//           }
//       }
//
//    static func sendInvite(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.send_invite.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
//
//    static func shareVideo(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.share_video.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
    static func increaseViewCount(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
           AppNetworking.POST(endPoint: EndPoint.increase_view_count_video.path, parameters: params, loader: false) {  (response) in
               switch response {
               case .success(let value):
                   print(value ?? "")
                   let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                   successCompletion?(response)
                   break
               case .failure(let message):

                   AlertController.alert(title: "", message:  message ?? "")

                   break
               }
           }
       }

    static func vote(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
              AppNetworking.POST(endPoint: EndPoint.vote_video.path, parameters: params, loader: false) {  (response) in
                  switch response {
                  case .success(let value):
                      print(value ?? "")
                      let response = Mapper<ResponseData<User>>().map(JSONObject:value)
                      successCompletion?(response)
                      break
                  case .failure(let message):

                      AlertController.alert(title: "", message:  message ?? "")

                      break
                  }
              }
          }
//
//
//    static func getChallengeVideoList(params: [String: Any], loadMore: Bool = true, successCompletion: ((ResponseData<RewardsModel>?)-> Void)?){
//              AppNetworking.POST(endPoint: EndPoint.trending_general_following.path, parameters: params, loader: loadMore) {  (response) in
//                  switch response {
//                  case .success(let value):
//                      print(value ?? "")
//                      let response = Mapper<ResponseData<RewardsModel>>().map(JSONObject:value)
//                      successCompletion?(response)
//                      break
//                  case .failure(let message):
//
//                      AlertController.alert(title: "", message:  message ?? "")
//
//                      break
//                  }
//              }
//          }
//
//
//    static func searchUser(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.find_friends.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
//
//
//
//    static func followUnfollowUser(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//           AppNetworking.POST(endPoint: EndPoint.follow_user.path, parameters: params, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   print(value ?? "")
//                   let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//
//                   AlertController.alert(title: "", message:  message ?? "")
//
//                   break
//               }
//           }
//       }
//
//    static func getGalleryList(successCompletion: ((ResponseData<RewardsModel>?)-> Void)?){
//             AppNetworking.GET(endPoint: EndPoint.app_gallery.path, loader: true) {  (response) in
//                 switch response {
//                 case .success(let value):
//                     print(value ?? "")
//                     let response = Mapper<ResponseData<RewardsModel>>().map(JSONObject:value)
//                     successCompletion?(response)
//                     break
//                 case .failure(let message):
//                     AlertController.alert(title: "", message:  message ?? "")
//                     break
//                 }
//             }
//         }
//
//    static func donateMoneyToUser(params: [String: Any], successCompletion: ((ResponseData<User>?)-> Void)?){
//        AppNetworking.POST(endPoint: EndPoint.donate_money_to_user.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<User>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//
//                AlertController.alert(title: "", message:  message ?? "")
//
//                break
//            }
//        }
//    }
//
//    static func sendmessage( params: [String: Any], files: [AttachmentInfo], successCompletion: ((ResponseData<Chat>?)-> Void)? ) {
//           AppNetworking.UPLOAD(endPoint: EndPoint.send_message.path, parameters: params, headers: [:], files: files, loader: true) {  (response) in
//               switch response {
//               case .success(let value):
//                   let response = Mapper<ResponseData<Chat>>().map(JSONObject:value)
//                   successCompletion?(response)
//                   break
//               case .failure(let message):
//                   AlertController.alert(title: "", message:  message ?? "")
//                   break
//               }
//           }
//       }
//
    static func deleteNotification(params: [String: Any], successCompletion: ((ResponseData<Initial>?)-> Void)?){
           AppNetworking.POST(endPoint: EndPoint.delete_notification.path, parameters: params, loader: true) {  (response) in
               switch response {
               case .success(let value):
                   print(value ?? "")
                   let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                   successCompletion?(response)
                   break
               case .failure(let message):

                   AlertController.alert(title: "", message:  message ?? "")

                   break
               }
           }
       }
    static func getNotification(params: [String: Any] ,successCompletion: ((ResponseData<NotificationModel>?)-> Void)?){
        AppNetworking.GET(endPoint: EndPoint.notifications.path, parameters: params, loader: true) {  (response) in
                switch response {
                case .success(let value):
                    print(value ?? "")
                    let response = Mapper<ResponseData<NotificationModel>>().map(JSONObject:value)
                    successCompletion?(response)
                    break
                case .failure(let message):
                    AlertController.alert(title: "", message:  message ?? "")
                    break
                }
            }
        }
    
    static func searchData(params: [String: Any] ,successCompletion: ((ResponseData<SearchModel>?)-> Void)?){
    AppNetworking.GET(endPoint: EndPoint.search.path, parameters: params, loader: true) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<SearchModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
    
    static func getVideoDetail(params: [String: Any], loader: Bool = true ,successCompletion: ((ResponseData<HomeModel>?)-> Void)?){
    AppNetworking.GET(endPoint: EndPoint.video_detail.path, parameters: params, loader: loader) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                break
            }
        }
    }
//
//    //get chat list
//
//    static func getChatList( successCompletion: ((ResponseData<Chat>?)-> Void)?) {
//        AppNetworking.GET(endPoint: EndPoint.view_users_list.path) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<Chat>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//                AlertController.alert(title: "", message: message ?? "")
//                break
//            }
//        }
//    }
//
//
    static func getVideos( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.get_videos.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
////
    //save video
    static func saveVideo( params: [String: Any], files: [AttachmentInfo], successCompletion: ((ResponseData<Initial>?)-> Void)?, progress: ((Double?) -> Void)? = nil) {
        
        AppNetworking.UPLOAD(endPoint: EndPoint.save_video_file.path, parameters: params, headers: [:], files: files, loader: false, completion: { (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                IHProgressHUD.dismiss()
                break
            }
        }) { (progressValue) in
            progress?(progressValue)
        }
    }
    
    static func replyVideo( params: [String: Any], files: [AttachmentInfo], successCompletion: ((ResponseData<Initial>?)-> Void)?, progress: ((Double?) -> Void)? = nil) {
        AppNetworking.UPLOAD(endPoint: EndPoint.reply_video.path, parameters: params, headers: [:], files: files, loader: false, completion : {  (response) in
               switch response {
               case .success(let value):
                   let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                   successCompletion?(response)
                   break
               case .failure(let message):
                   AlertController.alert(title: "", message:  message ?? "")
                   IHProgressHUD.dismiss()
                   break
               }
           }) { (progressValue) in
               progress?(progressValue)
           }
       }
    
        static func comment( params: [String: Any], successCompletion: ((ResponseData<Comment>?)-> Void)?) {
            AppNetworking.GET(endPoint: EndPoint.comment_list.path, parameters: params, loader: true) {  (response) in
                switch response {
                case .success(let value):
                    print(value ?? "")
                    let response = Mapper<ResponseData<Comment>>().map(JSONObject:value)
                    successCompletion?(response)
                    break
                case .failure(let message):
                    AlertController.alert(title: "", message:  message ?? "")
                    break
                }
            }
        }
    static func likeComment( params: [String: Any], successCompletion: ((ResponseData<Comment>?)-> Void)?) {
               AppNetworking.POST(endPoint: EndPoint.comment_like.path, parameters: params, loader: false) {  (response) in
                   switch response {
                   case .success(let value):
                       print(value ?? "")
                       let response = Mapper<ResponseData<Comment>>().map(JSONObject:value)
                       successCompletion?(response)
                       break
                   case .failure(let message):
                       AlertController.alert(title: "", message:  message ?? "")
                       break
                   }
               }
           }
    static func postComment( params: [String: Any], successCompletion: ((ResponseData<Initial>?)-> Void)?) {
               AppNetworking.POST(endPoint: EndPoint.comment.path, parameters: params, loader: true) {  (response) in
                   switch response {
                   case .success(let value):
                       print(value ?? "")
                       let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                       successCompletion?(response)
                       break
                   case .failure(let message):
                       AlertController.alert(title: "", message:  message ?? "")
                       break
                   }
               }
           }
    
    static func postReply( params: [String: Any], successCompletion: ((ResponseData<Comment>?)-> Void)?) {
                  AppNetworking.POST(endPoint: EndPoint.comment_reply.path, parameters: params, loader: true) {  (response) in
                      switch response {
                      case .success(let value):
                          print(value ?? "")
                          let response = Mapper<ResponseData<Comment>>().map(JSONObject:value)
                          successCompletion?(response)
                          break
                      case .failure(let message):
                          AlertController.alert(title: "", message:  message ?? "")
                          break
                      }
                  }
              }
    
    static func getRecentlyViewdVideos( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.recently_viewed_videos.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    static func getReplyVideos( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.get_reply_videos.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    static func getMyVideos( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.my_videos.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    static func getMyReplyVideos( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.my_reply_videos.path, parameters: params, loader: false) {  (response) in
            switch response {
            case .success(let value):
                print(value ?? "")
                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message:  message ?? "")
                successCompletion?(nil)
                break
            }
        }
    }
    
    
    static func getUsersQuestionsCount(userId: String, successCompletion: ((ResponseData<QuestionCount>?)-> Void)?) {
        AppNetworking.GET(endPoint: EndPoint.get_questions_count.path, parameters: [kUserId: userId], loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<QuestionCount>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                ProgressHud.hideActivityLoader()
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    
    static func getQbIdByEmailAddress(email: String, successCompletion: ((ResponseData<Initial>?)-> Void)?) {
        
        AppNetworking.GET(endPoint: EndPoint.get_qb_id_using_email.path, parameters: [kEmail: email], loader: false) {  (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<Initial>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    
    
    static func updateQuestionsCount(count: String, userId: String, successCompletion: ((ResponseData<QuestionCount>?)-> Void)?) {
        AppNetworking.POST(endPoint: EndPoint.update_questions_count.path, parameters: [kUserId: userId, kCounts: count], loader: false) { (response) in
            switch response {
            case .success(let value):
                let response = Mapper<ResponseData<QuestionCount>>().map(JSONObject:value)
                successCompletion?(response)
                break
            case .failure(let message):
                AlertController.alert(title: "", message: message ?? "")
                break
            }
        }
    }
    
    
//
//
//    //challenge list
//    static func getChallengesName(successCompletion: ((ResponseData<HomeModel>?)-> Void)?){
//        AppNetworking.GET(endPoint: EndPoint.challenge_name_list.path, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//                AlertController.alert(title: "", message:  message ?? "")
//                break
//            }
//        }
//    }
//
//    static func postVideoToChallenge( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
//        AppNetworking.POST(endPoint: EndPoint.post_video_challenge.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//                AlertController.alert(title: "", message:  message ?? "")
//                break
//            }
//        }
//    }
//

//
//    static func saveVideoToGallery( params: [String: Any], successCompletion: ((ResponseData<HomeModel>?)-> Void)?) {
//        AppNetworking.POST(endPoint: EndPoint.save_video_to_gallery.path, parameters: params, loader: true) {  (response) in
//            switch response {
//            case .success(let value):
//                print(value ?? "")
//                let response = Mapper<ResponseData<HomeModel>>().map(JSONObject:value)
//                successCompletion?(response)
//                break
//            case .failure(let message):
//                AlertController.alert(title: "", message:  message ?? "")
//                break
//            }
//        }
//    }
}



