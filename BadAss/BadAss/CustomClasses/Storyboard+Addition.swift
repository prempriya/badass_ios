//
//  Storyboard+Addition.swift
//  GreenEntertainment
//
//  Created by DAY on 08/06/20.
//  Copyright © 2020 DAY. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static var auth: UIStoryboard {
        return UIStoryboard(name: "Auth", bundle: nil)
    }
    
    static var home: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    static var chat: UIStoryboard {
        return UIStoryboard(name: "Chat", bundle: nil)
    }
        
    static var profile: UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: nil)
    }
    static var tabbar: UIStoryboard {
        return UIStoryboard(name: "Tabbar", bundle: nil)
    }
    
    static var settings: UIStoryboard {
        return UIStoryboard(name: "Settings", bundle: nil)
    }
    
    static var message: UIStoryboard {
        return UIStoryboard(name: "Message", bundle: nil)
    }
    
    public func get<T:UIViewController>(_ identifier: T.Type) -> T? {
        let storyboardID = String(describing: identifier)
        guard let viewController = instantiateViewController(withIdentifier: storyboardID) as? T else {
            return nil
        }
        return viewController
    }
    
}






