//
//  SocialLoginHelper.swift
//  BadAss
//
//  Created by DAY on 26/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import AuthenticationServices

protocol SocialLoginDelegate {
    func googleSignInFinished(with user: SocialUser?, error: Error?)
    func facebookSignInFinished(with user: SocialUser?, error: String?)
    func appleSignInFinished(with user: SocialUser?, error: String?)
}

extension SocialLoginDelegate {
    func googleSignInFinished(with user: SocialUser?, error: Error?) {}
    func facebookSignInFinished(with user: SocialUser?, error: String?) {}
    func appleSignInFinished(with user: SocialUser?, error: String?) {}
}

enum SocialType: String {
    case Google = "google"
    case Facebook = "facebook"
    case Apple = "apple"
}

class SocialLoginHelper: NSObject {
    static let shared: SocialLoginHelper = SocialLoginHelper()
    var delegate: SocialLoginDelegate?
    var controller: UIViewController?
}

// Apple Sign In
extension SocialLoginHelper: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    @available(iOS 13.0, *)
    func getAppleSignInRoundedButton() -> ASAuthorizationAppleIDButton {
        let appleButton = ASAuthorizationAppleIDButton(authorizationButtonType: .signIn, authorizationButtonStyle: .white)
        appleButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        appleButton.layer.cornerRadius = 22.5
        appleButton.maskToBounds = true
        appleButton.addTarget(self, action: #selector(appleSignInButtonTapped), for: .touchUpInside)
        return appleButton
    }
    
    @objc func appleSignInButtonTapped() {
        print("Apple Sign In Tapped")
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            if let email = email {
                
                let email = email
                let firstName = fullName?.givenName ?? ""
                let lastName = fullName?.familyName ?? ""
                
                let stringToSave = "\(email)|\(fullName?.givenName ?? "")|\(fullName?.familyName ?? "")"
                KeychainWrapper.standard.set(stringToSave, forKey: userIdentifier)
                let name = "\(firstName) \(lastName)"
                
                let user  = SocialUser(id: userIdentifier, name: name, givenName: firstName, familyName: lastName, email: email, hasImage: false, imageUrl: "", socialType: .Apple)
                self.delegate?.appleSignInFinished(with: user, error: nil)
                
            } else {
                if let savedString = KeychainWrapper.standard.string(forKey: userIdentifier) {
                    let parts = savedString.components(separatedBy: "|")
                    let email = parts.first ?? ""
                    let firstName = (parts.count > 0) ? parts[1] : ""
                    let lastName = parts.last ?? ""
                    let name = "\(firstName) \(lastName)"

                    let user  = SocialUser(id: userIdentifier, name: name, givenName: firstName, familyName: lastName, email: email, hasImage: false, imageUrl: "", socialType: .Apple)
                    self.delegate?.appleSignInFinished(with: user, error: nil)
                    
                } else {
                    AlertController.alert(title: "Alert!", message: "Not able to get your Email and Name.\n\n Go to setting > Account > Password and Security > Apple ID Logins and delete your app and try again.")
                }
            }
            print("***************************************\n\nIdentifier -->> \(userIdentifier)\nFull Name -->> \(String(describing: fullName))\nEmail -->> \(email)")
        default:
            self.delegate?.appleSignInFinished(with: nil, error: "Something went wrong!")
            break
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.controller?.view.window! ?? kAppDelegate.window ?? UIWindow()
    }
    
}




// Facebook Login
extension SocialLoginHelper {
    
    func initialiseFacebookLogin(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
    }
    
    func facebookSignIn(controller: UIViewController) {
        let fbLoginManager: LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: controller){ (result, error) in
            if (error == nil){
                let fbLoginresult: LoginManagerLoginResult = result!
                if  fbLoginresult.grantedPermissions.count > 0 {
                    if(fbLoginresult.grantedPermissions.contains("email")){
                        self.getFBData { [weak self](result, errorMessage) in
                            if let message = errorMessage {
                                self?.delegate?.facebookSignInFinished(with: nil, error: message)
                                fbLoginManager.logOut()
                            } else if let dict = result {
                                let fName = dict["first_name"] as! String
                                let lName = dict["last_name"] as! String
                                let name = dict["name"] as! String
                                let email = dict["email"] as! String
                                let id = dict["id"] as! String
                                
                                var imageUrl = ""
                                var hasImage = false
                                if let picture = dict["picture"] as? [String: Any], let imageData = picture["data"] as? [String: Any], let imgURL = imageData["url"] as? String {
                                    hasImage = true
                                    imageUrl = imgURL
                                }
                                let user = SocialUser(id: id, name: name, givenName: fName, familyName: lName, email: email, hasImage: hasImage, imageUrl: imageUrl, socialType: .Facebook)
                                
                                self?.delegate?.facebookSignInFinished(with: user, error: nil)
                                fbLoginManager.logOut()
                            } else {
                                self?.delegate?.facebookSignInFinished(with: nil, error: "Something went wrong!")
                                fbLoginManager.logOut()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getFBData(onCompletion: @escaping (_ result: [String: Any]?, _ errorMessage: String?) -> Void) {
        if (AccessToken.current) != nil {
            GraphRequest (graphPath: "me", parameters: ["fields":" id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: {(connection, result, error) -> Void in
                if (error == nil){
                    let faceDic = result as! [String: Any]
                    onCompletion(faceDic, nil)
                } else {
                    onCompletion(nil, error?.localizedDescription)
                }
            })
        } else {
            onCompletion(nil, "Facebook token not found.")
        }
    }
}



// Google Sign In
extension SocialLoginHelper: GIDSignInDelegate {

    func initialiseGoogleSignin() {
        GIDSignIn.sharedInstance().clientID = "168732895336-spk080ksdvbl1knfq27rd9s08kgmd0ok.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func googleUpdatePresentingViewController(controller: UIViewController) {
        GIDSignIn.sharedInstance()?.presentingViewController = controller
    }
    
    
    func googleSignIn() {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func googleSignOut() {
        GIDSignIn.sharedInstance()?.signOut()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            delegate?.googleSignInFinished(with: nil, error: error)
            return
        }
        let googleUser = SocialUser(id: user.userID , name: user.profile.name, givenName: user.profile.givenName, familyName: user.profile.familyName, email: user.profile.email, hasImage: user.profile.hasImage, imageUrl: user.profile.imageURL(withDimension: 400)?.absoluteString ?? "", socialType: .Google)
        delegate?.googleSignInFinished(with: googleUser, error: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        delegate?.googleSignInFinished(with: nil, error: error)
    }
    
}


struct SocialUser {
    
    var socialId: String
    var fullName: String
    var givenName: String
    var familyName: String
    var email: String
    var hasImage: Bool
    var imageUrl: String
    var socialType: SocialType
    
    init(id: String, name: String, givenName: String, familyName: String, email: String, hasImage: Bool, imageUrl: String, socialType: SocialType) {
        self.socialId = id
        self.fullName = name
        self.givenName = givenName
        self.familyName = familyName
        self.email = email
        self.hasImage = hasImage
        self.imageUrl = imageUrl
        self.socialType = socialType
    }
}
