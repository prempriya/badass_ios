//
//  HomeVideoViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 08/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//


enum FilterType {
    case trending
    case following
    case general
    case none
}

let valueAdedInIndexNumber = 1000000


import UIKit
import AVFoundation
import AVKit

class HomeVideoViewController: UIViewController {

   
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var overlayView: UIView!
   
    var aVAudioPlayer: AVAudioPlayer?

    var object : ChallengeModel?
    var isPlaying = true
    
    var completion: (() -> Void)?  = nil
 
    var filterType : FilterType = .general
    var player = AVPlayer()
    var videoFinshed = false
    var backButtonTapped = false
    var currentPage = 0
    var currentSec = 0
    var timer = Timer()
    var isList = false
    
    //for video model
    
//    var challenges = [ChallengeModel(challengeName: "Challenge 1", url: "https://quytech.net/green-entertainment/assets/uploads/videos/479cf38e6a03a46d73f6de9d3174e098.mp4"),
//                      ChallengeModel(challengeName: "Challenge 2", url: "https://quytech.net/green-entertainment/assets/uploads/videos/479cf38e6a03a46d73f6de9d3174e098.mp4"),
//                      ChallengeModel(challengeName: "Challenge 3", url: "https://quytech.net/green-entertainment/assets/uploads/videos/58173ea181d7178ef61518df85fb8c57.mp4"),
//                      ChallengeModel(challengeName: "Challenge 4", url: "https://quytech.net/green-entertainment/assets/uploads/videos/479cf38e6a03a46d73f6de9d3174e098.mp4"),
//                      ChallengeModel(challengeName: "Challenge 5", url: "https://quytech.net/green-entertainment/assets/uploads/videos/58173ea181d7178ef61518df85fb8c57.mp4"),
//                    ]
    
    
    var challenges = [
        ChallengeModel(challengeName: "Challenge 1", url: "https://appdeeplink007.000webhostapp.com/IMG_0615.mp4"),
        ChallengeModel(challengeName: "Challenge 2", url: "https://appdeeplink007.000webhostapp.com/IMG_0616.mp4"),
        ChallengeModel(challengeName: "Challenge 3", url: "https://appdeeplink007.000webhostapp.com/IMG_0617.mp4"),
        ChallengeModel(challengeName: "Challenge 4", url: "https://appdeeplink007.000webhostapp.com/IMG_0618.mp4"),
        ChallengeModel(challengeName: "Challenge 5", url: "https://appdeeplink007.000webhostapp.com/IMG_0620.mp4"),
        ChallengeModel(challengeName: "Challenge 1", url: "https://appdeeplink007.000webhostapp.com/IMG_0615.mp4"),
        ChallengeModel(challengeName: "Challenge 2", url: "https://appdeeplink007.000webhostapp.com/IMG_0616.mp4"),
        ChallengeModel(challengeName: "Challenge 3", url: "https://appdeeplink007.000webhostapp.com/IMG_0617.mp4"),
        ChallengeModel(challengeName: "Challenge 4", url: "https://appdeeplink007.000webhostapp.com/IMG_0618.mp4"),
        ChallengeModel(challengeName: "Challenge 5", url: "https://appdeeplink007.000webhostapp.com/IMG_0620.mp4"),
        ChallengeModel(challengeName: "Challenge 1", url: "https://appdeeplink007.000webhostapp.com/IMG_0615.mp4"),
        ChallengeModel(challengeName: "Challenge 2", url: "https://appdeeplink007.000webhostapp.com/IMG_0616.mp4"),
        ChallengeModel(challengeName: "Challenge 3", url: "https://appdeeplink007.000webhostapp.com/IMG_0617.mp4"),
        ChallengeModel(challengeName: "Challenge 4", url: "https://appdeeplink007.000webhostapp.com/IMG_0618.mp4"),
        ChallengeModel(challengeName: "Challenge 5", url: "https://appdeeplink007.000webhostapp.com/IMG_0620.mp4"),
    ]

    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        customSetup()
        delay(delay: 0.2) {
            self.challenges[0].player?.play()
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Method
    func customSetup(){
        collectionView.register(UINib.init(nibName: "GridVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GridVideoCollectionViewCell")
        
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        self.removeFromParent()
        
        for item in challenges {
            item.player?.pause()
            item.player?.replaceCurrentItem(with: nil)
        }
        
        challenges.removeAll()
    }
    
    func removeAllObject(){
        NotificationCenter.default.removeObserver(self)
        for item in challenges {
            item.player?.pause()
            item.player?.replaceCurrentItem(with: nil)
        }
        challenges.removeAll()
    }
    
    deinit {
        removeAllObject()
    }
    

    //video player methods
    @objc func playPauseButtonAction(_ sender: UIButton) {
        let cell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0))as!HomeVideoListCVC
        var obj = challenges[sender.tag]
        if(videoFinshed && !backButtonTapped){
            videoFinshed = false
//            isPlay = true
          //  setUpVideoPlayerViewController(cell: cell)
        }else if cell.player.timeControlStatus == .playing  {
//                  isPlay = false
            obj.isPlayed = true
          //  print("sdsd",obj.isPlayed)
//            cell.isPlayed = true
            isPlaying = false
            obj.player?.pause()
           // cell.player.pause()
           // cell.playPauseButton.setImage(UIImage.init(named: "icn_play"), for: .normal)
              }else {
//            isPlay = true
            backButtonTapped = false
            cell.isPlayed = true
            obj.isPlayed = false
            isPlaying = false
            
          //  cell.player.play()

            obj.player?.play()
         //  cell.playPauseButton.setImage(UIImage.init(named: ""), for: .normal)
              }
    }

    
  
    func rewindVideo(by seconds: Float64,obj:ChallengeModel,cell:HomeVideoListCVC) {
        let currentTime = cell.player.currentTime()
              var newTime = CMTimeGetSeconds(currentTime) - seconds
              if newTime <= 0 {
                  newTime = 0
              }
        cell.player.seek(to: CMTime(value: CMTimeValue(newTime * 1000), timescale: 1000))
      }

    func forwardVideo(by seconds: Float64,obj:ChallengeModel,cell:HomeVideoListCVC) {
        let currentTime = cell.player.currentTime()
        let duration = cell.player.currentItem?.duration
              var newTime = CMTimeGetSeconds(currentTime) + seconds
          if newTime >= CMTimeGetSeconds(duration!) {
              newTime = CMTimeGetSeconds(duration!)
              }
        cell.player.seek(to: CMTime(value: CMTimeValue(newTime * 1000), timescale: 1000))
      }
    
    @objc func multipleTapForward(_ sender: UIButton, event: UIEvent) {
        let cell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0))as!HomeVideoListCVC
        isPlaying = false

        let touch: UITouch = event.allTouches!.first!
        let obj = challenges[sender.tag]
        if (touch.tapCount == 2) {
            if(!videoFinshed){
                self.forwardVideo(by: 5.0,obj:obj,cell:cell)
            }
            if(cell.player.timeControlStatus == .playing){
                cell.player.play()
            }else{
                cell.player.pause()
            }
            
        }
    }
    
    @objc func multipleTapBackward(_ sender: UIButton, event: UIEvent) {
//        setUpVideoPlayerViewController()
        let cell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0))as!HomeVideoListCVC
        isPlaying = false

        if(videoFinshed){
            videoFinshed = false
           // setUpVideoPlayerViewController(cell:cell)
        }
        let obj = challenges[sender.tag]
        self.rewindVideo(by: 5.0,obj:obj,cell:cell)
        backButtonTapped = true
        if(cell.player.timeControlStatus == .playing){
            obj.player?.play()
            backButtonTapped = false
        }else{
            obj.player?.pause()
        }
    }

    //end video player
    

    
    //MARK:- Open Controller Method
    func openDonateScreen(cell:HomeVideoListCVC){
        cell.player.pause()
        
   
        
        
    }
    
    func openMessageScreen(cell:HomeVideoListCVC){
        cell.player.pause()
        
     
        
    }
    
    func openFeedScreen(cell:HomeVideoListCVC){
        self.timer.invalidate()
        self.completion?()
        cell.player.pause()
        
    }
    
    func openUserProfileScreen(cell:HomeVideoListCVC){
        cell.player.pause()
       
    }
    
    func openProfileScreen(cell:HomeVideoListCVC){
       
      
    }
    
    func openShootScreen(){
  
    }
    
    func openShareScreen(){
        
        
    }
    
  
    
    
    
    //MARK:- UIButton Method
    @IBAction func filterButtonAction(_ sender: UIButton){
    }
    
    @IBAction func listButtonAction(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        isList = !isList
        if !isList {
        for (index,_) in challenges.enumerated(){
            challenges[index].player = nil
            challenges[index].player?.replaceCurrentItem(with: nil)
        }
        }else {
            delay(delay: 0.2) {
                self.challenges[0].player?.play()
            }
        }
        self.collectionView.reloadData()
    }
   
    
}


extension HomeVideoViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return challenges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isList {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoListCVC", for: indexPath) as! HomeVideoListCVC
        
        if challenges[indexPath.row].player == nil {
            if let url = URL(string: challenges[indexPath.row].url) {
                challenges[indexPath.row].player = AVPlayer(url: url)
            }
        }
        
        cell.outerView.layer.cornerRadius = isList ? 0 : 15
        
        cell.playPauseButton.addTarget(self, action: #selector(playPauseButtonAction(_:)), for: .touchUpInside)
        cell.forwardButton.addTarget(self, action: #selector(multipleTapForward(_:event:)), for: UIControl.Event.touchDownRepeat)
        cell.backwardButton.addTarget(self, action: #selector(multipleTapBackward(_:event:)), for: UIControl.Event.touchDownRepeat)
       // self.setUpVideoPlayerViewController(cell:cell)
        cell.backwardButton.tag = indexPath.row
        cell.forwardButton.tag = indexPath.row
        cell.playPauseButton.tag = indexPath.row
       
 
        
    //     cell.setPlayer(object: challenges[indexPath.row])
        
        
        cell.messageClickCompletion = {
            self.openMessageScreen(cell:cell)
        }
        
        cell.followClickCompletion = {
         
        }
        
        cell.shareClickCompletion = {
            self.openShareScreen()
        }
        
        cell.donateClickCompletion = {
            self.openDonateScreen(cell: cell)
        }
        
        cell.feedClickCompletion = {
            self.openFeedScreen(cell:cell)
        }
        
        cell.shootClickCompletion = {
            self.openShootScreen()
        }
        
        cell.profileClickCompletion = {
            self.openProfileScreen(cell:cell)
        }
        return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridVideoCollectionViewCell", for: indexPath) as! GridVideoCollectionViewCell

             cell.voteLabel.attributedText = getTextWithImage(startString: "", imageName: "ic_vote_count", lastString: " 4688", imageAddtionalSize: 5)
             cell.fanLabel.attributedText = getTextWithImage(startString: "", imageName: "ic_fan_count", lastString: " 4688", imageAddtionalSize: 5)
            cell.commonImageView.image = UIImage.init(named: indexPath.row % 2 == 0 ? "sample_1" : "sample_2")
            
            cell.voteClicked = {
                
            }
            
            cell.shareClicked = {
                       
            }
            
            cell.playClicked = {
                              
                   }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isList {
            return CGSize.init(width: Window_Width , height: self.collectionView.frame.height)
        }else {
            return CGSize.init(width: self.collectionView.frame.width / 2 - 8 , height: 250)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 0
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return isList ? 0 : 10
      }
    
    
   
}

extension HomeVideoViewController {
    func rotateAnimation(imageView:UIButton,duration: CFTimeInterval = 5.0) {
           let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
           rotateAnimation.fromValue = 0.0
           rotateAnimation.toValue = CGFloat(.pi * 2.0)
           rotateAnimation.duration = duration
           rotateAnimation.repeatCount = .greatestFiniteMagnitude
           imageView.layer.add(rotateAnimation, forKey: nil)
       }
}

extension HomeVideoViewController {
    //MARK:- Switching the Page
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let pageWidth = scrollView.frame.size.height
        let currentPage = Int(floor((scrollView.contentOffset.y - pageWidth * 0.5) / pageWidth) + 1)
        print(currentPage)
        
        if self.currentPage == currentPage {
            return
        }
        
        if  let cell = collectionView.cellForItem(at: IndexPath(row: currentPage, section: 0)) as? HomeVideoListCVC {
            cell.playPauseButton.setImage(UIImage.init(named: ""), for: .normal)
            
        }
        
        self.currentPage = currentPage
        for (index, _) in challenges.enumerated() {
            if index == currentPage {
                isPlaying = true
                //  collectionView.reloadData()
                challenges[index].isPlayed = true
                if (challenges[index].player?.rate ?? 1) == 0 {
                    challenges[index].player?.play()
                }
                //                challengesArr[index].player?.play()
                challenges[index].removeObserver()
                challenges[index].addObserver()
            } else {
                isPlaying = false
                challenges[index].player?.pause()
                challenges[index].isPlayed = false
                challenges[index].removeObserver()
                if index < currentPage - 1 && index > currentPage + 1 {
                    challenges[index].player?.replaceCurrentItem(with: nil)
                    challenges[index].player = nil
                }
            }
        }
        
        
    }
}



struct ChallengeModel {
    var challengeName: String
    var url: String
    var player: AVPlayer?
    var isPlayed = false
    var timer = Timer()
    var counter = 0
    var isEnded = false

    
     func addObserver() {
            //  print(" ======== = = == = = == Observer added = == =  == = =  ")
    //          NotificationCenter.default.addObserver(self, selector: #selector(videoDidEnd), name:
    //              NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue:
            OperationQueue.main) {  notification in //1
             //   print(" ======== = = == = = == video ended called = == =  == = =  ")
             //   self.isEnded = true
                self.player?.seek(to: CMTime.zero)
                if self.isPlayed {
                   // self.player?.play()
                }
            }

          }
          
          func removeObserver() {
             // print(" ======== = = == = = == Observer removeObserver = == =  == = =  ")
              NotificationCenter.default.removeObserver(self)
          }
}
