//
//  GetAdviceHelpLayover.swift
//  BadAss
//
//  Created by Dharmendra Sinha on 02/01/21.
//  Copyright © 2021 Quytech. All rights reserved.
//

import UIKit

class GetAdviceHelpLayover: UIViewController {

    @IBOutlet weak var txtLabel: LocalisedLabel!
    @IBOutlet weak var titleLabel: LocalisedLabel!
    @IBOutlet weak var cancelButton: LocalisedButton!
    @IBOutlet weak var proceedButton: LocalisedButton!
    
    var completion: ((Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDetailLabel()
    }
    
    
    fileprivate func setDetailLabel() {
        let str1 = "Please upload a video (only 1 free question per month) or visit our website " //"Please upload a video for 1 free question or visit our website ".localized()
        let str2 = "https://badasscommunity.com/buy".localized()
        let str3 = " to purchase more questions.".localized()

        let detailString            = str1 + str2 + str3
        let attributedString        = NSMutableAttributedString(string: detailString)
        let startRange              = (detailString as NSString).range(of: str1)
        let linkRange               = (detailString as NSString).range(of: str2)
        let endRange                = (detailString as NSString).range(of: str3)

        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.black as Any], range: startRange)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemBlue as Any], range: linkRange)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.black as Any], range: endRange)
        
        txtLabel.numberOfLines    = 0
        txtLabel.attributedText   = attributedString
        let tapGesture         = UITapGestureRecognizer(target: self, action: #selector(tappedOnLinkLabel))
        tapGesture.numberOfTapsRequired    = 1
        tapGesture.numberOfTouchesRequired = 1
        txtLabel.addGestureRecognizer(tapGesture)
        txtLabel.isUserInteractionEnabled = true
    }
    
    @objc func tappedOnLinkLabel(gesture: UITapGestureRecognizer) {
        let text = (txtLabel.text)!
        let linkRange = (text as NSString).range(of: "https://badasscommunity.com/buy".localized())
        
        if gesture.didTapAttributedTextInLabel(label: txtLabel, inRange: linkRange){
            UIApplication.shared.open(URL(string: AppURLs.QuestionPurchaseURL)!, options: [:], completionHandler: nil)
            dismiss(animated: true, completion: nil)
        }
    }
    

    @IBAction func cancelTapped(_ sender: Any) {
        completion?(false)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func proceedTapped(_ sender: Any) {
        completion?(true)
        dismiss(animated: true, completion: nil)
    }
    
}
