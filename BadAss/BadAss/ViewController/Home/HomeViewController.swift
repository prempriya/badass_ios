//
//  HomeViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 27/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import MediaWatermark
import Photos

class HomeViewController: UIViewController, QBManagerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var adviceView: UIView!
    @IBOutlet weak var adviceImageView: UIImageView!
    @IBOutlet weak var connectionView: UIView!
    @IBOutlet weak var connectingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var getAdviceContainer: UIView!
    @IBOutlet weak var getAdviceLabel: UILabel!
    
    
    var titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 64))
    
    var categoryList: [Category] = categoriesArray ?? []
    var typeList = [TypeModel]()
    var finalURL : URL?
    var downloadTask = URLSessionDownloadTask()
    
    let filter: Filter = Filter.shared
    var page: Page = Page()
    
    var videos: [HomeModel] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.mainCollectionView.reloadData()
            }
        }
    }
    
    var imageArray = ["ic_get_advice","ic_change_language_eng","ic_get_advice"]
    
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        getVideosList()
    }
    
    //MARK:- Helper Method
    
    func updateTitle() {
        titleLabel.textColor = .white
        titleLabel.font = UIFont(name: "HeavyBlack", size: 28)
        titleLabel.text = GeneralStrings.badass.localized()
        titleLabel.textAlignment = .center
        self.navigationItem.titleView = titleLabel
        
        getAdviceLabel.text = StaticStrings.get_advice.localized()
    }
    
    func refreshData(){
        self.videos.removeAll()
        self.page.page = 1
        self.getVideosList()
        self.collectionView.reloadData()
    }
    
    
    func setConnetionView(hide: Bool, animated: Bool = true) {
        DispatchQueue.main.async {
            if hide {
                UIView.animate(withDuration: animated ? 0.2 : 0.0) {
                    self.connectingViewHeight.constant = 0
                    self.view.layoutIfNeeded()
                } completion: { (s) in
                    self.connectionView.isHidden = true
                }
            } else {
                UIView.animate(withDuration: animated ? 0.2 : 0.0) {
                    self.connectingViewHeight.constant = 30
                    self.view.layoutIfNeeded()
                } completion: { (s) in
                    self.connectionView.isHidden = false
                }
            }
        }
    }
    
    @objc func retryConnection() {
        if let label = self.view.viewWithTag(111) as? UILabel {
            if label.text == StaticStrings.connection_failed.localized() {
                QBManager.shared.manageUserAuthorization()
            }
        }
    }
    
    func connectionFailed() {
        setConnetionView(hide: false)
        if let label = self.view.viewWithTag(111) as? UILabel {
            label.text = StaticStrings.connection_failed.localized()
        }
    }
    
    func connectionEstablished() {
        setConnetionView(hide: true)
    }
    
    func connectionEstablishing() {
        setConnetionView(hide: false)
        if let label = self.view.viewWithTag(111) as? UILabel {
            label.text = StaticStrings.connecting.localized()
        }
    }
    
    func setNavigationBar() {
        updateTitle()
        
        if LocalizationHelper.currentLanguage() == kEnglish {
            navigationItem.leftBarButtonItem = AppUtility.leftBarButton("ic_change_language_eng", controller: self)
        } else {
            navigationItem.leftBarButtonItem = AppUtility.leftBarButton("ic_change_language_hin", controller: self)
        }
        
        navigationItem.rightBarButtonItem = AppUtility.rightBarButton("ic_search", controller: self)
    }
    
    func initialSetup() {
        
        
        page.pageSize = 10
        categoryList.filter({$0.id == "0"}).first?.isSelected = true
        dummyArray()
        setNavigationBar()
        mainCollectionView.register(UINib.init(nibName: "GridVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GridVideoCollectionViewCell")
        
        
        setConnetionView(hide: true, animated: false)
        QBManager.shared.connectionDelegates.append(self)
        QBManager.shared.manageUserAuthorization()
        
        
        adviceView.layer.cornerRadius = 15
        let layer1 = gradientLayer(adviceView.bounds, .adviceView)
        layer1.cornerRadius = 15
        adviceView.layer.insertSublayer(layer1, at: 0)
        
        if let email = AuthManager.shared.loggedInUser?.email {
            var isAdvisor = false
            GetAdviceType.allCases.forEach { (type) in
                if type.getEmailAddress() == email { isAdvisor = true }
            }
            if isAdvisor {
                self.getAdviceContainer.isHidden = true
            }
        }
        
        
        
        getAdviceContainer.transform = CGAffineTransform.init(translationX: 180, y: 0)
        delay(delay: 2) {
            UIView.animate(withDuration: 1.0) {
                self.getAdviceContainer.transform = CGAffineTransform.identity
                self.view.layoutIfNeeded()
            }
        }
        
        func translateToTopThenBottom() {
            UIView.animate(withDuration: 0.3) {
                self.adviceImageView.transform = CGAffineTransform.init(translationX: 0, y: -100)
                self.view.layoutIfNeeded()
            } completion: { (s) in
                UIView.animate(withDuration: 0) {
                    self.adviceImageView.transform = CGAffineTransform.init(translationX: 0, y: 100)
                } completion: { (s) in
                    UIView.animate(withDuration: 0.3) {
                        self.adviceImageView.transform = .identity
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }

        delay(delay: 3) {
            Timer.scheduledTimer(withTimeInterval: 2.0, repeats: true) { (t) in
                translateToTopThenBottom()
            }
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(retryConnection))
        connectionView.addGestureRecognizer(tapGesture)
    }
    
    @objc func leftBarButtonAction(_ button : UIButton) {
        if LocalizationHelper.currentLanguage() == kEnglish {
            UserDefaults.languageCode = kHindi
            UserDefaults.standard.set(kHindi, forKey: CurrentLanguageKey)
            navigationItem.leftBarButtonItem = AppUtility.leftBarButton("ic_change_language_hin", controller: self)
        } else {
            UserDefaults.languageCode = kEnglish
            UserDefaults.standard.set(kEnglish, forKey: CurrentLanguageKey)
            navigationItem.leftBarButtonItem = AppUtility.leftBarButton("ic_change_language_eng", controller: self)
        }

        
        APPDELEGATE.tabbar?.setTabTitle()
        updateTitle()
        typeCollectionView.reloadData()
        collectionView.reloadData()
    }
    
    @objc func rightBarButtonAction(_ button : UIButton) {
        if let vc = UIStoryboard.main.get(SearchViewController.self){
            APPDELEGATE.navigationController.pushViewController(vc, animated: true)
        }
    }
    
    
    func dummyArray(){
        typeList = [
            TypeModel(name: GeneralStrings.trending, status: true, imageName: "ic_trending"),
            TypeModel(name: GeneralStrings.latest, status: false, imageName: "ic_latest"),
            TypeModel(name: GeneralStrings.language, status: true, imageName: "ic_language")
        ]
    }
    
    func showActionSheet() {
        
        func showOptions() {
            let optionMenu =  UIAlertController.init(title: StaticStrings.selectGetAdviceOption.localized(), message: "", preferredStyle: .actionSheet)
            
            let appGalleryAction = UIAlertAction(title: GetAdviceType.LegalAdvisor.rawValue.localized(), style: .default, handler: {
                                                        (alert: UIAlertAction!) -> Void in
                                                            self.adviceSelected(type: .LegalAdvisor)
                                                    })
            
            let libraryAction = UIAlertAction(title: GetAdviceType.TarotCardReader.rawValue.localized(), style: .default, handler: {
                                                    (alert: UIAlertAction!) -> Void in
                self.adviceSelected(type: .TarotCardReader)
                                                })
            
            let shootAction = UIAlertAction(title: GetAdviceType.Astrologer.rawValue.localized(), style: .default, handler: {
                                                    (alert: UIAlertAction!) -> Void in
                self.adviceSelected(type: .Astrologer)
                                                })
            
            let cancelAction = UIAlertAction(title: StaticStrings.cancel.localized(), style: .destructive, handler: {
                                                    (alert: UIAlertAction!) -> Void in
                                                    print("Cancelled")
                                                })
            
            optionMenu.addAction(appGalleryAction)
            optionMenu.addAction(libraryAction)
            optionMenu.addAction(shootAction)
            optionMenu.addAction(cancelAction)
            
            self.present(optionMenu, animated: true, completion: nil)
        }
        
        
        func showDisclaimer() {
            AlertController.alert(title: StaticStrings.disclaimer.localized(), message: StaticStrings.disclaimer_message.localized(), buttons: [StaticStrings.ok.localized()]) { (action, code) in
                UserDefaults.isGetAdviceDisclaimerShown = true
                showOptions()
            }
        }
        
        
        UserDefaults.isGetAdviceDisclaimerShown ? showOptions() : showDisclaimer()

    }
    
    
    func showOptionsToPurchase() {
        
        let optionMenu =  UIAlertController.init(title: AppName.localized(), message: "What would you like to proceed with?", preferredStyle: .actionSheet)
        
        let appGalleryAction = UIAlertAction(title: "Upload Video", style: .default, handler: {
                                                    (alert: UIAlertAction!) -> Void in
            (self.tabBarController as? TabbarViewController)?.didTapOnAddVideoButton()
        })
        
        let libraryAction = UIAlertAction(title: "Purchase", style: .default, handler: {
                                                (alert: UIAlertAction!) -> Void in
            UIApplication.shared.open(URL(string: AppURLs.QuestionPurchaseURL)!, options: [:], completionHandler: nil)
                                            })
        
        let cancelAction = UIAlertAction(title: StaticStrings.cancel.localized(), style: .destructive, handler: {
                                                (alert: UIAlertAction!) -> Void in
                                                print("Cancelled")
                                            })
        
        optionMenu.addAction(appGalleryAction)
        optionMenu.addAction(libraryAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    func adviceSelected(type: GetAdviceType) {
        self.getQuestionsCount { (count, errorMessage) in
            if let count = count {
                if count > 0 {
                    self.getQbId(email: type.getEmailAddress()) { (id, error) in
                        if let id = id {
                            self.startChatWithAdvisor(otherQbId: id)
                        } else {
                            AlertController.alert(title: AppName.localized(), message: errorMessage)
                        }
                    }
                } else {
                    
//                    let textSignupLabel = "Please upload a video for 1 free question or visit our website www.badasscommunity.com/buy to purchase more questions."
//                    let underlineAttriStringLogin = NSMutableAttributedString(string: textSignupLabel)
//
//                    let rangeText = (textSignupLabel as NSString).range(of: "www.badasscommunity.com/buy")
//                    underlineAttriStringLogin.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.blue as Any, NSAttributedString.Key.font: Fonts.Nunito.regular.font(.large)], range: rangeText)
//
//                    let alert = UIAlertController(title: AppName.localized(), message: "", preferredStyle: .alert)
//                    alert.setValue(underlineAttriStringLogin, forKey: "attributedMessage")
//
//                    alert.addAction(UIAlertAction(title: StaticStrings.cancel.localized(), style: .cancel, handler: { (a) in
//
//                    }))
//
//                    alert.addAction(UIAlertAction(title: StaticStrings.proceed.localized(), style: .default, handler: { (a) in
//                        self.showOptionsToPurchase()
//                    }))
//
//                    self.present(alert, animated: true, completion: nil)
                    
                    if let vc = UIStoryboard.main.get(GetAdviceHelpLayover.self) {
                        
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.modalTransitionStyle = .crossDissolve
                        
                        vc.completion = { [weak self] status in
                            
                            if status {
                                self?.showOptionsToPurchase()
                            } else {
                            }
                        }
                        
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                }
            } else {
                AlertController.alert(title: AppName.localized(), message: errorMessage)
            }
        }
    }
    
    func openShareScreen(obj: HomeModel){
        self.downloadFile(videoUrl: obj.video_url ?? "")
    }
    
    @IBAction func getAdviceButtonAction(_ sender: UIButton){
        if QBManager.shared.isConnected() {
            showActionSheet()
        } else {
            AlertController.alert(title: AppName.localized(), message: StaticStrings.connection_progress.localized())
        }
    }
    
    
    fileprivate func startChatWithAdvisor(otherQbId: String) {
        
        func sendChatRequest() {
            QBManager.shared.checkForDialog(otherUserId: "\(otherQbId)", withAdvisor: true) { [weak self](success, dialog, isNew) in
                ProgressHud.hideActivityLoader()
                if let _ = self, success, let dialog = dialog {
                    
                    
                    if isNew {
                        AlertController.alert(title: AppName.localized(), message: StaticStrings.advice_start.localized(), buttons: [StaticStrings.ok.localized()]) { (a, i) in
                            if let chatVc = UIStoryboard.main.get(MessageViewController.self) {
                                chatVc.chatDialog = dialog
                                APPDELEGATE.navigationController.pushViewController(chatVc, animated: true)
                            }
                        }
                    } else {
                        if let chatVc = UIStoryboard.main.get(MessageViewController.self) {
                            chatVc.chatDialog = dialog
                            APPDELEGATE.navigationController.pushViewController(chatVc, animated: true)
                        }
                    }
                    
                    // Commented because
                    if isNew {
                        let payload = [
                            "message": "\(AuthManager.shared.loggedInUser?.fullname ?? "An user") started a conversation with you.",
                            "ios_badge": "0",
                            "ios_sound": "default",
                            "ios_content-available": 1,
                            "type": "chat_request",
                            "id": "\(dialog.id ?? "0")",
                        ] as [String : Any]

                        QBManager.shared.sendPush(toIds: [UInt(dialog.recipientID)], payload: payload)
                    }
                    
                } else {
                    AlertController.alert(title: AppName.localized(), message: "Failed to create chat.")
                }
            }
        }
        
        
        ProgressHud.showActivityLoader()
        QBManager.shared.sendContactRequest(qbId: otherQbId) { (status, error) in
            if error == nil {
                sendChatRequest()
            } else {
                ProgressHud.hideActivityLoader()
                Debug.log(error?.localizedDescription ?? "")
                AlertController.alert(title: AppName.localized(), message: "Failed to create chat.")
            }
        }
    }
    
    
    public func refreshCategories() {
        self.categoryList = categoriesArray ?? []
        DispatchQueue.main.async { [weak self] in
            self?.collectionView?.reloadData()
        }
    }
    
    
}



extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 0 ? categoryList.count : collectionView.tag == 1 ? typeList.count : videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {     // For Category
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SectionCVCell", for: indexPath) as! SectionCVCell
            let category = categoryList[indexPath.item]
            
            if LocalizationHelper.currentLanguage() == kEnglish {
                cell.headerLabel.text = categoryList[indexPath.item].category?.capitalized
            } else {
                cell.headerLabel.text = categoryList[indexPath.item].categoryHindi?.capitalized
            }
            
            cell.downLabel.isHidden = !categoryList[indexPath.item].isSelected
            
            
            if category.id == "0" {
                cell.commonImageView.image = UIImage(named: "ic_categories_all")
            } else {
                if let url = URL(string: category.image ?? "") {
                    cell.commonImageView.kf.setImage(with: url)
                } else {
                    cell.commonImageView.image = nil
                }
            }
            
            return cell
        } else if collectionView.tag == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridVideoCollectionViewCell", for: indexPath) as! GridVideoCollectionViewCell
            let obj = videos[indexPath.row]
            cell.loadVideo(obj: obj)
            
            cell.voteClicked = {
                self.voteUnvote(obj: self.videos[indexPath.row], index: indexPath.row)
            }
            
            cell.shareClicked = {
                self.openShareScreen(obj: self.videos[indexPath.row])
            }
            
            cell.playClicked = {
                
            }
            return cell
        }  else {    // For type
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeCollectionViewCell", for: indexPath) as! TypeCollectionViewCell
            cell.headerLabel.text = typeList[indexPath.item].name?.localized()
            cell.cellView.layer.borderColor = ((typeList[indexPath.item].status ?? false) ? UIColor.white : RGBA(125, g: 125, b: 125, a: 1)).cgColor
            cell.commonImageView.image = UIImage(named: typeList[indexPath.row].imageName ?? "")
            
            if indexPath.item == 2 {
                let name = typeList[indexPath.item].name?.localized() ?? GeneralStrings.language.localized()
                let count = Filter.shared.languages.count
                if count > 0 {
                    cell.headerLabel.text = "\(name) (\(count))"
                }
            }
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            
            categoryList = categoryList.map({ (object) -> Category in
                object.isSelected = false
                return object
            })
            
            categoryList[indexPath.item].isSelected = !categoryList[indexPath.item].isSelected
            filter.category = categoryList[indexPath.item].id ?? "0"
            refreshData()
            
        } else if collectionView.tag == 1 {
            if indexPath.item == 2 {
                if let vc = UIStoryboard.profile.get(SelectLanguageViewController.self){
                    vc.modalPresentationStyle = .custom
                    vc.modalTransitionStyle = .crossDissolve
                    vc.presentedFrom = .Home
                    vc.preselectedLanguages = Filter.shared.languages.map({ (id) -> Language in
                        return Language(id: id)
                    })
                    vc.selectedLanguages = { languages in
                        if let langs = languages {
                            Filter.shared.languages = langs.compactMap({$0.id})
                            collectionView.reloadData()
                            self.refreshData()
                        }
                    }
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
            } else {
                typeList = typeList.map({ (object) -> TypeModel in
                    object.status = false
                    return object
                })
                typeList[indexPath.item].status = !(typeList[indexPath.item].status ?? false)
                if typeList[indexPath.item].name?.lowercased() == "trending" {
                    filter.trending = typeList[indexPath.item].status ?? false
                    filter.isLatest = false
                } else {
                    filter.trending = false
                    filter.isLatest = typeList[indexPath.item].status ?? false
                }
                refreshData()
                self.typeCollectionView.reloadData()
            }
        } else {
            moveToMainThread { [weak self] in
                if let vc = UIStoryboard.main.get(PostViewController.self), let obj = self?.videos[indexPath.row] {
                    vc.videos.append(obj)
                    vc.voteCompletion = { [weak self] in
                        self?.getVideosList(false)
                    }
                    APPDELEGATE.navigationController.pushViewController(vc, animated: true)
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            let object  = categoryList[indexPath.item]
            let labelSize = (object.category?.width(withheight: 40, font: UIFont.systemFont(ofSize: 10)) ?? 0.0) + 2
            return CGSize(width: collectionView.bounds.width > labelSize ? labelSize : collectionView.bounds.width - 20, height: 77)
        } else if collectionView.tag == 2 {
            return CGSize.init(width: self.mainCollectionView.frame.width / 2 - 6 , height: 280)
        }  else {
            if indexPath.item == 2, Filter.shared.languages.count > 0 {
                let object  = typeList[indexPath.item]
                let str = (object.name ?? "") + "\(Filter.shared.languages.count)"
                let labelSize = (str.width(withheight: 40, font: UIFont.systemFont(ofSize: 10))) + 40 + 20
                return CGSize(width: collectionView.bounds.width > labelSize ? labelSize : collectionView.bounds.width - 20, height: 55)
            }
            
            let object  = typeList[indexPath.item]
            let labelSize = (object.name?.width(withheight: 40, font: UIFont.systemFont(ofSize: 10)) ?? 0.0) + 40
            return CGSize(width: collectionView.bounds.width > labelSize ? labelSize : collectionView.bounds.width - 20, height: 55)
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let rows = mainCollectionView.indexPathsForVisibleItems.map({$0.item})
        for (i,v) in videos.enumerated() {
            if v.player != nil, rows.contains(i) == false {
                v.deinitialisePlayer()
            } else if rows.contains(i) {
                v.initialisePlayer()
            }
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainCollectionView {
            
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            
            if page.isExecuting {
                return
            }
            
            if currentOffset - maximumOffset >= 15.0 {
                if self.page.hasNext {
                    self.page.page = self.page.page + 1
                    self.getVideosList(false)
                }
            }
        }
    }
}



class CategoryModel: NSObject {
    var name: String?
    var status: Bool?
    var imageName: String?
    
    init(name: String, status: Bool, imageName: String) {
        self.name = name
        self.status = status
        self.imageName = imageName
    }
}

class TypeModel: NSObject {
    var name: String?
    var status: Bool?
    var imageName: String?
    
    init(name: String, status: Bool, imageName: String) {
        self.name = name
        self.status = status
        self.imageName = imageName
    }
}


class SearchBarContainerView: UIView, UISearchBarDelegate {
    
    let searchBar: UISearchBar
    
    var onTextChange: (() -> Void)?
    
    init(customSearchBar: UISearchBar) {
        searchBar = customSearchBar
        super.init(frame: CGRect.zero)
        
        searchBar.delegate = self
        searchBar.placeholder = "Search".localized()
        searchBar.barTintColor = UIColor.white
        searchBar.searchBarStyle = .minimal
        searchBar.returnKeyType = .done
        addSubview(searchBar)
    }
    
    override convenience init(frame: CGRect) {
        self.init(customSearchBar: UISearchBar())
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        searchBar.frame = bounds
    }
}


extension HomeViewController {

    
    
    func getVideosList(_ hud: Bool = true) {
        var param = [String: Any]()
        param[kCategory_id] = filter.category
        param[kTrending] = filter.trending
        param[kLanguage_ids] = Filter.shared.languages.map({$0}).joined(separator: ",")
        param[kLatest] = filter.isLatest
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getVideos(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            
            if let array = response?.array, let self = self {
                if self.page.page == 1 {
                    self.videos.removeAll()
                }
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                self.videos.append(contentsOf: array)
            }  else {
                if response?.statusCode == 200 {
                    return
                }
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    func voteUnvote(obj: HomeModel, index: Int){
        
        var param = [String: Any]()
        param[kvideo_id] = obj.video_id
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param[kvote_status] = obj.vote_status == "0" ? 1 : 0
        
        WebServices.vote(params: param) { (response) in
            if response?.statusCode == 200 {
                self.videos[index].vote_status = self.videos[index].vote_status == "0" ? "1" : "0"
                let count = Int(self.videos[index].vote_count ?? "0") ?? 0
                if self.videos[index].vote_status == "0" {
                    self.videos[index].vote_count  = "\(count - 1)"
                }else {
                    self.videos[index].vote_count  = "\(count + 1)"
                }
                self.mainCollectionView.reloadItems(at: [IndexPath.init(item: index, section: 0)])
            }
        }
    }
    
    
    func getQuestionsCount(onCompletion: @escaping (Int?, String) -> Void) {
        ProgressHud.showActivityLoader()
        let userId = AuthManager.shared.loggedInUser?.user_id ?? 0
        WebServices.getUsersQuestionsCount(userId: "\(userId)") { (response) in
            ProgressHud.hideActivityLoader()
            if let obj = response?.object {
                let count = (Int(obj.free_count ?? "0") ?? 0) + (Int(obj.paid_count ?? "0") ?? 0)
                onCompletion(count, "")
            } else {
                onCompletion(nil, CommonMessage.something_went_wrong.localized())
            }
        }
    }
    
    func getQbId(email: String, onCompletion: @escaping (_ id: String?, _ errorMessage: String) -> Void) {
        ProgressHud.showActivityLoader()
        WebServices.getQbIdByEmailAddress(email: email) { (response) in
            ProgressHud.hideActivityLoader()
            if let obj = response?.object, let id = obj.qb_id {
                onCompletion(id, "")
            } else {
                onCompletion(nil, CommonMessage.something_went_wrong.localized())
            }
        }
    }
    
    
}


extension HomeViewController: URLSessionDownloadDelegate {
    
    func downloadFile(videoUrl: String) {
        IHProgressHUD.show(progress: 0.0, status: String(format: "\(StaticStrings.downloading.localized())(%d)", 0 ))
        let configuration = URLSessionConfiguration.default
        let operationQueue = OperationQueue()
        let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: operationQueue)
        guard let url = URL(string: videoUrl) else {
            IHProgressHUD.dismiss()
            return
        }
        let downloadTask = urlSession.downloadTask(with: url)
        self.downloadTask =  downloadTask
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("finish downloading")
        
        let fileManager = FileManager.default
        var url: URL?
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURL = documentDirectory.appendingPathComponent("badass_video.mp4")
            let data = try Data(contentsOf: location)
            try? data.write(to: fileURL)
            url = fileURL
        } catch {
            IHProgressHUD.dismiss()
            print(error)
        }
        
        if let url = url {
            DispatchQueue.main.sync {
                DispatchQueue.main.async {
                    IHProgressHUD.showInfowithStatus("Processing...")
                }
                addWaterMarkImage(url: url) { [weak self](url) in
                    if let url = url {
                        self?.finalURL = url
                        self?.sharePost(url: url)
                    }
                    IHProgressHUD.dismiss()
                }
            }
        } else {
            IHProgressHUD.dismiss()
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print(totalBytesWritten, totalBytesExpectedToWrite)
        let percentage = Double(Double(totalBytesWritten) / Double(totalBytesExpectedToWrite)) * 100
        DispatchQueue.main.async {
            IHProgressHUD.show(progress: CGFloat(percentage/100), status: String(format: "\(StaticStrings.downloading.localized())(%d)", Int((percentage/100) * 100)) )
        }
        print(percentage)
    }
    
    func sharePost(url: URL) {
        DispatchQueue.main.async {
            let shareAll:Array = [url as Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: [])
            activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.print,
                UIActivity.ActivityType.addToReadingList,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.openInIBooks,
                UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
            ]
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
}


extension UIImageView{
    func setImage(_ image: UIImage?, animated: Bool = true) {
        let duration = animated ? 0.8 : 0.0
        UIView.transition(with: self, duration: duration, options: .transitionCrossDissolve, animations: {
            self.image = image
        }, completion: nil)
    }
}
             
