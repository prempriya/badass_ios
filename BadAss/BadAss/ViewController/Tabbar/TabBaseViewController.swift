//
//  TabBaseViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 27/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

enum SelectedTab: Int {
    case home = 0
    case notification
    case add
    case chat
    case profile
}

class TabBaseViewController: UIViewController {
    
    @IBOutlet weak var homeLabel: UILabel!
       @IBOutlet weak var notificationLabel: UILabel!
       @IBOutlet weak var chatLabel: UILabel!
       @IBOutlet weak var profileLabel: UILabel!
              
       @IBOutlet weak var homeImageView: UIImageView!
       @IBOutlet weak var notificationImageView: UIImageView!
       @IBOutlet weak var chatImageView: UIImageView!
       @IBOutlet weak var profileImageView: UIImageView!
       
      
       
       @IBOutlet weak var containerView: UIView!
       @IBOutlet weak var tabView: UIView!
       @IBOutlet weak var stackView: UIStackView!
       
       var currentController: UIViewController?
       var currentTab = SelectedTab(rawValue: 0)
    
    let notificationVC = UIStoryboard.main.get(NotificationViewController.self)
       let homeVC = UIStoryboard.main.get(HomeViewController.self)
       let profileVC = UIStoryboard.profile.get(ProfileViewController.self)
       let chatVC = UIStoryboard.chat.get(ChatViewController.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
    }
    
    func customSetup(){
        deselectTab()
        updateChildController(selectedTab: currentTab!)
        tabView.aroundShadow()
        tabView.layer.cornerRadius = 30.0
        tabView.clipsToBounds = false
        tabView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
      //  stackView.customize(backgroundColor: .clear, radiusSize: 10)
        tabView.aroundShadow(.lightGray)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    
    
    
    
     // MARK: - UIButton Action Method
        @IBAction func commonButtonAction(_ sender: UIButton){
            updateChildController(selectedTab: SelectedTab(rawValue: sender.tag)!)
        }
        
        
        func deselectTab() {
            homeLabel.textColor =  .black
            notificationLabel.textColor =  .black
            chatLabel.textColor =  .black
            profileLabel.textColor =  .black
            
            homeImageView.image = UIImage.init(named: "ic_home_selected")
            notificationImageView.image = UIImage.init(named: "ic_notifications_unselected")
            chatImageView.image = UIImage.init(named: "ic_chat_unselected")
            profileImageView.image = UIImage.init(named: "ic_profile_unselected")
            
            
        }
        
        func updateChildController(selectedTab: SelectedTab) {
            deselectTab()
            
            switch selectedTab {
            case .home:
                currentController = homeVC
                updateTabUIOnSelection(selectedTab: selectedTab)
                break
            case .notification:
                currentController = notificationVC
                updateTabUIOnSelection(selectedTab: selectedTab)
                break
            case .chat:
              
                currentController = chatVC
                updateTabUIOnSelection(selectedTab: selectedTab)
                break

            case .profile:
              
                currentController = profileVC
                updateTabUIOnSelection(selectedTab: selectedTab)
                break
                
            case .add:
                break
            }
            
            
            
            currentTab = selectedTab
            
            
            // remove subview
            for controller in self.children {
                if containerView.subviews.contains(controller.view) {
                    controller.willMove(toParent: nil)
                    controller.view.removeFromSuperview()
                    controller.removeFromParent()
                }
            }
            
            //        guard let controller = self.currentNavigation else {
            //            return
            //        }
            
            guard let controller = self.currentController else {
                return
            }
            
            controller.view.frame = self.containerView.bounds
            addChild(controller)
            containerView.addSubview(controller.view)
            controller.didMove(toParent: self)
     
        }
        
        func updateTabUIOnSelection(selectedTab: SelectedTab) {
            switch selectedTab {
            case .home:
                homeImageView.image = UIImage.init(named: "ic_home_selected")
                homeLabel.textColor = kAppRedColor
                
                break
            case .notification:
                notificationLabel.textColor = kAppRedColor
                notificationImageView.image = UIImage.init(named: "ic_notification_selected")
                break
            case .chat:
               chatLabel.textColor = kAppRedColor
               chatImageView.image = UIImage.init(named: "ic_chat_selected")
                break
            case .profile:
                profileLabel.textColor = kAppRedColor
                profileImageView.image = UIImage.init(named: "ic_profile_selected")
                break
            case .add:
                break
            }
        }
        
    }


