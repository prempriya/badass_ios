//
//  TabbarViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 28/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        perform(#selector(addRoundedViewToTabbar), with: nil, afterDelay: 0.1)
        setupMiddleButton()
        setTabTitle()
        self.updateTheCount(user: AuthManager.shared.loggedInUser)
        NotificationCenter.default.addObserver(self, selector: #selector(getProfile), name: NSNotification.Name(rawValue: "profile"), object: nil)
        getProfile()
    }
    
    @objc func getProfile() {
        WebServices.getMyProfile { [weak self](response) in
            if let user = response?.object {
                self?.updateTheCount(user: user)
            }
        }
    }
    
    
    func updateTheCount(user: User?) {
        if let unreadCount = user?.unreadNotificationCount, (unreadCount == 0) {
            AppDelegate.shared.tabbar?.tabBar.items?.second()?.badgeValue = nil
        } else {
            AppDelegate.shared.tabbar?.tabBar.items?.second()?.badgeValue = "\(user?.unreadNotificationCount ?? 0)"
        }
    }
    
    
    func setTabTitle() {
        tabBar.items?[0].title = GeneralStrings.home.localized()
        tabBar.items?[1].title = GeneralStrings.notification.localized()
        tabBar.items?[2].title = GeneralStrings.addVideo.localized()
        tabBar.items?[3].title = GeneralStrings.chat.localized()
        tabBar.items?[4].title = GeneralStrings.profile.localized()
    }
    
    
    //MARK:- Helper Methods
    @objc fileprivate func addRoundedViewToTabbar() {
        let tabBackgroundView = UIView()
        tabBackgroundView.backgroundColor = UIColor.white
        tabBackgroundView.layer.cornerRadius = 15
        tabBackgroundView.clipsToBounds = true
        tabBackgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        tabBar.insertSubview(tabBackgroundView, at: 0)
        tabBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        tabBackgroundView.leadingAnchor.constraint(equalTo: tabBar.leadingAnchor, constant: 0).isActive = true
        tabBackgroundView.trailingAnchor.constraint(equalTo: tabBar.trailingAnchor, constant: 0).isActive = true
        tabBackgroundView.topAnchor.constraint(equalTo: tabBar.topAnchor, constant: 0).isActive = true
        tabBackgroundView.bottomAnchor.constraint(equalTo: tabBar.bottomAnchor, constant: 0).isActive = true
        
        tabBar.barTintColor = UIColor.black
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
    }
    
    
    // MARK: - Setups

    func setupMiddleButton() {
        let addVideoButton = UIButton()
        addVideoButton.setImage( UIImage(named: "ic_add_video"), for: .normal)
        addVideoButton.imageView?.contentMode = .scaleAspectFit
        addVideoButton.addTarget(self, action: #selector(didTapOnAddVideoButton), for: .touchUpInside)
        self.view.addSubview(addVideoButton)
        addVideoButton.translatesAutoresizingMaskIntoConstraints = false
        addVideoButton.centerYAnchor.constraint(equalTo: tabBar.topAnchor, constant: 0).isActive = true
        addVideoButton.centerXAnchor.constraint(equalTo: tabBar.centerXAnchor, constant: 0).isActive = true
        addVideoButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        addVideoButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }

    @objc func didTapOnAddVideoButton() {
        if let vc = UIStoryboard.main.get(AddVideoViewController.self){
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            vc.videoPickCompletion = { image, url in
                   // self.navigationController?.pushViewController(vc, animated: false)
                }
            
            let navController = UINavigationController(rootViewController: vc)
            navController.setNavigationBarHidden(true, animated: false)
            navController.modalPresentationStyle = .custom
            navController.modalTransitionStyle = .crossDissolve
            
            APPDELEGATE.navigationController.present(navController, animated: true, completion: nil)
        }
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.isKind(of: BlankViewController.self) {
            didTapOnAddVideoButton()
            return false
        } else {
            return true
        }
    }
    

    
}




class RoundShadowView: UIView {

    let containerView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func layoutView() {

        // set the shadow of the view's layer
        layer.backgroundColor = UIColor.clear.cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: -8.0)
        layer.shadowOpacity = 0.12
        layer.shadowRadius = 10.0
        containerView.layer.cornerRadius = 15
        containerView.layer.masksToBounds = true

        addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false

        // pin the containerView to the edges to the view
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
