//
//  PostViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 09/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//https://stackoverflow.com/questions/11849591/how-can-i-convert-iphone-captured-video-to-http-live-streaming-files

import UIKit
import MediaWatermark
import Photos

var automaticallyWaitsToMinimizeStalling = true


class PostViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var profileBottomImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var voteImage: UIImageView!
    @IBOutlet weak var replyVideoButton: UIButton!
    
//    var object : HomeModel?
    var isPlaying = true
    
    var completion: (() -> Void)?  = nil
    
    var filterType : FilterType = .general
    var player = AVPlayer()
    var videoFinshed = false
    var backButtonTapped = false
    var currentPage = 0
    var currentSec = 0
    var timer = Timer()
    
    var videos = [HomeModel]()
    var voteCompletion: (() -> Void)?  = nil
    var isFromNotification = false
    var videoId = 0
    var finalURL : URL?
    var downloadTask = URLSessionDownloadTask()
        
    
    static var commented = false
    
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        if !isFromNotification {
            delay(delay: 0.1) {
                self.videos[0].player?.automaticallyWaitsToMinimizeStalling = automaticallyWaitsToMinimizeStalling
                self.videos[0].player?.play()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeAllObject()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFromNotification {
            getVideoDetail()
        } else {
            customSetup()
            delay(delay: 0.05) {
                self.collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Helper Methods
    func removeAllObject(){
        NotificationCenter.default.removeObserver(self)
        for item in videos {
            item.player?.pause()
            item.player?.replaceCurrentItem(with: nil)
        }
    }
    
    deinit {
        removeAllObject()
    }
    
    func initialSetup() {
        let count = Int(videos.first?.comments_count ?? "0")
        commentLabel.text = count ?? 0 > 1 ? "\(self.videos.first?.comments_count ?? "0") \(StaticStrings.Comments.localized())" : "\(self.videos.first?.comments_count ?? "0") \(StaticStrings.Comment.localized())"
        replyVideoButton.setTitle("\(StaticStrings.watchreply.localized()) (\(self.videos.first?.reply_video_count ?? "0"))", for: .normal)
    }
    
    func customSetup() {
        initialSetup()
        self.viewHeightConstraint.constant = 0
        bottomView.isHidden = true
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.bottomView.layer.cornerRadius = 15
        
        if manageProfilePrivacy(isFollowing: false, showMyProfile: videos.first?.creator?.show_profile == "0") {
            usernameLabel.text = StaticStrings.anonymous.localized()
            profileImageView.image = ImageConstant.userPlaceholder
        } else {
            usernameLabel.text = videos.first?.creator?.name
            if let imageUrl = videos.first?.creator?.profile_pic, let url = URL(string: imageUrl) {
                self.profileImageView.kf.setImage(with: url)
            }
            profileImageView.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(openProfile))
            profileImageView.addGestureRecognizer(tapGesture)
            
            usernameLabel.isUserInteractionEnabled = true
            let tapGesture1 = UITapGestureRecognizer.init(target: self, action: #selector(openProfile))
            usernameLabel.addGestureRecognizer(tapGesture1)
        }
        if let imgUrl = AuthManager.shared.loggedInUser?.image, let url = URL(string: imgUrl) {
            self.profileBottomImageView.kf.setImage(with: url)
        }
    }
    
    @objc func openProfile(){
        if "\(videos.first?.creator?.user_id ?? 0)" == "\(AuthManager.shared.loggedInUser?.user_id ?? 0)" {
            if let vc = UIStoryboard.tabbar.get(ProfileViewController.self) {
                vc.isFromTab = false
                vc.isPlaying = self.videoFinshed
                vc.backCompletion =  { status in
                    if status {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.viewHeightConstraint.constant = 90
                            self.bottomView.isHidden = false
                            self.videoFinshed = true
                        }
                    }
                }
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
                vc.userId = "\(videos.first?.creator?.user_id ?? 0)"
                vc.isPlaying = self.videoFinshed
                vc.backCompletion =  { status in
                    if status {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.viewHeightConstraint.constant = 90
                            self.bottomView.isHidden = false
                            self.videoFinshed = true
                        }
                    }
                }
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func playerEndSetup(cell: PostCollectionViewCell) {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: cell.player.currentItem, queue:
        OperationQueue.main) {  notification in //1
            cell.overlayView.isHidden = false
            DispatchQueue.main.async {
                self.viewHeightConstraint.constant = 90
                self.bottomView.isHidden = false
                self.videoFinshed = true
            }
        }
    }
    
    
    //video player methods
    @objc func playPauseButtonAction(_ sender: UIButton) {
        let cell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0))as!PostCollectionViewCell
        var obj = videos[sender.tag]
        if(videoFinshed && !backButtonTapped){
            videoFinshed = false
        }else if cell.player.timeControlStatus == .playing  {
            obj.isPlayed = true
            isPlaying = false
            obj.player?.pause()
        }else {
            backButtonTapped = false
            cell.isPlayed = true
            obj.isPlayed = false
            
            obj.player?.play()
        }
    }
    
    func rewindVideo(by seconds: Float64,obj:HomeModel,cell:PostCollectionViewCell) {
        let currentTime = cell.player.currentTime()
        var newTime = CMTimeGetSeconds(currentTime) - seconds
        if newTime <= 0 {
            newTime = 0
        }
        cell.player.seek(to: CMTime(value: CMTimeValue(newTime * 1000), timescale: 1000))
    }
    
    func forwardVideo(by seconds: Float64,obj:HomeModel,cell:PostCollectionViewCell) {
        let currentTime = cell.player.currentTime()
        let duration = cell.player.currentItem?.duration
        var newTime = CMTimeGetSeconds(currentTime) + seconds
        if newTime >= CMTimeGetSeconds(duration!) {
            newTime = CMTimeGetSeconds(duration!)
        }
        cell.player.seek(to: CMTime(value: CMTimeValue(newTime * 1000), timescale: 1000))
    }
    
    @objc func multipleTapForward(_ sender: UIButton, event: UIEvent) {
        let cell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0))as!PostCollectionViewCell
        isPlaying = false
        
        let touch: UITouch = event.allTouches!.first!
        let obj = videos[sender.tag]
        if (touch.tapCount == 2) {
            if(!videoFinshed){
                self.forwardVideo(by: 5.0,obj:obj,cell:cell)
            }
            if(cell.player.timeControlStatus == .playing){
                cell.player.play()
            }else{
                cell.player.pause()
            }
            
        }
    }
    
    @objc func multipleTapBackward(_ sender: UIButton, event: UIEvent) {
        let cell = collectionView.cellForItem(at: IndexPath(row: sender.tag, section: 0))as!PostCollectionViewCell
        isPlaying = false
        if(videoFinshed){
            videoFinshed = false
        }
        let obj = videos[sender.tag]
        self.rewindVideo(by: 5.0,obj:obj,cell:cell)
        backButtonTapped = true
        if(cell.player.timeControlStatus == .playing){
            obj.player?.play()
            backButtonTapped = false
        }else{
            obj.player?.pause()
        }
    }
    
    //end video player
    
    
    
    //MARK:- Open Controller Method
    
    func openMessageScreen(cell:PostCollectionViewCell){
        cell.player.pause()
    }
    
    
    func openUserProfileScreen(cell:PostCollectionViewCell){
        cell.player.pause()
        
    }
    
    func openProfileScreen(cell:PostCollectionViewCell){
        
        
    }
    
    
    func openShareScreen(){
       // ProgressHud.showActivityLoader()
        self.downloadFile()

    }
    
    func openReplyScreen() {
        if let vc = UIStoryboard.main.get(ReplyVideoViewController.self){
            vc.videoId = "\(videos.first?.video_id ?? 0)"
            vc.backCompletion = {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.viewHeightConstraint.constant = 90
                    self.bottomView.isHidden = false
                    self.videoFinshed = true
                }
            }
            // vc.replyVideos = videos.first?.reply_videos as! [HomeModel]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openChatScreen() {
        
        guard let otherQbId = videos.first?.creator?.qb_id else {
            return
        }
        
        func sendChatRequest() {
            QBManager.shared.checkForDialog(otherUserId: "\(otherQbId)") { [weak self](success, dialog, isNewDialog) in
                ProgressHud.hideActivityLoader()
                if let self = self, success, let dialog = dialog {
                    
                    if let isResquested = dialog.data?["is_requested"] as? Bool, isResquested, let requesterId = dialog.data?["requester_id"] as? String, requesterId != "\(QBManager.shared.currentUserId)" {
                        AlertController.alert(title: AppName.localized(), message: "You have got chat request from this user. Go to chat screen and accept the request to start the conversation.".localized())
                        return
                    }
                    
                    if let chatVc = UIStoryboard.main.get(MessageViewController.self) {
                        chatVc.chatDialog = dialog
                        chatVc.isPlaying = self.videoFinshed
                        chatVc.backCompletion =  { status in
                            if status {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    self.viewHeightConstraint.constant = 90
                                    self.bottomView.isHidden = false
                                    self.videoFinshed = true
                                }
                            }
                        }
                        self.navigationController?.pushViewController(chatVc, animated: true)
                    }
                    
                    if isNewDialog {
                        let payload = [
                            "message": "\(AuthManager.shared.loggedInUser?.fullname ?? "An user") sent you a request for chat.",
                            "ios_badge": "0",
                            "ios_sound": "default",
                            "ios_content-available": 1,
                            "type": "chat_request",
                            "id": "\(dialog.id ?? "0")",
                            ] as [String : Any]
                        QBManager.shared.sendPush(toIds: [UInt(dialog.recipientID)], payload: payload)
                    }
                    
                } else {
                    AlertController.alert(title: AppName.localized(), message: "Failed to create chat.")
                }
            }
        }
        
        
        ProgressHud.showActivityLoader()
        QBManager.shared.sendContactRequest(qbId: otherQbId) { (status, error) in
            if error == nil {
                sendChatRequest()
            } else {
                ProgressHud.hideActivityLoader()
                Debug.log(error?.localizedDescription ?? "")
                AlertController.alert(title: AppName.localized(), message: "Failed to create chat.")
            }
        }
    }
    
    
    
    
    @IBAction func backButtonAction(_ sender: UIButton){
        for item in videos {
            item.player?.pause()
            item.player?.replaceCurrentItem(with: nil)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func commentButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.main.get(CommentViewController.self) {
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            vc.videoId = videos.first?.video_id
            vc.backCompletion = {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.viewHeightConstraint.constant = 90
                    self.bottomView.isHidden = false
                    self.videoFinshed = true
                }
            }
            navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func watchReplyVideosAction(_ sender: UIButton) {
        
        if (self.videos.first?.reply_video_count ?? "0") == "0" {
            return
        }
        
        if let vc = UIStoryboard.main.get(ReplyVideosViewController.self) {
            vc.videoId = "\(videos.first?.video_id ?? 0)"
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension PostViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCell", for: indexPath) as!  PostCollectionViewCell
        let videoObject = videos[indexPath.row]
               
        cell.voteLabel.attributedText = Int(videoObject.vote_count ?? "0") ?? 0 > 1 ? "\(videoObject.vote_count ?? "0")\n\(StaticStrings.votes.localized())".getAttributedString("\(StaticStrings.votes.localized())", color: .white, font: Fonts.Nunito.regular.font(.small)) : "\(videoObject.vote_count ?? "0")\n\(StaticStrings.vote.localized())".getAttributedString("\(StaticStrings.vote.localized())", color: .white, font: Fonts.Nunito.regular.font(.small))
        
        cell.fanLabel.attributedText = Int(videoObject.creator?.fan_count ?? "0") ?? 0 > 1 ? "\(videoObject.creator?.fan_count ?? "0")\n\(StaticStrings.fans.localized())".getAttributedString("\(StaticStrings.fans.localized())", color: .white, font: Fonts.Nunito.regular.font(.small)) : "\(videoObject.creator?.fan_count ?? "0")\n\(StaticStrings.fan.localized())".getAttributedString("\(StaticStrings.fan.localized())", color: .white, font: Fonts.Nunito.regular.font(.small))
        
        if let url = URL(string: videoObject.video_url ?? "") {
            let playerItem = CachingPlayerItem(url: url)
            playerItem.download()
            videoObject.playerItem = playerItem
            videoObject.player = AVPlayer(playerItem: videoObject.playerItem)
            videoObject.player?.automaticallyWaitsToMinimizeStalling = automaticallyWaitsToMinimizeStalling
            
            if !videoFinshed {
                videos[indexPath.row].player?.play()
            }
        }
        
        self.playerEndSetup(cell: cell)
        cell.playPauseButton.addTarget(self, action: #selector(playPauseButtonAction(_:)), for: .touchUpInside)
        cell.forwardButton.addTarget(self, action: #selector(multipleTapForward(_:event:)), for: UIControl.Event.touchDownRepeat)
        cell.backwardButton.addTarget(self, action: #selector(multipleTapBackward(_:event:)), for: UIControl.Event.touchDownRepeat)
        // self.setUpVideoPlayerViewController(cell:cell)
        cell.backwardButton.tag = indexPath.row
        cell.forwardButton.tag = indexPath.row
        cell.playPauseButton.tag = indexPath.row
        
        cell.setPlayer(object: videoObject)
        
        cell.voteClicked = {
            self.voteUnvote()
        }
        
        cell.shareClicked = {
            self.openShareScreen()
        }
        
        cell.replyClicked = {
            self.openReplyScreen()
        }
        
        cell.viewCompletion = {
            self.viewVideo()
        }
        
        cell.playAgainClicked = {
            
            cell.overlayView.isHidden = true
            self.videos[indexPath.row].player?.seek(to: CMTime.zero)
            self.videos[indexPath.row].player?.play()
            DispatchQueue.main.async {
                self.viewHeightConstraint.constant = 0
                self.bottomView.isHidden = true
                
            }
        }
        
        if self.videos[indexPath.row].creator?.user_id != AuthManager.shared.loggedInUser?.user_id {
            cell.chatButton.isHidden =  self.videos[indexPath.row].creator?.chat_privacy == "0"
        }else {
            cell.chatButton.isHidden = true
        }
        
        cell.chatClicked = {
            self.openChatScreen()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: Window_Width , height: self.collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}


extension PostViewController {
    //MARK:- Switching the Page
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.height
        let currentPage = Int(floor((scrollView.contentOffset.y - pageWidth * 0.5) / pageWidth) + 1)
        print(currentPage)
        
        if self.currentPage == currentPage {
            return
        }
        
        
        if  let cell = collectionView.cellForItem(at: IndexPath(row: currentPage, section: 0)) as? HomeVideoListCVC {
            cell.playPauseButton.setImage(UIImage.init(named: ""), for: .normal)
        }
        
        self.currentPage = currentPage
        for (index, _) in videos.enumerated() {
            if index == currentPage {
                isPlaying = true
                //  collectionView.reloadData()
                videos[index].isPlayed = true
                if (videos[index].player?.rate ?? 1) == 0 {
                    videos[index].player?.play()
                }
                //                challengesArr[index].player?.play()
                videos[index].removeObserver()
                videos[index].addObserver()
            } else {
                isPlaying = false
                videos[index].player?.pause()
                videos[index].isPlayed = false
                videos[index].removeObserver()
                if index < currentPage - 1 && index > currentPage + 1 {
                    videos[index].player?.replaceCurrentItem(with: nil)
                    videos[index].player = nil
                }
            }
        }
    }
}

extension PostViewController {
    func viewVideo(){
        var param = [String: Any]()
        param[kvideo_id] = videos.first?.video_id
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        
        WebServices.increaseViewCount(params: param) { (response) in
            if response?.statusCode == 200 {
                
            }
        }
    }
    
    
    func voteUnvote(){
        
        
        if (videos.first?.vote_status ?? "0") == "0" {
            
            self.voteImage.isHidden = false
            self.voteImage.transform = CGAffineTransform.init(scaleX: 0, y: 0)
            
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                self.voteImage.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }) { _ in
                self.voteImage.isHidden = true
            }
        }
        
        
        var param = [String: Any]()
        param[kvideo_id] = videos.first?.video_id
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param[kvote_status] = ((videos.first?.vote_status ?? "0") == "0") ? 1 : 0
        
        WebServices.vote(params: param) { (response) in
            if response?.statusCode == 200 {
                self.videos.first?.vote_status = (self.videos.first?.vote_status ?? "0") == "0" ? "1" : "0"
                let count = Int(self.videos.first?.vote_count ?? "0") ?? 0
                if self.videos.first?.vote_status == "0" {
                    self.videos.first?.vote_count  = "\(count - 1)"
                }else {
                    self.videos.first?.vote_count  = "\(count + 1)"
                }
                self.voteCompletion?()
                let cell  = self.collectionView.cellForItem(at: IndexPath.init(item: 0, section: 0)) as! PostCollectionViewCell
                cell.voteLabel.attributedText = Int(self.videos.first?.vote_count ?? "0") ?? 0 > 1 ? "\(self.videos.first?.vote_count ?? "0")\nvotes".getAttributedString("votes", color: .white, font: Fonts.Nunito.regular.font(.small)) : "\(self.videos.first?.vote_count ?? "0")\nvote".getAttributedString("vote", color: .white, font: Fonts.Nunito.regular.font(.small))
            }
        }
        
    }
    
    
    func getVideoDetail() {
        
        var param = [String: Any]()
        param[kvideo_id] = videoId
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        WebServices.getVideoDetail(params: param) { (response) in
            if response?.statusCode == 200 {
                if let arr = response?.array {
                    self.videos = arr
                    self.initialSetup()
                    self.customSetup()
                    delay(delay: 0.05) {
                        self.collectionView.reloadData()
                    }
                    delay(delay: 0.1) {
                        self.videos[0].player?.play()
                    }
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    
    
    func updateVideoDetail() {
        
        PostViewController.commented = false
        
        var param = [String: Any]()
        param[kvideo_id] = videoId
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        WebServices.getVideoDetail(params: param, loader: false) { [weak self](response) in
            if response?.statusCode == 200 {
                if let arr = response?.array?.first, let `self` = self {
                    
                    DispatchQueue.main.async {
                        let count = Int(arr.comments_count ?? "0")
                        self.commentLabel.text = count ?? 0 > 1 ? "\(arr.comments_count ?? "0") \(StaticStrings.Comments.localized())" : "\(arr.comments_count ?? "0") \(StaticStrings.Comment.localized())"
                    }

                }
            }
        }
    }
    
}


extension PostViewController {
    
    func save(url: URL) {
        print("Doneeee")

        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
        }) { saved, error in
            if saved {
                ProgressHud.hideActivityLoader()
                let alertController = UIAlertController(title: "Your video was successfully saved", message: nil, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}


extension PostViewController: URLSessionDownloadDelegate {
     func downloadFile() {
    //        let videoImageUrl = "http://13.127.138.63/woweapp/assets/uploads/episodes/video_1019013136.mp4"
          //  let videoImageUrl = "http://13.127.138.63/woweapp/assets/uploads/episodes/video_641114193.mp4"
            let configuration = URLSessionConfiguration.default
            let operationQueue = OperationQueue()
            let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: operationQueue)
        guard let url = URL(string: videos.first?.video_url ?? "") else { return }
            let downloadTask = urlSession.downloadTask(with: url)
            self.downloadTask =  downloadTask
            downloadTask.resume()
        }
        
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
            print("finish downloading")

            let fileManager = FileManager.default
            var url: URL?
            do {
                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                let fileURL = documentDirectory.appendingPathComponent("project_file.mp4")
                let data = try Data(contentsOf: location)
                try? data.write(to: fileURL)
                url = fileURL
            } catch {
                print(error)
            }

            if let url = url {
                DispatchQueue.main.sync {
                    DispatchQueue.main.async {
                        IHProgressHUD.showInfowithStatus("Processing...")
                    }
                    addWaterMarkImage(url: url) { [weak self](url) in
                        if let url = url {
                            IHProgressHUD.dismiss()
                            self?.finalURL = url
                            self?.sharePost(url: url)
                        }
                    }
                }
            }
        }
        
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
            print(totalBytesWritten, totalBytesExpectedToWrite)
            let percentage = Double(Double(totalBytesWritten) / Double(totalBytesExpectedToWrite)) * 100
            DispatchQueue.main.async {
                print("\(Float(percentage/100))")
                IHProgressHUD.show(progress: CGFloat(percentage/100), status: String(format: "\(StaticStrings.downloading.localized())(%d)", Int((percentage/100) * 100)) )

               // self.progressView.progress = Float(percentage/100)
            }
            
            print(percentage)
        }
    
    func sharePost(url: URL) {
        DispatchQueue.main.async {
             let shareAll:Array = [url as Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: [])
            activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.print,
                UIActivity.ActivityType.addToReadingList,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.openInIBooks,
                UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
            ]
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
        
}
