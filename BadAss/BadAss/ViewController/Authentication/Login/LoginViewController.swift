//
//  LoginViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 25/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import AuthenticationServices
import Toast_Swift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var socialButtonStackView: UIStackView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordEyeButton: UIButton!
    
    var user = User()
    var socialUser: SocialUser? {
        didSet {
            hitLoginAPI(isSocial: true)
        }
    }
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        doInitialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(loginButton.bounds, .button)
        loginButton.layer.insertSublayer(layer, at: 0)
        
        
        // Gradient on header image
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
            self.img.layer.sublayers?.removeAll()
            let layerGradient = CAGradientLayer()
            layerGradient.colors = [
                UIColor.black.withAlphaComponent(0).cgColor,
                UIColor.black.withAlphaComponent(0).cgColor,
                UIColor.black.withAlphaComponent(0).cgColor,
                UIColor.black.withAlphaComponent(0).cgColor,
                UIColor.black.withAlphaComponent(0.5).cgColor,
                UIColor.black.cgColor,
                UIColor.black.cgColor]
            layerGradient.frame = self.img.frame
            self.img.layer.addSublayer(layerGradient)
        }
    }
    
    //MARK:- Helper Method
    fileprivate func doInitialSetup() {
        SocialLoginHelper.shared.delegate = self
        
        if #available(iOS 13.0, *) {
            socialButtonStackView.insertArrangedSubview(SocialLoginHelper.shared.getAppleSignInRoundedButton(), at: 0)
        }
        
        emailTextField.placeholder = StaticStrings.enter_email_phone_username.localized()
        passwordTextField.placeholder = StaticStrings.enter_password.localized()
    }
    
    func validateField() -> Bool {
        var isValid = false
        if user.email == nil {
            emailErrorLabel.text = ValidationMessage.blankUsernameEmailAndNumber.localized()
        } else if user.email != nil, user.email!.length == 0 {
            emailErrorLabel.text = ValidationMessage.blankUsernameEmailAndNumber.localized()
        } else if (user.email?.containsNumberOnly() ?? false), user.email!.length < 10 {
            emailErrorLabel.text = ValidationMessage.validPhone.localized()
        } else if user.email!.length < 2 {
            emailErrorLabel.text = ValidationMessage.validUsername_email.localized()
        } else if user.email!.contains("@"), !(user.email?.isEmail ?? false) {
            emailErrorLabel.text = ValidationMessage.validEmail.localized()
        } else if !user.email!.contains("@"), !user.email!.isValidUserName {
            emailErrorLabel.text = ValidationMessage.validUserName.localized()
        } else if user.password == nil {
            passwordErrorLabel.text = ValidationMessage.blankPassword.localized()
        } else if user.password!.length < 8 {
            passwordErrorLabel.text = ValidationMessage.minLengthPassword.localized()
        } else {
            isValid = true
        }
        return isValid
    }
    
    
    fileprivate func navigateToVerifyOTP(userid: String, user: User) {
        if let vc = UIStoryboard.auth.get(VerificationViewController.self) {
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.verificationType = .Login
            vc.userId = userid
            
            vc.email = user.email
            vc.phone = user.phone_number
            vc.phoneCode = user.phoneCode
            
            vc.completion = { [weak self](success) in
                if success {
                    self?.navigateToHome()
                }
            }
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    fileprivate func navigateToHome() {
        DispatchQueue.main.async { [weak self] in
            if let vc = UIStoryboard.tabbar.get(TabbarViewController.self) {
                APPDELEGATE.tabbar = vc
                self?.navigationController?.viewControllers = [vc]
            }
        }
    }
    
    
    fileprivate func navigateToSignup() {
        if let vc = UIStoryboard.auth.get(SignupViewController.self){
            let user = User()
            user.email = socialUser?.email
            user.socialType = socialUser?.socialType.rawValue
            user.socialIdString = socialUser?.socialId
            user.fullname = socialUser?.fullName
            user.image = socialUser?.imageUrl
            vc.user = user
            vc.isSocial = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- UIButton Action Method
    
    @IBAction func didTapOnPasswordEyeButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    
    @IBAction func loginButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        if validateField() {
           hitLoginAPI(isSocial: false)
        }
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        if let vc = UIStoryboard.auth.get(ForgetPasswordViewController.self){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func signupButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        guard let vc = UIStoryboard.auth.get(SignupViewController.self) else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func commonSocialButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        switch sender.tag {
        case 100:
            //Facebook
            SocialLoginHelper.shared.facebookSignIn(controller: self)
            break
        case 101:
            // Google
            SocialLoginHelper.shared.googleUpdatePresentingViewController(controller: self)
            SocialLoginHelper.shared.googleSignIn()
            break
        default:
            break
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag == 0 { // email field
            if str.length > 256 || string == " " {
                return false
            }
        } else if textField.tag == 1 { // password field
            if str.length > 16 || string == " " {
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            user.email = textField.text?.trimWhiteSpace
        default:
            user.password = textField.text?.trimWhiteSpace
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            emailErrorLabel.text = ""
        default:
            passwordErrorLabel.text = ""
        }
    }
    
}



extension LoginViewController: SocialLoginDelegate {
    
    func googleSignInFinished(with user: SocialUser?, error: Error?) {
        if let error = error {
            AlertController.alert(title: "", message: error.localizedDescription)
            return
        }
        self.socialUser = user
    }
    
    func facebookSignInFinished(with user: SocialUser?, error: String?) {
        if let error = error {
            AlertController.alert(title: "", message: error)
            return
        }
        self.socialUser = user
    }
    
    func appleSignInFinished(with user: SocialUser?, error: String?) {
        if let error = error {
            AlertController.alert(title: "", message: error)
            return
        }
        self.socialUser = user
    }
    
}



//MARK:- Web API Methods
extension LoginViewController {
    
    func hitLoginAPI(isSocial: Bool) {
        
        var params = [String: Any]()
        params[kUsername] = isSocial ? (socialUser?.email ?? "") : (user.email ?? "")
        params[kPassword] = isSocial ? "" : (user.password ?? "")
        params[kDeviceToken] = "Dummy Device Token"
        params[kDeviceType] = "ios"
        params[kSocialLogin] = isSocial ? "1" : "0"
        params[kSocialId] = socialUser?.socialId ?? ""
        params[kSocialType] = socialUser?.socialType.rawValue ?? ""

        WebServices.login(params: params) { (response) in
            if let response = response {
                if response.statusCode == 200, let user = response.object {
                    AuthManager.shared.loggedInUser = user
                    DispatchQueue.main.async { [weak self] in
                        self?.navigateToHome()
                    }
                } else if response.statusCode == 201, let data = response.object, let userid = data.user_id {
                    DispatchQueue.main.async { [weak self] in
                        self?.navigateToVerifyOTP(userid: "\(userid)", user: data)
                    }
                } else if response.statusCode == 202 {
                    DispatchQueue.main.async { [weak self] in
                        self?.navigateToSignup()
                    }
                } else {
                    AlertController.alert(title: AppName, message: response.message ?? CommonMessage.something_went_wrong)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }
    
}
