//
//  ChangePasswordViewController.swift
//  BadAss
//
//  Created by DAY on 23/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var oldPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var submitButton: LocalisedButton!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    
    var userId: String?

 // MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(submitButton.bounds, .button)
        submitButton.layer.insertSublayer(layer, at: 0)
    }

    // MARK:- Helper  Method
    func validateField() -> Bool {
        
        var isValid = false
        
        if oldPasswordTextField.text?.length == 0 {
            oldPasswordTextField.errorMessage = ValidationMessage.blankOldPassword.localized()
        } else if oldPasswordTextField.text!.length < 8 {
            oldPasswordTextField.errorMessage = ValidationMessage.minLengthOldPassword.localized()
        } else if passwordTextField.text?.length == 0 {
            passwordTextField.errorMessage = ValidationMessage.blankNewPassword.localized()
        } else if  passwordTextField.text!.length < 8 {
            passwordTextField.errorMessage = ValidationMessage.minLengthNewPassword.localized()
        } else if confirmPasswordTextField.text?.length == 0 {
            confirmPasswordTextField.errorMessage = ValidationMessage.blankConfirmPassword.localized()
        } else if  confirmPasswordTextField.text!.length < 8 {
            confirmPasswordTextField.errorMessage = ValidationMessage.minLengthConfirmPassword.localized()
        } else if  confirmPasswordTextField.text! != passwordTextField.text! {
            confirmPasswordTextField.errorMessage = ValidationMessage.passwordNotMatch.localized()
        } else {
            isValid = true
        }
        
        return isValid
    }
    
    func initialSetup() {
        
        userId = "\(AuthManager.shared.loggedInUser?.user_id ?? 0)"
        
        oldPasswordTextField.errorMessagePlacement = .bottom
        passwordTextField.errorMessagePlacement = .bottom
        confirmPasswordTextField.errorMessagePlacement = .bottom
        
        oldPasswordTextField.placeholder = StaticStrings.old_password.localized()
        passwordTextField.placeholder = StaticStrings.new_password.localized()
        confirmPasswordTextField.placeholder = StaticStrings.confirm_password.localized()

        
    }
    
     // MARK:- UIButton Action Method
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if validateField(){
            callChangePasswordAPI()
        }
    }
    

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
 
    
}

extension ChangePasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        } else {
            
            let nextTag = textField.tag  + 1
            if let nextField = self.view.viewWithTag(nextTag) as? UITextField {
                nextField.becomeFirstResponder()
            }
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if str.length > 16 || string == " " {
            return false
        }
        
        return true
    }
    
}


//MARK:- API Calling Methods

extension ChangePasswordViewController {

    fileprivate func callChangePasswordAPI() {
        
        var params = [String: Any]()
        params[kUserId] = userId
        params[kNewPassword] = confirmPasswordTextField.text?.trimWhiteSpace
        params[kOldPassword] = oldPasswordTextField.text?.trimWhiteSpace
        
        WebServices.change_password(params: params) { (response) in
            if let data = response, data.statusCode == 200 {
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
}
