//
//  ResetPasswordViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 06/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var resetButton: LocalisedButton!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!

    var userId: String?
    
     // MARK:- UIViewController Lifecycle Method
        override func viewDidLoad() {
            super.viewDidLoad()
            initialSetup()
            // Do any additional setup after loading the view.
        }
        
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(resetButton.bounds, .button)
        resetButton.layer.insertSublayer(layer, at: 0)
    }
    
        // MARK:- Helper  Method
        func validateField() -> Bool{
            var isValid = false
            if passwordTextField.text?.length == 0 {
                passwordTextField.errorMessage = ValidationMessage.blankNewPassword.localized()
            } else if  passwordTextField.text!.length < 8 {
                passwordTextField.errorMessage = ValidationMessage.minLengthNewPassword.localized()
            } else if confirmPasswordTextField.text?.length == 0 {
                confirmPasswordTextField.errorMessage = ValidationMessage.blankConfirmPassword.localized()
            } else if  confirmPasswordTextField.text!.length < 8 {
                confirmPasswordTextField.errorMessage = ValidationMessage.minLengthConfirmPassword.localized()
            } else if  confirmPasswordTextField.text! != passwordTextField.text! {
                confirmPasswordTextField.errorMessage = ValidationMessage.passwordNotMatch.localized()
            } else {
                isValid = true
            }
            
            return isValid
        }
        
        func initialSetup(){
            passwordTextField.errorMessagePlacement = .bottom
            confirmPasswordTextField.errorMessagePlacement = .bottom
        }
        
         // MARK:- UIButton Action Method
        @IBAction func sendButtonAction(_ sender: UIButton){
            self.view.endEditing(true)
            if validateField(){
                callResetPasswordAPI()
            }
        }
        
        @IBAction func backButtonAction(_ sender: UIButton){
            self.view.endEditing(true)
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }

    extension ResetPasswordViewController: UITextFieldDelegate {
        func textFieldDidBeginEditing(_ textField: UITextField) {
            if textField == passwordTextField {
                passwordTextField.errorMessage = ""
            }else {
                confirmPasswordTextField.errorMessage = ""
            }
        }
    }

//MARK:- API Calling Methods

extension ResetPasswordViewController {

    fileprivate func callResetPasswordAPI() {
        
        var params = [String: Any]()
        params[kUserId] = userId
        params[kNewPassword] = confirmPasswordTextField.text?.trimWhiteSpace
        
        WebServices.reset_password(params: params) { (response) in
            if let data = response, data.statusCode == 200 {
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
}
