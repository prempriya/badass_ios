//
//  SignupViewController.swift
//  BadAss
//
//  Created by DAY on 08/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fullNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var usernameField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var loginButton: LocalisedButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordViewHeight: NSLayoutConstraint!
    @IBOutlet weak var signupButton: LocalisedButton!
    @IBOutlet weak var passwordEyeButton: UIButton!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var phoneErrorLabel: UILabel!
    @IBOutlet weak var codeButton: UIButton!
    @IBOutlet weak var countryCodeField: SkyFloatingLabelTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    public var user = User()
    public var isSocial: Bool = false
    fileprivate var profileImage: UIImage?
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doInitialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(signupButton.bounds, .button)
        signupButton.layer.insertSublayer(layer, at: 0)
        
        let layer1 = gradientLayer(imageBackgroundView.bounds, .view)
        imageBackgroundView.layer.insertSublayer(layer1, at: 0)
        imageBackgroundView.clipsToBounds = true
        imageContainerView.clipsToBounds = true
        
        let layer2 = gradientLayer(cameraButton.bounds, .buttonBottom)
        cameraButton.layer.insertSublayer(layer2, at: 0)
    }
    
    //MARK:- Helper Methods
    
    fileprivate func doInitialSetup() {
        
        user.phoneCode = "91"
        codeButton.setTitle("+" + (user.phoneCode ?? "91"), for: .normal)
        countryCodeField.text = "+" + (user.phoneCode ?? "91")
        
        setTermsAndConditionsLabel()
        setupTextFields()
        
        if isSocial {
            passwordViewHeight.constant = 0
            passwordView.isHidden = true
            
            emailField.returnKeyType = .done
            
            fullNameField.text = user.fullname
            emailField.text = user.email
            
            if let imgString = user.image, imgString.length > 0, let url = URL(string: imgString) {
                do {
                    let image = try? UIImage(data: Data(contentsOf: url))
                    self.profileImage = image
                    self.profileImageView.image = self.profileImage
                }
            }
        }
    }
    
    fileprivate func setupTextFields() {
        fullNameField.delegate = self
        usernameField.delegate = self
        phoneField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        
        fullNameField.errorMessagePlacement = .bottom
        usernameField.errorMessagePlacement = .bottom
//        phoneField.errorMessagePlacement = .bottom
        emailField.errorMessagePlacement = .bottom
        passwordField.errorMessagePlacement = .bottom
        
        fullNameField.placeholder = StaticStrings.fullnamee.localized()
        usernameField.placeholder = StaticStrings.usernamee.localized()
        phoneField.placeholder = StaticStrings.phonee.localized()
        emailField.placeholder = StaticStrings.emailId.localized()
        countryCodeField.placeholder = StaticStrings.countryCode.localized()
        passwordField.placeholder = StaticStrings.passwordd.localized()
    }
    
    
    fileprivate func setTermsAndConditionsLabel() {
        let str1 = "I agree to the ".localized()
        let str2 = "terms & conditions".localized()
        let str3 = " " + "and".localized() + " "
        let str4 = "privacy policy".localized()

        let termsOfServiceString    = str1 + str2 + str3 + str4
        let attributedString        = NSMutableAttributedString(string: termsOfServiceString)
        let textRange               = (termsOfServiceString as NSString).range(of: str1)
        let termsRange              = (termsOfServiceString as NSString).range(of: str2)
        let andRange                = (termsOfServiceString as NSString).range(of: str3)
        let ppRange                 = (termsOfServiceString as NSString).range(of: str4)

        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.white as Any], range: textRange)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "ORANGE") as Any], range: termsRange)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.white as Any], range: andRange)
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "ORANGE") as Any], range: ppRange)
        termsLabel.numberOfLines    = 0
        termsLabel.attributedText   = attributedString
        let tapGestureLogin         = UITapGestureRecognizer(target: self, action: #selector(tappedOnTermsLabel))
        tapGestureLogin.numberOfTapsRequired    = 1
        tapGestureLogin.numberOfTouchesRequired = 1
        termsLabel.addGestureRecognizer(tapGestureLogin)
        termsLabel.isUserInteractionEnabled = true
    }
    
    @objc func tappedOnTermsLabel(gesture: UITapGestureRecognizer) {
        let text = (termsLabel.text)!
        let termsRange = (text as NSString).range(of: "terms & conditions".localized())
        let ppRange = (text as NSString).range(of: "privacy policy".localized())
        
        if gesture.didTapAttributedTextInLabel(label: termsLabel, inRange: termsRange){
            if let vc = UIStoryboard.settings.get(WebViewController.self) {
                vc.type = .TermsOfService
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if gesture.didTapAttributedTextInLabel(label: termsLabel, inRange: ppRange){
            if let vc = UIStoryboard.settings.get(WebViewController.self) {
                vc.type = .PrivacyPolicy
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    fileprivate func navigateToVerifyOTP(userid: String) {
        if let vc = UIStoryboard.auth.get(VerificationViewController.self) {
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.verificationType = .Signup
            vc.userId = userid
            
            vc.phoneCode = "91"
            vc.email = user.email
            vc.phone = user.phone_number
            
            vc.completion = { [weak self](success) in
                if success {
                    self?.navigateToHome()
                }
            }
            present(vc, animated: true, completion: nil)
        }
    }
    
    func openCountryPicker() {
        let storyboard = UIStoryboard(name: "Country", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CountryPickerViewController") as! CountryPickerViewController
        controller.completion = { [weak self] dict in
            let code = dict["dial_code"] ?? ""
            self?.codeButton.setTitle(code, for: .normal)
            self?.user.phoneCode = code.replacingOccurrences(of: "+", with: "")
            self?.countryCodeField.text = code
        }
        controller.modalPresentationStyle = .custom
        self.present(controller, animated: true, completion: nil)
    }
    
    
    fileprivate func navigateToHome() {
        DispatchQueue.main.async { [weak self] in
            if let vc = UIStoryboard.tabbar.get(TabbarViewController.self) {
                APPDELEGATE.tabbar = vc
                self?.navigationController?.viewControllers = [vc]
            }
        }
    }
    
    fileprivate func validateFields() -> Bool {
                
        if user.fullname == nil {
            fullNameField.errorMessage = ValidationMessage.blankFullName.localized()
            scrollView.contentOffset = fullNameField.frame.origin
            return false
        } else if user.fullname!.length == 0 {
            fullNameField.errorMessage = ValidationMessage.blankFullName.localized()
            scrollView.contentOffset = fullNameField.frame.origin
            return false
        } else if user.fullname!.length < 2 {
            fullNameField.errorMessage = ValidationMessage.invalidFullName.localized()
            scrollView.contentOffset = fullNameField.frame.origin
            return false
        }
        
        else if user.username == nil {
            usernameField.errorMessage = ValidationMessage.blankUsername.localized()
            scrollView.contentOffset = usernameField.frame.origin
            return false
        } else if user.username!.length == 0 {
            usernameField.errorMessage = ValidationMessage.blankUsername.localized()
            scrollView.contentOffset = usernameField.frame.origin
            return false
        } else if user.username!.length < 2 {
            usernameField.errorMessage = ValidationMessage.minLengthUsername.localized()
            scrollView.contentOffset = usernameField.frame.origin
            return false
        } else if !user.username!.isValidUserName {
            usernameField.errorMessage = ValidationMessage.invalidUserName.localized()
            scrollView.contentOffset = usernameField.frame.origin
            return false
        }
        
            
        else if user.phone_number == nil {
            phoneErrorLabel.text = ValidationMessage.blankPhoneNumber.localized()
            return false
        } else if user.phone_number!.length == 0 {
            phoneErrorLabel.text = ValidationMessage.blankPhoneNumber.localized()
            return false
        } else if user.phone_number!.length < 10 {
            phoneErrorLabel.text = ValidationMessage.validPhone.localized()
            return false
        } else if user.phone_number!.isContainsAllZeros() {
            phoneErrorLabel.text = ValidationMessage.validPhone.localized()
            return false
        }
            
        else if user.email == nil {
            emailField.errorMessage = ValidationMessage.blankEmail.localized()
            return false
        } else if user.email!.length == 0 {
            emailField.errorMessage = ValidationMessage.blankEmail.localized()
            return false
        } else if user.email!.length < 2 {
            emailField.errorMessage = ValidationMessage.validEmail.localized()
            return false
        } else if !user.email!.isEmail {
            emailField.errorMessage = ValidationMessage.validEmail.localized()
            return false
        }
        
        if isSocial == false {
            if user.password == nil {
                passwordField.errorMessage = ValidationMessage.blankPassword.localized()
                return false
            } else if user.password!.length == 0 {
                passwordField.errorMessage = ValidationMessage.blankPassword.localized()
                return false
            } else if user.password!.length < 8 {
                passwordField.errorMessage = ValidationMessage.minLengthPassword.localized()
                return false
            }
        }
        
        if !termsButton.isSelected {
            AlertController.alert(title: "", message: "Please accept Terms and Conditions.")
            return false
        }
        
        return true
    }

    //MARK:- UIButton Action
    
    @IBAction func didTapOnSingupButton(_ sender: UIButton) {
        view.endEditing(true)
        if validateFields() {
            registerUser()
        }
    }
    
    @IBAction func didTapOnTermsButton(_ sender: UIButton) {
        view.endEditing(true)
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func didTapOnCameraButton(_ sender: Any) {
        view.endEditing(true)
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self, message: AttachmentHandler.Constants.updatePhotoMessage)
        AttachmentHandler.shared.imagePickedBlock = { [weak self](image) in
            self?.profileImage = image
            self?.profileImageView.image = image
        }
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapOnPasswordEyeButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordField.isSecureTextEntry = !passwordField.isSecureTextEntry
    }

    @IBAction func didTapOnPhoneCodeButton(_ sender: UIButton){
        view.endEditing(true)
        openCountryPicker()
    }
    
}

//MARK:- UITextField Delegate Methods

extension SignupViewController: UITextFieldDelegate {

    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            user.fullname = textField.text?.trimWhiteSpace
            break
        case 1:
            user.username = textField.text?.trimWhiteSpace
            break
        case 2:
            user.phone_number = textField.text?.trimWhiteSpace
//            if user.phone_number?.count == 0 {
//                self.phoneField.placeholder = "Phone *"
//            }
            break
        case 3:
            user.email = textField.text?.trimWhiteSpace
            break
        case 4:
            user.password = textField.text?.trimWhiteSpace
            break
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 2 {
//            self.phoneField.placeholder = ""
            phoneErrorLabel.text = ""
        }
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
         case 101:
             textField.resignFirstResponder()
             self.view.endEditing(true)
             openCountryPicker()
             return false
         default:
             return true
         }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag == 0 { // name field
            if str.length > 64  {
                return false
            }
        } else if textField.tag == 1 { // username field
            if str.length > 32 || string == " " {
                return false
            }
        } else if textField.tag == 2 { // phone field
            if str.length > 10 || string == " " {
                return false
            }
        } else if textField.tag == 3 { // email field
            if str.length > 256 || string == " " {
                return false
            }
        } else if textField.tag == 4 { // password field
            if str.length > 16 || string == " " {
                return false
            }
        }
        return true
    }
    
}


//MARK:- Web API Calling Methods

extension SignupViewController {
        
    fileprivate func registerUser() {
        
        var params = [String: Any]()
        params[kName] = user.fullname
        params[kUsername] = user.username
        params[kEmail] = user.email
        params[kPassword] = user.password ?? ""
        params[kPhoneNumber] = user.phone_number
        params[kPhoneCode] = user.phoneCode
        params[kDeviceToken] = "Dummy Device Token"
        params[kDeviceType] = "ios"
        params[kSocialSignup] = isSocial ? "1" : "0"
        params[kSocialId] = user.socialIdString ?? ""
        params[kSocialType] = user.socialType

        WebServices.registration(params: params) { (response) in
            if let data = response?.object, let userid = data.user_id {
                DispatchQueue.main.async { [weak self] in
                    self?.navigateToVerifyOTP(userid: "\(userid)")
                    self?.uploadProfilePicture(userId: userid)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    
    fileprivate func uploadProfilePicture(userId: Int) {
        
        let image = self.profileImage ?? UIImage(named: "user") ?? UIImage()
        
        var params = [String: Any]()
        params[kUserId] = "\(userId)"
        let attach = AttachmentInfo(withImage: image, imageName: "\(Date().timeIntervalSince1970).jpg", apiName: "profile_picture")
        WebServices.uploadImage(params: params, files: [attach]) { (response) in
            
        }
    }
    
}
