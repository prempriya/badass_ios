//
//  ForgetPasswordViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 05/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var sendButton: LocalisedButton!
    
    // MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(sendButton.bounds, .button)
        sendButton.layer.insertSublayer(layer, at: 0)
    }
    
    
    // MARK:- Helper  Method
    func validateField() -> Bool{
        
        let email = emailTextField.text ?? ""
        
        var isValid = false
        if email.length == 0 {
            emailTextField.errorMessage = ValidationMessage.blankEmailAndNumber.localized()
        } else if email.containsNumberOnly(), email.length < 10 {
            emailTextField.errorMessage = ValidationMessage.validPhone.localized()
        } else if !email.containsNumberOnly(), email.length < 2 {
            emailTextField.errorMessage = ValidationMessage.validEmail.localized()
        } else if !email.containsNumberOnly(), !email.isEmail {
             emailTextField.errorMessage = ValidationMessage.validEmail.localized()
        }else {
            isValid = true
        }
        
        return isValid
    }
    
    func initialSetup(){
        emailTextField.placeholder = StaticStrings.emailid_phone.localized()
        emailTextField.errorMessagePlacement = .bottom
    }
    
    
    fileprivate func navigateToVerifyOTP(userid: String, user: User) {
        if let vc = UIStoryboard.auth.get(VerificationViewController.self) {
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.verificationType = .ForgotPassword
            vc.userId = userid
            
            vc.email = user.email
            vc.phone = user.phone_number
            vc.phoneCode = user.phoneCode
            
            vc.completion = { [weak self](success) in
                if success {
                    self?.navigateToResetPasswordScreen(userId: userid)
                }
            }
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    fileprivate func navigateToResetPasswordScreen(userId: String) {
        
        guard let vc = UIStoryboard.auth.get(ResetPasswordViewController.self) else {
            return
        }
        
        vc.userId = userId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
     // MARK:- UIButton Action Method
    @IBAction func sendButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        if validateField(){
            callForgotPasswordAPI()
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension ForgetPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        emailTextField.errorMessage = ""
    }
}


//MARK:- Web API Methods

extension ForgetPasswordViewController {

    fileprivate func callForgotPasswordAPI() {
        WebServices.forget_password(params: [kEmail: emailTextField.text?.trimWhiteSpace ?? ""]) { (response) in
            
            if let data = response?.object {
                DispatchQueue.main.async { [weak self] in
                    self?.navigateToVerifyOTP(userid: "\(data.user_id ?? 0)", user: data)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
            
        }
    }
    
}
