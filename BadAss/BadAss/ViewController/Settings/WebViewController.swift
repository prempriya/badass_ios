//
//  WebViewController.swift
//  BadAss
//
//  Created by DAY on 28/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var titleLabel: LocalisedLabel!
    @IBOutlet weak var backButton: UIButton!
    
    enum WebPage: Int {
        case TermsOfService = 0
        case PrivacyPolicy
        case AboutUs
        case Help
        
        
        func title() -> String {
            switch self {
            case .TermsOfService:
                return StaticStrings.terms_of_service.localized()
            case .PrivacyPolicy:
                return StaticStrings.privacy_policy.localized()
            case .Help:
                return StaticStrings.help.localized()
            case .AboutUs:
                return StaticStrings.aboout_us.localized()
            }
        }
        
        func urlString() -> String {
            switch self {
            case .TermsOfService:
                return "https://www.badasscommunity.com/pages/terms_conditions.html"
            case .PrivacyPolicy:
                return "https://www.badasscommunity.com/pages/privacy_policy.html"
            case .AboutUs:
                return "https://badasscommunity.com"
            case .Help:
                return "https://badasscommunity.com"
            }
        }
        
    }
    
    
    var type: WebPage = .TermsOfService
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = type.title()
        loadURL()
    }
    
    //MARK:- Helper Methods
    
    fileprivate func loadURL() {
        guard let url = URL(string: type.urlString()) else {
            return
        }
        webView.load(URLRequest(url: url))
    }
    
    
    //MARK:- UIButton Action
    @IBAction func didTapOnBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

}
