//
//  SelectLanguageViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 01/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class SelectLanguageViewController: UIViewController {
    
    @IBOutlet weak var roundView: CurveView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var selectLanguageLabel: UILabel!
    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    enum PresentedFrom: Int {
        case Home = 0
        case EditProfile
    }
    
    var presentedFrom: PresentedFrom = .EditProfile
    var languages: [Language] = languagesArray ?? []
    var preselectedLanguages: [Language] = []
    var selectedLanguages: (([Language]?) -> Void)?
    
    
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(showButton.bounds, .halfButton)
        showButton.layer.insertSublayer(layer, at: 0)
        let layer1 = gradientLayer(circularView.bounds, .view)
        circularView.layer.insertSublayer(layer1, at: 0)
    }
    
    //MARK:- Helper Method
    
    func initialSetup(){
        roundView.transform = view.transform.rotated(by: .pi)
        selectLanguageLabel.attributedText = StaticStrings.selectLanguage.localized().getAttributedString(StaticStrings.languages.localized(), color: kAppRedColor, font: Fonts.Nunito.bold.font(.xxxLarge))
        bottomConstraint.constant = Window_Width > 375 ? 0 : -30
        
        switch presentedFrom {
        case .Home:
            showButton.setTitle("Show Videos", for: .normal)
            break
        case .EditProfile:
            showButton.setTitle("Select Languages", for: .normal)
            break
        }
        
        let selectedLanguagesId = preselectedLanguages.map({$0.id})
        
        languages.forEach { (language) in
            if selectedLanguagesId.contains(language.id) {
                language.isSelected = true
            }
        }
        
    }
    
    //MARK:- UIButton Action  Method
    @IBAction func showButtonAction(_ sender: UIButton){
        selectedLanguages?(languages.filter({$0.isSelected}))
        dismiss(animated: true, completion: nil)
    }
    
}


extension SelectLanguageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return languages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LanguageCollectionViewCell", for: indexPath) as! LanguageCollectionViewCell
        cell.seperatorView.isHidden = indexPath.row % 2 != 0
        let obj = languages[indexPath.row]
        cell.languageLabel.text = obj.language
        cell.tickImageView.image = UIImage(named: obj.isSelected ? "ic_tick_mark" : "ic_tick_blank")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (self.collectionView.frame.width / 2), height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = languages[indexPath.row]
        obj.isSelected = !obj.isSelected
        self.collectionView.reloadData()
    }
    
}


class LanguageModel: NSObject {
    var name: String?
    var status: Bool?
    
    init(name: String, status: Bool) {
        self.name = name
        self.status = status
    }
}
