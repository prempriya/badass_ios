//
//  BlockedUserViewController.swift
//  BadAss
//
//  Created by Prempriya on 07/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class BlockedUserViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navView: UIView!
    
    var blockedUsers: [FollowedUser] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        getBlockList()
        //        self.navView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        //        self.navView.layer.cornerRadius = 15
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: UIButton){
        navigationController?.popViewController(animated: true)
    }
}

extension BlockedUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockedUserTableViewCell") as! BlockedUserTableViewCell
        cell.loadUser(user: blockedUsers[indexPath.row])
        cell.unblockClicked = {
            self.unBlockUser(user: self.blockedUsers[indexPath.row], index: indexPath.row)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}


extension BlockedUserViewController {
    fileprivate func getBlockList (_ hud: Bool = true) {
        
        var params = [String: Any]()
        params[kUserId] = AuthManager.shared.loggedInUser?.user_id
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        WebServices.getBlockList(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let array = response?.array, let self = self {
                self.blockedUsers = array
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }
    
    fileprivate func unBlockUser (_ hud: Bool = true, user: FollowedUser,index: Int) {
        
        var params = [String: Any]()
        params[kUserId] = AuthManager.shared.loggedInUser?.user_id
        params[kBlock_user_id] = user.user_id
        params[kBlock_status] = "0"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        WebServices.blockUnblock(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if response?.statusCode == 200 {
                QBManager.shared.unblock(qbId: user.qb_id ?? "") { (s) in
                    
                }
                self?.blockedUsers.remove(at: index)
                self?.tableView.reloadData()
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
}
