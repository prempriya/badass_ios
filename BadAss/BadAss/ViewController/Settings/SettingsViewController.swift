//
//  SettingsViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 15/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var titleArray = [
        StaticStrings.about.localized(),
        StaticStrings.help.localized(),
        StaticStrings.privacy.localized(),
        StaticStrings.change_password.localized(),
        StaticStrings.blocked_user.localized()
    ]
    
    var imageArray = [
        "ic_about_settings",
        "ic_help_settings",
        "ic_privacy_settings",
        "ic_changepassword_settings",
        "ic_block_user"
    ]
    
    var isPrivacySelected = false
    var privacyObj = SettingChatPrivacy()
    
    var userSetting: UserSetting = UserSetting() {
        didSet{
            self.tableView.reloadData()
        }
    }
    
    var isSocialUser = (AuthManager.shared.loggedInUser?.socialId?.length ?? 0) > 0
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        getSettings()
    }
    
    //MARK:- Target Method
    
    @objc func commonButtonAction(_ sender: UIButton){
        switch sender.tag {
        case 0:
            if let vc = UIStoryboard.settings.get(WebViewController.self) {
                vc.type = .AboutUs
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 1:
            if let vc = UIStoryboard.settings.get(WebViewController.self) {
                vc.type = .Help
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 2:
            isPrivacySelected = !isPrivacySelected
            break
        case 3:
            if let vc = UIStoryboard.auth.get(ChangePasswordViewController.self) {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 4:
            if let vc = UIStoryboard.settings.get(BlockedUserViewController.self) {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 100:
            userSetting.chatPrivacy = "1"
        case 101:
            userSetting.chatPrivacy = "0"
        case 102:
            userSetting.showMyProfile = "1"
        case 103:
            userSetting.showMyProfile = "0"
        default:
            break
        }
        tableView.reloadData()
        if sender.tag >= 100 && sender.tag <= 103 {
            setSettings()
        }
    }
    
    //MARK:- UIButton Method
    @IBAction func backButtonAction(_  sender: UIButton){
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoutButtonAction(_  sender: UIButton){
        AlertController.alert(title: "", message: StaticStrings.logout_alert.localized(), buttons: [StaticStrings.no.localized(), StaticStrings.yes.localized()]) { (action, code) in
            if code == 1 {
                AuthManager.shared.logout()
            }
        }
    }
}

extension SettingsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titleArray.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 2 ? isPrivacySelected ? 2 : 0 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 3, isSocialUser {
            return nil
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.titleLabel.text = titleArray[section]
        cell.commonButton.tag = section
        cell.commonButton.addTarget(self, action: #selector(commonButtonAction(_ :)), for: .touchUpInside)
        cell.seperatorView.isHidden = section == 2 ? isPrivacySelected : false
        cell.commonImageView.image = UIImage(named: imageArray[section])
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatPrivacyTableViewCell") as!
        ChatPrivacyTableViewCell
        switch indexPath.row {
        case 0:
            
            cell.headingLabel.text = StaticStrings.chat_privacy.localized()
            cell.noOneButton.tag = 101
            cell.anyOneButton.tag = 100
            
            if (userSetting.chatPrivacy ?? "0") == "1" || (userSetting.chatPrivacy ?? "1") == "" {
                cell.anyOneButton.isSelected = true
                cell.noOneButton.isSelected = false
            } else {
                cell.anyOneButton.isSelected = false
                cell.noOneButton.isSelected = true
            }

            cell.anyOneButton.addTarget(self, action: #selector(commonButtonAction(_ :)), for: .touchUpInside)
            cell.noOneButton.addTarget(self, action: #selector(commonButtonAction(_ :)), for: .touchUpInside)
            cell.anyOneButton.setTitle(StaticStrings.anyone_can_send.localized(), for: .normal)
            cell.noOneButton.setTitle(StaticStrings.no_one_send.localized(), for: .normal)
            cell.anyOneButton.setTitle(StaticStrings.anyone_can_send.localized(), for: .selected)
            cell.noOneButton.setTitle(StaticStrings.no_one_send.localized(), for: .selected)
            cell.seperatorView.isHidden = true
            return cell
        default:
            cell.headingLabel.text = StaticStrings.profile_privacy.localized()
            cell.noOneButton.tag = 103
            cell.anyOneButton.tag = 102
            
            if (userSetting.showMyProfile ?? "0") == "1" || (userSetting.showMyProfile ?? "1") == "" {
                cell.anyOneButton.isSelected = true
                cell.noOneButton.isSelected = false
            } else {
                cell.anyOneButton.isSelected = false
                cell.noOneButton.isSelected = true
            }
            
            cell.anyOneButton.setTitle(StaticStrings.show_my_profile.localized(), for: .normal)
            cell.noOneButton.setTitle(StaticStrings.hide_my_profile.localized(), for: .normal)
            cell.anyOneButton.setTitle(StaticStrings.show_my_profile.localized(), for: .selected)
            cell.noOneButton.setTitle(StaticStrings.hide_my_profile.localized(), for: .selected)
            cell.anyOneButton.addTarget(self, action: #selector(commonButtonAction(_ :)), for: .touchUpInside)
            cell.noOneButton.addTarget(self, action: #selector(commonButtonAction(_ :)), for: .touchUpInside)
            cell.seperatorView.isHidden = false
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3, isSocialUser {
            return 0
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 125 : 125
    }
    
}

class SettingChatPrivacy: NSObject {
    var chatNoOneSelected: Bool?
    var chatAnyoneSelected: Bool?
    var profileShow: Bool?
    var profileHide: Bool?
}


//MARK:- Web API Calling Methods
extension SettingsViewController {
    
    fileprivate func getSettings() {
        WebServices.getUserSettings { [weak self](response) in
            if let object = response?.object {
                self?.userSetting = object
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong.localized())
            }
        }
    }
    
    fileprivate func setSettings() {
        
        guard let userId = AuthManager.shared.loggedInUser?.user_id else {
            return
        }
        
        var params = [String: Any]()
        params[kUserId] = userId
        params[kProfilePrivacy] = (userSetting.showMyProfile == "0") ? "no" : "yes"
        params[kChatPrivacy] = (userSetting.chatPrivacy == "0") ? "no" : "yes"
        
        WebServices.updateUserSettings(params: params) { [weak self](response) in
            if let object = response?.object {
                self?.userSetting = object
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong.localized())
            }
        }
        
    }
    
}
