//
//  VerificationViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 25/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {
    
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var textFirst: UITextField!
    @IBOutlet weak var textSecond: UITextField!
    @IBOutlet weak var textThird: UITextField!
    @IBOutlet weak var textFourth: UITextField!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    enum VerificationType: String {
        case Signup = "signup"
        case Login = "login"
        case ForgotPassword = "forget_password"
    }
    
    var verificationType: VerificationType = .Signup
    var userId: String?
    var email: String?
    var phone: String?
    var phoneCode: String?
    
    var otpStr = ""
    
    var completion: ((Bool) -> Void)?
    
    // MARK: - UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dimissController))
        bgView.addGestureRecognizer(gesture)
        
        sendOTP()
        setupHeaderLabel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(verifyButton.bounds, .button)
        verifyButton.layer.insertSublayer(layer, at: 0)
        
        let layer1 = gradientLayer(circularView.bounds, .view)
        circularView.layer.insertSublayer(layer1, at: 0)
    }
    
    
    //MARK:- Helper Methods
    
    @objc func dimissController() {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func setupHeaderLabel() {
        
        switch verificationType {
        case .Login, .Signup:
            
            let email = self.email ?? ""
            var convertedPhoneNumber = self.phone ?? ""
            if convertedPhoneNumber.count > 7 {
                let fisrtTwoDigits = String(convertedPhoneNumber.prefix(2))
                let lastTwoDigits = String(convertedPhoneNumber.suffix(2))
                convertedPhoneNumber = fisrtTwoDigits + "XXXXXX" + lastTwoDigits
            }
            let phone = "+" + (self.phoneCode ?? "91") + " " + convertedPhoneNumber
            let string = "We have sent you an access code on \(phone) or registered \(email)"
            
            let attributedString = NSMutableAttributedString(string: string)
            
            let numberRange = (string as NSString).range(of: phone)
            let emailRange = (string as NSString).range(of: email)

            let font = Fonts.Nunito.medium.font(Fonts.FontSize.large)
            attributedString.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black], range: numberRange)
            attributedString.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black], range: emailRange)
            
            headerLabel.attributedText = attributedString
            
            break
        case .ForgotPassword:
            
            let email = self.email ?? ""
  
            let string = "We have sent you an access code on \(email) you entered"
            
            let attributedString = NSMutableAttributedString(string: string)
            
            let emailRange = (string as NSString).range(of: email)
            let font = Fonts.Nunito.medium.font(Fonts.FontSize.large)
            attributedString.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black], range: emailRange)
            headerLabel.attributedText = attributedString
            
            break
        }
        
        
    }
    
    
    
    // MARK: - UIButton Action Method
    @IBAction func verifyButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        otpStr = textFirst.text! + textSecond.text! + textThird.text! + textFourth.text!
        if otpStr.length == 0  {
         AlertController.alert(title: "", message: "Please enter otp.")
        }else if otpStr.trimWhiteSpace.length < 4{
          AlertController.alert(title: "", message: "Please enter valid otp.")
        } else {
            verifyOTP()
        }
    }
    
    @IBAction func resendButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        textFirst.text = ""
        textSecond.text = ""
        textThird.text = ""
        textFourth.text = ""
        otpStr = ""
        //Call Api
        sendOTP()
    }

}

// MARK: - Delegates
extension VerificationViewController: UITextFieldDelegate {
    //MARK: - UITextFieldDelegateMethods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.count)! < 1, string.count > 0 {
            let nextTag = textField.tag + 1
            
            // get next responder
            var nextResponder =  self.view.viewWithTag(nextTag) as? UITextField
            
            if nextResponder == nil {
                nextResponder =  self.view.viewWithTag(1001) as? UITextField
            }
            textField.text = string
            nextResponder?.becomeFirstResponder()
            return false
            
        } else if (textField.text?.count)! >= 1, string.count == 0 {
            // on deleting value from Textfield
            let previousTag = textField.tag - 1
            
            // get next responder
            var previousResponder = self.view.viewWithTag(previousTag) as? UITextField
            
            if previousResponder == nil {
                previousResponder = self.view.viewWithTag(1001) as? UITextField
            }
            textField.text = " "
            previousResponder?.becomeFirstResponder()
            return false
            
        } else if (textField.text?.length)! == 1 {
            let nextTag = textField.tag + 1
            textField.text = " "
            // get next responder
            let nextResponder = self.view.viewWithTag(nextTag) as? UITextField
            textField.text = string
            nextResponder?.becomeFirstResponder()
            return false
        }
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        otpStr = textFirst.text! + textSecond.text! + textThird.text! + textFourth.text!
    }
    
}


//MARK:- Web API Calling Methods

extension VerificationViewController {
    
    fileprivate func sendOTP() {
        WebServices.sendOTP(params: [kUserId: self.userId ?? "", kType: self.verificationType.rawValue]) { (response) in
            if let code = response?.object?.otp {
                self.view.makeToast("Verification Code for testing: \(code)")
            }
        }
    }
    
    fileprivate func verifyOTP() {
        var params = [String: Any]()
        params[kUserId] = self.userId
        params[kType] = verificationType.rawValue
        params[kCode] = otpStr
        WebServices.verifyOTP(params: params) { [weak self](response) in
            guard let self = self else { return }
            if let data = response?.object {
                DispatchQueue.main.async {
                    switch self.verificationType {
                    case .Login:
                        AuthManager.shared.loggedInUser = data
                        self.completion?(true)
                        self.dismiss(animated: true, completion: nil)
                        break
                    case .ForgotPassword:
                        self.completion?(true)
                        self.dismiss(animated: true, completion: nil)
                        break
                    case .Signup:
                        AuthManager.shared.loggedInUser = data
                        self.completion?(true)
                        self.dismiss(animated: true, completion: nil)
                        break
                    }
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
}
