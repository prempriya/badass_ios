//
//  NotificationViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 27/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navView: UIView!
    
    var page: Page = Page()
    
    var notifications: [NotificationModel] = [] {
          didSet {
              DispatchQueue.main.async { [weak self] in
                  self?.tableView.reloadData()
              }
          }
      }

    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.navView.layer.cornerRadius = 15
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getNotification(true)
    }
    
    
}

extension NotificationViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        cell.loadData(obj: notifications[indexPath.row])
        cell.followClicked = {
            self.followUser(userId: self.notifications[indexPath.row].sender_user_id ?? "", indexPath: indexPath)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = notifications[indexPath.row]
        
        switch obj.type {
        case NotificationType.FOLLOW_TYPE:
            if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
                vc.userId = "\(notifications[indexPath.row].sender_user_id ?? "0")"
                navigationController?.pushViewController(vc, animated: true)
            }
        case NotificationType.COMMENT_TYPE,NotificationType.COMMENT_REPLY_TYPE:
            if let vc = UIStoryboard.main.get(CommentViewController.self) {
                vc.videoId = notifications[indexPath.row].id
                vc.isFromNotification = true
                navigationController?.pushViewController(vc, animated: true)
            }
        case NotificationType.VIDEO_REPLY_TYPE:
            if let vc = UIStoryboard.main.get(PostViewController.self){
                vc.videoId = notifications[indexPath.row].id ?? 0
                vc.isFromNotification = true
                //vc.userId = "\(notifications[indexPath.row].user_id ?? "0")"
                navigationController?.pushViewController(vc, animated: true)
            }
        case NotificationType.VIDEO_UPLOAD:
            if let vc = UIStoryboard.main.get(PostViewController.self){
                vc.videoId = notifications[indexPath.row].id ?? 0
                vc.isFromNotification = true
                //vc.userId = "\(notifications[indexPath.row].user_id ?? "0")"
                navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
               -> UISwipeActionsConfiguration? {
               let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                   // delete the item here
                  // completionHandler(true)
                self.deleteNotification(index: indexPath.row, obj:self.notifications[indexPath.row])
               }
               deleteAction.image = UIImage.init(named: "ic_chat_dustbin")
                   deleteAction.backgroundColor = RGBA(56, g: 56, b: 56, a: 1.0)
               let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
               return configuration
       }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           
               
               let currentOffset = scrollView.contentOffset.y
               let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
               
               if page.isExecuting {
                   return
               }
               
               if currentOffset - maximumOffset >= 15.0 {
                   if self.page.hasNext {
                       self.page.page = self.page.page + 1
                       self.getNotification(false)
                   }
               }
       }
       
}

extension NotificationViewController {
    func getNotification(_ hud: Bool = true){
        var param = [String: Any]()
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id

        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        
        WebServices.getNotification(params: param) { (response) in
            self.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            
            if let array = response?.array , response?.statusCode == 200{
                
                if self.page.page == 1 {
                    self.notifications.removeAll()
                }
                
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                
                self.notifications.append(contentsOf: array)
                
                DispatchQueue.main.async {
                    AppDelegate.shared.tabbar?.tabBar.items?.second()?.badgeValue = nil
                }
                
            } else if response?.statusCode == 200 {
                
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    fileprivate func followUser(userId: String, indexPath: IndexPath) {
           var params = [String: Any]()
           params[kStatus] = "1"
           params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
           params[kFollowingId] = userId
           
           ProgressHud.showActivityLoader()
           WebServices.followUser(params: params) { [weak self](response) in
               ProgressHud.hideActivityLoader()
               if let response = response, response.statusCode == 200, let self = self {
                self.notifications[indexPath.row].follow_status = "1"
                self.tableView.reloadData()
               } else {
                   AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
               }
           }
       }
    
    func  deleteNotification(index: Int, obj: NotificationModel){
        var params = [String: Any]()
        params["id"] = obj.notification_id
        
        WebServices.deleteNotification(params: params) { (response) in
            if response?.statusCode == 200 {
                self.notifications.remove(at: index)
                self.tableView.reloadData()
            }
        }
    }
}
