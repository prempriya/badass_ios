//
//  UserQuestionsPopover.swift
//  BadAss
//
//  Created by Dharmendra Sinha on 16/11/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class UserQuestionsPopover: UIViewController {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var useLabel: UILabel!
    @IBOutlet weak var questionsLAbel: UILabel!
    
    var questionsCount = 0
    var name = ""
    var userId = ""
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setDetailLabel(count: questionsCount)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer = gradientLayer(submitButton.bounds, .button)
        submitButton.layer.insertSublayer(layer, at: 0)
    }
    
    fileprivate func setDetailLabel(count: Int) {
        detailLabel.text = name + " has ".localized() + "\(questionsCount)" + " available questions.".localized()
    }
    
    
    //MARK:- UIButton Actions
    
    @IBAction func didTapOnClose(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        if let enteredCount = Int(txtField.text?.trimWhiteSpace ?? "0"), enteredCount > questionsCount {
            AlertController.alert(title: AppName.localized(), message: "Please enter the value less than or equal to available questions.".localized())
            return
        }
        ProgressHud.showActivityLoader()
        WebServices.updateQuestionsCount(count: txtField.text?.trimWhiteSpace ?? "0", userId: userId) { (response) in
            ProgressHud.hideActivityLoader()
            if let _ = response?.object {
                DispatchQueue.main.async {
                    AlertController.alert(title: AppName.localized(), message: "Successfull".localized(), buttons: [StaticStrings.ok.localized()]) { (a, i) in
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            } else {
                AlertController.alert(title: AppName.localized(), message: response?.message ?? CommonMessage.something_went_wrong.localized())
            }
        }
    }
}


//MARK:- UITextField Delegate Methods
extension UserQuestionsPopover: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if let int = Int(str as String), int == 0 {
            return false
        }
        
        return true
    }
    
    
}
