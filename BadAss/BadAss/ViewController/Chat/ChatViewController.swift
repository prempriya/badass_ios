//
//  ChatViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 27/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Quickblox

class ChatViewController: UIViewController, QBChatContactListProtocol, QBChatDelegate, QBManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    
    @IBOutlet weak var connectionView: UIView!
    @IBOutlet weak var connectionViewHeight: NSLayoutConstraint!
    
    
    var filteredDialogs: [QBChatDialog] = []
    var dialogs: [QBChatDialog] = []
    
    var paging: Page = Page()
    static let pageSize = 300
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.placeholder = "Search".localized()
        searchBar.showsCancelButton = false
        
        paging.page = 0
        paging.pageSize = ChatViewController.pageSize
        QBChat.instance.addDelegate(self)
        NotificationCenter.default.addObserver(self, selector: #selector(handleRefresh), name: NSNotification.Name.chatRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleRefresh), name: NSNotification.Name.chatRequest, object: nil)
        
        
        setConnetionView(hide: true, animated: false)
        QBManager.shared.connectionDelegates.append(self)
        
        switch QBManager.shared.connectionStatus {
        case .Connected:
            self.connectionEstablished()
            break
        case .Connecting:
            self.connectionEstablishing()
        case .Failed:
            self.connectionFailed()
        case .None:
            setConnetionView(hide: true, animated: false)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(retryConnection))
        connectionView.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDialogs()
    }
    
    
    @objc func handleRefresh() {
        self.paging.page = 0
        self.paging.totalPages = 1
        self.paging.pageSize = ChatViewController.pageSize
        getDialogs()
    }
    
    func chatContactListDidChange(_ contactList: QBContactList) {
//        if let user = contactList.contacts?.filter({$0.userID == (chatDialog?.recipientID ?? 0)}).first {
//        }
    }
    
    
    //MARK:- Connection Methods
    
    func setConnetionView(hide: Bool, animated: Bool = true) {
        DispatchQueue.main.async {
            if hide {
                UIView.animate(withDuration: animated ? 0.2 : 0.0) {
                    self.connectionViewHeight.constant = 0
                    self.view.layoutIfNeeded()
                } completion: { (s) in
                    self.connectionView.isHidden = true
                }
            } else {
                UIView.animate(withDuration: animated ? 0.2 : 0.0) {
                    self.connectionViewHeight.constant = 30
                    self.view.layoutIfNeeded()
                } completion: { (s) in
                    self.connectionView.isHidden = false
                }
            }
        }
    }
    
    
    @objc func retryConnection() {
        if let label = self.view.viewWithTag(111) as? UILabel {
            if label.text == StaticStrings.connection_failed.localized() {
                QBManager.shared.manageUserAuthorization()
            }
        }
    }
    
    func connectionFailed() {
        setConnetionView(hide: false)
        if let label = self.view.viewWithTag(111) as? UILabel {
            label.text = "Connection Failed...".localized()
        }
    }
    
    func connectionEstablished() {
        setConnetionView(hide: true)
    }
    
    func connectionEstablishing() {
        setConnetionView(hide: false)
        if let label = self.view.viewWithTag(111) as? UILabel {
            label.text = StaticStrings.connecting.localized()
        }
    }
    
    
    
    
    //MARK:- UIButton Action Method
    @IBAction func searchButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            searchBarHeight.constant = 56
            searchBar.becomeFirstResponder()
        } else {
            searchBarHeight.constant = 0
            searchDialogs("")
            searchBar.resignFirstResponder()
        }
    }
    
    
    fileprivate func acceptRequest(_ dialog: QBChatDialog, indexPath: IndexPath) {
        
        func acceptChatRequest() {
            var data = dialog.data
            data?["class_name"] = "Request"
            data?["is_requested"] = false
            let chatDialog = QBChatDialog(dialogID: dialog.id, type: .private)
            chatDialog.data = data
            
            QBRequest.update(chatDialog, successBlock: { [weak self](response, dialog) in
                ProgressHud.hideActivityLoader()
                if response.isSuccess {
                    self?.getDialogs(showHud: false)
                    
                    let message: QBChatMessage = QBChatMessage()
                    let params = NSMutableDictionary()
                    params["event_type"] = "accept"
                    params["dialog_id"] = dialog.id
                    message.customParameters = params
                    message.recipientID = UInt(dialog.recipientID)
                    QBChat.instance.sendSystemMessage(message) { (error) in
                        
                    }
                    
                    if let vc = UIStoryboard.main.get(MessageViewController.self) {
                        vc.chatDialog = self?.filteredDialogs[indexPath.row]
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    AlertController.alert(title: AppName.localized(), message: response.error?.error?.localizedDescription ?? CommonMessage.something_went_wrong.localized())
                }
            }) { (response) in
                ProgressHud.hideActivityLoader()
                AlertController.alert(title: AppName.localized(), message: response.error?.error?.localizedDescription ?? CommonMessage.something_went_wrong.localized())
            }
        }

        
        if QBManager.shared.isConnected() == false {
            AlertController.alert(title: AppName.localized(), message: StaticStrings.connection_progress.localized())
            return
        }
        
        
        guard let requesterId = dialog.occupantIDs?.filter({ $0 != NSNumber(integerLiteral: Int(QBManager.shared.currentUserId))}).first else { return }
        ProgressHud.showActivityLoader()
        QBManager.shared.acceptContactRequest(qbId: "\(requesterId)") { (success, error) in
            if error == nil {
                acceptChatRequest()
            } else {
                Debug.log(error?.localizedDescription ?? "")
                ProgressHud.hideActivityLoader()
                AlertController.alert(title: AppName.localized(), message: error?.localizedDescription ?? CommonMessage.something_went_wrong.localized())
            }
        }
        
    }
    
    fileprivate func declineRequest(_ dialog: QBChatDialog, indexPath: IndexPath) {
        
        func deleteChat() {
            ProgressHud.showActivityLoader()
            QBManager.shared.deleteDialog(roomId: dialog.id ?? "") { success in
                ProgressHud.hideActivityLoader()
                if success {
                    self.filteredDialogs.remove(at: indexPath.row)
                    self.dialogs = self.dialogs.filter({$0.id != dialog.id})
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    self.tableView.reloadData()
                }
            }
        }
        
        
        if QBManager.shared.isConnected() == false {
            AlertController.alert(title: AppName.localized(), message: StaticStrings.connection_progress.localized())
            return
        }
        
        
        var data = dialog.data
        data?["class_name"] = "Request"
        data?["is_declined"] = true

        let chatDialog = QBChatDialog(dialogID: dialog.id, type: .private)
        chatDialog.data = data
        
        ProgressHud.showActivityLoader()
        QBRequest.update(chatDialog, successBlock: { (response, dialog) in
            ProgressHud.hideActivityLoader()
            if response.isSuccess {
                
                let message: QBChatMessage = QBChatMessage()
                let params = NSMutableDictionary()
                params["event_type"] = "decline"
                params["dialog_id"] = dialog.id
                message.customParameters = params
                message.recipientID = UInt(dialog.recipientID)
                QBChat.instance.sendSystemMessage(message) { (error) in
                    
                }
                
                deleteChat()
            } else {
                AlertController.alert(title: AppName.localized(), message: response.error?.error?.localizedDescription ?? CommonMessage.something_went_wrong.localized())
            }
        }) { (response) in
            ProgressHud.hideActivityLoader()
            AlertController.alert(title: AppName.localized(), message: response.error?.error?.localizedDescription ?? CommonMessage.something_went_wrong.localized())
        }

    }
    
}


//MARK:- UITableView Delegate and DataSource

extension ChatViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredDialogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let dialog = filteredDialogs[indexPath.row]
        
        if let isResquested = dialog.data?["is_requested"] as? Bool, let requesterId = dialog.data?["requester_id"] as? String, isResquested, requesterId != "\(QBManager.shared.currentUserId)" {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MessageAcceptTableViewCell.self), for: indexPath) as! MessageAcceptTableViewCell
            
            cell.loadData(dialog: dialog)
            
            cell.acceptClicked = { [weak self] in
                self?.acceptRequest(dialog, indexPath: indexPath)
            }
            
            cell.declineClicked = { [weak self] in
                self?.declineRequest(dialog, indexPath: indexPath)
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MessageListTableViewCell.self), for: indexPath) as! MessageListTableViewCell
               cell.loadData(dialog: dialog)
               return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dialog = filteredDialogs[indexPath.row]
        if let isResquested = dialog.data?["is_requested"] as? Bool, isResquested, let requesterId = dialog.data?["requester_id"] as? String, requesterId != "\(QBManager.shared.currentUserId)" {
            return
        }
        
        if let vc = UIStoryboard.main.get(MessageViewController.self) {
            vc.chatDialog = filteredDialogs[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
               // completionHandler(true)
                
                if QBManager.shared.isConnected() == false {
                    AlertController.alert(title: AppName.localized(), message: StaticStrings.connection_progress.localized())
                    return
                }
                
                AlertController.alert(title: "", message: "Are you sure you want to delete this chat?".localized(), buttons: [StaticStrings.yes.localized(), StaticStrings.no.localized()], tapBlock: { (action, index) in
                    if index == 0 {
                        ProgressHud.showActivityLoader()
                        let obj = self.filteredDialogs[indexPath.row]
                        QBManager.shared.deleteDialog(roomId: obj.id ?? "") { success in
                            ProgressHud.hideActivityLoader()
                            if success {
                                self.filteredDialogs.remove(at: indexPath.row)
                                self.dialogs = self.dialogs.filter({$0.id != obj.id})
                                self.tableView.deleteRows(at: [indexPath], with: .fade)
                                self.tableView.reloadData()
                            }
                        }
                    }
                })
            }
                
            deleteAction.image = UIImage.init(named: "ic_chat_dustbin")
                deleteAction.backgroundColor = RGBA(56, g: 56, b: 56, a: 1.0)
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
    
}



//MARK:- Search Bar Delegate Methods

extension ChatViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.filteredDialogs = self.dialogs
        self.tableView.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchDialogs(searchBar.text ?? "")
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchDialogs(searchText)
    }
    
    fileprivate func searchDialogs(_ search: String) {
        if search.length < 1 {
            self.filteredDialogs = self.dialogs
            self.tableView.reloadData()
            return
        }
        
        self.filteredDialogs = self.dialogs.filter({($0.name?.lowercased().contains(find: search.lowercased()) ?? false)})
        self.tableView.reloadData()
    }
    
}


//MARK:- Quickblox Methods

extension ChatViewController {
    
    func getDialogs(showHud: Bool = true) {

        if dialogs.count == 0 {
            ProgressHud.showActivityLoader()
        }
        
        self.paging.isExecuting = true
        QBManager.shared.getAllDialogs(pageSize: self.paging.pageSize, page: self.paging.page) { dialogs in
            
            self.paging.isExecuting = false
            self.refreshControl.endRefreshing()
            ProgressHud.hideActivityLoader()
            
            if self.paging.page == 0 {
                self.dialogs.removeAll()
            }
            
            self.dialogs.append(contentsOf: dialogs)
            self.filteredDialogs = self.dialogs
            
            moveToMainThread { [weak self] in
                self?.refreshControl.endRefreshing()
                self?.tableView.reloadData()
                self?.tableView.tableFooterView = nil
            }
        }
        
    }
    
}
