//
//  MessageViewController.swift
//  BadAss
//
//  Created by Prempriya on 24/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//


import UIKit
import Quickblox
import ISEmojiView
import IQKeyboardManagerSwift

fileprivate let chatMessageLimit = 50

class MessageViewController: UIViewController, QBManagerDelegate {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chatTextView: IQTextView!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var sendButton: UIButton!

    @IBOutlet weak var typingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var typingView: UIView!
    @IBOutlet weak var otherUserImageView: UIImageView!
    @IBOutlet var typingDots: [UIView]!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var emojiButton: UIButton!
    @IBOutlet weak var activeNowView: UIView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var infoButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var connectionView: UIView!
    @IBOutlet weak var connectingViewHeight: NSLayoutConstraint!
    
    
    var pingTimer: Timer?
    var backCompletion: ((Bool) -> Void)? = nil
    var isPlaying = false
    
    lazy var emojiView: EmojiView = {
        let keyboardSettings = KeyboardSettings(bottomType: .categories)
        let emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        return emojiView
    }()
    
    fileprivate var isEmoji: Bool = false {
        didSet {
            if isEmoji {
                chatTextView.inputView = emojiView
                chatTextView.reloadInputViews()
            } else {
                chatTextView.inputView = nil
                chatTextView.reloadInputViews()
            }
        }
    }
    
    private var pagination: Page = Page()
    private var messages: [QBChatMessage] = []
    public var chatDialog: QBChatDialog? = nil
    private var otherUserProfilePicURL: URL? {
        didSet {
            moveToMainThread { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    private var otherUser: QBUUser? = nil {
        didSet {
            if let url = URL(string: otherUser?.customData ?? "") {
                otherUserProfilePicURL = url
                otherUserImageView.kf.setImage(with: url, placeholder: userPlaceholder)
            }
        }
    }
    
    var chatArray  = [ChatModel]()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        initialMethod()
        setUpTheUser()
        checkForDeniedRequest()
        
        self.pagination.page = 0
        self.pagination.pageSize = chatMessageLimit
        self.pagination.totalPages = 1
        getMessages()
        
        
        QBManager.shared.connectionDelegates.append(self)
        setConnetionView(hide: true, animated: false)
        
        switch QBManager.shared.connectionStatus {
        case .Connected:
            self.connectionEstablished()
            break
        case .Connecting:
            self.connectionEstablishing()
        case .Failed:
            self.connectionFailed()
        case .None:
            setConnetionView(hide: true, animated: false)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(retryConnection))
        connectionView.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer2 = gradientLayer(sendButton.bounds, .view)
        sendButton.layer.insertSublayer(layer2, at: 0)
        sendButton.clipsToBounds = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.shared.currentDialogId = self.chatDialog?.id
        
        let userId = Int(chatDialog?.recipientID ?? 0)
        let timeout = 5.0
        pingTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true, block: { (timer) in
            QBChat.instance.pingUser(withID: UInt(userId), timeout: timeout) { [weak self](timeInterval, success) in
                self?.activeNowView.isHidden = !success
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.shared.currentDialogId = nil
        
        pingTimer?.invalidate()
        pingTimer = nil
    }
    
    
    //MARK:- Helper Method
    func initialMethod() {
        
        activeNowView.isHidden = true
        
        tableView.addSubview(refreshControl)
        
        tableView.register(UINib(nibName: "SenderChatCell", bundle: nil), forCellReuseIdentifier: "SenderChatCell")
        tableView.register(UINib(nibName: "ReceiverChatCell", bundle: nil), forCellReuseIdentifier: "ReceiverChatCell")
        
        tableView.register(UINib(nibName: "SenderImageTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderImageTableViewCell")
        tableView.register(UINib(nibName: "ReceiverImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiverImageTableViewCell")
        
        chatTextView.delegate = self
        chatTextView.placeholder = "Type a message...".localized()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    fileprivate func setUpTheUser() {
        getUserData()
        
        userNameLabel.text = chatDialog?.name
    
        chatDialog?.onUserIsTyping = { [weak self] (userID: UInt) in
            self?.showTyping()
        }
        
        chatDialog?.onUserStoppedTyping = { [weak self] (userID: UInt) in
           self?.hideTyping()
        }
        
        
        // Showing Info button if current user is one of the advisor.
        if let email = AuthManager.shared.loggedInUser?.email {
            var isAdvisor = false
            GetAdviceType.allCases.forEach { (type) in
                if type.getEmailAddress() == email { isAdvisor = true }
            }
            if isAdvisor { infoButtonWidth.constant = 50 }
        }
        
    }
    
    func getUserData() {
        QBRequest.user(withID: UInt("\(chatDialog?.recipientID ?? 0)")!, successBlock: { (response, user) in
            self.otherUser = user
        }) { (response) in
            
        }
    }
    
    
    fileprivate func checkForDeniedRequest() {
        if let requester_id = chatDialog?.data?["requester_id"] as? String, requester_id == "\(QBManager.shared.currentUserId)", let is_declined = chatDialog?.data?["is_declined"] as? Bool, is_declined {
            AlertController.alert(title: AppName.localized(), message: "Your chat request has been declined.".localized(), buttons: ["OK"]) { (action, code) in
                QBManager.shared.deleteDialog(roomId: self.chatDialog?.id ?? "") { (success) in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    
    func showTyping() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            self.typingViewHeight.constant = 60
            self.typingView.alpha = 1.0
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.heartAnimation(self.typingDots[0], duration: 3)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.heartAnimation(self.typingDots[1], duration: 3)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.heartAnimation(self.typingDots[2], duration: 3)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.scrollToBottom(true)
        }
    }
    
    func hideTyping() {
        
        self.typingViewHeight.constant = 0
        self.typingView.alpha = 0.0

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        typingDots.forEach { (dot) in
            dot.layer.removeAllAnimations()
        }
    }
    
    func heartAnimation(_ view: UIView, duration: Double) {
        let pulse1 = CASpringAnimation(keyPath: "transform.scale")
        pulse1.duration = duration
        pulse1.fromValue = 1.0
        pulse1.toValue = 1.10
        pulse1.autoreverses = true
        pulse1.repeatCount = 99999
        pulse1.initialVelocity = 0.5
        pulse1.damping = 0.8
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = 9999
        animationGroup.repeatCount = 9999
        animationGroup.animations = [pulse1]
        view.layer.add(animationGroup, forKey: "pulse")
    }
    
    
    fileprivate func enableDisableInput(enable: Bool) {
        if enable {
            self.sendButton.isEnabled = enable
            self.chatTextView.isUserInteractionEnabled = enable
            self.emojiButton.isEnabled = enable
        } else {
            self.sendButton.isEnabled = enable
            self.chatTextView.isUserInteractionEnabled = enable
            self.emojiButton.isEnabled = enable
        }
    }
    
    
    
    //MARK:- Connection Methods
    
    func setConnetionView(hide: Bool, animated: Bool = true) {
        DispatchQueue.main.async {
            if hide {
                UIView.animate(withDuration: animated ? 0.2 : 0.0) {
                    self.connectingViewHeight.constant = 0
                    self.view.layoutIfNeeded()
                } completion: { (s) in
                    self.connectionView.isHidden = true
                }
            } else {
                UIView.animate(withDuration: animated ? 0.2 : 0.0) {
                    self.connectingViewHeight.constant = 30
                    self.view.layoutIfNeeded()
                } completion: { (s) in
                    self.connectionView.isHidden = false
                }
            }
        }
    }
    
    @objc func retryConnection() {
        if let label = self.view.viewWithTag(111) as? UILabel {
            if label.text == StaticStrings.connection_failed.localized() {
                QBManager.shared.manageUserAuthorization()
            }
        }
    }
    
    func connectionFailed() {
        setConnetionView(hide: false)
        if let label = self.view.viewWithTag(111) as? UILabel {
            label.text = "Connection Failed...".localized()
        }
    }
    
    func connectionEstablished() {
        setConnetionView(hide: true)
    }
    
    func connectionEstablishing() {
        setConnetionView(hide: false)
        if let label = self.view.viewWithTag(111) as? UILabel {
            label.text = StaticStrings.connecting.localized()
        }
    }
    
    
    //MARK:- Keyboard Methods
    @objc func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height
            let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double) ?? 0.25
            if let keyboardHeight = keyboardHeight {
                viewBottomConstraint.constant = -keyboardHeight
                UIView.animate(withDuration: duration, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                    self.perform(#selector(self.scrollToBottom), with: nil, afterDelay: 0.0)
                })
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        viewBottomConstraint.constant = 0.0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded()
        })
        self.perform(#selector(scrollToBottom), with: nil, afterDelay: 0.2)
    }
    
    //MARK:- Target Method
    @objc func scrollToBottom(_ animated: Bool = false) {
        if self.messages.count > 0 {
            moveToMainThread { [weak self] in
                self?.tableView.scrollToRow(at: IndexPath.init(row: (self?.messages.count ?? 0) - 1 , section: 0), at:.bottom, animated: false)
            }
        }
    }
    
    //MARK:- UIButton Action Method
    @IBAction func backButtonAction(_ sender: UIButton){
        self.backCompletion?(isPlaying)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonAction(_ sender: UIButton){
        self.view.endEditing(true)
        
        if QBManager.shared.isConnected() == false {
            AlertController.alert(title: AppName.localized(), message: StaticStrings.connection_progress.localized())
            return
        }
        
        if chatTextView.text.trimWhiteSpace.isEmpty {
            cameraImageAction()
        } else {
            sendMessage(text: self.chatTextView.text.trimWhiteSpacesAndNewLine, attachment: nil)
            let obj = ChatModel(message: chatTextView.text.trimWhiteSpace, is_Sender: true, chatImage: UIImage())
            chatArray.append(obj)
            tableView.reloadData()
            scrollToBottom()
            chatTextView.text = ""
        }
    }
    
    @IBAction func emojiButtonAction(_ sender: UIButton) {
        if isEmoji {
            isEmoji = false
        } else {
            isEmoji = true
        }
    }
    
    @IBAction func cameraButtonAction(_ sender: UIButton){
//        cameraImageAction()
    }
    
    @IBAction func searchButtonAction(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            let alert = UIAlertController(title: "Search your messages".localized(), message: "Enter text to search chat messages.".localized(), preferredStyle: .alert)
            
            alert.addTextField { (textField) in
                textField.placeholder = "Search".localized()
            }
            
            let submitAction = UIAlertAction(title: "Submit".localized(), style: .default) { [unowned alert] _ in
                let answer = alert.textFields![0].text
                if (answer?.length ?? 0) > 0 {
                    self.enableDisableInput(enable: false)
                    self.pagination.page = 0
                    self.pagination.pageSize = chatMessageLimit
                    self.pagination.totalPages = 1
                    self.getMessages(search: answer!)
                } else {
                    sender.isSelected = false
                }
            }
            
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive) { _ in
                sender.isSelected = false
            }
            
            alert.addAction(cancelAction)
            alert.addAction(submitAction)
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            enableDisableInput(enable: true)
            self.pagination.page = 0
            self.pagination.pageSize = chatMessageLimit
            self.pagination.totalPages = 1
            getMessages()
        }
    }
    
    
    @IBAction func infoButtonAction(_ sender: UIButton) {
        ProgressHud.showActivityLoader()
        guard let userId = otherUser?.externalUserID else { return }
        WebServices.getUsersQuestionsCount(userId: "\(userId)") { (response) in
            ProgressHud.hideActivityLoader()
            if let obj = response?.object {
                
                let count = (Int(obj.free_count ?? "0") ?? 0) + (Int(obj.paid_count ?? "0") ?? 0)
                
                DispatchQueue.main.async {
                    if let vc = UIStoryboard.main.get(UserQuestionsPopover.self) {
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.questionsCount = count
                        vc.name = self.otherUser?.fullName ?? "Username"
                        vc.userId = "\(userId)"
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                
            } else {
                AlertController.alert(title: AppName.localized(), message: CommonMessage.something_went_wrong.localized())
            }
        }
    }
    
    
    
}

extension MessageViewController: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - UITableViewDelegateDataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let obj = messages[indexPath.row]

        if obj.senderID == QBManager.shared.currentUserId {
            
            if (obj.attachments?.count ?? 0) > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageTableViewCell", for: indexPath) as! SenderImageTableViewCell
                cell.shapeView.setNeedsDisplay()
                cell.timeLabel.text = obj.dateSent?.dateStringFromDate(DateFormat.chatMessage)
                cell.setupMessageData(obj: obj)
                
                if (obj.readIDs?.filter({$0 == NSNumber(integerLiteral: (chatDialog?.recipientID ?? 0))}).count ?? 0) > 0 {
                    cell.messageStatusImageView.image = #imageLiteral(resourceName: "ic_chat_doubletick").maskWithColor(.cyan)
                } else if (obj.deliveredIDs?.filter({$0 == NSNumber(integerLiteral: (chatDialog?.recipientID ?? 0))}).count ?? 0) > 0 {
                    cell.messageStatusImageView.image = #imageLiteral(resourceName: "ic_chat_doubletick")
                } else {
                    cell.messageStatusImageView.image = #imageLiteral(resourceName: "ic_chat_singletick")
                }
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderChatCell", for: indexPath) as! SenderChatCell
                cell.messageLabel.text = obj.text
                cell.shapeView.setNeedsDisplay()
                cell.timeLabel.text = obj.dateSent?.dateStringFromDate(DateFormat.chatMessage)
                
                if (obj.readIDs?.filter({$0 == NSNumber(integerLiteral: (chatDialog?.recipientID ?? 0))}).count ?? 0) > 0 {
                    cell.messageStatusImageView.image = #imageLiteral(resourceName: "ic_chat_doubletick").maskWithColor(.cyan)
                } else if (obj.deliveredIDs?.filter({$0 == NSNumber(integerLiteral: (chatDialog?.recipientID ?? 0))}).count ?? 0) > 0 {
                    cell.messageStatusImageView.image = #imageLiteral(resourceName: "ic_chat_doubletick")
                } else {
                    cell.messageStatusImageView.image = #imageLiteral(resourceName: "ic_chat_singletick")
                }
                
                return cell
            }
        } else {
            
            if let readIds = obj.readIDs, readIds.contains(NSNumber(integerLiteral: Int(QBManager.shared.currentUserId))) == false {
                QBChat.instance.read(obj, completion: nil)
            }
            
            if (obj.attachments?.count ?? 0) > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageTableViewCell", for: indexPath) as! ReceiverImageTableViewCell
                if let url = otherUserProfilePicURL {
                    cell.userImageView.kf.setImage(with: url, placeholder: userPlaceholder)
                } else {
                    cell.userImageView.image = userPlaceholder
                }
                cell.shapeView.setNeedsDisplay()
                cell.timeLabel.text = obj.dateSent?.dateStringFromDate(DateFormat.chatMessage)
                cell.setupMessageData(obj: obj)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverChatCell", for: indexPath) as! ReceiverChatCell
                cell.messageLabel.text = obj.text
                if let url = otherUserProfilePicURL {
                    cell.userImageView.kf.setImage(with: url, placeholder: userPlaceholder)
                } else {
                    cell.userImageView.image = userPlaceholder
                }
                cell.shapeView.setNeedsDisplay()
                cell.timeLabel.text = obj.dateSent?.dateStringFromDate(DateFormat.chatMessage)
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        moveToMainThread { [weak self] in
            
            var imageView = UIImageView()
            if let cell = tableView.cellForRow(at: indexPath) as? SenderImageTableViewCell {
                imageView = cell.messageImageView
                let imageInfo   = GSImageInfo(image: imageView.image ?? UIImage(), imageMode: .aspectFit)
                let transitionInfo = GSTransitionInfo(fromView: imageView)
                let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
                
                imageViewer.dismissCompletion = {
                    print("dismissCompletion")
                }
                
                self?.present(imageViewer, animated: true, completion: nil)
            } else if let cell = tableView.cellForRow(at: indexPath) as? ReceiverImageTableViewCell {
                imageView = cell.messageImageView
                let imageInfo   = GSImageInfo(image: imageView.image ?? UIImage(), imageMode: .aspectFit)
                let transitionInfo = GSTransitionInfo(fromView: imageView)
                let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
                
                imageViewer.dismissCompletion = {
                    print("dismissCompletion")
                }
                
                self?.present(imageViewer, animated: true, completion: nil)
            }
        }
    }
    
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        guard scrollView.isDragging else { return }
//
//        let currentOffset = scrollView.contentOffset.y
//        print("Current Offset \(currentOffset)")
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
//        print("Maximum Offset \(maximumOffset)")
//
//        if pagination.isExecuting {
//            return
//        }
//
//        if currentOffset < 200 {
//            if (self.pagination.page < self.pagination.totalPages) {
//                self.pagination.page = self.pagination.page + 1
//                getMessages()
//            } else {
//                refreshControl.endRefreshing()
//            }
//        }
//    }
    
}

//MARK: - decription and encryption
extension String {
    
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            
            return data.base64EncodedString()
        }
        return nil
    }
    
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self.replacingOccurrences(of: "\n", with: "")) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}


extension MessageViewController  {
    func cameraImageAction() {
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self, message: AttachmentHandler.Constants.updatePhotoMessage)
        AttachmentHandler.shared.imagePickedBlock = { [weak self](image) in
            guard let self = self else {
                return
            }
            self.uploadImage(attachmentData: image.pngData()!, localURL: nil)
        }
    }
}

// MARK: - Delegates
extension MessageViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let pickedImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage {
            let obj = ChatModel(message: "", is_Sender: true, chatImage: pickedImage)
            self.chatArray.append(obj)
            self.tableView.reloadData()
            self.scrollToBottom()
            //imageView.contentMode = .scaleAspectFit
            // imageView.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
}


class ChatModel {
    var message:String?
    var is_Sender: Bool?
    var chatImage: UIImage?

    init(message:String,is_Sender:Bool, chatImage: UIImage){
        self.message = message
        self.is_Sender = is_Sender
        self.chatImage = chatImage
    }
}

//MARK:- UITextView Delegate
extension MessageViewController : UITextViewDelegate {
    
    
    func textViewDidChange(_ textView: UITextView) {
        resizeTextView(textView)
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        textView.isScrollEnabled = false
        isEmoji = false
        chatDialog?.sendUserStoppedTyping()
        resizeTextView(textView)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        chatDialog?.sendUserIsTyping()
        resizeTextView(textView)
    }
    
    fileprivate func resizeTextView(_ textView: UITextView) {
        
        var newFrame = textView.frame
        let width = newFrame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        
        if newSize.height > 70 {
            textView.isScrollEnabled = true
            return
        } else {
            textView.isScrollEnabled = false
        }
        
        newFrame.size = CGSize(width: width, height: newSize.height)
        textViewHeight.constant = newFrame.size.height
        
        sendButton.setImage(UIImage.init(named: textView.text.count > 0 ? "ic_send" : "ic_camera"), for: .normal)
    }
}


//MARK:- EmojiView Delegate
extension MessageViewController: EmojiViewDelegate {
    
    // callback when tap a emoji on keyboard
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        chatTextView.insertText(emoji)
        resizeTextView(chatTextView)
    }

    // callback when tap change keyboard button on keyboard
    func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        chatTextView.inputView = nil
        chatTextView.keyboardType = .default
        chatTextView.reloadInputViews()
        resizeTextView(chatTextView)
    }
        
    // callback when tap delete button on keyboard
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        chatTextView.deleteBackward()
        resizeTextView(chatTextView)
    }

    // callback when tap dismiss button on keyboard
    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        chatTextView.resignFirstResponder()
        resizeTextView(chatTextView)
    }
    
}


//MARK:- Messages Methods
extension MessageViewController: QBChatDelegate, QBChatContactListProtocol {
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if pagination.isExecuting {
            refreshControl.endRefreshing()
            return
        }
        if (self.pagination.page < self.pagination.totalPages) {
            self.pagination.page = self.pagination.page + 1
            getMessages()
        } else {
            refreshControl.endRefreshing()
        }
    }
    
    
    func getMessages(search: String? = nil) {
        
        guard let chatDialog = self.chatDialog else { return }
        
        if messages.count == 0 {
            ProgressHud.showActivityLoader()
        }
        
        pagination.isExecuting = true
        QBManager.shared.getChatHistory(pageSize: self.pagination.pageSize, page: self.pagination.page , dialogId: chatDialog.id ?? "", search: search) { messages in
            self.pagination.isExecuting = false
            self.refreshControl.endRefreshing()
            ProgressHud.hideActivityLoader()
            
            if self.pagination.page == 0 {
                self.messages.removeAll()
            }
            self.messages.insert(contentsOf: messages.reversed(), at: 0)
            
            self.tableView.reloadData()
            if self.pagination.page == 0 {
                self.scrollToBottom()
            } else {
                if messages.count > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: .top, animated: false)
                }
            }
        }
        
        
        QBManager.shared.getMessagesCount(dialogId: chatDialog.id ?? "") { [weak self](count) in
            if let count = count {
                let remainder = count % (self?.pagination.pageSize ?? chatMessageLimit)
                let pages = (count / (self?.pagination.pageSize ?? chatMessageLimit))
                self?.pagination.totalPages = (remainder > 0) ? (pages + 1) : pages
            }
        }
    }
    
    func sendPushNotifications(message: String = "", isAttachmentAvailable: Bool) {
        
        guard let chatDialog = self.chatDialog else { return }
        
        if let isRequested = chatDialog.data?["is_requested"] as? Bool, isRequested {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            
            let payload = [
                "message": "\(AuthManager.shared.loggedInUser?.fullname ?? "An user") : \(isAttachmentAvailable ? "Sent you an attachment." : message).",
                "ios_badge": "0",
                "ios_sound": "default",
                "ios_content-available": 1,
                "type": "chat_message",
                "id": "\(self.chatDialog?.id ?? "0")",
                "sender_id": "\(QBManager.shared.currentUserId)",
                "receiver_id": "\(chatDialog.recipientID)"
                ] as [String : Any]
            
            QBManager.shared.sendPush(toIds: [UInt(chatDialog.recipientID)], payload: payload)
        }
    }
    
    
    func sendMessage(text: String, attachment: [QBChatAttachment]?) {
        
        guard let chatDialog = self.chatDialog else { return }
        
        QBChat.instance.addDelegate(self)
        
        sendButton.isEnabled = false
        
        let message: QBChatMessage = QBChatMessage.markable()
        message.text = text
        message.senderID = QBManager.shared.currentUserId
        message.dialogID = chatDialog.id
        message.dateSent = Date()
        message.markable = true
        message.deliveredIDs = [(NSNumber(value: QBManager.shared.currentUserId))]
        message.readIDs = [(NSNumber(value: QBManager.shared.currentUserId))]
        
        let params = NSMutableDictionary()
        params["save_to_history"] = true
        params["liked"] = "Liked"
        params["send_to_chat"] = "1"
        
        message.customParameters = params
        message.attachments = attachment
        
        chatDialog.send(message) { [weak self] (error) in
            
            if error == nil {
                
                self?.chatTextView.text = ""
                self?.sendButton.isEnabled = true
                
                QBChat.instance.mark(asDelivered: message) { (err) in
                    
                }
                
                self?.messages.append(message)
                
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.scrollToBottom()
                }
                
                self?.sendPushNotifications(message: text, isAttachmentAvailable: ((attachment?.count ?? 0) > 0))
            } else {
                
                QBManager.shared.manageUserAuthorization()
                
                AlertController.alert(title: AppName.localized(), message: error?.localizedDescription ?? CommonMessage.something_went_wrong.localized())
                self?.sendButton.isEnabled = true
            }
        }
    }
    
    func uploadImage(attachmentData: Data, localURL: NSURL? = nil) {
        QBChat.instance.addDelegate(self)
        ProgressHud.showActivityLoader()
        QBRequest.tUploadFile(attachmentData, fileName: "\(Date().timeIntervalSinceNow)", contentType: "image/png", isPublic: false,
                              successBlock: {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                                ProgressHud.hideActivityLoader()
                                
                                let uploadedFileID: String = uploadedBlob.uid ?? ""
                                let attachment: QBChatAttachment = QBChatAttachment()
                                attachment.type = "photo"
                                attachment.id = String(uploadedFileID)
                                attachment.url = uploadedBlob?.privateUrl()
                                if let url = localURL?.absoluteString {
                                    attachment.name = url
                                } else {
                                    attachment.name = "\(Date().timeIntervalSinceNow)"
                                }
                                self.sendMessage(text: "🌄 Attachment", attachment: [attachment])
        }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
        }, errorBlock: {(response: QBResponse!) in
            ProgressHud.hideActivityLoader()
            Debug.log("\(response.error?.reasons as Any)")
        })
    }
    
    func chatDidReceive(_ message: QBChatMessage) {
        if chatDialog?.id == message.dialogID {
            Debug.log("\(message)")
            messages.append(message)
            QBManager.shared.readMessage(message: message)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.scrollToBottom()
            }
        }
    }
    
    
    func chatRoomDidReceive(_ message: QBChatMessage, fromDialogID dialogID: String) {
        if dialogID == chatDialog?.id {
            Debug.log("\(message)")
            messages.append(message)
            QBManager.shared.readMessage(message: message)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.scrollToBottom()
            }
        }
    }
    
    func chatDidReadMessage(withID messageID: String, dialogID: String, readerID: UInt) {
        guard dialogID == chatDialog?.id else { return }
        messages.filter({$0.id == messageID}).first?.readIDs?.append(NSNumber(integerLiteral: Int(readerID)))
        self.tableView.reloadData()
    }
    
    func chatDidDeliverMessage(withID messageID: String, dialogID: String, toUserID userID: UInt) {
        guard dialogID == chatDialog?.id else { return }
        messages.filter({$0.id == messageID}).first?.deliveredIDs?.append(NSNumber(integerLiteral: Int(userID)))
        self.tableView.reloadData()
    }
    
    func chatDidReceiveContactItemActivity(_ userID: UInt, isOnline: Bool, status: String?) {
        if let recipentId = chatDialog?.recipientID, UInt(recipentId) == userID {
            DispatchQueue.main.async { [weak self] in
                self?.activeNowView?.isHidden = !isOnline
            }
        }
    }
    
    
    func chatDidReceiveSystemMessage(_ message: QBChatMessage) {
        Debug.log("\(message)")
        if message.dialogID == chatDialog?.id {
            if let param = message.customParameters, let dialogId = param["dialog_id"] as? String, dialogId == chatDialog?.id, let event = param["event_type"] as? String {
                if event == "accept" {
                    chatDialog?.data?["is_requested"] = false
                } else if event == "decline" {
                    chatDialog?.data?["is_declined"] = true
                    checkForDeniedRequest()
                }
            }
        }
    }
    
    
    func chatDidAccidentallyDisconnect() {
        QBManager.shared.loginToChat()
    }
    
    
    func chatDidDisconnectWithError(_ error: Error) {
        QBManager.shared.loginToChat()
    }
    
    
    func chatContactListDidChange(_ contactList: QBContactList) {
        if let user = contactList.contacts?.filter({$0.userID == (chatDialog?.recipientID ?? 0)}).first {
            self.activeNowView.isHidden = !user.isOnline
        }
    }

}


    
