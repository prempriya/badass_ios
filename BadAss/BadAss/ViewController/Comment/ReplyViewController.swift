//
//  ReplyViewController.swift
//  BadAss
//
//  Created by Prempriya on 20/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ReplyViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: IQTextView!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!
    
    var commentId: String?
    var page: Page = Page()
    var videoId: Int?
    
    var comment = Comment()
    var replies = [ReplyUserData]()
    var commentCompletion: (() -> Void)? = nil

    
    //MARK:- UIViewController Lifecycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Method
    
    func customSetup(){
        self.backgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        page.pageSize = 10
        textView.delegate = self
        
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.navView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.navView.layer.cornerRadius = 15
        
        if let imgUrl = AuthManager.shared.loggedInUser?.image, let url = URL(string: imgUrl) {
            self.profileButton.kf.setImage(with: url, for: .normal)
        }
        
        self.commentCountLabel.text = self.replies.count > 1 ? "\(self.replies.count) \(StaticStrings.replies.localized())" :  "\(self.replies.count) \(StaticStrings.reply.localized())"
        textView.placeholder = StaticStrings.add_a_reply.localized()
    }
    
    //MARK:- Keyboard Methods
    @objc func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height
            let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double) ?? 0.25
            if let keyboardHeight = keyboardHeight {
                viewBottomConstraint.constant = -keyboardHeight
                UIView.animate(withDuration: duration, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                    self.perform(#selector(self.scrollToBottom), with: nil, afterDelay: 0.0)
                })
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        viewBottomConstraint.constant = 0.0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded()
        })
        self.perform(#selector(scrollToBottom), with: nil, afterDelay: 0.2)
    }
    
    //MARK:- Target Method
    @objc func scrollToBottom() {
        //        if self.chatArray.count > 0 {
        //            self.tableView.scrollToRow(at: IndexPath.init(row: self.chatArray.count - 1 , section: 0), at:.bottom, animated: false)
        //        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton){
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonAction(_ sender: UIButton){
        view.endEditing(true)
        if !textView.text.isEmpty {
            commentOnReply()
        }
    }
}

extension ReplyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  replies.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCommentTableViewCell") as! MainCommentTableViewCell
        if indexPath.row == 0 {
            cell.leadingConstraint.constant = 15
            cell.seperatorLeadingConstraint.constant = 15
            cell.loadMainComment(obj: comment)
            cell.replyButton.isHidden = false

        }else {
            cell.leadingConstraint.constant = 55
            cell.seperatorLeadingConstraint.constant = 55
            cell.loadComment(obj: replies[indexPath.row - 1])
            cell.likeClicked = {
                self.likeOnComment(obj: self.replies[indexPath.row - 1], index: indexPath.row - 1)
            }
            cell.replyButton.isHidden = true
            cell.dotLabel.isHidden = true

        }
        return cell

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  95
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   
    
}


extension ReplyViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard var str = textView.text, let range = Range(range, in: str) else { return true }
        str = str.replacingCharacters(in: range, with: text)
        let numLines = Double(textView.contentSize.height) / Double(textView.font?.lineHeight ?? 18)
        textView.isScrollEnabled = numLines > 6
        self.view.layoutSubviews()
        return true
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        textView.isScrollEnabled = false
    }
    
}

extension ReplyViewController {
    
        func likeOnComment(obj: ReplyUserData, index: Int){
        var param = [String: Any]()
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param["like_status"] = obj.is_liked == "false" ? 1 : 0
        param["comment_id"] = obj.comment_id
        param["video_id"] = videoId
        
        WebServices.likeComment(params: param) { (response) in
            
            PostViewController.commented = true
            
            self.replies[index].is_liked =  self.replies[index].is_liked == "false" ? "true" : "false"
            let likeCount = Int(obj.likes_count ?? "0")
            if self.replies[index].is_liked == "false" {
                self.replies[index].likes_count  = "\((likeCount ?? 0) - 1)"
            }else {
                self.replies[index].likes_count  = "\((likeCount ?? 0) + 1)"
            }
            self.tableView.reloadData()
        }
    }
    
    func commentOnReply(){
        var param = [String: Any]()
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param["reply"] = textView.text.trimWhiteSpace
        param["comment_id"] = comment.comment_id
        param["video_id"] = videoId

        WebServices.postReply(params: param) { (response) in
            if response?.statusCode == 200 {
                
                let obj = ReplyUserData()
                obj.comment_id = response?.object?.reply_id ?? 0
                obj.name = AuthManager.shared.loggedInUser?.fullname
                obj.username = AuthManager.shared.loggedInUser?.username
                obj.comment = self.textView.text.trimWhiteSpace
                obj.profile_pic = AuthManager.shared.loggedInUser?.image
                obj.user_id = AuthManager.shared.loggedInUser?.user_id
                obj.is_liked = "false"
                obj.created_date = Int(self.dateStringFromDate())
                obj.likes_count = "0"
                obj.replies_count =  "0"
                self.replies.insert(obj, at: 0)
                self.comment.replies_count = "\(self.replies.count)"

                self.tableView.reloadData()
                self.textView.text = ""
                 self.commentCountLabel.text = self.replies.count > 1 ? "\(self.replies.count) \(StaticStrings.replies.localized())" :  "\(self.replies.count) \(StaticStrings.reply.localized())"
                
                self.commentCompletion?()
            }
        }
    }
    
    func dateStringFromDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: Date())
    }
    
}

