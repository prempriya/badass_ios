//
//  CommentViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 19/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CommentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: IQTextView!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!

    var isFromNotification = false
    var videoId: Int?
    var page: Page = Page()
    var backCompletion: (() -> Void)? = nil
    
    
    var comments: [Comment] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    //MARK:- UIViewController Lifecycle Method

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customSetup()
        getComment()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name.videoComment, object: nil)
    }
    
    @objc func refreshData() {
        page.page = 1
        page.pageSize = 50
        getComment()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.videoComment, object: nil)
    }
    
    //MARK:- Helper Method
    
    func customSetup(){
        self.backgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        page.pageSize = 50
        textView.delegate = self
        textView.placeholder = "\(StaticStrings.add_a_comment.localized())"
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.navView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
               self.navView.layer.cornerRadius = 15
        
        if let imgUrl = AuthManager.shared.loggedInUser?.image, let url = URL(string: imgUrl) {
            self.profileButton.kf.setImage(with: url, for: .normal)
        }
    }
    
    //MARK:- Keyboard Methods
    @objc func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height
            let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double) ?? 0.25
            if let keyboardHeight = keyboardHeight {
                viewBottomConstraint.constant = -keyboardHeight
                UIView.animate(withDuration: duration, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                    self.perform(#selector(self.scrollToBottom), with: nil, afterDelay: 0.0)
                })
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        viewBottomConstraint.constant = 0.0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded()
        })
        self.perform(#selector(scrollToBottom), with: nil, afterDelay: 0.2)
    }
    
    //MARK:- Target Method
    @objc func scrollToBottom() {
//        if self.chatArray.count > 0 {
//            self.tableView.scrollToRow(at: IndexPath.init(row: self.chatArray.count - 1 , section: 0), at:.bottom, animated: false)
//        }
    }
    
    func openReply(comment: Comment){
        
        if let vc = UIStoryboard.main.get(ReplyViewController.self) {
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            vc.videoId = videoId
            vc.commentId = "\(comment.comment_id ?? 0)"
            vc.replies = comment.reply_user_data
            vc.comment = comment
            vc.commentCompletion = {
                self.getComment(false)
            }
            let navController = UINavigationController(rootViewController: vc)
            navController.setNavigationBarHidden(true, animated: false)
            navController.modalPresentationStyle = .custom
            self.present(navController, animated: false, completion: nil)
        }
    }
   
    @IBAction func backButtonAction(_ sender: UIButton){
        view.endEditing(true)
        if isFromNotification {
            self.navigationController?.popViewController(animated: true)
        }else {
            self.backCompletion?()
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func sendButtonAction(_ sender: UIButton){
        view.endEditing(true)
        if !textView.text.isEmpty {
            commentOnVideo()
        }
    }

}

extension CommentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell
        cell.loadComment(obj: comments[indexPath.row])
        cell.likeClicked = {
            self.likeOnComment(obj: self.comments[indexPath.row], index: indexPath.row)
        }
        cell.replyClicked = {
            self.openReply(comment: self.comments[indexPath.row])
        }
      //  cell.commentLabel.text = "shgdjhjd gjsahgdgsaj gsjdghjas gsjahdg gdjhgasj dsaghdjasjdgsjagdj  jshgdjasgd jagsd gsjdgas jdjhagsdj sdgjsagd jasgdjags"
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            
            if page.isExecuting {
                return
            }
            
            if currentOffset - maximumOffset >= 15.0 {
                if self.page.hasNext {
                    self.page.page = self.page.page + 1
                    tableView.tableFooterView = getViewWithActivityIndicatot(withHeight : 60)
                    getComment(false)
                }
            }
        }
    
}


extension CommentViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
       
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard var str = textView.text, let range = Range(range, in: str) else { return true }
        str = str.replacingCharacters(in: range, with: text)
        let numLines = Double(textView.contentSize.height) / Double(textView.font?.lineHeight ?? 18)
        textView.isScrollEnabled = numLines > 6
        self.view.layoutSubviews()
        return true
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        textView.isScrollEnabled = false
    }
    
}

extension CommentViewController {
    
    func commentOnVideo(){
        var param = [String: Any]()
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param["comment"] = textView.text.trimWhiteSpace
        param["video_id"] = videoId
        
        WebServices.postComment(params: param) { (response) in
            if response?.statusCode == 200 {
                
                PostViewController.commented = true

                let obj = Comment()
                obj.comment_id = response?.object?.comment_id ?? 0
                obj.creator.name = AuthManager.shared.loggedInUser?.fullname
                obj.creator.username = AuthManager.shared.loggedInUser?.username
                obj.comment = self.textView.text.trimWhiteSpace
                obj.creator.profile_pic = AuthManager.shared.loggedInUser?.image
                obj.creator.user_id = AuthManager.shared.loggedInUser?.user_id
                obj.is_liked = "false"
                obj.created_at = Int(self.dateStringFromDate())
                obj.likes_count = "0"
                obj.replies_count =  "0"
                self.comments.insert(obj, at: 0)
                self.tableView.reloadData()
                self.textView.text = ""
                self.commentCountLabel.text = self.comments.count > 1 ? "\(self.comments.count) \(StaticStrings.Comments.localized())" :  "\(self.comments.count) \(StaticStrings.Comment.localized())"
            }
        }
    }
    
    func likeOnComment(obj: Comment, index: Int){
           var param = [String: Any]()
           param[kUserId] = AuthManager.shared.loggedInUser?.user_id
           param["like_status"] = obj.is_liked == "false" ? 1 : 0
           param["comment_id"] = obj.comment_id
           param["video_id"] = videoId
           
           WebServices.likeComment(params: param) { (response) in
            self.comments[index].is_liked =  self.comments[index].is_liked == "false" ? "true" : "false"
            let likeCount = Int(obj.likes_count ?? "0")
            if self.comments[index].is_liked == "false" {
                self.comments[index].likes_count  = "\((likeCount ?? 0) - 1)"
            }else {
                self.comments[index].likes_count  = "\((likeCount ?? 0) + 1)"
            }
            self.tableView.reloadData()
           }
       }
    
    
    func getComment(_ hud: Bool = true){
        
        var param = [String: Any]()
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param["video_id"] = videoId
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.comment(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            self?.tableView.tableFooterView = UIView()
            if let array = response?.array, let self = self {
                
                if self.page.page == 1 {
                    self.comments.removeAll()
                }
                
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                
                self.comments.append(contentsOf: array)
                self.commentCountLabel.text = self.comments.count > 1 ? "\(self.comments.count) \(StaticStrings.Comments.localized())" :  "\(self.comments.count) \(StaticStrings.Comment.localized())"
            } else if response?.statusCode == 200 {
                
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }
    
    func dateStringFromDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: Date())
    }
    
}
