//
//  EditProfileViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 12/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var fullNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var usernameField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var countryTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var dateOfBirthTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var languageTextField: SkyFloatingLabelTextField!

    @IBOutlet weak var phoneErrorLabel: UILabel!
    @IBOutlet weak var aboutTextView: IQTextView!
    @IBOutlet weak var stateErrorLabel: UILabel!
    
    @IBOutlet weak var codeButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var aboutTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var genderImageView: UIImageView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var aboutSepartor: UIView!
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    
    @IBOutlet weak var maleView: UIView!
    @IBOutlet weak var femaleView: UIView!
    @IBOutlet weak var languageButton: UIButton!

    @IBOutlet weak var countryCodeField: SkyFloatingLabelTextField!
    @IBOutlet weak var preferNotToTellButton: UIButton!
    @IBOutlet weak var showYourAgeButton: UIButton!
    
    @IBOutlet weak var langaugeCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate var profileImage: UIImage?
    
    var user = User()
    var countries: [Country] = []
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = AuthManager.shared.loggedInUser ?? User()
        
        getCountries()
        setupTextFields()
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(languageCollectionTapped))
//        gesture.cancelsTouchesInView = false
//        langaugeCollectionView.addGestureRecognizer(gesture)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let layer1 = gradientLayer(imageContainerView.bounds, .view)
        imageContainerView.layer.insertSublayer(layer1, at: 0)
        imageContainerView.layer.cornerRadius = 50
        imageContainerView.clipsToBounds = true
        imageBackgroundView.clipsToBounds = true
        let layer2 = gradientLayer(cameraButton.bounds, .view)
        cameraButton.layer.insertSublayer(layer2, at: 0)
        cameraButton.clipsToBounds = true
    }
    
    //MARK:- Helper Method

    @objc func languageCollectionTapped() {
        languageButtonAction(UIButton())
    }
    
    func openCountryPicker() {
        let storyboard = UIStoryboard(name: "Country", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CountryPickerViewController") as! CountryPickerViewController
        controller.completion = { [weak self] dict in
            self?.codeButton.setTitle(" \(dict["dial_code"] ?? "")  ", for: .normal)
            self?.countryCodeField.text = " \(dict["dial_code"] ?? "")  "
        }
        controller.modalPresentationStyle = .custom
        self.present(controller, animated: true, completion: nil)
    }
    
    func validateFields() -> Bool {
        
        if user.fullname == nil {
            fullNameField.errorMessage = ValidationMessage.blankFullName.localized()
            scrollView.contentOffset = fullNameField.frame.origin
            return false
        } else if user.fullname!.length == 0 {
            fullNameField.errorMessage = ValidationMessage.blankFullName.localized()
            scrollView.contentOffset = fullNameField.frame.origin
            return false
        } else if user.fullname!.length < 2 {
            fullNameField.errorMessage = ValidationMessage.invalidFullName.localized()
            scrollView.contentOffset = fullNameField.frame.origin
            return false
        }
            
//        else if user.username == nil {
//            usernameField.errorMessage = ValidationMessage.blankUsername.localized()
//            return false
//        } else if user.username!.length == 0 {
//            usernameField.errorMessage = ValidationMessage.blankUsername.localized()
//            return false
//        } else if user.username!.length < 2 {
//            usernameField.errorMessage = ValidationMessage.minLengthUsername.localized()
//            return false
//        } else if !user.username!.isValidUserName {
//            usernameField.errorMessage = ValidationMessage.invalidUserName.localized()
//            return false
//        }
            
//        else if user.phone_number == nil {
//            self.phoneErrorLabel.text = ValidationMessage.blankPhoneNumber.localized()
//            return false
//        } else if user.phone_number!.length == 0 {
//            self.phoneErrorLabel.text = ValidationMessage.blankPhoneNumber.localized()
//            return false
//        } else if user.phone_number!.length < 10 {
//            self.phoneErrorLabel.text = ValidationMessage.validPhone.localized()
//            return false
//        } else if user.phone_number!.isContainsAllZeros() {
//            self.phoneErrorLabel.text = ValidationMessage.validPhone.localized()
//            return false
//        } else if user.country?.length == 0 {
//            self.stateErrorLabel.text = ValidationMessage.blankCountryName.localized()
//            return false
//        }
//
//
//        else if user.email == nil {
//            emailField.errorMessage = ValidationMessage.blankEmail.localized()
//            return false
//        } else if user.email!.length == 0 {
//            emailField.errorMessage = ValidationMessage.blankEmail.localized()
//            return false
//        } else if user.email!.length < 2 {
//            emailField.errorMessage = ValidationMessage.validEmail.localized()
//            return false
//        } else if !user.email!.isEmail {
//            emailField.errorMessage = ValidationMessage.validEmail.localized()
//            return false
//        }
        
        else {
            return true
        }
    }
    
    fileprivate func setupTextFields() {
        
        usernameField.isUserInteractionEnabled = false
        emailField.isUserInteractionEnabled = false
        phoneField.isUserInteractionEnabled = false
        countryCodeField.isUserInteractionEnabled = false

        fullNameField.delegate = self
        usernameField.delegate = self
        phoneField.delegate = self
        emailField.delegate = self
        countryCodeField.delegate = self

        fullNameField.errorMessagePlacement = .bottom
        usernameField.errorMessagePlacement = .bottom
        emailField.errorMessagePlacement = .bottom
        
        let userData = AuthManager.shared.loggedInUser
        self.user = userData ?? User()
        
        usernameField.text = user.username
        emailField.text  = user.email
        phoneField.text = user.phone_number
        fullNameField.text = user.fullname?.capitalized
        languageTextField.text = " "
        
        
        if let imageUrl = user.image, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url)
        }
        
        self.codeButton.setTitle(user.phoneCode, for: .normal)
        self.countryCodeField.text = "+" +  (user.phoneCode ?? "91")
     
        
        //Gender
        if user.gender?.lowercased() == "male" {
            maleButtonAction(maleButton)
        } else if user.gender?.lowercased() == "female" {
            femaleButtonAction(femaleButton)
        }
        self.preferNotToTellButton.isSelected = user.showGender == "0"
        preferNotToTellButton.setTitle(StaticStrings.prefer_not_to_tell.localized(), for: .normal)
        preferNotToTellButton.setTitle(StaticStrings.prefer_not_to_tell.localized(), for: .selected)

        
        //Date Of Birth
        if let dateString = user.dob, dateString.length > 0 {
            dateOfBirthTextField.text = convertDate(date: dateString, from: DateFormat.dateTime, to: DateFormat.mediumDate)
        }
        self.showYourAgeButton.isSelected = user.showDob == "1"
        
        
        //Country
        self.countryTextField.text = user.country?.name
        
        //About
        aboutTextView.text = user.about
        resizeTextView(aboutTextView)
        
        //Preferred Languages
        
        let selectedLanguages = user.preferredLanguagesIDs
        user.preferredLanguages = (languagesArray ?? []).filter({ selectedLanguages.contains($0.id)})
        langaugeCollectionView.reloadData()
        
        fullNameField.placeholder = StaticStrings.fullname.localized()
        usernameField.placeholder = StaticStrings.username.localized()
        phoneField.placeholder = StaticStrings.phone.localized()
        countryTextField.placeholder = StaticStrings.country.localized()
        emailField.placeholder = StaticStrings.email.localized()
        aboutLabel.text = StaticStrings.about.localized()
        dateOfBirthTextField.placeholder = StaticStrings.birth_date.localized()
        languageTextField.placeholder = StaticStrings.preferred_languages.localized()
        countryTextField.placeholder = StaticStrings.country.localized()
        countryCodeField.placeholder = StaticStrings.countryCode.localized()
        aboutTextView.placeholder = StaticStrings.about.localized()
    }
    
    func customeSetup() {
        aboutTextView.delegate = self
        aboutTextView.placeholder = "About"
        aboutTextView.placeholderTextColor = .darkGray
        aboutLabel.isHidden = true
        
        
    }
    
    //MARK:- UIButton Action Method
    
    @IBAction func backButtonAction(_ sender: UIButton){
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cameraButtonAction(_ sender: UIButton){
        view.endEditing(true)
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self, message: AttachmentHandler.Constants.updatePhotoMessage)
        AttachmentHandler.shared.imagePickedBlock = { [weak self](image) in
           self?.profileImage = image
           self?.uploadProfilePicture(userId: AuthManager.shared.loggedInUser?.user_id ?? 0)
        }
        
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton){
        view.endEditing(true)
        if validateFields() {
            updateProfile()
        }
    }
    
    @IBAction func showAgeButtonAction(_ sender: UIButton){
        view.endEditing(true)
        sender.isSelected = !sender.isSelected
        user.showDob = sender.isSelected ? "1" : "0"
    }
    
    @IBAction func showGenderButtonAction(_ sender: UIButton){
        view.endEditing(true)
        sender.isSelected = !sender.isSelected
        user.showGender = sender.isSelected ? "0" : "1"
    }
    
    @IBAction func codeButtonAction(_ sender: UIButton){
        view.endEditing(true)
        openCountryPicker()
    }
    
    @IBAction func femaleButtonAction(_ sender: UIButton){
        view.endEditing(true)
        let layer1 = gradientLayer(femaleView.bounds, .genderView)
        femaleView.layer.insertSublayer(layer1, at: 0)
        maleView.isHidden = true
        femaleView.isHidden = false
        maleButton.backgroundColor = .clear
        femaleButton.setTitleColor(.white, for: .normal)
        maleButton.setTitleColor(.lightGray, for: .normal)
        femaleButton.setImage(UIImage(named: "ic_female_white"), for: .normal)
        maleButton.setImage(UIImage(named: "ic_male_grey"), for: .normal)
        genderImageView.image = UIImage(named: "ic_female_white")
        user.gender = "female"
    }
    
    
    @IBAction func maleButtonAction(_ sender: UIButton){
        view.endEditing(true)
        let layer1 = gradientLayer(maleView.bounds, .genderView)
        maleView.layer.insertSublayer(layer1, at: 0)
        maleView.isHidden = false
        femaleView.isHidden = true
        maleButton.setTitleColor(.white, for: .normal)
        femaleButton.setTitleColor(.lightGray, for: .normal)
        femaleButton.backgroundColor = .clear
        femaleButton.setImage(UIImage(named: "ic_female_grey"), for: .normal)
        maleButton.setImage(UIImage(named: "ic_male_white"), for: .normal)
        genderImageView.image = UIImage(named: "ic_male_white")
        user.gender = "male"
    }
    
    @IBAction func languageButtonAction(_ sender: UIButton){
        view.endEditing(true)
        if let vc = UIStoryboard.profile.get(SelectLanguageViewController.self){
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            vc.presentedFrom = .EditProfile
            vc.preselectedLanguages = user.preferredLanguages
            vc.selectedLanguages = { [weak self] languages in
                if let langauges = languages {
                    self?.user.preferredLanguages = langauges
                    self?.langaugeCollectionView.reloadData()
                }
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    

}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag ==  2, textField.tag == 3 {
            self.phoneErrorLabel.text = ""
            self.stateErrorLabel.text = ""
        }else {
            (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 2:
            self.phoneField.placeholder = ""
            return true
        case 3: //Country
            textField.resignFirstResponder()
            self.view.endEditing(true)
            RPicker.sharedInstance.selectOption(dataArray: countries.map({$0.name ?? ""}), selectedIndex: 0) { (selectedValue, selectedIndex) in
                self.countryTextField.text = selectedValue
                let country = Country(id: self.countries[selectedIndex].id ?? "0", name: selectedValue)
                self.user.country = country
            }
            return false
        case 7: // DOB
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let maxDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            RPicker.selectDate(title: "Birth Date", hideCancel: false, datePickerMode: .date, selectedDate: maxDate, minDate: nil, maxDate: maxDate) { (date) in
                self.dateOfBirthTextField.text = getDateString(from: date, with: DateFormat.mediumDate)
                self.user.dob = getDateString(from: date, with: DateFormat.dateTime)
            }
            return false
        case 101:
            textField.resignFirstResponder()
            self.view.endEditing(true)
            openCountryPicker()
            return false
        default:
            return true
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            user.fullname = textField.text?.trimWhiteSpace
            break
        case 1:
            user.username = textField.text?.trimWhiteSpace
            break
        case 2:
            user.phone_number = textField.text?.trimWhiteSpace
            if user.phone_number?.count == 0 {
                self.phoneField.placeholder = "Phone *"
            }
            break
        case 3:
            user.country?.name = textField.text?.trimWhiteSpace
            break
        case 5:
            user.email = textField.text?.trimWhiteSpace
            break
        case 7:
            user.dob = textField.text?.trimWhiteSpace
            break
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag == 0 { // name field
            if str.length > 64 {
                return false
            }
        } else if textField.tag == 1 { // username field
            if str.length > 32 || string == " " {
                return false
            }
        } else if textField.tag == 2 { // phone field
            if str.length > 10 || string == " " {
                return false
            }
        } else if textField.tag == 5 { // email field
            if str.length > 256 || string == " " {
                return false
            }
        } 
        return true
    }
    
}
extension EditProfileViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.1) {
            self.aboutLabel.isHidden = false
        }
        resizeTextView(textView)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        resizeTextView(textView)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        user.about = textView.text!.trimWhiteSpace
        if user.about?.length == 0 {
            self.aboutLabel.isHidden = true
        }
        resizeTextView(textView)
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard var str = textView.text, let range = Range(range, in: str) else { return true }
        str = str.replacingCharacters(in: range, with: text)
        
       // textView.isScrollEnabled = numLines > 6
        self.view.layoutSubviews()
        resizeTextView(textView)

        return self.textLimit(existingText: textView.text,
                              newText: text,
                              limit: 250)
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
       // textView.isScrollEnabled = false
        user.about = textView.text.trimWhiteSpace
        resizeTextView(textView)
    }
    
    fileprivate func resizeTextView(_ textView: UITextView) {
        var newFrame = textView.frame
        let width = newFrame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        newFrame.size = CGSize(width: width, height: newSize.height)
        aboutTextViewHeight.constant = newFrame.size.height
        
        aboutLabel.isHidden = textView.text.isEmpty
        aboutLabel.textColor = textView.isFirstResponder ? UIColor(named: "ORANGE") : RGBA(165, g: 165, b: 165, a: 1.0)
        aboutSepartor.backgroundColor = textView.isFirstResponder ? UIColor(named: "ORANGE") : UIColor.lightGray
    }
    
    
    private func textLimit(existingText: String?,
                           newText: String,
                           limit: Int) -> Bool {
        let text = existingText ?? ""
        let isAtLimit = text.count + newText.count <= limit
        return isAtLimit
    }
}


extension EditProfileViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (section == 0) ? user.preferredLanguages.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PreferredLanguageCollectionViewCell.self), for: indexPath) as! PreferredLanguageCollectionViewCell
        if indexPath.section == 0 {
            cell.languageLabel.text = user.preferredLanguages[indexPath.item].language
            cell.crossButton.setTitle("X", for: .normal)
            cell.crossButton.isUserInteractionEnabled = true
            cell.languageCrossSelection = { [weak self] in
                self?.view.endEditing(true)
                self?.user.preferredLanguages.remove(at: indexPath.item)
                collectionView.reloadData()
            }
        } else {
            cell.languageLabel.text = "Add"
            cell.crossButton.setTitle("+", for: .normal)
            cell.crossButton.isUserInteractionEnabled = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
        } else {
            languageButtonAction(UIButton())
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            let object  = user.preferredLanguages[indexPath.item].language
            let labelSize = (object ?? "").width(withheight: 35, font: UIFont.systemFont(ofSize: 17)) + 3
            return CGSize(width: collectionView.bounds.width > labelSize ? labelSize : collectionView.bounds.width - 20, height: 35)
        } else {
            let object  = "Add"
            let labelSize = (object).width(withheight: 35, font: UIFont.systemFont(ofSize: 17)) + 3
            return CGSize(width: collectionView.bounds.width > labelSize ? labelSize : collectionView.bounds.width - 20, height: 35)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return (section == 0) ? .init(top: 0, left: 0, bottom: 0, right: 0) : .init(top: 0, left: 12, bottom: 0, right: 0)
    }
    
}

//MARK:- API calling Methods
extension EditProfileViewController {
    
    fileprivate func getCountries() {
        ProgressHud.showActivityLoader()
        WebServices.getCountries { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let data = response?.array {
                self?.countries = data
            }
        }
    }

    fileprivate func uploadProfilePicture(userId: Int) {
        
        guard let image = self.profileImage else { return }
        
        var params = [String: Any]()
        params[kUserId] = "\(userId)"
        let attach = AttachmentInfo(withImage: image, imageName: "\(Date().timeIntervalSince1970).jpg", apiName: "profile_picture")
        WebServices.uploadImage(params: params, files: [attach]) { (response) in
            if let object = response?.object, let imageUrl = object.profile_picture {
                let user = AuthManager.shared.loggedInUser
                user?.image = imageUrl
                AuthManager.shared.loggedInUser = user
                
                QBManager.shared.uploadProfilePic()
                
                self.profileImageView.image = self.profileImage
                self.profileImage = nil
            }
        }
    }
    
    fileprivate func updateProfile() {
        
        var params = [String: Any]()
        params[kName] = user.fullname
        params[kUserId] = user.user_id
        params[kCountryId] = user.country?.id
        params[kAboutMe] = user.about
        
        if (user.gender ?? "").length > 0 {
            params[kGender] = user.gender
        }
        
        if (user.showGender ?? "").length > 0 {
            params[kShowGender] = (user.showGender == "0") ? "no" : "yes"
        }
        
        if (user.dob ?? "").length > 0 {
            let date = getDate(from: user.dob ?? "", with: DateFormat.dateTime)
            params[kDOB] = "\(date.timeIntervalSince1970)"
        }
        
        if (user.showDob ?? "").length > 0 {
            params[kShowDob] = (user.showDob == "0") ? "no" : "yes"
        }
        
        params[kPreferredLanguage] = user.preferredLanguages.compactMap({$0.id})
        
        WebServices.updateProfile(params: params) { [weak self] (response) in
            if let user = response?.object {
                AuthManager.shared.loggedInUser = user
                
                QBManager.shared.updateProfileName()
                
                DispatchQueue.main.async {
                    self?.navigationController?.popViewController(animated: true)
                }
            } else {
                AlertController.alert(title: "", message: response?.message ?? CommonMessage.something_went_wrong.localized())
            }
        }
        
    }
    
    
}
