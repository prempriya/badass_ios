//
//  ProfileViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 27/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var fanLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var availableQuestionsLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    var backCompletion: ((Bool) -> Void)? = nil

    
    fileprivate var profileImage: UIImage?
    var page: Page = Page()
    
    var viewedVideos = [HomeModel]()
    var myVideos = [HomeModel]()
    var myReplyVideos = [HomeModel]()
    var isPlaying = false
    var isFromTab: Bool = true
    
    
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getRecentlyViewedVideosList()
        getMyVideosList()
        getMyReplyList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let layer1 = gradientLayer(imageContainerView.bounds, .view)
        imageContainerView.layer.insertSublayer(layer1, at: 0)
        imageBackgroundView.clipsToBounds = true
        let layer2 = gradientLayer(cameraButton.bounds, .view)
        cameraButton.layer.insertSublayer(layer2, at: 0)
        cameraButton.clipsToBounds = true
        
        let layer3 = gradientLayer(gradientView.bounds, .bottomView)
        gradientView.layer.insertSublayer(layer3, at: 0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customSetup()
        getProfile()
    }
    
    
    //MARK:- Helper  Method
    
    fileprivate func customSetup() {
        tableView.reloadData()
        
        backButton.isHidden = isFromTab
        
        editButton.setTitle("  \(ProfileStrings.editProfile.localized())  ", for: .normal)
        
        let user = AuthManager.shared.loggedInUser
        
        AppDelegate.shared.tabbar?.updateTheCount(user: user)
        
        usernameLabel.text = user?.username
        nameLabel.text = user?.fullname?.capitalized
        
        let ques_counts = (Int(user?.freePoints ?? "0") ?? 0) + (Int(user?.paidPoints ?? "0") ?? 0)
        availableQuestionsLabel.text = StaticStrings.avaialble_questions.localized() + ": " + "\(ques_counts)"
        
        if let imageUrl = user?.image, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url)
            self.backgroundImageView.kf.setImage(with: url)
        }
        
        let votes = ProfileStrings.votes.localized()
        let myFans = ProfileStrings.myFans.localized()
        let followings = ProfileStrings.followings.localized()
        
        let fanCount = user?.fanCount ?? "0"
        let followingCount = user?.followingCount ?? "0"
        let voteCount = user?.voteCount ?? 0
        
        let attrVotes = "\(votes)\n\(voteCount)".getAttributedString(votes, color: .white, font: Fonts.Nunito.regular.font(.small))
        voteLabel.attributedText = attrVotes
        
        let attrFans = "\(myFans)\n\(fanCount)".getAttributedString(myFans, color: .white, font: Fonts.Nunito.regular.font(.small))
        fanLabel.attributedText = attrFans
        
        let attrFollowings = "\(followings)\n\(followingCount)".getAttributedString(followings, color: .white, font: Fonts.Nunito.regular.font(.small))
        followingLabel.attributedText = attrFollowings
        
        tableView.isScrollEnabled = Window_Width == 375
    }
    
    @IBAction func editButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.profile.get(EditProfileViewController.self){
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func settingButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.settings.get(SettingsViewController.self){
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func fansButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.settings.get(FanBaseViewController.self){
            vc.followType = .myFan
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func followingButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.settings.get(FanBaseViewController.self){
            vc.followType = .following
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didSelectOnCameraButton(_ sender: UIButton) {
        view.endEditing(true)
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self, message: AttachmentHandler.Constants.updatePhotoMessage)
        AttachmentHandler.shared.imagePickedBlock = { [weak self](image) in
            self?.profileImage = image
            self?.uploadProfilePicture(userId: AuthManager.shared.loggedInUser?.user_id ?? 0)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.backCompletion?(isPlaying)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
        if indexPath.row == 0 {
            cell.commonLabel.attributedText = "\(ProfileStrings.gallery.localized().uppercased()) (\(ProfileStrings.uploadedVideo.localized()))".getAttributedString(" (\(ProfileStrings.uploadedVideo.localized()))", color: .lightGray, font: Fonts.Nunito.medium.font(.medium))
            cell.myVideos(list: myVideos)
            cell.cellClicked = { indexPath in
                self.openPost(indexPath: indexPath)
            }
            
        } else if indexPath.row == 1 {
            cell.commonLabel.text = ProfileStrings.recentlyViewed.localized().uppercased()
            cell.loadViewedVideos(list: viewedVideos)
            cell.cellClicked = { indexPath in
                self.openPostForRecent(indexPath: indexPath)
            }
        }else {
            cell.commonLabel.text = ProfileStrings.repliedVideos.localized().uppercased()
            cell.loadViewedVideos(list: myReplyVideos)
            cell.cellClicked = { indexPath in
                self.openPostForReplyVideos(indexPath: indexPath)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return self.myVideos.count == 0 ? 0 : 190
        case 1:
            return self.viewedVideos.count == 0 ? 0 : 190
        default:
            return self.myReplyVideos.count == 0 ? 0 : 190
        }
    }
    
    func openPost(indexPath: IndexPath){
        moveToMainThread { [weak self] in
            if let vc = UIStoryboard.main.get(PostViewController.self), let obj = self?.myVideos[indexPath.row] {
                vc.videos.append(obj)
                APPDELEGATE.navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
    func openPostForRecent(indexPath: IndexPath){
        moveToMainThread { [weak self] in
            if let vc = UIStoryboard.main.get(PostViewController.self), let obj = self?.viewedVideos[indexPath.row] {
                vc.videos.append(obj)
                APPDELEGATE.navigationController.pushViewController(vc, animated: true)
                //  self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func openPostForReplyVideos(indexPath: IndexPath){
        moveToMainThread { [weak self] in
            if let vc = UIStoryboard.main.get(ChallengeVideoViewController.self), let obj = self?.myReplyVideos[indexPath.row] {
                vc.videoObj = obj
                APPDELEGATE.navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
}


//MARK:- API Calling Methods
extension ProfileViewController {
    
    fileprivate func uploadProfilePicture(userId: Int) {
        
        guard let image = self.profileImage else { return }
        
        var params = [String: Any]()
        params[kUserId] = "\(userId)"
        let attach = AttachmentInfo(withImage: image, imageName: "\(Date().timeIntervalSince1970).jpg", apiName: "profile_picture")
        WebServices.uploadImage(params: params, files: [attach]) { (response) in
            if let object = response?.object, let imageUrl = object.profile_picture {
                let user = AuthManager.shared.loggedInUser
                user?.image = imageUrl
                AuthManager.shared.loggedInUser = user
                
                QBManager.shared.uploadProfilePic()
                
                self.profileImageView.image = self.profileImage
                self.backgroundImageView.image = self.profileImage
                self.profileImage = nil
            }
        }
    }
    
    
    fileprivate func getProfile() {
        WebServices.getMyProfile { [weak self](response) in
            if let user = response?.object {
                AuthManager.shared.loggedInUser = user
                self?.customSetup()
            }
        }
    }
    
    func getRecentlyViewedVideosList(_ hud: Bool = true){
        
        
        var param = [String: Any]()
        
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getRecentlyViewdVideos(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            if response?.statusCode == 200 {
                if let array = response?.array, let self = self {
                    if self.page.page == 1 {
                        self.viewedVideos.removeAll()
                    }
                    if array.count == self.page.pageSize {
                        self.page.hasNext = true
                    } else {
                        self.page.hasNext = false
                    }
                    self.viewedVideos.append(contentsOf: array)
                    self.tableView.reloadData()
                }
            }  else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    func getMyVideosList(_ hud: Bool = true){
        
        
        var param = [String: Any]()
        
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getMyVideos(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            if response?.statusCode == 200 {
                if let array = response?.array, let self = self , let tableView = self.tableView{
                    if self.page.page == 1 {
                        self.myVideos.removeAll()
                    }
                    if array.count == self.page.pageSize {
                        self.page.hasNext = true
                    } else {
                        self.page.hasNext = false
                    }
                    self.myVideos.append(contentsOf: array)
                    
                    tableView.reloadData()
                }
            }else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    
    func getMyReplyList(_ hud: Bool = true){
           
           
           var param = [String: Any]()
           
           param[kUserId] = AuthManager.shared.loggedInUser?.user_id
           param[kPage] = "\(page.page)"
           param[kPageSize] = "\(page.pageSize)"
           
           if hud {
               ProgressHud.showActivityLoader()
           }
           page.isExecuting = true
           WebServices.getMyReplyVideos(params: param) { [weak self](response) in
               self?.page.isExecuting = false
               ProgressHud.hideActivityLoader()
               if response?.statusCode == 200 {
                   if let array = response?.array, let self = self , let tableView = self.tableView{
                       if self.page.page == 1 {
                           self.myReplyVideos.removeAll()
                       }
                       if array.count == self.page.pageSize {
                           self.page.hasNext = true
                       } else {
                           self.page.hasNext = false
                       }
                       self.myReplyVideos.append(contentsOf: array)
                       
                       tableView.reloadData()
                   }
               }else {
                   AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
               }
           }
       }
    
    
}
