//
//  OtherUserViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Quickblox

class OtherUserViewController: UIViewController {
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var fanLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var startChatButton: UIButton!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var userId = "0"
    var isOther = true
    var blockCompletion: (() -> Void)? = nil
    var page: Page = Page()
    var backCompletion: ((Bool) -> Void)? = nil
    var isPlaying = false
    var otherUser: OtherUser? {
        didSet {
            self.setup()
            self.tableView.reloadData()
        }
    }
    
    var myVideos = [HomeModel]()

    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        getUserData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let layer1 = gradientLayer(imageContainerView.bounds, .view)
        imageContainerView.layer.insertSublayer(layer1, at: 0)
        imageBackgroundView.clipsToBounds = true
        
        let layer3 = gradientLayer(gradientView.bounds, .bottomView)
        gradientView.layer.insertSublayer(layer3, at: 0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !QBChat.instance.isConnected {
            QBManager.shared.loginToChat()
        }
    }
    
    
    //MARK:- Helper  Method
    
    fileprivate func setup() {

        let votes = ProfileStrings.votes.localized()
        let myFans = ProfileStrings.myFans.localized()
        let followings = ProfileStrings.followings.localized()
        
        let fanCount = otherUser?.fan_count ?? "0"
        let followingCount = otherUser?.followings_count ?? "0"
        let voteCount = otherUser?.votes_count ?? 0
        
        let attrVotes = "\(votes)\n\(voteCount)".getAttributedString(votes, color: .white, font: Fonts.Nunito.regular.font(.small))
        voteLabel.attributedText = attrVotes
        
        let attrFans = "\(myFans)\n\(fanCount)".getAttributedString(myFans, color: .white, font: Fonts.Nunito.regular.font(.small))
        fanLabel.attributedText = attrFans
        
        let attrFollowings = "\(followings)\n\(followingCount)".getAttributedString(followings, color: .white, font: Fonts.Nunito.regular.font(.small))
        followingLabel.attributedText = attrFollowings
        
        // Image
        if let imageUrl = otherUser?.profile_pic, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url)
            self.backgroundImageView.kf.setImage(with: url)
        } else {
            self.backgroundImageView.image = nil
        }
        
        //Name
        usernameLabel.text = otherUser?.username
        nameLabel.text = otherUser?.name?.capitalized
     
        //Chat Button
        startChatButton.isHidden = otherUser?.chat_privacy == "0"
    }
    
    
    fileprivate func follow() {
        var params = [String: Any]()
        params[kStatus] = "1"
        params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
        params[kFollowingId] = userId
        
        ProgressHud.showActivityLoader()
        WebServices.followUser(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let response = response, response.statusCode == 200, let self = self {
                self.otherUser?.follow_status = "1"
                
                let fanCount = self.otherUser?.fan_count ?? "0"
                self.otherUser?.fan_count = "\(Int(fanCount)! + 1)"
                self.setup()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    
    fileprivate func unfollow() {
        var params = [String: Any]()
        params[kStatus] = "0"
        params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
        params[kFollowingId] = userId
        
        ProgressHud.showActivityLoader()
        WebServices.followUser(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let response = response, response.statusCode == 200, let self = self {
                self.otherUser?.follow_status = "0"
                
                if Int(self.otherUser?.fan_count ?? "0")! != 0 {
                    let fanCount = self.otherUser?.fan_count ?? "0"
                    self.otherUser?.fan_count = "\(Int(fanCount)! - 1)"
                }
                self.setup()

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    func openPost(indexPath: IndexPath){
        moveToMainThread { [weak self] in
            if let vc = UIStoryboard.main.get(PostViewController.self), let obj = self?.myVideos[indexPath.row] {
                vc.videos.append(obj)
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func backButtonAction(_ sender: UIButton){
        self.backCompletion?(isPlaying)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func chatButtonAction(_ sender: UIButton){
        
        guard let otherQbId = otherUser?.qb_id else {
            return
        }
        
        func sendChatRequest() {
            QBManager.shared.checkForDialog(otherUserId: "\(otherQbId)") { [weak self](success, dialog, isNew) in
                ProgressHud.hideActivityLoader()
                if let self = self, success, let dialog = dialog {

                    if let isResquested = dialog.data?["is_requested"] as? Bool, isResquested, let requesterId = dialog.data?["requester_id"] as? String, requesterId != "\(QBManager.shared.currentUserId)" {
                        AlertController.alert(title: AppName.localized(), message: "You have got chat request from this user. Go to chat screen and accept the request to start the conversation.".localized())
                        return
                    }

                    if let chatVc = UIStoryboard.main.get(MessageViewController.self) {
                        chatVc.chatDialog = dialog
                        self.navigationController?.pushViewController(chatVc, animated: true)
                    }
                    
                    if isNew {
                        let payload = [
                            "message": "\(AuthManager.shared.loggedInUser?.fullname ?? "An user") sent you a request for chat.",
                            "ios_badge": "0",
                            "ios_sound": "default",
                            "ios_content-available": 1,
                            "type": "chat_request",
                            "id": "\(dialog.id ?? "0")",
                            ] as [String : Any]
                        QBManager.shared.sendPush(toIds: [UInt(dialog.recipientID)], payload: payload)
                    }
                    
                } else {
                    AlertController.alert(title: AppName.localized(), message: "Failed to create chat.")
                }
            }
        }
        
        
        ProgressHud.showActivityLoader()
        QBManager.shared.sendContactRequest(qbId: otherQbId) { (status, error) in
            if error == nil {
                sendChatRequest()
            } else {
                ProgressHud.hideActivityLoader()
                Debug.log(error?.localizedDescription ?? "")
                AlertController.alert(title: AppName.localized(), message: "Failed to create chat.")
            }
        }
        
    }
    
    
    @IBAction func fansButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.settings.get(FanBaseViewController.self){
            vc.followType = .myFan
            vc.isOther = true
            vc.userId = userId
            vc.userName = otherUser?.name ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func followingButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.settings.get(FanBaseViewController.self){
            vc.followType = .following
            vc.isOther = true
            vc.userId = userId
            vc.userName = otherUser?.name ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension OtherUserViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAboutTableViewCell") as! ProfileAboutTableViewCell
            
            if let about = otherUser?.about_me, about.count > 0 {
                cell.aboutLabel.text = about
            } else {
                cell.aboutLabel.text = "NA"
            }
            
            
            if let user = otherUser {
                
                if (user.gender?.length ?? 0) > 0, (user.show_gender ?? "1") == "1" {
                    cell.genderButton.isHidden = false
                    cell.genderButton.setTitle(user.gender?.capitalized.localized(), for: .normal)
                } else {
                    cell.genderButton.isHidden = true
                }
                
                
                if (user.dob?.length ?? 0) > 0, (user.show_dob ?? "1") == "1" {
                    cell.dobButton.isHidden = false
                    if let dateString = user.dob, dateString.length > 0 {
                        cell.dobButton.setTitle(convertDate(date: dateString, from: DateFormat.dateTime, to: DateFormat.mediumDate), for: .normal)
                    } else {
                        cell.dobButton.isHidden = true
                    }
                } else {
                    cell.dobButton.isHidden = true
                }
                
            } else {
                cell.genderButton.isHidden = true
                cell.dobButton.isHidden = true
            }
            
            
            if otherUser?.blocked_status == "1" {
                cell.blockButton.setTitle(StaticStrings.Unblock.localized(), for: .normal)
            } else {
                cell.blockButton.setTitle(StaticStrings.block.localized(), for: .normal)
            }
            
            if otherUser?.follow_status == "1" {
                cell.fanIcon.isHidden = true
                cell.fanButton.isHidden = true
                cell.unfollowButton.isHidden = false
            } else {
                cell.fanIcon.isHidden = false
                cell.fanButton.isHidden = false
                cell.unfollowButton.isHidden = true
            }
            
            cell.blockClicked = { [weak self] in
                self?.unBlockUser()
            }
            
            cell.fanClicked = { [weak self] in
                animateFan(image: cell.fanIcon)
                self?.follow()
            }
            
            cell.unfollowClicked = { [weak self] in
                self?.unfollow()
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            cell.commonLabel.attributedText = "\(StaticStrings.Gallery.localized()) (\(StaticStrings.uploadedVideo.localized()))".getAttributedString(" (\(StaticStrings.uploadedVideo.localized()))", color: .lightGray, font: Fonts.Nunito.medium.font(.medium))
            cell.myVideos(list: myVideos)
            cell.cellClicked = { indexPath in
                self.openPost(indexPath: indexPath)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? UITableView.automaticDimension : myVideos.count == 0 ? 0 : 190
    }
  
}

//MARK:- Web API Calls
extension OtherUserViewController {
    
    fileprivate func getUserData() {
        ProgressHud.showActivityLoader()
        WebServices.getOtherUserProfile(otherUserId: userId) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let user = response?.object, let self = self {
                self.otherUser = user
                self.getMyVideosList()

            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    fileprivate func unBlockUser (_ hud: Bool = true) {
        
        var params = [String: Any]()
        params[kUserId] = AuthManager.shared.loggedInUser?.user_id
        params[kBlock_user_id] = userId
        params[kBlock_status] = otherUser?.blocked_status == "0" ? "1" : "0"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        WebServices.blockUnblock(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if response?.statusCode == 200 {
                self?.blockCompletion?()
                self?.otherUser?.blocked_status = self?.otherUser?.blocked_status == "0" ? "1" : "0"
                
                if self?.otherUser?.blocked_status == "1" {
                    QBManager.shared.block(qbId: self?.otherUser?.qb_id ?? "") { (s) in
                        
                    }
                } else {
                    QBManager.shared.unblock(qbId: self?.otherUser?.qb_id ?? "") { (s) in
                        
                    }
                }

                self?.tableView.reloadData()
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }
    
    func getMyVideosList(_ hud: Bool = true){
        
        
        var param = [String: Any]()
       
        param[kUserId] = otherUser?.user_id
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getMyVideos(params: param) { [weak self](response) in
            self?.page.isExecuting = false
           ProgressHud.hideActivityLoader()
             if response?.statusCode == 200 {
            if let array = response?.array, let self = self {
                if self.page.page == 1 {
                    self.myVideos.removeAll()
                }
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                self.myVideos.append(contentsOf: array)
                self.tableView.reloadData()
            }
             }else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    
    
}
