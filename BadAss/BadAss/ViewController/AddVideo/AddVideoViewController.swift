//
//  AddVideoViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 28/07/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Photos
import MediaWatermark

class AddVideoViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowImageView: UIImageView!

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet var videoView: UIView!
    @IBOutlet var galleryLabel: UILabel!
    @IBOutlet weak var lblTimer: UILabel!


    var isCameraForImageMode = false
    var frontCamera = true
    let cameraController = CameraController()
    var videoArray = [URL]()
    var imageArray = [UIImage]()
    var finalURL : URL?
    var videoPickCompletion: ((UIImage,URL) -> Void)? = nil
    var hideGallery = true
    var photos: PHFetchResult<PHAsset>! // user photos array in collectionView for disaplying video thumnail
     var timer = Timer()
     var counter = VideoConstant.videoMaxLength
    
    //MARK:- UIViewController Lifecycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.bottomView.layer.cornerRadius = 20
        self.flashButton.isHidden = true
        
        lblTimer.text = String(format: "00:%02d", counter)
            
        DispatchQueue.global().async {
            self.fetchAllVideos()
        }
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(showGallery))
        self.galleryLabel.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        setInitialDataForCamera()
        lblTimer.text = "00:60"
        lblTimer.isHidden = true
        counter = VideoConstant.videoMaxLength
        startButton.tag = 101
        self.perform(#selector(setInitialDataForCamera), with: nil, afterDelay: 0.3)
        
        //  toggleFlashButton.setImage(UIImage(named: "baseline_flash_auto_white_36pt"), for: .normal)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopTimer()
        self.cameraController.stopSession()
        self.cameraController.previewLayer?.removeFromSuperlayer()
    }
    
    //MARK:- Helper Method
    
    func startVideoTimer() {
           self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
       }
       
       func stopTimer()  {
           self.timer.invalidate()
       }
    
    //MARK:- Target Method
    
    @objc func updateTimerLabel() {
        counter -= 1
        lblTimer.isHidden = false
        lblTimer.text = String(format: "00:%02d", counter)
        UIView.animate(withDuration: 0.001) {
            self.view.layoutIfNeeded()
        }
        print("counter::---\(counter)")
        
        if counter == 0 {
            stopTimer()
            counter = VideoConstant.videoMaxLength
            cameraController.stopRecording { (videoURL, error) in
                if (error == nil) {
                    guard self.getThumbnailImageForVideoAtURL(url: videoURL!) != nil else {
                        print(error ?? "Image thumbnail error")
                        return
                    }
                    ProgressHud.showActivityLoader()
                    
                    // let asset = AVAsset.init(url: videoURL!)
                    DispatchQueue.main.async {
                        if let vc = UIStoryboard.main.get(AddPostVideoViewController.self){
                            vc.videoURL = videoURL
                            vc.thumbnailImage = self.getThumbnailImageForVideoAtURL(url: videoURL! as URL) ?? UIImage()
                            vc.isFromAdd = true
                            vc.backCompletion = { status in
                                self.startButton.tag = 101
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                            ProgressHud.hideActivityLoader()
                        }
                    }
                }
                }
            }
    }
    
    //required functions for custom camera
    @objc func showGallery(){
        if hideGallery {
            hideGallery = false
            UIView.animate(withDuration: 0.1) {
                self.heightConstraint.constant = 35
                self.collectionHeightConstraint.constant = 0
            }
            
        }else {
            hideGallery = true
            UIView.animate(withDuration: 0.1) {
                self.heightConstraint.constant = self.videoArray.count == 0 ? 35 : 155
                self.collectionHeightConstraint.constant = self.videoArray.count == 0 ? 0 : 110
            }
            
        }
    }
    
    func configureCameraController() {
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }
            // Display camera preview..
            try? self.cameraController.displayPreview(on: self.videoView)
        }
    }
    
    
    @objc func setInitialDataForCamera(){
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch status {
        case .authorized:
            self.configureCameraController()
        case .denied,.restricted:
            AlertController.alert(title: "Required Camera Access", message: "Please allow device camera to record video.", buttons: ["Settings","Cancel"]) { (action, index) in
                if(index == 0){
                    if let url = URL(string:UIApplication.openSettingsURLString) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                }
            }
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                DispatchQueue.main.async {
                    if granted {
                        self.configureCameraController()
                    } else {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        @unknown default:
            break
        }
        // audio permission
        let audioStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        switch audioStatus {
        case .authorized:
            print("authorized")
        case .denied,.restricted:
            AlertController.alert(title: "Required Microphone Access", message: "Please allow device microphone to record audio.", buttons: ["Settings","Cancel"]) { (action, index) in
                if(index == 0){
                    if let url = URL(string:UIApplication.openSettingsURLString) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                }else{
                }
                
            }
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: AVMediaType.audio) { granted in
                DispatchQueue.main.async {
                    if granted {
                        print("granted")
                    }
                    else {
                        print("denied")
                    }
                }
            }
        @unknown default:
            break
        }
        
    }
    
    func getThumbnailImageForVideoAtURL(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 2, preferredTimescale: 30)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print(error.localizedDescription)
            return UIImage(named: "record")
        }
        // return nil
    }
    
    func toggleFlash() {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if ((device?.hasTorch) != nil) {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    
    func fetchAllVideos(){
        //let albumName = "blah"
        let fetchOptions = PHFetchOptions()
        //        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        //uncomment this if you want video from custom folder
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d ", PHAssetMediaType.video.rawValue )
        
        let allVideo = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        allVideo.enumerateObjects { (asset, index, bool) in
            // videoAssets.append(asset)
            let imageManager = PHCachingImageManager()
            imageManager.requestAVAsset(forVideo: asset, options: nil, resultHandler: { (asset, audioMix, info) in
                if asset != nil {
                    
                    let avasset = asset as? AVURLAsset
                    print("Duration::-\(String(describing: avasset?.duration.seconds))")
                    if (Int(avasset?.duration.seconds ?? 0.0) >= VideoConstant.videoMinLength)  && (Int(avasset?.duration.seconds ?? 0.0) <= VideoConstant.videoMaxLength)  {
                        if let url = avasset?.url {
                            DispatchQueue.main.async { [weak self] in
                                let img = self?.getThumbnailImageForVideoAtURL(url: url) ?? UIImage()
                                self?.imageArray.append(img)
                                self?.videoArray.append(url)
                            }
                        }
                        delay(delay: 0.1) {
                            self.collectionView.reloadData()
                        }
                    }
                }
            })
            
        }
        
    }
    
    
    // MARK:- UIButton Action Method
    @IBAction func backButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Toogle Camera
    @IBAction func toogleCameraAction(_ sender: Any) {
        do {
            try cameraController.switchCameras()
            frontCamera = !frontCamera
            self.flashButton.isHidden = frontCamera
            
        }
        catch {
            print(error)
        }
    }
    
    @IBAction func saveVideoAction(_ sender: Any) {
        
    }
    
    
    // Capture Image/Video
    @IBAction func captureVideoAction(_ sender: Any) {
        
        if isCameraForImageMode {
            //for image
            cameraController.captureImage {(image, error) in
                guard image != nil else {
                    print(error ?? "Image capture error")
                    return
                }
                // AlertController.alert(message: "cameradone")
                // self.presentCameraPreviewWithOutput(image, nil)
            }
        }
        else{
            // Capture Video
            let button = sender as! UIButton
            if button.tag == 101 {
                // start capture
                  button.setImage(UIImage(named: "ic_stop_button"), for: .normal)
                button.tag = 102
                cameraController.startRecording()
                lblTimer.isHidden = false
                startVideoTimer()
            }
            else if button.tag == 102 {
                // pause capture
                 button.setImage(UIImage(named: "ic_record_button"), for: .normal)
               // cameraController.pauseRecording()
                
                cameraController.stopRecording { (videoURL, error) in
                    if (error == nil) {
                        //                        guard self.getThumbnailImageForVideoAtURL(url: videoURL!) != nil else {
                        //                            print(error ?? "Image thumbnail error")
                        //                            return
                        //                        }
                        ProgressHud.showActivityLoader()
                        
                        // let asset = AVAsset.init(url: videoURL!)
                        DispatchQueue.main.async {
                            
                            if let vc = UIStoryboard.main.get(AddPostVideoViewController.self){
                                vc.videoURL = videoURL
                                vc.thumbnailImage = self.getThumbnailImageForVideoAtURL(url: videoURL! as URL) ?? UIImage()
                                vc.isFromAdd = true
                                
                                vc.backCompletion = { status in
                                    self.startButton.tag = 101
                                }
                                //                                let navController = UINavigationController(rootViewController: vc)
                                //                                navController.setNavigationBarHidden(true, animated: false)
                                //                                navController.modalPresentationStyle = .custom
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                ProgressHud.hideActivityLoader()
                            }
                        }
                        
                        
                        //                        self.addWaterMarkImage(url: videoURL!) { (url) in
                        //                                if let url = url {
                        //                                    //  self.save(url: url)
                        //
                        //                                }
                        //                        }
                    }
                }
                
                // button.tag = 103
                //  cameraController.pauseRecording()
            }
            else if button.tag == 103 {
                // resume capture
                //   button.setImage(UIImage(named: "butn_start"), for: .normal)
                button.tag = 102
                cameraController.resumeRecording()
            }
        }
        
    }
    
    @IBAction func galleryButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func flashButtonAction(_ sender: Any) {
        if !frontCamera {
            toggleFlash()
        }
    }
}

extension AddVideoViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
        
        cell.commonImageView.image = imageArray[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 140, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let url = videoArray[indexPath.row]
        
      //  self.addWaterMarkImage(url: url) { (url) in
        //    if let url = url {
                //  self.save(url: url)
                if let vc = UIStoryboard.main.get(AddPostVideoViewController.self){
                    vc.videoURL = url
                    vc.thumbnailImage =  self.imageArray[indexPath.row]
                    vc.isFromAdd = true
                    vc.backCompletion = { status in
                        self.startButton.tag = 101
                    }
                    vc.videoCompletion = { status in
                        APPDELEGATE.navigationController.dismiss(animated: false, completion: nil)
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
      //      }
      //  }
    }
    
    func save(url: URL) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
        }) { saved, error in
            if saved {
                let alertController = UIAlertController(title: "Your video was successfully saved", message: nil, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}




