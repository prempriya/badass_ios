//
//  AddPostVideoViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 27/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Cloudinary

class AddPostVideoViewController: UIViewController {
    
    @IBOutlet weak var videPlayerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var centerPlayButton: UIButton!
    @IBOutlet weak var titleTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var categoryTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var languageTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var imagePreviewImageView: UIImageView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var postButton: UIButton!
    var file: AttachmentInfo?
    var imageFile = AttachmentInfo()

    var user = User()
    
    var player = AVPlayer()
    var videoURL: URL?
    var videoFinshed = false
    var thumbnailImage = UIImage()
    var isFromAdd = false
    var videoCompletion: ((Bool) -> Void)? = nil
    var backCompletion: ((Bool) -> Void)? = nil
    var languages: [Language] = languagesArray ?? []
    var categories: [Category] = categoriesArray ?? []
    var videoId: String?
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVideoPlayerViewController()
        customSetup()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        player.pause()
    }
    
    //MARK:- Helper Method
    
    func customSetup(){
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.bottomView.layer.cornerRadius = 25
        let layer = gradientLayer(addButton.bounds, .button)
        addButton.layer.insertSublayer(layer, at: 0)
        let layer1 = gradientLayer(postButton.bounds, .button)
        postButton.layer.insertSublayer(layer1, at: 0)
        self.imagePreviewImageView.image = thumbnailImage
        languageTextField.errorMessagePlacement = .bottom
        titleTextField.errorMessagePlacement = .bottom
        categoryTextField.errorMessagePlacement = .bottom
        bottomView.isHidden = !isFromAdd
        postButton.isHidden = isFromAdd
        outerView.isHidden = true
        
        
        
        titleTextField.placeholder = "Title".localized()
        categoryTextField.placeholder = "Category".localized()
        languageTextField.placeholder = "Language".localized()
    }
    
    
    func validateField() -> Bool{
        
        var isValid = false
        if user.videoTitle == nil || user.videoTitle?.length == 0 {
            titleTextField.errorMessage = ValidationMessage.blankTitle.localized()
        }  else if user.videoCategory == nil || user.videoCategory?.length == 0 {
            categoryTextField.errorMessage = ValidationMessage.blankCategory.localized()
        } else if user.language == nil || user.language?.length == 0{
            languageTextField.errorMessage = ValidationMessage.blankLanguage.localized()
        }else {
            isValid = true
        }
        
        return isValid
    }
    
    
    func setUpVideoPlayerViewController() {
        if let videoURL = videoURL {
            let avPlayer = AVPlayer(url: videoURL)
            let avPlayerViewController = AVPlayerViewController()
            player = avPlayer
            player.automaticallyWaitsToMinimizeStalling = automaticallyWaitsToMinimizeStalling

            //new
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
            avPlayerViewController.player = avPlayer
            //                avPlayerViewController.view.contentMode = .scaleAspectFill
            self.addChild(avPlayerViewController)
            avPlayerViewController.view.frame = CGRect(x: videPlayerView.frame.origin.x, y: 0, width: videPlayerView.frame.size.width, height: videPlayerView.frame.size.height)
            avPlayerViewController.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
            avPlayerViewController.showsPlaybackControls = false
            videPlayerView.addSubview(avPlayerViewController.view)
            avPlayerViewController.didMove(toParent: self)
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        player.pause()
        centerPlayButton.setImage(UIImage.init(named: "ic_play"), for: .normal)
        outerView.isHidden = true
        videoFinshed = true
        
    }
    
    //MARK:- UIButton Action Method
    @IBAction func playPauseButtonAction(_ sender: UIButton) {
        outerView.isHidden = false
        if videoFinshed {
            videoFinshed = false
            player.seek(to: .zero)
            player.play()
            centerPlayButton.setImage(UIImage.init(named: "icn_pause"), for: .normal)
        }
        if player.timeControlStatus == .playing  {
            player.pause()
            centerPlayButton.setImage(UIImage.init(named: "ic_play"), for: .normal)
            if isFromAdd{
                bottomView.isHidden = false
            }
        }else {
            if(player.currentItem == nil){
                setUpVideoPlayerViewController()
            }else{
                player.play()
            }
            if isFromAdd{
                bottomView.isHidden = true
            }
            centerPlayButton.setImage(UIImage.init(named: "icn_pause"), for: .normal)
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.backCompletion?(true)
        if isFromAdd {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }else {
            navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func postButtonAction(_ sender: UIButton) {
        
        func showError() {
            ProgressHud.hideActivityLoader()
            AlertController.alert(title: AppName.localized(), message: CommonMessage.something_went_wrong.localized())
        }
        
        ProgressHud.showActivityLoader()
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
        self.compressVideo(inputURL: videoURL! as URL,
                           outputURL: compressedURL) { exportSession in
                            guard let session = exportSession else {
                                return
                            }
                            
                            switch session.status {
                            case .unknown, .waiting, .exporting, .failed, .cancelled:
                                showError()
                                break
                            case .completed:
                                guard let compressedData = try? Data(contentsOf: compressedURL) else {
                                    showError()
                                    return
                                }
                                self.callApiForReplyVideo(data: compressedData as Data, url: self.videoURL!)
                            @unknown default:
                                showError()
                                print("Error")
                            }
                            
        }
        
        
        //        guard let compressedData = NSData(contentsOf: videoURL! )else { return }
        //        self.callApiForReplyVideo(data: compressedData, url: videoURL!)
        
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        
        func showError() {
            ProgressHud.hideActivityLoader()
            AlertController.alert(title: AppName.localized(), message: CommonMessage.something_went_wrong.localized())
        }
        
        if validateField() {
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
            ProgressHud.showActivityLoader()
            self.compressVideo(inputURL: videoURL! as URL,
                               outputURL: compressedURL) { exportSession in
                
                guard let session = exportSession else {
                    showError()
                    return
                }
                switch session.status {
                case .unknown, .waiting, .exporting, .failed, .cancelled:
                    showError()
                    break
                case .completed:
                    guard let compressedData = try? Data(contentsOf: compressedURL) else {
                        showError()
                        return
                    }
//                    self.callApiForSaveVideo(data: compressedData, url: self.videoURL!)
                    self.compressAgain(url: compressedURL)
                @unknown default:
                    showError()
                }
            }
        }
    }
    
    func compressAgain(url: URL) {
        
        func showError() {
            ProgressHud.hideActivityLoader()
            AlertController.alert(title: AppName.localized(), message: CommonMessage.something_went_wrong.localized())
        }
        
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")

        ProgressHud.showActivityLoader()
        self.compressVideo(inputURL: url,
                           outputURL: compressedURL) { exportSession in
            
            guard let session = exportSession else {
                showError()
                return
            }
            switch session.status {
            case .unknown, .waiting, .exporting, .failed, .cancelled:
                showError()
                break
            case .completed:
                guard let compressedData = try? Data(contentsOf: compressedURL) else {
                    showError()
                    return
                }
                self.callApiForSaveVideo(data: compressedData, url: self.videoURL!)
            @unknown default:
                showError()
            }
        }
    }
    
}


extension AddPostVideoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag == 0 { // title field
            if str.length > 256 {
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            user.videoTitle = textField.text?.trimWhiteSpace
        case 1:
            user.videoCategory = textField.text?.trimWhiteSpace
        default:
            user.language = textField.text?.trimWhiteSpace
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
         case 1:
             textField.resignFirstResponder()
             self.view.endEditing(true)
             RPicker.sharedInstance.selectOption(dataArray: categories.map{($0.category?.capitalized ?? "")}, selectedIndex: 0) { (selectedValue, index) in
                self.categoryTextField.text = selectedValue
                self.user.videoCategory = selectedValue
                self.user.videoCategoryID = self.categories.filter{($0.category == selectedValue)}.first?.id
             }
             return false
        case 2:
            textField.resignFirstResponder()
            self.view.endEditing(true)
            RPicker.sharedInstance.selectOption(dataArray: languages.map{($0.language ?? "")}, selectedIndex: 0) { (selectedValue, index) in
               self.languageTextField.text = selectedValue
               self.user.language = selectedValue
               self.user.languageID =  self.languages.filter{($0.language == selectedValue)}.first?.id
            }
            return false
         default:
             return true
         }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
    
}


extension AddPostVideoViewController {

    func callApiForSaveVideo(data:Data, url : URL) {

        var params = [String: Any]()
        params["title"] = user.videoTitle
        params["language_id"] =  self.user.languageID
        params["category_id"] =  self.user.videoCategoryID
        params["user_id"] = AuthManager.shared.loggedInUser?.user_id
        
        self.file = AttachmentInfo(withData: data , fileName: "video.mp4", apiName: "video_file")
        self.imageFile = AttachmentInfo(withImage: UIImage(data: thumbnailImage.jpegData(compressionQuality: 0.5)!)!, imageName: "thumbnail")
        var videoData = self.file ?? AttachmentInfo()
        videoData.apiKey = Constants.kVideo_File
        ProgressHud.showActivityLoader()
        WebServices.saveVideo(params: params, files: [videoData,self.imageFile ], successCompletion: { (resposne) in
            ProgressHud.hideActivityLoader()
            IHProgressHUD.dismiss()
            if let _ = resposne?.object {
                AlertController.alert(title: "", message: "Video posted successfully.".localized(), buttons: [StaticStrings.ok.localized()]) { (alert, index) in
                    APPDELEGATE.tabbar?.selectedIndex = 0
                    
                    if let vc = AppDelegate.shared.tabbar?.viewControllers?.first as? HomeViewController {
                        vc.refreshData()
                    }
                    
                    if let vc = AppDelegate.shared.tabbar?.viewControllers?.filter({ ($0 as? ProfileViewController) != nil }).first as? ProfileViewController {
                        vc.page.page = 1
                        vc.getMyVideosList()
                    }
                    
                    if let vc = AppDelegate.shared.tabbar?.viewControllers?.filter({ ($0 as? HomeViewController) != nil }).first as? HomeViewController {
                        vc.page.page = 1
                        vc.refreshData()
                    }
                                        
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
            }
        }) { progressValue in
            if progressValue != nil {
                ProgressHud.hideActivityLoader()
                IHProgressHUD.show(progress: CGFloat(progressValue ?? 0), status: String(format: "\(StaticStrings.uploading.localized())(%d)", Int((progressValue ?? 0) * 100)) )
            }
        }
    }
    
    
    
    func callApiForReplyVideo(data:Data, url : URL){
        var params = [String: Any]()
        
        params["video_id"] = Int(videoId ?? "0")
        params[kUserId] = AuthManager.shared.loggedInUser?.user_id
        
        self.file = AttachmentInfo(withData: data as Data, fileName: "video.mp4", apiName: "video_file")
        self.imageFile =  AttachmentInfo(withImage: UIImage(data: thumbnailImage.jpegData(compressionQuality: 0.5)!)! , imageName: "thumbnail")
        var videoData = self.file ?? AttachmentInfo()
        videoData.apiKey = Constants.kVideo_File
        ProgressHud.showActivityLoader()
        WebServices.replyVideo(params: params, files: [videoData,self.imageFile ], successCompletion: { (resposne) in
            ProgressHud.hideActivityLoader()
            IHProgressHUD.dismiss()
            if let _ = resposne?.object  {
                AlertController.alert(title: "", message: "Video replied successfully.".localized(), buttons: [StaticStrings.ok.localized()]) { (alert, index) in
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }) { (progressValue) in
            if progressValue != nil {
                ProgressHud.hideActivityLoader()
                IHProgressHUD.show(progress: CGFloat(progressValue ?? 0), status: String(format: "\(StaticStrings.uploading.localized())(%d)", Int((progressValue ?? 0) * 100)) )
            }
        }
        
    }
}


extension AddPostVideoViewController {
    func compressVideo(inputURL: URL,
                       outputURL: URL,
                       handler:@escaping (_ exportSession: AVAssetExportSession?) -> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset,
                                                       presetName: AVAssetExportPreset640x480) else {
                                                        handler(nil)
                                                        return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously {
            handler(exportSession)
        }
    }
}
