//
//  ReplyVideoViewController.swift
//  BadAss
//
//  Created by Prempriya on 25/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import MediaWatermark

class ReplyVideoViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var replyLabel: UILabel!

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet var videoView: UIView!
     @IBOutlet weak var lblTimer: UILabel!
    
    var isCameraForImageMode = false
    var frontCamera = true
    let cameraController = CameraController()
    var replyVideos = [HomeModel]()
    var page: Page = Page()
    var backCompletion: (() -> Void)? = nil

    
    let imagePickerController = UIImagePickerController()
    var videoId: String?
    var timer = Timer()
    var counter = VideoConstant.videoMaxLength
    //MARK:- UIViewController Lifecycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        // Do any additional setup after loading the view.
        getReplyVideosList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setInitialDataForCamera()
        lblTimer.text = "00:00"
        lblTimer.isHidden = true
        counter = VideoConstant.videoMaxLength
        
        //  toggleFlashButton.setImage(UIImage(named: "baseline_flash_auto_white_36pt"), for: .normal)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopTimer()
        self.cameraController.stopSession()
        self.cameraController.previewLayer?.removeFromSuperlayer()
    }
    
    //MARK:- Helper Method
    
    func customSetup(){
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        bottomView.layer.cornerRadius = 20
        flashButton.isHidden = true
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        
        
    }
    
      func startVideoTimer() {
             self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
         }
         
         func stopTimer()  {
             self.timer.invalidate()
         }
      
      //MARK:- Target Method
      
    @objc func updateTimerLabel() {
        counter -= 1
        lblTimer.isHidden = false
        lblTimer.text = String(format: "00:%02d", counter)
        UIView.animate(withDuration: 0.001) {
            self.view.layoutIfNeeded()
        }
      //  print("counter::---\(counter)")
        
        if counter == 0 {
            stopTimer()
            counter = VideoConstant.videoMaxLength
            cameraController.stopRecording { (videoURL, error) in
                if (error == nil) {
                    guard self.getThumbnailImageForVideoAtURL(url: videoURL!) != nil else {
                        print(error ?? "Image thumbnail error")
                        return
                    }
                    moveToMainThread {
                        if let vc = UIStoryboard.main.get(AddPostVideoViewController.self){
                            vc.videoURL = videoURL
                            vc.thumbnailImage = self.getThumbnailImageForVideoAtURL(url: videoURL! as URL) ?? UIImage()
                            vc.videoId = self.videoId
                            vc.backCompletion = { status in
                                self.startButton.tag = 101
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                    }
                }
            }
        }
    }
      
    
    //required functions for custom camera
    func configureCameraController() {
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }
            // Display camera preview..
            try? self.cameraController.displayPreview(on: self.videoView)
        }
    }
    
    
    func setInitialDataForCamera(){
        
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch status {
        case .authorized:
            self.configureCameraController()
        case .denied,.restricted:
            
            AlertController.alert(title: "Required Camera Access", message: "Please allow device camera to record video.", buttons: ["Settings","Cancel"]) { (action, index) in
                if(index == 0){
                    if let url = URL(string:UIApplication.openSettingsURLString) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                }else{
                }
                
            }
            
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                DispatchQueue.main.async {
                    if granted {
                        self.configureCameraController()
                    } else {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        @unknown default:
            break
        }
        // audio permission
        let audioStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        switch audioStatus {
        case .authorized:
            print("authorized")
        case .denied,.restricted:
            AlertController.alert(title: "Required Microphone Access", message: "Please allow device microphone to record audio.", buttons: ["Settings","Cancel"]) { (action, index) in
                if(index == 0){
                    if let url = URL(string:UIApplication.openSettingsURLString) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                }else{
                }
                
            }
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: AVMediaType.audio) { granted in
                DispatchQueue.main.async {
                    if granted {
                        print("granted")
                    }
                    else {
                        print("denied")
                    }
                }
            }
        @unknown default:
            break
        }
        
    }
    
    func getThumbnailImageForVideoAtURL(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        let time = CMTime(seconds: 5, preferredTimescale: 30)
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print(error.localizedDescription)
            return UIImage(named: "record")
        }
        // return nil
    }
    
    func toggleFlash() {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if ((device?.hasTorch) != nil) {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    
    // MARK:- UIButton Action Method
    @IBAction func backButtonAction(_ sender: Any) {
        self.backCompletion?()
        navigationController?.popViewController(animated: true)
    }
    
    // Toogle Camera
    @IBAction func toogleCameraAction(_ sender: Any) {
        do {
            try cameraController.switchCameras()
            frontCamera = !frontCamera
            flashButton.isHidden = frontCamera
        }
        catch {
            print(error)
        }
    }
    
    
    // Capture Image/Video
    @IBAction func captureVideoAction(_ sender: Any) {
        
        if isCameraForImageMode {
            //for image
            cameraController.captureImage {(image, error) in
                guard image != nil else {
                    print(error ?? "Image capture error")
                    return
                }
                // AlertController.alert(message: "cameradone")
                // self.presentCameraPreviewWithOutput(image, nil)
            }
        }
        else{
            // Capture Video
            let button = sender as! UIButton
            if button.tag == 101 {
                // start capture
                button.setImage(UIImage(named: "ic_stop_button"), for: .normal)
                button.tag = 102
                cameraController.startRecording()
                lblTimer.isHidden = false
                startVideoTimer()
            }
            else if button.tag == 102 {
                // pause capture
                // button.setImage(UIImage(named: "icn_stop"), for: .normal)
                
                cameraController.stopRecording { (videoURL, error) in
                    if (error == nil) {
//                        guard self.getThumbnailImageForVideoAtURL(url: videoURL!) != nil else {
//                            print(error ?? "Image thumbnail error")
//                            return
                        }
                     //   self.addWaterMarkImage(url: videoURL!) { (url) in
                         //       if let url = url {
                                    //  self.save(url: url)
                    moveToMainThread {
                        if let vc = UIStoryboard.main.get(AddPostVideoViewController.self){
                            vc.videoURL = videoURL
                            vc.thumbnailImage = self.getThumbnailImageForVideoAtURL(url: videoURL! as URL) ?? UIImage()
                            vc.videoId = self.videoId
                            vc.backCompletion = { status in
                                self.startButton.tag = 101
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                    }

                              //  }
                       // }
                    }
                
                
                // button.tag = 103
                //  cameraController.pauseRecording()
            }
            else if button.tag == 103 {
                // resume capture
                //   button.setImage(UIImage(named: "butn_start"), for: .normal)
                button.tag = 102
                cameraController.resumeRecording()
            }
        }
        
    }
    
    @IBAction func galleryButtonAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.allowsEditing = true
            imagePickerController.mediaTypes = ["public.movie"]
            imagePickerController.videoMaximumDuration = TimeInterval(VideoConstant.videoMaxLength)
            imagePickerController.videoQuality = .typeMedium
            //   imagePickerController.isEditing = true
            present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func flashButtonAction(_ sender: Any) {
        if !frontCamera {
            toggleFlash()
        }
    }
}

extension ReplyVideoViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return replyVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
        cell.loadVideoData(obj: replyVideos[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 160, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = replyVideos[indexPath.row]
        if let vc = UIStoryboard.main.get(ChallengeVideoViewController.self){
            vc.videoObj = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}

extension ReplyVideoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imagePickerController.dismiss(animated: true, completion: nil)
        let videoURL = info[.mediaURL] as? NSURL
        
        if let url = videoURL {
            let asset = AVURLAsset.init(url: url as URL) // AVURLAsset.init(url: outputFileURL as URL) in swift 3
            // get the time in seconds
            let durationInSeconds = asset.duration.seconds
            Debug.log("==== Duration is \(durationInSeconds)")
            
            if Int(durationInSeconds) < VideoConstant.videoMinLength {
                Alerts.shared.show(alert: .warning, message: "Video file is too small. Select video of minimum 5 seconds.", type: .warning)
            }
                
            else if Int(durationInSeconds) > VideoConstant.videoMaxLength {
                Alerts.shared.show(alert: .warning, message: "Video file is too large. Select video of maximum \(VideoConstant.videoMaxLength) seconds.", type: .warning)
            } else {
                DispatchQueue.main.async {
                    if let vc = UIStoryboard.main.get(AddPostVideoViewController.self){
                        vc.videoURL = videoURL as URL?
                        vc.thumbnailImage = self.getThumbnailImageForVideoAtURL(url: videoURL! as URL) ?? UIImage()
                         vc.videoId = self.videoId
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }
        }
    }
    
    
    func getReplyVideosList(_ hud: Bool = true){
        
        var param = [String: Any]()
        
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        param[kvideo_id] = videoId
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getReplyVideos(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            
            if let array = response?.array, let self = self {
                if self.page.page == 1 {
                    self.replyVideos.removeAll()
                }
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                self.replyVideos.append(contentsOf: array)
                self.collectionView.reloadData()
                self.heightConstraint.constant = self.replyVideos.count == 0 ? 10 : 180
                self.replyLabel.isHidden = self.replyVideos.count == 0
            } else {
                self?.heightConstraint.constant = self?.replyVideos.count == 0 ? 10 : 180
                self?.replyLabel.isHidden = self?.replyVideos.count == 0
                // AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
}


