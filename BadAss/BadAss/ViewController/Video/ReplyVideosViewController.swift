//
//  ReplyVideosViewController.swift
//  BadAss
//
//  Created by Dharmendra Sinha on 15/11/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ReplyVideosViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var replyVideos = [HomeModel]()
    var page: Page = Page()
    
    var videoId: String?
    
    //MARK: View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getReplyVideosList()
    }
    
    //MARK:- UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ReplyVideosViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return replyVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
        cell.loadVideoData(obj: replyVideos[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.collectionView.frame.width / 2 - 6 , height: 280)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return .init(top: 0, left: 16, bottom: 0, right: 16)
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = replyVideos[indexPath.row]
        if let vc = UIStoryboard.main.get(ChallengeVideoViewController.self){
            vc.videoObj = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}

//MARK:- Web API Methods
extension ReplyVideosViewController {
    
    func getReplyVideosList(_ hud: Bool = true){
        
        var param = [String: Any]()
        
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        param[kvideo_id] = videoId
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        
        page.isExecuting = true
        WebServices.getReplyVideos(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            if let array = response?.array, let self = self {
                if self.page.page == 1 {
                    self.replyVideos.removeAll()
                }
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                self.replyVideos.append(contentsOf: array)
                self.collectionView.reloadData()
            }
        }
    }
    
}
