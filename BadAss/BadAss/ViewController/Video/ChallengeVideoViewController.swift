//
//  ChallengeVideoViewController.swift
//  GreenEntertainment
//
//  Created by Sunil Joshi on 16/06/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit


class ChallengeVideoViewController: UIViewController{
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var videPlayerView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgViewPreview: UIImageView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var backwardButton: UIButton!
    
    @IBOutlet weak var doubleTapForward: UIButton!
    @IBOutlet weak var doubleTapBackward: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var postBottomView: UIView!
    @IBOutlet weak var centerPlayButton: UIButton!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    //    var delegate: EditProfileViewController!
    var player = AVPlayer()
    var videoFinshed = false
    var viewTouched = false
    var backButtonTapped = false
    var firstTime = true
    var aVAudioPlayer: AVAudioPlayer?
    
    var isFirstVideo = true
    
    var videoObj = HomeModel()
    
    public typealias CompletionBlock = (_ success: Bool, _ data: Data, _ error: Error?) -> Void
    
    let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
    
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        doubleTapForward.addTarget(self, action: #selector(multipleTapForward(_:event:)), for: UIControl.Event.touchDownRepeat)
        doubleTapBackward.addTarget(self, action: #selector(multipleTapBackward(_:event:)), for: UIControl.Event.touchDownRepeat)
        
        if let imageUrl = videoObj.thumbnail, let url = URL(string: imageUrl) {
            self.imgViewPreview.kf.setImage(with: url, placeholder: ImageConstant.videoPlaceholder)
        } else {
            self.imgViewPreview.image = ImageConstant.videoPlaceholder
        }
        self.centerPlayButton.isHidden = false
        
        
        setUpVideoPlayerViewController()
        self.player.play()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        firstTime = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        player.pause()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        self.removeFromParent()
        player.pause()
        self.player.replaceCurrentItem(with: nil)
    }
    
    //MARK:- Helper Method
    func customiseView(){
        
        
    }
    
    func initialMethod(){
        
    }
    
    
    
    func rewindVideo(by seconds: Float64) {
        let currentTime = player.currentTime()
        var newTime = CMTimeGetSeconds(currentTime) - seconds
        if newTime <= 0 {
            newTime = 0
        }
        player.seek(to: CMTime(value: CMTimeValue(newTime * 1000), timescale: 1000))
    }
    
    func forwardVideo(by seconds: Float64) {
        let currentTime = player.currentTime()
        let duration = player.currentItem?.duration
        var newTime = CMTimeGetSeconds(currentTime) + seconds
        if newTime >= CMTimeGetSeconds(duration!) {
            newTime = CMTimeGetSeconds(duration!)
        }
        player.seek(to: CMTime(value: CMTimeValue(newTime * 1000), timescale: 1000))
    }
    
    func setUpVideoPlayerViewController() {
        let videoURl = (URL(string:videoObj.video_url ?? ""))!
        //  if let videoURL = videoObj.video_path{
        
        //   let url = URL(string:videoURL)!
        let avPlayer = AVPlayer(url: videoURl)
        let avPlayerViewController = AVPlayerViewController()
        player = avPlayer
        
        
        
        self.player.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 600), queue: DispatchQueue.main) {
            [weak self] time in
            
            if self?.player.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                
                if let isPlaybackLikelyToKeepUp = self?.player.currentItem?.isPlaybackLikelyToKeepUp {
                    //do what ever you want with isPlaybackLikelyToKeepUp value, for example, show or hide a activity indicator.
                    //  print("isPlaybackLikelyToKeepUp::--\(isPlaybackLikelyToKeepUp)")
                    self?.activityIndicator.isHidden = isPlaybackLikelyToKeepUp
                    self?.activityIndicator.startAnimating()
                    self?.imgViewPreview.isHidden = isPlaybackLikelyToKeepUp
                    
                }
            }
            
            
            if let currentTime = self?.player.currentItem?.currentTime().seconds {
                let currentTime = Int(currentTime)
                let minutes = currentTime/60
                let seconds = currentTime - minutes * 60
                if let totalCurrentTime = (self?.player.currentItem?.duration.seconds){
                    if totalCurrentTime > 0 {
                        let totalCurrentTime = Int(totalCurrentTime)
                        let totalMinutes = totalCurrentTime/60
                        let totalSeconds = totalCurrentTime - totalMinutes * 60
                        self?.timerLabel.text = "\(String(format: "%02d:%02d", minutes,seconds) as String) / \(String(format: "%02d:%02d", totalMinutes,totalSeconds) as String)"
                    }
                }
            }
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        
        
        avPlayerViewController.player = avPlayer
        self.addChild(avPlayerViewController)
        avPlayerViewController.view.frame = CGRect(x: videPlayerView.frame.origin.x, y: 0, width: videPlayerView.frame.size.width, height: videPlayerView.frame.size.height)
        //  avPlayerViewController.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
        avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        avPlayerViewController.showsPlaybackControls = false
        videPlayerView.addSubview(avPlayerViewController.view)
        avPlayerViewController.didMove(toParent: self)
    }
    
    //MARK:- Target Method
    @objc func multipleTapForward(_ sender: UIButton, event: UIEvent) {
        //        setUpVideoPlayerViewController()
        outerView.isHidden = false
        
        let touch: UITouch = event.allTouches!.first!
        if (touch.tapCount == 2) {
            if(!videoFinshed){
                self.forwardVideo(by: 5.0)
            }
            if(player.timeControlStatus == .playing){
                player.play()
                //   playPauseButton.setImage(UIImage.init(named: "icn_play"), for: .normal)
            }else{
                player.pause()
            }
            
        }
    }
    
    @objc func multipleTapBackward(_ sender: UIButton, event: UIEvent) {
        //  setUpVideoPlayerViewController()
        outerView.isHidden = false
        
        
        if(videoFinshed){
            videoFinshed = false
        }
        self.rewindVideo(by: 5.0)
        backButtonTapped = true
        if(player.timeControlStatus == .playing){
            player.play()
            backButtonTapped = false
        }else{
            player.pause()
        }
        
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        player.pause()
        //  playPauseButton.setImage(UIImage.init(named: "icn_play"), for: .normal)
        print("Video Finished")
        videoFinshed = true
        
    }
    
    
    //MARK:- UIButton Action Method
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func centerPlayButtonAction(_ sender: UIButton) {
        outerView.isHidden = false
        firstTime = false
        
        //        if videoType == .myPost || videoType == .myTrophy{
        if(player.timeControlStatus == .playing){
            player.pause()
            //   self.centerPlayButton.setImage(UIImage(named:"icn_play"), for: .normal)
        }else{
            player.play()
            //   self.centerPlayButton.setImage(UIImage(named:"icn_pause"), for: .normal)
        }
        //        }else{
        //            player.play()
        //            centerPlayButton.isHidden = true
        //           // playPauseButton.setImage(UIImage.init(named: "icn_pause"), for: .normal)
        //        }
        
    }
    @IBAction func playPauseButtonAction(_ sender: UIButton) {
        outerView.isHidden = false
        firstTime = false
        self.centerPlayButton.isHidden = true
        if(videoFinshed && !backButtonTapped){
            videoFinshed = false
            player.play()
            // playPauseButton.setImage(UIImage.init(named: "icn_pause"), for: .normal)
        }
        if player.timeControlStatus == .playing  {
            player.pause()
            //  playPauseButton.setImage(UIImage.init(named: "icn_play"), for: .normal)
        }else {
            backButtonTapped = false
            if(player.currentItem == nil){
                setUpVideoPlayerViewController()
            }else{
                player.play()
            }
            //  playPauseButton.setImage(UIImage.init(named: "icn_pause"), for: .normal)
        }
    }
    
    @IBAction func forwardButtonAction(_ sender: Any) {
        outerView.isHidden = false
        
        
        if(!videoFinshed){
            self.forwardVideo(by: 5.0)
        }
        if(player.timeControlStatus == .playing){
            player.play()
        }else{
            player.pause()
        }
    }
    
    @IBAction func backwardButtonAction(_ sender: UIButton) {
        outerView.isHidden = false
        
        if(videoFinshed){
            videoFinshed = false
        }
        self.rewindVideo(by: 5.0)
        backButtonTapped = true
        if(player.timeControlStatus == .playing){
            player.play()
            backButtonTapped = false
        }else{
            player.pause()
        }
    }
    
}



