//
//  SearchViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    var page: Page = Page()
    var searchObj = SearchModel() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.showBackgroundLabel()
            }
        }
    }
    
    lazy var backgroundLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "No data found".localized()
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func removeData(){
        self.searchObj.users.removeAll()
        searchObj.videos.removeAll()
    }
    
    func customSetup(){
        tableView.register(UINib.init(nibName: "BeAFanTableViewCell", bundle: nil), forCellReuseIdentifier: "BeAFanTableViewCell")
        searchTextField.returnKeyType = .search
    }
    
    
    private func showBackgroundLabel() {
        if searchObj.videos.count == 0, searchObj.users.count == 0 {
            self.tableView.backgroundView = backgroundLabel
        } else {
            self.tableView.backgroundView = nil
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton){
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
}

extension SearchViewController: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return searchObj.videos.count == 0 ? 0 : 1
        } else {
            return searchObj.users.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        } else {
            if searchObj.users.count == 0 {
                return nil
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleTableViewCell") as! PeopleTableViewCell
                return cell.contentView
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            if searchObj.users.count == 0 {
                return 0
            } else {
                return 35
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchVideoTableViewCell") as! SearchVideoTableViewCell
            cell.loadVideos(arr: self.searchObj.videos)
            cell.cellClicked = { index in
                moveToMainThread { [weak self] in
                    if let vc = UIStoryboard.main.get(PostViewController.self){
                        vc.videoId = self?.searchObj.videos[indexPath.row].video_id ?? 0
                        vc.isFromNotification = true
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            return cell
        case 1:
            let cell =  tableView.dequeueReusableCell(withIdentifier: "BeAFanTableViewCell") as! BeAFanTableViewCell
            cell.fanButton.isHidden = self.searchObj.users[indexPath.row].follow_status == "1"
            cell.fanView.isHidden = self.searchObj.users[indexPath.row].follow_status == "1"
            
            cell.loadSuggestedUser(user: self.searchObj.users[indexPath.row])

            cell.fanClicked = { [weak self] in
                animateFan(image: cell.fanImageView)
                self?.follow(userId: "\(self?.searchObj.users[indexPath.row].user_id ?? 0)", index: indexPath.row)
            }
            
            if searchObj.users[indexPath.row].show_my_profile ?? "0" == "0" {
                cell.fanButton.isHidden = true
                cell.fanView.isHidden = true
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 210
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            return
        } else if indexPath.section == 1, searchObj.users.count > 0 {
            if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
                
                if searchObj.users[indexPath.row].show_my_profile ?? "0" == "0" {
                    return
                }
                
                vc.isOther = true
                vc.userId = "\(searchObj.users[indexPath.row].user_id ?? 0)"
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        removeData()
        textField.resignFirstResponder()
        if textField.text?.count ?? 0 > 0 {
            getSearchData(text: textField.text ?? "")
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        removeData()
        textField.resignFirstResponder()
        if textField.text?.count ?? 0 > 0 {
            getSearchData(text: textField.text ?? "")
        }
    }
}

extension SearchViewController {
    func getSearchData(_ hud: Bool = true , text: String){
        var param = [String: Any]()
        param[kUserId] = AuthManager.shared.loggedInUser?.user_id
        param["search_key"] = text
        param["type"] = "all"
        param[kPage] = "\(page.page)"
        param[kPageSize] = "\(page.pageSize)"
        
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        
        WebServices.searchData(params: param) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            
            if let obj = response?.object , response?.statusCode == 200, let self = self {
                self.searchObj = obj
                if self.page.page == 1 {
                    // self.notifications.removeAll()
                }
                DispatchQueue.main.async { [weak self] in
                    self?.tableView.reloadData()
                }
            } else if response?.statusCode == 200 {
                self?.searchObj = SearchModel()
                DispatchQueue.main.async { [weak self] in
                    self?.tableView.reloadData()
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
                self?.searchObj = SearchModel()
                DispatchQueue.main.async { [weak self] in
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    fileprivate func follow(userId: String, index: Int) {
        var params = [String: Any]()
        params[kStatus] = "1"
        params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
        params[kFollowingId] = userId
        
        ProgressHud.showActivityLoader()
        WebServices.followUser(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let response = response, response.statusCode == 200, let self = self {
                self.searchObj.users[index].follow_status = "1"
                self.tableView.reloadData()
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
}
