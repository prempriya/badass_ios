//
//  FollowingViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class FollowingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var hasMoreFollowings: Bool = false
    
    var isOther = false
    var userId = "0"
    var userName = ""
    
    var followingUsers: [FollowedUser] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    var suggestedUsers: [SuggestedUser] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
 
    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        // Do any additional setup after loading the view.
        getFollowingList()
        getUserSuggestions()
    }
    
    //MARK:- Helper Method

    func customSetup(){
        tableView.register(UINib.init(nibName: String(describing: FanTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: FanTableViewCell.self))
        tableView.register(UINib.init(nibName: String(describing: BeAFanTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: BeAFanTableViewCell.self))
    }

    
    fileprivate func follow(userId: String, indexPath: IndexPath) {
        var params = [String: Any]()
        params[kStatus] = "1"
        params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
        params[kFollowingId] = userId
        
        ProgressHud.showActivityLoader()
        WebServices.followUser(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let response = response, response.statusCode == 200, let self = self {
                self.suggestedUsers.remove(at: indexPath.row)
                self.getFollowingList(false)
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    
    fileprivate func unfollow(userId: String, indexPath: IndexPath) {
        var params = [String: Any]()
        params[kStatus] = "0"
        params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
        params[kFollowingId] = userId
        
        ProgressHud.showActivityLoader()
        WebServices.followUser(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let response = response, response.statusCode == 200, let self = self {
                self.followingUsers.remove(at: indexPath.row)
                if self.followingUsers.count < 3 {
                    self.getFollowingList(false)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    @objc func viewAllFollowings() {
        if let vc = UIStoryboard.settings.get(AllFollowingsViewController.self) {
            vc.isOther = isOther
            vc.userId = userId
            vc.userName = userName
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}


//MARK:- UITableView Cell

extension FollowingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if section == 1 {
            return nil
        }
        
        if hasMoreFollowings == false {
            return nil
        }

        let view = UIView(frame: CGRect(x: 100, y: 0, width: tableView.bounds.width - 16, height: 60))
        let button = UIButton(frame: view.bounds)
        button.setTitle(StaticStrings.view_all.localized(), for: .normal)
        button.setTitleColor(UIColor(named: "YELLOW"), for: .normal)
        button.titleLabel?.font = Fonts.Nunito.regular.font(.medium)
        button.contentHorizontalAlignment = .right
        
        button.addTarget(self, action: #selector(viewAllFollowings), for: .touchUpInside)
        
        view.backgroundColor = .black
        view.addSubview(button)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        
        if hasMoreFollowings == false {
            return 0
        }

        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            return nil
        }
        
        if suggestedUsers.count == 0 {
            return nil
        }
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width - 32, height: 60))
        let button = UIButton(frame: CGRect(x: 16, y: 0, width: tableView.bounds.width - 32, height: 60))
        button.setTitle(StaticStrings.see_all_suggestions.localized(), for: .normal)
        button.setTitleColor(RGBA(126, g: 126, b: 126, a: 1.0), for: .normal)
        button.titleLabel?.font = Fonts.Nunito.bold.font(.large)
        button.contentHorizontalAlignment = .left
        view.backgroundColor = .black
        view.addSubview(button)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        
        if suggestedUsers.count == 0 {
            return 0
        }
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return followingUsers.count
        default:
            return suggestedUsers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FanTableViewCell") as! FanTableViewCell
            cell.widthConstraint.constant = 85
            cell.followButton.setTitle(StaticStrings.unfollow.localized(), for: .normal)
            let user = followingUsers[indexPath.row]
            cell.loadUser(user: user)
            cell.followClicked = { [weak self] in
                self?.unfollow(userId: "\(user.user_id ?? 0)", indexPath: indexPath)
            }
            cell.followButton.isHidden = isOther
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BeAFanTableViewCell") as! BeAFanTableViewCell
            let user = suggestedUsers[indexPath.row]
            cell.loadSuggestedUser(user: user)
            cell.fanClicked = { [weak self] in
                animateFan(image: cell.fanImageView)
                self?.follow(userId: "\(user.user_id ?? 0)", indexPath: indexPath)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
                vc.userId = "\(self.followingUsers[indexPath.row].user_id ?? 0)"
                vc.isOther = isOther
                vc.blockCompletion = {
                    self.getFollowingList(false)
                    self.getUserSuggestions()
                }
                navigationController?.pushViewController(vc, animated: true)
            }
        default:
            if !manageProfilePrivacy(isFollowing: false, showMyProfile: suggestedUsers[indexPath.row].show_my_profile == "0") {
                if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
                    vc.isOther = isOther
                    vc.userId = "\(suggestedUsers[indexPath.row].user_id ?? 0)"
                    vc.blockCompletion = {
                        self.getFollowingList(false)
                        self.getUserSuggestions()
                    }
                    navigationController?.pushViewController(vc, animated: true)
                }
            }        }
    }
    
    
}



//MARK:- Web API calls

extension FollowingViewController {
 
    fileprivate func getFollowingList (_ hud: Bool = true) {
        
        var params = [String: Any]()
        params[kUserId] = isOther ? userId : AuthManager.shared.loggedInUser?.user_id
        params[kPage] = "1"
        params[kPageSize] = "5"
        if hud {
            ProgressHud.showActivityLoader()
        }
        WebServices.getFollowingList(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let array = response?.array, let self = self {
                
                if array.count == 5 {
                    self.hasMoreFollowings = true
                }
                
                self.followingUsers = array
            } else if response?.statusCode == 200 {
                
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }
    
    
    
    fileprivate func getUserSuggestions() {
        
        var params = [String: Any]()
        params[kUserId] = AuthManager.shared.loggedInUser?.user_id
        
        WebServices.getUserSuggestions(params: params) { [weak self](response) in
            if let array = response?.array, let self = self {
                self.suggestedUsers = array
            } else if response?.statusCode == 200 {
                
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }
    
}
