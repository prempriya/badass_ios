//
//  FanViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class FanViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var isOther = false
    var userId = "0"
    var fanUsers: [FollowedUser] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    var page: Page = Page()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        getFansList()
    }
    
    func customSetup(){
        page.pageSize = 20
        tableView.register(UINib.init(nibName: "FanTableViewCell", bundle: nil), forCellReuseIdentifier: "FanTableViewCell")
    }
    
}

extension FanViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fanUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FanTableViewCell") as! FanTableViewCell
        cell.followButton.isHidden = true
        let user = fanUsers[indexPath.row]
        cell.loadUser(user: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
            vc.userId = String(fanUsers[indexPath.row].user_id ?? 0)
            vc.blockCompletion = {
                self.getFansList()
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- Scroll View
      
      func scrollViewDidScroll(_ scrollView: UIScrollView) {
          
          let currentOffset = scrollView.contentOffset.y
          let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
          
          if page.isExecuting {
              return
          }
          
          if currentOffset - maximumOffset >= 15.0 {
              if self.page.hasNext {
                  self.page.page = self.page.page + 1
                  tableView.tableFooterView = getViewWithActivityIndicatot(withHeight : 60)
                  getFansList(false)
              }
          }
      }
    
}


//MARK:- Web API calls

extension FanViewController {
 
    fileprivate func getFansList (_ hud: Bool = true) {
        
        var params = [String: Any]()
        params[kUserId] = isOther ? userId : AuthManager.shared.loggedInUser?.user_id
        params[kPage] = "\(page.page)"
        params[kPageSize] = "\(page.pageSize)"
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getFansList(params: params) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            self?.tableView.tableFooterView = UIView()
            if let array = response?.array, let self = self {
                
                if self.page.page == 1 {
                    self.fanUsers.removeAll()
                }
                
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                
                self.fanUsers.append(contentsOf: array)
            } else if response?.statusCode == 200 {
                
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }

}
