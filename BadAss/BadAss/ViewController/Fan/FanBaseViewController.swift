//
//  FanBaseViewController.swift
//  BadAss
//
//  Created by Prateek Keshari on 16/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

enum FollowType {
    case myFan
    case following
}


class FanBaseViewController: UIViewController {
    
    @IBOutlet weak var fanLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var fanView: UIView!
    @IBOutlet weak var followingView: UIView!
    @IBOutlet weak var navLabel: UILabel!
    @IBOutlet weak var fanBtn: UIButton!
    
    var followType: FollowType = .myFan
    var isOther = false
    var userId = "0"
    var userName = ""
    
    @IBOutlet weak var frameView: UIView! {
        willSet {
            self.addChild(self.pageMaster)
            newValue.addSubview(self.pageMaster.view)
            newValue.fitToSelf(childView: self.pageMaster.view)
            self.pageMaster.didMove(toParent: self)
        }
    }
    
    private var viewControllerList: [UIViewController] = [
        
    ]
    
    let pageMaster = PageMaster([])
    var selectedIndex: Int = 0
    
    //MARK:- UIViewController Lifecycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Methods
    
    private func setupPageViewController() {
        self.pageMaster.pageDelegate = self
        self.pageMaster.setup(viewControllerList)
    }
    
    private func setButtonActionForSegment(index: Int) {
        
        switch index {
        case 0:
            self.fanView.isHidden = false
            self.followingView.isHidden = true
            self.selectedIndex = index
            pageMaster.setPage(index, animated: true)
            break
        case 1:
            self.fanView.isHidden = true
            self.followingView.isHidden = false
            self.selectedIndex = index
            pageMaster.setPage(index, animated: true)
            break
        default:
            break
        }
    }
    
    func customSetup(){
        
        navLabel.text = AuthManager.shared.loggedInUser?.fullname?.capitalized
        if isOther {
            navLabel.text = userName
            fanLabel.text = StaticStrings.fan.localized().uppercased()
        }
        
        let fanViewController = UIStoryboard.settings.get(FanViewController.self)!
        let followingViewController = UIStoryboard.settings.get(FollowingViewController.self)!
        followingViewController.isOther =  isOther
        fanViewController.isOther =  isOther
        fanViewController.userId = userId
        followingViewController.userId = userId
        followingViewController.userName = userName
        viewControllerList = [fanViewController, followingViewController]
        setupPageViewController()
        setButtonActionForSegment(index: self.followType == .myFan ? 0 : 1)
    }
    
    //MARK:- UIButton Action Method
    
    @IBAction func backButtonAction(_ sender: UIButton){
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func settingButtonAction(_ sender: UIButton){
        if let vc = UIStoryboard.settings.get(SettingsViewController.self){
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnTypeButton(_ sender: UIButton) {
        setButtonActionForSegment(index: sender.tag)
    }
    
}

// MARK: - PageMasterDelegate
extension FanBaseViewController: PageMasterDelegate {
    
    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        self.selectedIndex = page
        self.setButtonActionForSegment(index: page)
        switch page {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        default:
            break
        }
    }
    
}

extension UIView {
    func fitToSelf(childView: UIView) {
        childView.translatesAutoresizingMaskIntoConstraints = false
        let bindings = ["childView": childView]
        self.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat : "H:|[childView]|",
                options          : [],
                metrics          : nil,
                views            : bindings
        ))
        self.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat : "V:|[childView]|",
                options          : [],
                metrics          : nil,
                views            : bindings
        ))
    }
}
