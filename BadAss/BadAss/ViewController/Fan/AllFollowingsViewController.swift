//
//  AllFollowingsViewController.swift
//  BadAss
//
//  Created by DAY on 06/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class AllFollowingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var isOther = false
    var userId = "0"
    var userName = ""
    
    var followingUsers: [FollowedUser] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    var page: Page = Page()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        getFollowingList(true)
    }
    
    func customSetup(){
        
        titleLabel.text = AuthManager.shared.loggedInUser?.fullname?.capitalized
        if isOther {
            titleLabel.text = userName
        }
        
        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: String(describing: FanTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: FanTableViewCell.self))
    }
    
    //MARK:- Helper Methods
    fileprivate func unfollow(userId: String, indexPath: IndexPath) {
        var params = [String: Any]()
        params[kStatus] = "0"
        params[kFollowerId] = AuthManager.shared.loggedInUser?.user_id
        params[kFollowingId] = userId
        
        ProgressHud.showActivityLoader()
        WebServices.followUser(params: params) { [weak self](response) in
            ProgressHud.hideActivityLoader()
            if let response = response, response.statusCode == 200, let self = self {
                self.followingUsers.remove(at: indexPath.row)
                if self.followingUsers.count < 3 {
                    self.getFollowingList(false)
                }
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
    }
    
    

    //MARK:- UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followingUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FanTableViewCell") as! FanTableViewCell
        cell.widthConstraint.constant = 85
        cell.followButton.setTitle("Unfollow", for: .normal)
        let user = followingUsers[indexPath.row]
        cell.loadUser(user: user)
        cell.followClicked = { [weak self] in
            self?.unfollow(userId: "\(user.user_id ?? 0)", indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard.profile.get(OtherUserViewController.self){
            vc.isOther = isOther
            vc.userId = "\(followingUsers[indexPath.row].user_id ?? 0)"
            vc.blockCompletion = {
                self.getFollowingList(false)
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- Scroll View
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard scrollView.isDragging else {
            return
        }
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if page.isExecuting {
            return
        }
        
        if currentOffset - maximumOffset >= 15.0 {
            if self.page.hasNext {
                self.page.page = self.page.page + 1
                tableView.tableFooterView = getViewWithActivityIndicatot(withHeight : 60)
                getFollowingList(false)
            }
        }
    }
    
    
    //MARK:- UIButton Actions
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    
}



//MARK:- Web API calls

extension AllFollowingsViewController {
 
    fileprivate func getFollowingList (_ hud: Bool = true) {
        
        var params = [String: Any]()
        params[kUserId] = isOther ? userId : AuthManager.shared.loggedInUser?.user_id
        params[kPage] = "\(page.page)"
        params[kPageSize] = "\(page.pageSize)"
        if hud {
            ProgressHud.showActivityLoader()
        }
        page.isExecuting = true
        WebServices.getFollowingList(params: params) { [weak self](response) in
            self?.page.isExecuting = false
            ProgressHud.hideActivityLoader()
            self?.tableView.tableFooterView = UIView()
            if let array = response?.array, let self = self {
                
                if self.page.page == 1 {
                    self.followingUsers.removeAll()
                }
                
                if array.count == self.page.pageSize {
                    self.page.hasNext = true
                } else {
                    self.page.hasNext = false
                }
                
                self.followingUsers.append(contentsOf: array)
            } else if response?.statusCode == 200 {
                self?.page.hasNext = false
            } else {
                AlertController.alert(title: AppName, message: response?.message ?? CommonMessage.something_went_wrong)
            }
        }
        
    }

}
