//
//  GridVideoCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 08/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class GridVideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var fanLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var commonImageView: UIImageView!
    @IBOutlet weak var voteButton: UIButton!
    
    
    var voteClicked : (() -> Void)? = nil
    var shareClicked : (() -> Void)? = nil
    var playClicked : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let layer1 = gradientLayer(bottomView.bounds, .bottomView)
        bottomView.layer.insertSublayer(layer1, at: 0)
    }
    
    
    func loadVideo(obj: HomeModel){
        
        voteLabel.attributedText = getTextWithImage(startString: "", imageName: "ic_vote_count", lastString: " \(obj.vote_count ?? "0")", imageAddtionalSize: 5)
        fanLabel.attributedText = getTextWithImage(startString: "", imageName: "ic_fan_count", lastString: " \(obj.creator?.fan_count ?? "0")", imageAddtionalSize: 5)
        if let imageUrl = obj.thumbnail, let url = URL(string: imageUrl) {
            self.commonImageView.kf.setImage(with: url, placeholder: ImageConstant.thumbnailPlaceholder)
        } else {
            self.commonImageView.image = ImageConstant.thumbnailPlaceholder
        }
        
        if manageProfilePrivacy(isFollowing: false, showMyProfile: obj.creator?.show_profile == "0") {
            self.userImageView.image = ImageConstant.userPlaceholder
            self.usernameLabel.text = StaticStrings.anonymous.localized()
        } else {
            usernameLabel.text = obj.creator?.name
            if let imageUrl = obj.creator?.profile_pic, let url = URL(string: imageUrl) {
                self.userImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
            } else {
                self.userImageView.image = ImageConstant.userPlaceholder
            }
        }
    }
    
    
    @IBAction func voteButtonAction(_ sender: UIButton){
        self.voteClicked?()
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton){
        self.shareClicked?()
    }
    
    @IBAction func playButtonAction(_ sender: UIButton){
        self.playClicked?()
    }
    
    
}
