//
//  LanguageCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 01/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class LanguageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    
    
}
