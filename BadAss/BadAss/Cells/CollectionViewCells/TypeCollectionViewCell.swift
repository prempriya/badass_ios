//
//  TypeCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 01/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var commonImageView: UIImageView!
    @IBOutlet weak var cellView: UIView!
}
