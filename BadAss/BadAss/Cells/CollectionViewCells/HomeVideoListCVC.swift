//
//  ChallengeListCollectionViewCell.swift
//  GreenEntertainment
//
//  Created by Prateek Keshari on 17/06/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import AVKit


class HomeVideoListCVC: UICollectionViewCell {
   
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var videoPlayerView: UIView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    
    var messageClickCompletion:(()  -> Void)? = nil
    var shareClickCompletion:(()  -> Void)? = nil
    var followClickCompletion:(()  -> Void)? = nil
    var donateClickCompletion:(()  -> Void)? = nil
    var feedClickCompletion:(()  -> Void)? = nil
    var shootClickCompletion:(()  -> Void)? = nil
    var profileClickCompletion:(()  -> Void)? = nil
    var player = AVPlayer()
    var isPlayed = false
    let avPlayerViewController = AVPlayerViewController()

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avPlayerViewController.view.frame = CGRect(x: videoPlayerView.frame.origin.x, y: 0, width: videoPlayerView.frame.size.width, height:  videoPlayerView.frame.size.height)
        avPlayerViewController.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
        avPlayerViewController.showsPlaybackControls = false
        videoPlayerView.addSubview(avPlayerViewController.view)
    }
    
    
    
    
    @objc func playerDidFinishPlaying(){
    }
    
    func setPlayer(object: HomeModel) {
        self.player = object.player ?? AVPlayer()
        self.player.automaticallyWaitsToMinimizeStalling = automaticallyWaitsToMinimizeStalling
        self.avPlayerViewController.player = self.player
        //   print("status",object.player)
        avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        //  self.avPlayerViewController.player?.currentItem?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
        
        self.avPlayerViewController.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 600), queue: DispatchQueue.main) {
            [weak self] time in
            
            if self?.avPlayerViewController.player?.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                
                if let isPlaybackLikelyToKeepUp = self?.avPlayerViewController.player?.currentItem?.isPlaybackLikelyToKeepUp {
                    
                }
            }
        }
    }
    
    //video player methods
    func setUpVideoPlayerViewController(cell:HomeVideoListCVC) {
    }

    
    
    @IBAction func messageButtonAction(_ sender: UIButton){
        self.messageClickCompletion?()
    }
    @IBAction func feedButtonAction(_ sender: UIButton){
        self.feedClickCompletion?()
    }
    @IBAction func shootButtonAction(_ sender: UIButton){
        self.shootClickCompletion?()
    }
    @IBAction func shareButtonAction(_ sender: UIButton){
        self.shareClickCompletion?()
    }
    @IBAction func donateButtonAction(_ sender: UIButton){
        self.donateClickCompletion?()
    }
    @IBAction func followButtonAction(_ sender: UIButton){
        self.followClickCompletion?()
    }
    @IBAction func profileButtonAction(_ sender: UIButton){
        self.profileClickCompletion?()
    }
}



