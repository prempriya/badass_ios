//
//  PreferredLanguageCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 15/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class PreferredLanguageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var languageView: UIView!
    var languageCrossSelection: (() -> Void)? = nil
    
    @IBAction func crossButtonAction(_ sender: UIButton){
        languageCrossSelection?()
    }
}
