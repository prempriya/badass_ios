//
//  VideoCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 11/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class VideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var commonImageView: UIImageView!
    
    
    var playClicked : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        delay(delay: 0.1) {
            let layer1 = gradientLayer(self.bottomView.bounds, .bottomView)
            self.bottomView.layer.insertSublayer(layer1, at: 0)
            self.bottomView.alpha = 0.7
        }
        
        
    }
    
    func loadVideoData(obj: HomeModel){
        if let imageUrl = obj.thumbnail, let url = URL(string: imageUrl) {
            self.commonImageView.kf.setImage(with: url, placeholder: ImageConstant.videoPlaceholder)
        } else {
            self.commonImageView.image = ImageConstant.videoPlaceholder
        }
    }
    
    
    
    @IBAction func playButtonAction(_ sender: UIButton){
        self.playClicked?()
    }
}
