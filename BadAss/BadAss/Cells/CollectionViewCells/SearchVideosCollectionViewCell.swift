//
//  SearchVideosCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class SearchVideosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var commonImageView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    var playClicked : (() -> Void)? = nil
      
      override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
         
          
      }
    
    func loadVideo(obj: HomeModel){
        usernameLabel.text = obj.creator?.username
        nameLable.text = obj.creator?.name
        if let imageUrl = obj.thumbnail, let url = URL(string: imageUrl) {
            self.commonImageView.kf.setImage(with: url, placeholder: ImageConstant.videoPlaceholder)
        } else {
            self.commonImageView.image = ImageConstant.videoPlaceholder
        }
        
        let layer1 = gradientLayer(bottomView.bounds, .bottomView)
            bottomView.layer.insertSublayer(layer1, at: 0)
            bottomView.alpha = 0.7
        
    }
      
      
      
      @IBAction func playButtonAction(_ sender: UIButton){
          self.playClicked?()
      }
    
    
    
}
