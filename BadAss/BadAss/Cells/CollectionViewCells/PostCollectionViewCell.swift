//
//  PostCollectionViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 09/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import AVKit

class PostCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var fanLabel: UILabel!
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var topGradientView: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var videoPlayerView: UIView!
    
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var durationLabel: UILabel!

    
    
    var voteClicked : (() -> Void)? = nil
    var chatClicked : (() -> Void)? = nil
    var shareClicked : (() -> Void)? = nil
    var playAgainClicked : (() -> Void)? = nil
    var replyClicked : (() -> Void)? = nil
    var viewCompletion:(()  -> Void)? = nil
    
    
    var player = AVPlayer()
    var isPlayed = false
    let avPlayerViewController = AVPlayerViewController()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let layer1 = gradientLayer(gradientView.bounds, .bottomView)
        gradientView.layer.insertSublayer(layer1, at: 0)
        let layer2 = gradientLayer(topGradientView.bounds, .upView)
        topGradientView.layer.insertSublayer(layer2, at: 0)
        
        avPlayerViewController.view.frame = CGRect(x: videoPlayerView.frame.origin.x, y: 0, width: videoPlayerView.frame.size.width, height:  videoPlayerView.frame.size.height)
        avPlayerViewController.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspect.rawValue)
        avPlayerViewController.showsPlaybackControls = false
        videoPlayerView.addSubview(avPlayerViewController.view)
    }
    
    @objc func playerDidFinishPlaying(){
    }
    
    func setPlayer(object: HomeModel) {
        self.player = object.player ?? AVPlayer()
        self.player.automaticallyWaitsToMinimizeStalling = automaticallyWaitsToMinimizeStalling
        
        self.avPlayerViewController.player = self.player
        
        if avPlayerViewController.videoBounds.height > avPlayerViewController.videoBounds.width {
            avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill
        } else {
            avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspect
        }
        
        var isFirstTime = true
        self.activityIndicator.startAnimating()
        self.avPlayerViewController.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 600), queue: DispatchQueue.main) {
            [weak self] time in
            
            if let currentTime = self?.player.currentItem?.currentTime().seconds {
                let currentTime = Int(currentTime)
                let minutes = currentTime/60
                let seconds = currentTime - minutes * 60
                if let totalCurrentTime = (self?.player.currentItem?.duration.seconds){
                    if totalCurrentTime > 0 {
                        let totalCurrentTime = Int(totalCurrentTime)
                        let totalMinutes = totalCurrentTime/60
                        let totalSeconds = totalCurrentTime - totalMinutes * 60
                        self?.durationLabel.text = "\(String(format: "%02d:%02d", minutes,seconds) as String) / \(String(format: "%02d:%02d", totalMinutes,totalSeconds) as String)"
                    }
                }
            }
            
            
            if self?.avPlayerViewController.player?.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                if let isPlaybackLikelyToKeepUp = self?.avPlayerViewController.player?.currentItem?.isPlaybackLikelyToKeepUp {
                    self?.activityIndicator.isHidden = isPlaybackLikelyToKeepUp
                    self?.playPauseButton.isHidden = !isPlaybackLikelyToKeepUp
                    
                    if (self?.avPlayerViewController.videoBounds.height ?? 0) > (self?.avPlayerViewController.videoBounds.width ?? 0) {
                        self?.avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    } else {
                        self?.avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspect
                    }
                    
                    if !isPlaybackLikelyToKeepUp {
                        self?.activityIndicator.startAnimating()
                    }
                }
            }
            
            
            if let currentTime = self?.avPlayerViewController.player?.currentItem?.currentTime().seconds {
                Debug.log("currentTime::-\(currentTime)")
                if currentTime > 2 {
                    if isFirstTime {
                        self?.viewCompletion?()
                        isFirstTime = false
                    }
                }
            }
        }
 
    }
    
    //video player methods
    func setUpVideoPlayerViewController(cell:HomeVideoListCVC) {
    }
    
    @IBAction func voteButtonAction(_ sender: UIButton){
        self.voteClicked?()
    }
    @IBAction func chatButtonAction(_ sender: UIButton){
        self.chatClicked?()
    }
    @IBAction func shareButtonAction(_ sender: UIButton){
        self.shareClicked?()
    }
    @IBAction func playAgainButtonAction(_ sender: UIButton){
        self.playAgainClicked?()
    }
    @IBAction func replyButtonAction(_ sender: UIButton){
        self.replyClicked?()
    }
    
    
}
