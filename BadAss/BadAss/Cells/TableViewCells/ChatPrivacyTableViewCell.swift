//
//  ChatPrivacyTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 15/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ChatPrivacyTableViewCell: UITableViewCell {
    @IBOutlet weak var noOneButton: UIButton!
    @IBOutlet weak var anyOneButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
