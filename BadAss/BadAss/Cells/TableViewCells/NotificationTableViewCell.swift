//
//  NotificationTableViewCell.swift
//  BadAss
//
//  Created by Prempriya on 25/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!

    var followClicked: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(obj: NotificationModel){
        titleLabel.text = obj.message
        
        if let imageUrl = obj.profile_pic, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
        } else {
            self.profileImageView.image = ImageConstant.userPlaceholder
        }
        
        if let strDate = obj.created_at {
            let date =  getDateFromTimestamp(timestamp: "\(strDate)")
            dateLabel.text = Date().offset(from: date)
        } else {
            dateLabel.text = ""
        }
        
        switch obj.type {
        case NotificationType.FOLLOW_TYPE:
            followButton.isHidden = obj.follow_status == "1"
        default:
             followButton.isHidden = true
        }
        
    }
    
    @IBAction func followButtonAction(_ sender: UIButton){
        self.followClicked?()
    }

}
