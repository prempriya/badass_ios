//
//  ProfileAboutTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 18/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ProfileAboutTableViewCell: UITableViewCell {
    
    @IBOutlet weak var aboutLabel: UILabel!

    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var dobButton: UIButton!
    
    @IBOutlet weak var fanView: UIView!
       
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var fanButton: UIButton!
    @IBOutlet weak var fanIcon: UIImageView!
    @IBOutlet weak var unfollowButton: UIButton!
    
    
    
    var fanClicked: (() -> Void)? = nil
    var blockClicked: (() -> Void)? = nil
    var unfollowClicked: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let layer1 = gradientLayer(fanButton.bounds, .fanView)
        fanButton.layer.insertSublayer(layer1, at: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func fanButtonAction(_ sender: UIButton){
        self.fanClicked?()
    }
    
    @IBAction func blockButtonAction(_ sender: UIButton){
        self.blockClicked?()
    }

    @IBAction func unfollowButtonAction(_ sender: UIButton) {
        self.unfollowClicked?()
    }
    
    
}
