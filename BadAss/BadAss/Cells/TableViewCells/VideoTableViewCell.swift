//
//  VideoTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 11/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var commonLabel: UILabel!
    var viewedVideos = [HomeModel]()
    
    var cellClicked: ((IndexPath) -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadViewedVideos(list: [HomeModel]){
        self.viewedVideos = list
        self.collectionView.reloadData()
    }
    
    func myVideos(list: [HomeModel]){
        self.viewedVideos = list
        self.collectionView.reloadData()
    }
    
    

}
extension VideoTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewedVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
        let obj = viewedVideos[indexPath.row]
        if let imageUrl = obj.thumbnail, let url = URL(string: imageUrl) {
                   cell.commonImageView.kf.setImage(with: url, placeholder: ImageConstant.videoPlaceholder)
               } else {
                   cell.commonImageView.image = ImageConstant.videoPlaceholder
               }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 160, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.cellClicked?(indexPath)
    }
    
    
}
