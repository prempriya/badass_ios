//
//  MessageAcceptTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 18/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Quickblox

class MessageAcceptTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
       @IBOutlet weak var timeLabel: UILabel!
       @IBOutlet weak var messageLabel: UILabel!
       @IBOutlet weak var countLabel: UILabel!
       @IBOutlet weak var profileImageView: ChatImageView!
       @IBOutlet weak var statusView: UIView!
       @IBOutlet weak var tickImageView: UIImageView!
    
    @IBOutlet weak var acceptView: UIView!

    @IBOutlet weak var declineView: UIView!
    
    var acceptClicked : (() -> Void)? = nil
    var declineClicked : (() -> Void)? = nil


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let layer1 = gradientLayer(declineView.bounds, .fanView)
        declineView.layer.insertSublayer(layer1, at: 0)
        
        let layer2 = gradientLayer(acceptView.bounds, .acceptView)
        acceptView.layer.insertSublayer(layer2, at: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadData(dialog: QBChatDialog) {
        
        tickImageView.isHidden = true
        countLabel.isHidden = dialog.unreadMessagesCount == 0
        countLabel.text = "\(dialog.unreadMessagesCount)"
        
        nameLabel.text = dialog.name?.capitalized
        messageLabel.text = dialog.lastMessageText
        
        statusView.isHidden = true
        
        profileImageView.userImageKeyId = "\(dialog.occupantIDs?.filter({$0.uintValue != QBManager.shared.currentUserId}).first ?? 0)"
        profileImageView.setImageFromDialog(dialog: dialog) {
            
        }
        
        if let date = dialog.lastMessageDate {
            timeLabel.text = Date().offset(from: date)
        } else {
            timeLabel.text = ""
        }
    }
    
    @IBAction func acceptButtonAction(_ sender: UIButton){
        self.acceptClicked?()
    }
    
    @IBAction func declineButtonAction(_ sender: UIButton){
        self.declineClicked?()
    }
}
