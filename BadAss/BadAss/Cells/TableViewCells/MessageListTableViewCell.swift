//
//  MessageListTableViewCell.swift
//  GreenEntertainment
//
//  Created by Prateek Keshari on 13/06/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Quickblox

class MessageListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var profileImageView: ChatImageView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var tickImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        messageLabel.numberOfLines = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func loadData(dialog: QBChatDialog) {
        
        tickImageView.isHidden = true
        countLabel.isHidden = dialog.unreadMessagesCount == 0
        countLabel.text = "\(dialog.unreadMessagesCount)"
        
        nameLabel.text = dialog.name?.capitalized
        messageLabel.text = dialog.lastMessageText
        
        statusView.isHidden = true
        profileImageView.image = #imageLiteral(resourceName: "user")
        profileImageView.userImageKeyId = "\(dialog.occupantIDs?.filter({$0.uintValue != QBManager.shared.currentUserId}).first ?? 0)"
        profileImageView.setImageFromDialog(dialog: dialog) {
            
        }
        
        if let date = dialog.lastMessageDate {
            timeLabel.text = Date().offset(from: date)
        } else {
            timeLabel.text = ""
        }
    }

}
