//
//  ReceiverImageTableViewCell.swift
//  GreenEntertainment
//
//  Created by Prateek Keshari on 15/06/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit
import Quickblox

class ReceiverImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shapeView: ChatView!
    @IBOutlet weak var userImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shapeView.messageType = .recieved
        self.shapeView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        self.shapeView.layer.cornerRadius = 15
        shapeView.setNeedsDisplay()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMessageData(obj: QBChatMessage) {
           selectionStyle = .none
           if let url = URL(string: obj.attachments?.first?.name ?? "") {
               do {
                   let data = try Data(contentsOf: url)
                   let img = UIImage(data: data)
                   messageImageView.image = img
               } catch {
                   if let str = obj.attachments?.first?.url, let url = URL(string: str) {
                       messageImageView.kf.setImage(with: url, placeholder: chatImagePlaceholder)
                   }
               }
           } else {
               if let str = obj.attachments?.first?.url, let url = URL(string: str) {
                   messageImageView.kf.setImage(with: url, placeholder: chatImagePlaceholder)
               }
           }
       }
    
}
