//
//  BlockedUserTableViewCell.swift
//  BadAss
//
//  Created by Prempriya on 07/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class BlockedUserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!

    @IBOutlet weak var unblockButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!

    var unblockClicked: (() -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let layer1 = gradientLayer(unblockButton.bounds, .button)
        unblockButton.layer.insertSublayer(layer1, at: 0)
        
    }
    
    func loadUser(user: FollowedUser) {
        nameLabel.text = user.name?.capitalized
        usernameLabel.text = user.username
        if let imageUrl = user.profile_pic, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
        } else {
            self.profileImageView.image = ImageConstant.userPlaceholder
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func unblockButtonAction(_ sender: UIButton){
           self.unblockClicked?()
       }
}
