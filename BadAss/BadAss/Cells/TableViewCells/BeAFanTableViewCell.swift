//
//  BeAFanTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class BeAFanTableViewCell: UITableViewCell {
    @IBOutlet weak var fanButton: UIButton!
    @IBOutlet weak var myfanButton: UIButton!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fanImageView: UIImageView!

    @IBOutlet weak var fanView: UIView!
    
    var fanClicked: (() -> Void)? = nil
    var myfanClicked: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let layer1 = gradientLayer(fanButton.bounds, .fanView)
        fanButton.layer.insertSublayer(layer1, at: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Setups

    func loadSuggestedUser(user: SuggestedUser) {
        if manageProfilePrivacy(isFollowing: false, showMyProfile: user.show_my_profile == "0") {
            profileImageView.image = ImageConstant.userPlaceholder
            nameLabel.text = StaticStrings.anonymous.localized()
            usernameLabel.text = StaticStrings.anonymous.localized()
        } else {
            nameLabel.text = user.name?.capitalized
            usernameLabel.text = user.username
            if let imageUrl = user.profile_pic, let url = URL(string: imageUrl) {
                self.profileImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
            } else {
                self.profileImageView.image = ImageConstant.userPlaceholder
            }
        }
        myfanButton.isHidden = true
    }
    
    //MARK:- Actions
    @IBAction func fanButtonAction(_ sender: UIButton){
        self.fanClicked?()
    }
    
    @IBAction func myfanButtonAction(_ sender: UIButton){
        self.myfanClicked?()
    }
    
}
