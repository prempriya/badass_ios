//
//  MainCommentTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 24/09/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class MainCommentTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var seperatorLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var dotLabel: UILabel!
    
    
    var likeClicked: (() -> Void)? = nil
    var replyClicked: (() -> Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func loadMainComment(obj: Comment){
        nameLabel.text = obj.creator.name
        commentLabel.text = obj.comment
        if let imageUrl = obj.creator.profile_pic, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
        } else {
            self.profileImageView.image = ImageConstant.userPlaceholder
        }
        likeButton.setImage(UIImage.init(named: obj.is_liked == "false" ? "ic_unlike" : "ic_like"), for: .normal)
        likeButton.setTitle(Int(obj.likes_count ?? "0") ?? 0 > 1 ? "\(obj.likes_count ?? "") Likes" : "\(obj.likes_count ?? "") Like", for: .normal)
        replyButton.setTitle(Int(obj.replies_count ?? "0") ?? 0 > 1 ? "\(obj.replies_count ?? "") Replies" : "\(obj.replies_count ?? "") Reply", for: .normal)
        
        if let strDate = obj.created_at {
            let date =  getDateFromTimestamp(timestamp: "\(strDate)")
            dateLabel.text = Date().offset(from: date)
        } else {
            dateLabel.text = ""
        }
    }
    
    func loadComment(obj: ReplyUserData){
        nameLabel.text = obj.name
        commentLabel.text = obj.comment
        if let imageUrl = obj.profile_pic, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
        } else {
            self.profileImageView.image = ImageConstant.userPlaceholder
        }
        likeButton.setImage(UIImage.init(named: obj.is_liked == "false" ? "ic_unlike" : "ic_like"), for: .normal)
        likeButton.setTitle(Int(obj.likes_count ?? "0") ?? 0 > 1 ? "\(obj.likes_count ?? "") \(StaticStrings.Likes.localized())" : "\(obj.likes_count ?? "") \(StaticStrings.Like.localized())", for: .normal)
         likeButton.setTitle(Int(obj.likes_count ?? "0") ?? 0 > 1 ? "\(obj.likes_count ?? "") \(StaticStrings.Likes.localized())" : "\(obj.likes_count ?? "") \(StaticStrings.Like.localized())", for: .selected)
        replyButton.setTitle(Int(obj.replies_count ?? "0") ?? 0 > 1 ? "\(obj.replies_count ?? "") \(StaticStrings.replies.localized())" : "\(obj.replies_count ?? "") \(StaticStrings.reply.localized())", for: .normal)
          replyButton.setTitle(Int(obj.replies_count ?? "0") ?? 0 > 1 ? "\(obj.replies_count ?? "") \(StaticStrings.replies.localized())" : "\(obj.replies_count ?? "") \(StaticStrings.reply.localized())", for: .selected)
        
        if let strDate = obj.created_date {
            let date =  getDateFromTimestamp(timestamp: "\(strDate)")
            dateLabel.text = Date().offset(from: date)
        } else {
            dateLabel.text = ""
        }
    }
    
    @IBAction func likeButtonAction(_ sender: UIButton){
        self.likeClicked?()
    }
    
    @IBAction func replyButtonAction(_ sender: UIButton){
        self.replyClicked?()
    }
    
}
