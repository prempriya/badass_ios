//
//  FanTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 16/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class FanTableViewCell: UITableViewCell {
    
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!

    var followClicked: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadUser(user: FollowedUser) {
        nameLabel.text = user.name?.capitalized
        usernameLabel.text = user.username
        if let imageUrl = user.profile_pic, let url = URL(string: imageUrl) {
            self.profileImageView.kf.setImage(with: url, placeholder: ImageConstant.userPlaceholder)
        } else {
            self.profileImageView.image = ImageConstant.userPlaceholder
        }
    }
    
    @IBAction func followButtonActiomn(_ sender: UIButton){
        self.followClicked?()
    }
    
}
