//
//  SenderChatCell.swift
//  HP
//
//  Created by apple on 05/11/19.
//  Copyright © 2019 Quytech. All rights reserved.
//

import UIKit

class SenderChatCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shapeView: ChatView!
    @IBOutlet weak var messageStatusImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shapeView.messageType = .sent
        self.shapeView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner,.layerMinXMinYCorner]
        self.shapeView.layer.cornerRadius = 15
        shapeView.setNeedsDisplay()
    }

//    func populateCell(with model: ChatList) {
//        messageLabel.text = model.chatText?.base64Decoded()
//        let time = getReadableDateWithTime(timeStamp: model.chatTimeStamp ?? "")
//        timeLabel.text = time
//
//        shapeView.messageType = .sent
//        shapeView.setNeedsDisplay()
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
