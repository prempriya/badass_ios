//
//  SearchVideoTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class SearchVideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var videos = [HomeModel]()
    var cellClicked: ((IndexPath) -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadVideos(arr: [HomeModel]){
        self.videos = arr
        self.collectionView.reloadData()
    }

}

extension SearchVideoTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchVideosCollectionViewCell", for: indexPath) as! SearchVideosCollectionViewCell
        cell.loadVideo(obj: videos[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 160, height: 175)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.cellClicked?(indexPath)
    }
}
