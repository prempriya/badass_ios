//
//  ViewAllTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 17/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class ViewAllTableViewCell: UITableViewCell {
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!

    var viewAllClicked: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func viewAllButtonActiomn(_ sender: UIButton){
        self.viewAllClicked?()

    }

}
