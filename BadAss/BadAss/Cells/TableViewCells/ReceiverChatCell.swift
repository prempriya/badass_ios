//
//  ReceiverChatCell.swift
//  HP
//
//  Created by apple on 05/11/19.
//  Copyright © 2019 Quytech. All rights reserved.
//

import UIKit

class ReceiverChatCell: UITableViewCell {

   @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shapeView: ChatView!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shapeView.messageType = .recieved
        self.shapeView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner,.layerMaxXMaxYCorner]
            self.shapeView.layer.cornerRadius = 15
        shapeView.setNeedsDisplay()
    }

//    func populateCell(with model: ChatList) {
//        messageLabel.text = model.chatText?.base64Decoded()
//        let time = getReadableDateWithTime(timeStamp: model.chatTimeStamp ?? "")
//        timeLabel.text = time
//        shapeView.messageType = .recieved
//        shapeView.setNeedsDisplay()
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
