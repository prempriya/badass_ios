//
//  HomeTableViewCell.swift
//  BadAss
//
//  Created by Prateek Keshari on 01/08/20.
//  Copyright © 2020 Quytech. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var fanLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var commonImageView: UIImageView!

    
    var voteClicked : (() -> Void)? = nil
    var shareClicked : (() -> Void)? = nil
    var playClicked : (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let layer1 = gradientLayer(bottomView.bounds, .bottomView)
        bottomView.layer.insertSublayer(layer1, at: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func voteButtonAction(_ sender: UIButton){
        self.voteClicked?()
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton){
        self.shareClicked?()
    }
    
    @IBAction func playButtonAction(_ sender: UIButton){
        self.playClicked?()
    }

}
