//
//  APIConstants.swift
//  Obsidiam
//
//  Created by DAY on 13/04/20.
//  Copyright © 2020 DAY. All rights reserved.
//

import Foundation


struct AppURLs {
    static let termsAndConditionsUrl = "https://obsidiam.com/term-and-condition"
    static let privacyPolicyUrl = "https://obsidiam.com/privacy-policy"
    static let authyAppStoreUrl = "https://apps.apple.com/us/app/authy/id494168017"
    static let faqSectionUrl = "https://support.obsidiam.com/hc/en-us"
    
    
    static let QuestionPurchaseURL = "https://pages.razorpay.com/pl_FpsaEEztZTbp7I/view"

}

struct CommonMessage {
    static let something_went_wrong = "Something went wrong!"
}


let AppName = "Badass"

let kToken = "token"
let kAuthToken = "authToken"
let kLanguageCode = "languageCode"
let kETag = "ETag"

let kQbId = "qb_id"
let kSocialType = "social_type"
let kSocialId = "social_id"
let kSocialSignup = "social_signup"
let kSocialLogin = "social_login"
let kLanguage = "language"
let kAuthyCode = "authyCode"
let kAuthyId = "authyId"
let kData = "data"
let kEmail = "email"
let kPassword = "password"
let kNewPassword = "new_password"
let kOldPassword = "old_password"
let kBrowser = "browser"
let kDeviceToken = "device_token"
let kDeviceType = "device_type"
let kIpAddress = "ipAddress"
let kSource = "source"
let kName = "name"
let kUsername = "username"
let kLabel = "label"
let kFirstName = "firstName"
let kUserId = "user_id"
let kOtherUserId = "other_user_id"
let kMyUserId = "my_user_id"
let kCountryId = "country_id"
let kAboutMe = "about_me"
let kGender = "gender"
let kShowGender = "show_gender"
let kDOB = "dob"
let kShowDob = "show_dob"
let kCounts = "counts"
let kLanguages = "languages"
let kSkipAuth = "skipAuth"
let kCountryCode = "countryCode"
let kIOS3166CountryCode = "countryCodeISO3"
let kCountryCodeNumeric = "countryCodeNumeric"
let kExperienceLevel = "experienceLevel"
let kPhoneNumber = "phone"
let kPhoneCode = "phone_code"
let kPreferredLanguage = "preferred_languages"
let kReferralCode = "referralCode"
let kReferenceNumber = "referenceNumber"
let kVerificationCode = "verificationCode"
let kAddressId = "addressId"
let kVerificationStatus = "verificationStatus"
let kVerificationStatusString = "verificationStatusString"
let kPreferredCurrency = "preferredCurrency"
let kDashboardClicked = "dashboardClicked"
let kBuySellClicked = "buySellClicked"
let kExchangeClicked = "exchangeClicked"
let kWalletClicked = "walletClicked"
let kObsidiamPayClicked = "obsidiamPayClicked"
let kLoginId = "loginId"
let kVerificationCount = "verificationCount"
let kId = "id"
let kCode = "code"
let kType = "type"
let kNewEmail = "newEmail"
let kWrongEmail = "wrongEmail"
let kNewCountryCode = "newCountryCode"
let kNewNumber = "newNumber"
let kSmsCode = "smsCode"
let kNumber = "number"
let kStatus = "status"

let kDestinationTag = "destinationTag"
let kFeeFromAmount = "feeFromAmount"
let kAmount = "amount"
let kPayeeID = "payeeId"
let kCoinShortName = "coinShortName"
let kPage = "page"
let kPageSize = "pageSize"
let kCount = "count"
let kTransactionType = "transactionType"
let kRecentTransactions = "recentTransactions"
let kTransactions = "transactions"
let kInterval = "interval"
let kWithdrawals = "withdrawals"
let kAddress = "address"
let kDetailedNotification = "detailedNotification"
let kFeedbackType = "feedbackType"
let kFeedbacks = "feedbacks"

let kNotificationType = "notificationType"
let kOrderStatus = "orderStatus"
let kPrivateOrder = "privateOrder"
let kAllOrderResponseDtos = "allOrderResponseDtos"
let kMakersOrders = "makerOrders"

let kFeedback = "feedback"
let kOrderActionId = "orderActionId"
let kApproved = "approved"
let kOrderId = "orderId"
let kBody = "body"


let kProfilePrivacy =  "profile_privacy"
let kChatPrivacy = "chat_privacy"
let kFollowerId = "follower_id"
let kFollowingId = "following_id"



let kBlock_user_id = "block_user_id"
let kBlock_status = "block_status"


let kLatest = "latest"
let kLanguage_ids = "language_ids"
let kTrending = "trending"
let kCategory_id = "category_id"
let kvideo_id = "video_id"
let kvote_status = "vote_status"
