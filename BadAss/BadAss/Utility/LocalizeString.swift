//
//  LocalizeString.swift
//  Loco
//
//  Created by abc on 04/06/18.
//  Copyright © 2018 Quy Technology Pvt Ltd. All rights reserved.
//
import Foundation


struct GeneralStrings {
    static let badass = "BADASS"
    static let trending = "Trending"
    static let latest = "Latest"
    static let language = "Language"
    static let home = "Home"
    static let notification = "Notification"
    static let addVideo = "Add Video"
    static let chat = "Chat"
    static let profile = "Profile"
    
    
    static let all = "All"
    static let family = "Family"
    static let friends = "Friends"
    static let landlord = "Landlord"
    static let lover = "Lover"
}


struct ProfileStrings {
    
    static let editProfile = "Edit Profile"
    static let votes = "Votes"
    static let myFans = "My Fans"
    static let followings = "Followings"
    
    static let gallery = "Gallery"
    static let uploadedVideo = "Uploaded Video"
    static let recentlyViewed = "Recently Viewed"
    static let repliedVideos = "Replied Videos"

}

