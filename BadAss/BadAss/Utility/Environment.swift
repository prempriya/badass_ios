//
//  Environment.swift
//  Obsidiam
//
//  Created by DAY on 16/04/20.
//  Copyright © 2020 DAY. All rights reserved.
//

import Foundation

enum Server {
    case developement
    case staging
    case production
}

class Environment {
    
    static let server: Server = .staging
    
    class func baseURL() -> String {
        switch self.server {
        case .developement:
            return "https://www.uknow.co.in/badass/api/"
        case .staging:
            return "https://www.badasscommunity.com/api/"
        case .production:
            return "https://www.uknow.co.in/badass/api/"
        }
    }
    
}
