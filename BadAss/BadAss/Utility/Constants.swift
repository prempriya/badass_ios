//
//  Constants.swift
//  GreenEntertainment
//
//  Created by Quytech on 11/19/19.
//  Copyright © 2019 Quytech. All rights reserved.
//

import UIKit

enum AlertMessages: String {
    case sessionExpired = "Sorry, your account has been logged in other device! Please login again to continue."
    func getLocalizedValue() -> String {
        return NSLocalizedString(self.rawValue, comment: self.rawValue)
    }
}

struct UserDefaultsKey{
    static let deviceTokenn       = "deviceTokenn"
    static let IS_LOGIN           = "is_login"
    static let USER_ID            = "userId"
    static let USER_CONTACTS      = "userContacts"
    static let isNotificationOn   = "isNotificationOn"
    static let deviceId           = "deviceId"
    static let facebookId         = "facebookId"
    static let callingCode        = "callingCode"
    static let isBackUpOn         = "isBackUpOn"
    static let loggedInUserInfo   = "loggedInUserInfo"
    static let signupInfo         = "signupInfo"
    static let device_token_ios_voip    = "device_token_ios_voip"
}


struct ValidationMessage {
    static let blankUsername = "*Please enter username."
    static let minLengthUsername = "*Username must be of at least 2 characters."
    static let invalidUserName = "*Please enter valid username."
    static let blankEmail = "*Please enter an email address."
    static let invalidEmail = "*Please enter a valid email address."
    static let blankPassword = "*Please enter password."
    static let minLengthPassword = "*Password must be of at least 8 characters."
    static let blankPhoneNumber = "*Please enter phone number."
    static let minLengthPhoneNumber = "*Phone number must be of at least 7 digits."
    static let blankExperienceLevel = "Please select experience level."
    static let blankReferralCode = "Please enter referral code."
    static let invalidReferralCode = "Please enter valid referral code."
    static let blankUsernameEmailAndNumber = "*Please enter E-mail/Phone/Username."
    static let blankEmailAndNumber = "*Please enter E-mail/Phone."
    static let blankCountryName = "*Please select country."
    static let blankTitle = "*Please enter title."
    static let blankCategory = "*Please select category."
    static let blankLanguage = "*Please select language."

    static let please_enter_amount_greater_than = "Please enter an amount greater than"
    static let plase_enter_amount_lesser_than_available_balance = "Please enter an amount lesser than your available balance."
    static let your_transfer_daily_limit_reached = "Your transfer daily limit reached.";
    
    
    // Login
    static let validUsername_email = "*Please enter valid username or email."
    static let validUserName = "*Please enter valid username."
    static let validEmail = "*Please enter valid email."
    static let minPassword = "Password must be 8 characters long."
    static let alphanemericPassword = "Password must be alphanumeric."
    static let minPhoneNumber = "Phone number must be 8 characters long."
    static let validPhone  =  "*Please enter valid phone number."
    
    static let blankNewPassword = "*Please enter new password."
    static let minLengthNewPassword = "*New password must be of at least 8 characters."
    
    static let blankOldPassword = "*Please enter old password."
    static let minLengthOldPassword = "*Old password must be of at least 8 characters."
    
    static let blankConfirmPassword = "*Please enter confirm password."
    static let minLengthConfirmPassword = "*Confirm password must be of at least 8 characters."
    
    static let passwordNotMatch = "*New password and confirm password must be same."

    
    static let blankFullName = "*Please enter full name."
    static let invalidFullName = "*Please enter valid full name."
}


struct StaticStrings {
    static let advice_start = "Please type your question and wait for the expert to answer your question."
    static let selectGetAdviceOption = "Please choose what type of question you want to ask."
    static let selectLanguage = "Pick your preferred languages"
    static let languages = "languages"
    static let recentlyViewed = "RECENTLY VIEWED"
    static let Gallery = "GALLERY"
    static let uploadedVideo = "Uploaded Video"
    static let editProfile = "Edit Profile"
    static let anonymous = "Anonymous"
    static let ok = "OK"
    static let proceed = "Proceed"
    static let uploading = "Uploading..."
    static let downloading = "Downloading..."
    
    // Login
    static let enter_email_phone_username = "Enter e-mail/phone/username"
    static let enter_password = "Enter password"
    
    // Forgot
    static let emailid_phone = "Email ID/Phone"
    
    // Home
    static let cancel = "Cancel"
    static let disclaimer = "*Disclaimer*"
    static let disclaimer_message = "The information provided is for entertainment purpose only. You will be responsible for any action taken on the basis of the information provided."
    static let connection_progress = "Please wait! Connection is in progress..."
    static let get_advice = "Get Advice"
    
    static let connection_failed = "Connection Failed... Tap to retry"
    static let connecting = "Connecting..."

    static let avaialble_questions = "Available Questions"
    
    
    // Setting
    static let settings = "Settings"
    static let about = "About"
    static let help = "Help"
    static let privacy = "Privacy"
    static let change_password = "Change Password"
    static let blocked_user = "Blocked User"
    static let logout_alert = "Are you sure you want to logout?"
    static let yes = "Yes"
    static let no = "No"
    static let chat_privacy = "Chat Privacy"
    static let anyone_can_send = "Anyone can send me message"
    static let no_one_send = "No one can send me message"
    static let profile_privacy = "Profile Privacy"
    static let show_my_profile = "Show my profile to everyone"
    static let hide_my_profile = "Hide my profile"
    
    //Generic Content
    static let terms_of_service = "Terms Of Service"
    static let privacy_policy = "Privacy Policy"
    static let aboout_us = "About Us"

    // Change password
    static let old_password = "Old Password"
    static let new_password = "New Password"
    static let confirm_password = "Confirm Password"
    
    // Other Profile
    static let Unblock = "Unblock"
    static let block = "Block"
    static let view_all = "View All"
    static let see_all_suggestions = "Suggestions For You"
    static let unfollow = "Unfollow"
    
    
    //Edit Profile
    static let fullname = "Full Name *"
    static let username = "Username"
    static let phone = "Phone"
    static let country = "Country"
    static let email = "Email"
    static let gender = "Gender"
    static let female = "Female"
    static let male = "Male"
    static let prefer_not_to_tell = "Prefer not to tell"
    static let birth_date = "Birth Date"
    static let show_your_age = "Show your age"
    static let preferred_languages = "Preferred Languages"
    static let countryCode = "Country Code"
    
    
    
    // Message
    static let years_ago = "years ago"
    static let year_ago = "year ago"
    static let month_ago = "month ago"
    static let months_ago = "months ago"
    static let day_ago = "day ago"
    static let days_ago = "days ago"
    static let weeks_ago = "weeks ago"
    static let week_ago = "week ago"
    static let hours_ago = "hours ago"
    static let hour_ago = "hour ago"
    static let mins_ago = "mins ago"
    static let min_ago = "min ago"
    static let secs_ago = "secs ago"
    static let sec_ago = "sec ago"
    static let just_now = "just now"
    static let add_a_reply = "Add a reply..."

    
    // Add Video
    static let title = "Title"
    static let category = "Category"
    static let language = "Language"
    static let fan = "fan"
    static let vote = "vote"
    static let fans = "fans"
    static let votes = "votes"
    static let comment = "comment"
    static let comments = "comments"
    static let reply = "Reply"
    static let replies = "Replies"
    static let like = "like"
    static let likes = "likes"
    static let Comment = "Comment"
    static let Comments = "Comments"
    static let watchreply = "Watch Reply Videos"
    static let Like = "Like"
    static let Likes = "Likes"
    static let add_a_comment = "Add a comment..."

    // Signup
    static let fullnamee = "Full Name *"
    static let usernamee = "Username *"
    static let phonee = "Phone *"
    static let countryy = "Country *"
    static let emailId = "Email *"
    static let passwordd = "Password *"


}




class Constants: NSObject {
    //CollectioViewCell Identifiers
    static  let kTutorialCollectionViewCellIdentifier = "TutorialCollectionViewCell"
    static let kAgreeTermsAndConditions = "I agree to Terms and Conditions"
    static let kTermsAndCondition =  "Terms and Conditions"
    static let kForgotPassword = "Forgot Password?"
    static let kVerificationCode = "Verification Code"

     // Keys
       static let kMessage = "message"
       static let kMessageText = "Message"
       static let kTest = "Test"
       static let kData = "data"
       static let Code = "code"
       static let OTP = "otp"
       static let AUTH_TOKEN = "auth_token"
       static let Video_ID = "video_id"
       static let Video_URL = "video_url"
       static let kImage_URL = "image_url"
       static let kVideo_File = "file"
       static let kProfile_Picture = "profile_picture"
    
       static let kSEND_CHAT_MESSAGE = "SEND_CHAT_MESSAGE"
       static let kVIDEO_SHARE = "VIDEO_SHARE"
       static let kVIDEO_VOTE = "VIDEO_VOTE"
       static let kUSER_FOLLOW = "USER_FOLLOW"
       static let kDONATION_RECEIVED = "DONATION_RECEIVED"
       static let kGOT_NEW_BADGE = "GOT_NEW_BADGE"
       static let kDONATION_SENT = "DONATION_SENT"
       static let kMONEY_ADDED_TO_WALLET = "MONEY_ADDED_TO_WALLET"
       static let kWITHDRAW_MONEY_FROM_WALLET = "WITHDRAW_MONEY_FROM_WALLET"
       static let kREWARD_CLAIMED = "REWARD_CLAIMED"
}


enum Fonts {
    
    enum FontSize : CGFloat {
        case small = 12.0
        case medium = 14.0
        case large = 16.0
        case xLarge = 18.0
        case xXLarge = 20.0
        case xxLarge = 19.0
        case xxLLarge = 22.0
        case xxxLarge = 25.0
        case xXXLarge = 32.0
        case XVLarge = 50.0
    }
    
    enum Montserrat : String {
        
        case light    = "Montserrat-Light"
        case regular    = "Montserrat-Regular"
        case medium     = "Montserrat-Medium"
        case bold       = "Montserrat-Bold"
        case semiBold = "Montserrat-SemiBold"
        
        func font(_ size : FontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue)!
        }
    }
    
    enum Nunito : String {
        case light    = "Nunito-Light"
        case regular    = "Nunito-Regular"
        case medium     = "Nunito-SemiBold"
        case bold       = "Nunito-Bold"
        func font(_ size : FontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue)!
        }
    }
}

enum ScreenType: String {
    case termsAndCondition  = "Terms and Conditions"
    case aboutus  = "About Us"
}


enum OTPScreenType {
    case forgotPassword
    case verification
}

enum CameraScreenType {
    case signup
    case edit
}


enum GetAdviceType: String, CaseIterable {
    case LegalAdvisor = "Legal Advisor"
    case TarotCardReader = "Tarot Card Reader"
    case Astrologer = "Astrologer"
    
    func getEmailAddress() -> String {
        switch self {
        case .Astrologer:
            return "astro@badasscommunity.com"
        case .LegalAdvisor:
            return "legal@badasscommunity.com"
        case .TarotCardReader:
            return "tarot@badasscommunity.com"
        }
    }
    
}


struct AppThemeColors {
    static let PURPLE_COLOR: UIColor = UIColor(red: 133.0/255.0, green: 0.0/255.0, blue: 198.0/255.0, alpha: 1.0)
    static let playlistGradientColors = [#colorLiteral(red: 0.9680548925, green: 0.1193479551, blue: 0.1896455123, alpha: 1).cgColor, #colorLiteral(red: 0.760699749, green: 0.02532918751, blue: 0.4897094965, alpha: 1).cgColor]
    static let viewGradientColors = [kAppRedColor.cgColor, kAppOrangeColor.cgColor]
    static let buttonGradientColors = [kAppRedColor.cgColor, kAppOrangeColor.cgColor]
    static let halfButtonGradientColors = [kAppRedColor.cgColor,kAppRedColor.cgColor, kAppOrangeColor.cgColor]
    static let blackGradientColors = [UIColor.clear.cgColor, UIColor.black.cgColor]
    static let blackTopGradientColors = [UIColor.black.cgColor, UIColor.clear.cgColor]
    static let fanGradientColors = [KfanUpperColor.cgColor, KfanLowerColor.cgColor]
    static let acceptGradientColors = [kAcceptUpperColor.cgColor, kAcceptLowerColor.cgColor]
    static let adviceGradientColors = [kAdviceUpperColor.cgColor, kAdviceLowerColor.cgColor]

    static let navBackground = UIColor.RGB(r: 30, g: 30, b: 30, alpha: 1.0)
}


struct DateFormat {
    static let shortDate = "dd-MM-yyyy"
    static let mediumDate = "MMM dd, yyyy"
    static let longDate = "dd MMMM, yyyy"
    
    static let dateTime = "yyyy-MM-dd HH:mm:ss"
    
    static let chatMessage = "dd MMM, hh:mm a"
}


struct VideoConstant {
    static let videoMaxLength = 30
    static let videoMinLength = 5
}


struct ImageConstant {
    static let userPlaceholder = UIImage(named: "user")
    static let videoPlaceholder = UIImage(named: "ic_video_placeholder") // ic_thumbnail_placeholder
    static let thumbnailPlaceholder = UIImage(named: "ic_video_placeholder") // ic_thumbnail_placeholder
}


struct NotificationType {
    static let FOLLOW_TYPE = "follow"
    static let COMMENT_TYPE = "comment"
    static let COMMENT_REPLY_TYPE = "comment_reply"
    static let VIDEO_REPLY_TYPE = "video_reply"
    static let VIDEO_UPLOAD = "video"
    static let CHAT_REQUEST = "chat_request"
    static let CHAT_MESSAGE = "chat_message"
    static let ADMIN_NOTIFICATION = "admin_notification"
}
 

extension Notification.Name {
    static let chatRequest = Notification.Name("chat_request")
    static let chatMessage = Notification.Name("chat_message")
    static let videoComment = Notification.Name("video_comment")
}



