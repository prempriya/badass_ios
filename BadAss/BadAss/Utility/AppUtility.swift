//
//  AppUtility.swift
//  SimpleMerchantApp
//
//  Created by apple on 19/11/19.
//  Copyright © 2019 Quytech. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import EventKit
import Kingfisher
import MediaWatermark


// MARK: - Short Terms
//var shared: SceneDelegate?


let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let userPlaceholder = UIImage(named: "user")
let chatImagePlaceholder = UIImage(named: "Badass_logo_L")

@available(iOS 13.0, *)

//let SCENEDELEGATE = UIScene.delegate as! SceneDelegate
let KAppWhiteColor = UIColor.white
let KAppDarkGrayColor = RGBA(38, g: 50, b: 56, a: 1)
let kAppOrangeColor = RGBA(255, g: 128, b: 0, a: 1)
let kAppRedColor = RGBA(224, g: 27, b: 34, a: 1)
let KAppLightGreyBackgroundColor = RGBA(224,g:225,b: 228,a:1)

let KfanUpperColor = RGBA(224,g:27,b: 34,a:1)
let KfanLowerColor = RGBA(254,g:76,b: 0,a:1)

let kAcceptUpperColor = RGBA(12,g:183,b: 84,a:1)
let kAcceptLowerColor = RGBA(99,g:202,b: 44,a:1)

let kAdviceUpperColor = RGBA(0,g:0,b: 0,a:1)
let kAdviceLowerColor = RGBA(38,g:38,b: 38,a:1)


//224, 225, 228
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let showLog = true
let Window_Width = UIScreen.main.bounds.size.width
let Window_Height = UIScreen.main.bounds.size.height

// MARK: - Helper functions
func RGBA(_ r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) -> UIColor {
    return UIColor(red: (r/255.0), green: (g/255.0), blue: (b/255.0), alpha: a)
}

func getRoundRect(_ obj : UIButton){
    obj.layer.cornerRadius = obj.frame.size.height/2
    obj.layer.borderColor = UIColor.white.cgColor
    obj.layer.borderWidth = obj.frame.size.width/2
    obj.clipsToBounds = true
}

func getRoundImage(_ obj : UIImageView){
    obj.layer.cornerRadius = obj.frame.size.height/2
    obj.layer.borderColor = UIColor.white.cgColor
    obj.layer.borderWidth = 5
    obj.layer.masksToBounds = true
}

func getViewWithTag(_ tag:NSInteger, view:UIView) -> UIView {
    return view.viewWithTag(tag)!
}

 func gradientLayer(_ frame: CGRect, _ gradient: GradientType) -> CAGradientLayer {
    let layerGradient = CAGradientLayer()
    
    if gradient == .button {
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.colors = AppThemeColors.buttonGradientColors
    } else if  gradient == .buttonBottom {
        layerGradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        layerGradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        layerGradient.colors = AppThemeColors.buttonGradientColors
    } else if gradient == .halfButton {
        
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.colors = AppThemeColors.halfButtonGradientColors
        
    }else if gradient == .view {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.viewGradientColors
        layerGradient.locations = [0.0, 1.0]
    }else if gradient == .bottomView {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.blackGradientColors
        layerGradient.locations = [0.0, 1.0]
    }else if gradient == .upView {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.blackTopGradientColors
        layerGradient.locations = [0.0, 1.0]
    }else if gradient == .genderView {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.playlistGradientColors
        layerGradient.locations = [0.0, 1.0]
    }else if gradient == .fanView {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.fanGradientColors
        layerGradient.locations = [0.0, 1.0]
    }else if gradient == .acceptView {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.acceptGradientColors
        layerGradient.locations = [0.0, 1.0]
    }else if gradient == .adviceView {
        //layerGradient.startPoint = CGPoint(x: 10/frame.width, y: 0.5)
       // layerGradient.endPoint = CGPoint(x: 1, y: 1.0)
        layerGradient.colors = AppThemeColors.adviceGradientColors
        layerGradient.locations = [0.0, 1.0]
    }

    layerGradient.frame = frame
    return layerGradient
}

 func getTextWithImage(startString : String, imageName:String = "ic_vote_count", lastString : String, imageAddtionalSize:CGFloat = 0) -> NSAttributedString {
    let attachment = NSTextAttachment()
    attachment.image = UIImage(named: imageName)
    let imageOffsetY:CGFloat = -3.0;
    attachment.bounds = CGRect(x: 0, y: imageOffsetY, width: attachment.image!.size.width + imageAddtionalSize, height: attachment.image!.size.height + imageAddtionalSize)
    let attachmentString = NSAttributedString(attachment: attachment)
    let myString = NSMutableAttributedString(string: startString)
    myString.append(attachmentString)
    myString.append(NSAttributedString(string: lastString))
    return myString
}

 func isInternetConnectionAvailable()->Bool{
    var isConnection = false
    let reachability = try! Reachability()

    reachability.whenReachable = { reachability in
        if reachability.connection == .wifi {
            print("Reachable via WiFi")
            isConnection = true
        } else {
            isConnection = true
            print("Reachable via Cellular")
        }
    }
    reachability.whenUnreachable = { _ in
        isConnection = false
        print("Not reachable")
    }
    return isConnection
  }

func validPhoneNumberForLogin(_ number: String?) -> Bool {
          let regex = "^[0-9]*$"
          return number?.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
   }

//func openGoogleMap(strSource:String, strDestination:String){
//    if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
//
////        UIApplication.shared.openURL(URL(string:
////            "comgooglemaps://?saddr=\(strSource)&daddr=\(strDestination)&center=\(strSource)&directionsmode=driving")!)
//        UIApplication.shared.open(URL(string:
//            "comgooglemaps://?saddr=\(strSource)&daddr=\(strDestination)&center=\(strSource)&directionsmode=driving")!, options: ["":""], completionHandler: nil)
//    } else {
//        AlertController.alert(title: "Warning", message: "Please install google map application.", buttons: ["OK"]) { (action, index) in }
//    }
//}


func dialNumber(number : String) {
    
    if let url = URL(string: "tel://\(number)"),
        UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        // add error message here
    }
}

//for show no data label
func noDataLabel(tableView: UITableView , labelText:String) {
    let noDataLabel = UILabel()
    noDataLabel.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height)
    noDataLabel.text    = labelText
    noDataLabel.textColor = UIColor.darkGray
    noDataLabel.textAlignment    = .center
    tableView.backgroundView = noDataLabel
}



func noDataLabelForDashboard(tableView: UITableView , labelText:String) {
    let noDataLabel = UILabel(frame: CGRect(x: 0, y: tableView.frame.height / 2 + 35 , width: tableView.bounds.size.width, height: 45.0))
    noDataLabel.text    = labelText
    noDataLabel.textColor = UIColor.darkGray
    noDataLabel.textAlignment    = .center
    noDataLabel.tag = 100
    
    let subViews = tableView.subviews
    for subview in subViews {
        subview.removeFromSuperview()
    }
    
    //tableView.backgroundView = noDataLabel
    tableView.addSubview(noDataLabel)
}

func chatListEmptyText(status:Bool,tableView:UITableView){
    if (status){
        tableView.setEmptyTextScreen(status: true, titleImage: "no_messages", title: "No Messages, yet.", subTitle: "No message in your inbox, yet!Start chatting with volunteers around you.", isSubTitleNeeded: true)
    }else{
        tableView.setEmptyTextScreen(status: false, titleImage: "no_messages", title: "No Messages, yet.", subTitle: "No message in your inbox, yet!Start chatting with volunteers around you.", isSubTitleNeeded: true)
        
    }
}


func voteFormatPoints(num: Double) ->String{
    let thousandNum = num/1000
    let millionNum = num/1000000
    if num >= 1000 && num < 1000000{
        if(floor(thousandNum) == thousandNum){
            return("\(Int(thousandNum))K")
        }
        return("\(Int(thousandNum))K")
        // return("\(thousandNum.roundToPlaces(1))k")
    }
    if num > 1000000{
        if(floor(millionNum) == millionNum){
            return("\(Int(millionNum))M")
        }
        return("\(Int(millionNum))M")
        // return ("\(millionNum.roundToPlaces(1))M")
    }
    else{
        if(floor(num) == num){
            return ("\(Int(num))")
        }
        return ("\(num)")
    }
    
}

func manageProfilePrivacy (isFollowing: Bool, showMyProfile: Bool) -> Bool{
    return isFollowing || showMyProfile
}

func animateFan(image: UIImageView){
       let duration = 2.0
       let delay = 0.0
       let options = UIView.KeyframeAnimationOptions.calculationModeLinear
       let fullRotation = CGFloat(M_PI * 2)


       UIView.animateKeyframes(withDuration: duration, delay: delay, options: options, animations: {
           // each keyframe needs to be added here
           // within each keyframe the relativeStartTime and relativeDuration need to be values between 0.0 and 1.0
           
           UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/3, animations: {
               // start at 0.00s (5s × 0)
               // duration 1.67s (5s × 1/3)
               // end at   1.67s (0.00s + 1.67s)
               image.transform = CGAffineTransform(rotationAngle: 1/3 * fullRotation)
           })
           UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3, animations: {
               image.transform = CGAffineTransform(rotationAngle: 2/3 * fullRotation)
           })
           UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3, animations: {
               image.transform = CGAffineTransform(rotationAngle: 3/3 * fullRotation)
           })
           
           }, completion: {finished in
               // any code entered here will be applied
               // once the animation has completed
           
           })
   }


// custom log
func logInfo(_ message: String, file: String = #file, function: String = #function, line: Int = #line, column: Int = #column) {
    if (showLog) {
        print("\(function): \(line): \(message)")
    }
}

func presentAlert(_ titleStr : String?,msgStr : String?,controller : AnyObject?){
    DispatchQueue.main.async(execute: {
        let alert=UIAlertController(title: titleStr, message: msgStr, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil));
        
        //event handler with closure
        controller!.present(alert, animated: true, completion: nil);
    })
}

func getDateFromString(strDate: String) -> String {
    
    let fromDate = Date.init(timeIntervalSince1970: (Double(strDate)!))
    //let date = Date()
    let timePeriodFormatter = DateFormatter()
    timePeriodFormatter.dateFormat = "hh:mm a"
    
    var dateString :String = ""
    
    let timeString = timePeriodFormatter.string(from: fromDate)
    let calendar = NSCalendar.current
    if calendar.isDateInYesterday(fromDate) {
        dateString = "Yesterday"
    }else if calendar.isDateInToday(fromDate) {
        dateString = "Today"
    }else if calendar.isDateInTomorrow(fromDate) {
        dateString = "Tomorrow"
    }else {
        
        let datePeriodFormatter = DateFormatter()
        datePeriodFormatter.dateFormat = "MMM dd YYYY"
        dateString = datePeriodFormatter.string(from: fromDate)
        
    }
    return String.init(format: "%@, %@", dateString,timeString)
}
func getShortDateFromString(strDate: String) -> String {
    
    let fromDate = Date.init(timeIntervalSince1970: (Double(strDate)!))
    //let date = Date()
    let timePeriodFormatter = DateFormatter()
    timePeriodFormatter.dateFormat = "hh:mm a"
    
    var dateString :String = ""
    
    let timeString = timePeriodFormatter.string(from: fromDate)
    let calendar = NSCalendar.current
    if calendar.isDateInYesterday(fromDate) {
        dateString = "Yesterday"
    }else if calendar.isDateInToday(fromDate) {
        dateString = "Today"
    }else if calendar.isDateInTomorrow(fromDate) {
        dateString = "Tomorrow"
    }else {
        
        let datePeriodFormatter = DateFormatter()
        datePeriodFormatter.dateFormat = "MMM dd YYYY"
        dateString = datePeriodFormatter.string(from: fromDate)
        
    }
    return String.init(format: "%@, %@", dateString,timeString)
}
func getNameFromDay(dayPass : String) ->  String {
    var dayName = ""
    if dayPass == "0" {
        dayName = "Sunday"
    } else if (dayPass == "1")  {
        dayName = "Monday"
    } else if (dayPass == "2")  {
        dayName = "Tuesday"
    } else if (dayPass == "3")  {
        dayName = "Wednesday"
    } else if (dayPass == "4")  {
        dayName = "Thursday"
    } else if (dayPass == "5")  {
        dayName = "Friday"
    } else if (dayPass == "6")  {
        dayName = "Saturday"
    }
    return dayName
}

func getCurrentFormatDateOrder(strDate : String) -> String {
    if !strDate.isEmpty {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateForm = dateFormatter.date(from: strDate)
        
        if dateForm == nil {
            return ""
        } else {
            dateFormatter.dateFormat = "dd MMM yyyy hh:mm:ss a"
            let dateString = dateFormatter.string(from: dateForm!)
            return dateString
        }
    }else {
        return ""
    }
}


func currentDateTime() -> String {
    let currentDate = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MMM, HH:mm"
    return dateFormatter.string(from: currentDate)
}



func getCurrentFormatDateOrderTime(strDate : String) -> String {
    if !strDate.isEmpty {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateForm = dateFormatter.date(from: strDate)
        
        if dateForm == nil {
            return ""
        } else {
            
            //            let dateAsString = "13:15"
            //            let dateFormatter = DateFormatter()
            //            dateFormatter.dateFormat = "HH:mm"
            //
            //            let date = dateFormatter.date(from: dateAsString)
            //            dateFormatter.dateFormat = "h:mm a"
            //            let Date12 = dateFormatter.string(from: date!)
            
            
            dateFormatter.dateFormat = "h:mm a"
            let dateString = dateFormatter.string(from: dateForm!)
            return dateString
        }
    }else {
        return ""
    }
}





func getDesiredDateTimeStringFromGivenString(strPassString:String ,fromFormat:String, toFormat:String) -> String {
    if !strPassString.isEmpty {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let dateForm = dateFormatter.date(from: strPassString)
        
        if dateForm == nil {
            return ""
        } else {
            
            dateFormatter.dateFormat = toFormat
            let dateString = dateFormatter.string(from: dateForm!)
            return dateString
        }
    }else {
        return ""
    }
}


func getDateFromModalString(strPassString:String ,fromFormat:String) -> Date {
    
    if !strPassString.isEmpty {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let dateForm = dateFormatter.date(from: strPassString)
        
        if dateForm == nil {
            return Date()
        } else {
            return dateForm!
        }
    }else{
        return Date()
    }
    
}

func getReadableShortTime(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm"
        return dateFormatter.string(from: date)
    }
    return ""
}

func getReadableTime(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: date)
    }
    return ""
}

func getReadableTimeWith24HourDateFormat(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm"
        return dateFormatter.string(from: date)
    }
    return ""
}




func getReadableDate(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: date)
    }
    return ""
}

func getReadableDateWithYear(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: date)
    }
    return ""
}
func getReadableDateWithYearAndTime(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM,yyyy,hh:mm a"
        return dateFormatter.string(from: date)
    }
    return ""
}

func getReadableDateWithTime(timeStamp: String) -> String? {
    if let timeDouble = Double(timeStamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, hh:mm a"
        return dateFormatter.string(from: date)
    }
    return ""
}

func getDateString(from date: Date, with format: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = TimeZone.current
    return dateFormatter.string(from: date)
}

func getDate(from string: String, with format: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = TimeZone.current
    return dateFormatter.date(from: string) ?? Date()
}

func convertDate(date: String, from: String, to: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = from
    dateFormatter.timeZone = TimeZone.current
    
    let date = dateFormatter.date(from: date) ?? Date()
    
    dateFormatter.dateFormat = to
    
    return dateFormatter.string(from: date)
}


func getDateFromTimestamp(timestamp: String) -> Date {
    if let timeDouble = Double(timestamp){
        let date = Date(timeIntervalSince1970: timeDouble)
        return date
    }
    return Date()
}

//MARK:- NO DATA FOUND
func noDataFound(message : String,tableView: UITableView,tag: Int){
    let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.tag = tag
        noDataLabel.text          = message
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        tableView.backgroundView  = noDataLabel
        tableView.separatorStyle  = .none
}


func getViewWithActivityIndicatot(withHeight : CGFloat) -> UIView {
    let indicator = UIActivityIndicatorView()
    indicator.color = .orange
    indicator.hidesWhenStopped = true
    indicator.startAnimating()
    indicator.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 15, y: 50 / 2 - 15, width: 30, height: 30)
    let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: withHeight))
    view.addSubview(indicator)
    return view
}

//MARK:- NO DATA FOUND LABEL FOR VC
func noDataFound(message : String,viewController: UIViewController,tag: Int){
    let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        noDataLabel.tag = tag
        noDataLabel.text          = message
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
    viewController.view.addSubview(noDataLabel)
}

func getDateForInfoFromTimeStamp(strDate: String) -> String {
    
    let fromDate = Date.init(timeIntervalSince1970: (Double(strDate)!))
    
    var dateString :String = ""
    var formattedDateString : String = ""
    var prefixString : String = ""
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd"
    dateString = dateFormatter.string(from: fromDate)
    
    if(dateString == "1" || dateString == "21" || dateString == "31"){
        prefixString = "st"
        
    } else if (dateString ==  "2" || dateString == "22"){
        prefixString = "nd"
        
    } else if (dateString == "3" || dateString == "23"){
        prefixString = "rd"
        
    }else{
        prefixString = "th"
    }
    
    let dateCompleteFormatter = DateFormatter()
    dateCompleteFormatter.dateFormat = String.init(format:"MMMM dd'%@', YYYY" , prefixString)
    formattedDateString = dateCompleteFormatter.string(from: fromDate)
    return formattedDateString
}

func getTimeFromString(strDate: String) -> String {
    
    let fromDate = Date.init(timeIntervalSince1970: (Double(strDate)!))
    let timePeriodFormatter = DateFormatter()
    timePeriodFormatter.dateFormat = "hh:mm a"
    let timeString = timePeriodFormatter.string(from: fromDate)
    return timeString
}

func getTimeFromDate(strDate: Date) -> String {
    
    let timePeriodFormatter = DateFormatter()
    timePeriodFormatter.dateFormat = "hh:mm a"
    let timeString = timePeriodFormatter.string(from: strDate)
    return timeString
}



func getAddressFromLatLon(selectedLatitude: Double, withLongitude selectedLongitude: Double) {
    var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
    let ceo: CLGeocoder = CLGeocoder()
    center.latitude = selectedLatitude
    center.longitude = selectedLongitude
    var addressString : String = ""
    let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
    
    ceo.reverseGeocodeLocation(loc, completionHandler:
        {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            let pm = placemarks! as [CLPlacemark]
            
            if pm.count > 0 {
                let pm = placemarks![0]
                
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
            }
    })
}

func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
    DispatchQueue.global(qos: .background).async { () -> Void in
        let eventStore = EKEventStore()
        
        if (EKEventStore.authorizationStatus(for: .event) != EKAuthorizationStatus.authorized) {
            eventStore.requestAccess(to: .event, completion: {
                granted, error in
                eventStore.requestAccess(to: .event, completion: { (granted, error) in
                    if (granted) && (error == nil) {
                        let event = EKEvent(eventStore: eventStore)
                        event.title = title
                        event.startDate = startDate
                        event.endDate = endDate
                        event.notes = description
                        event.calendar = eventStore.defaultCalendarForNewEvents
                        do {
                            try eventStore.save(event, span: .thisEvent)
                        } catch let e as NSError {
                            completion?(false, e)
                            return
                        }
                        completion?(true, nil)
                    } else {
                        completion?(false, error as NSError?)
                    }
                })
            })
        } else {
            eventStore.requestAccess(to: .event, completion: { (granted, error) in
                if (granted) && (error == nil) {
                    let event = EKEvent(eventStore: eventStore)
                    event.title = title
                    event.startDate = startDate
                    event.endDate = endDate
                    event.notes = description
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    
                    do {
                        try eventStore.save(event, span: .thisEvent)
                    } catch let e as NSError {
                        completion?(false, e)
                        return
                    }
                    completion?(true, nil)
                } else {
                    completion?(false, error as NSError?)
                }
            })
        }
        
    }
}

func presentAlertWithOptions(_ title: String, message: String,controller : AnyObject, buttons:[String], tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert, buttons: buttons, tapBlock: tapBlock)
    controller.present(alert, animated: true, completion: nil)
    return alert
}

func delay(delay: Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func getHint(element:AnyObject) -> IndexPath {
    let accessibilityString :String = (element.accessibilityLabel as? String)!
    let accessibilityArray : Array<String> = accessibilityString.components(separatedBy: ",")
    return (accessibilityArray.count == 2) ? IndexPath(row: Int(accessibilityArray.first!)!, section: Int(accessibilityArray.last!)!) : IndexPath(row: Int(-1), section: Int(-1))
}

func convertImageToBase64(image: UIImage) -> String {
    let imageData = image.pngData()!
    return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
}



func getDateFromString(dateString : String) -> Date {
    
    if !dateString.isEmpty {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.date(from: dateString)!
    }
    
    return Date()
}


//MARK:- Thread

func moveToMainThread(_ work: @escaping () -> Void) {
    if Thread.isMainThread {
        work()
    } else {
        DispatchQueue.main.async {
            work()
        }
    }
}


private extension UIAlertController {
    
    convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style, buttons:[String], tapBlock:((UIAlertAction,Int) -> Void)?) {
        self.init(title: title, message: message, preferredStyle:preferredStyle)
        var buttonIndex = 0
        for buttonTitle in buttons {
            let action = UIAlertAction(title: buttonTitle, preferredStyle: .default, buttonIndex: buttonIndex, tapBlock: tapBlock)
            buttonIndex += 1
            self.addAction(action)
        }
    }
}

private extension UIAlertAction {
    convenience init(title: String?, preferredStyle: UIAlertAction.Style, buttonIndex:Int, tapBlock:((UIAlertAction,Int) -> Void)?) {
        
        
        self.init(title: title, style: preferredStyle) {
            (action:UIAlertAction) in
            if let block = tapBlock {
                block(action,buttonIndex)
            }
        }
    }
}

class AppUtility: NSObject {
    
    class  func leftBarButton(_ imageName : NSString,controller : UIViewController) -> UIBarButtonItem {
        let button:UIButton = UIButton.init(type: UIButton.ButtonType.custom)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named: imageName as String), for: UIControl.State())
        button.addTarget(controller, action: #selector(leftBarButtonAction(_:)), for: UIControl.Event.touchUpInside)
        let leftBarButtonItem:UIBarButtonItem = UIBarButtonItem(customView: button)
        
        return leftBarButtonItem
    }
    
    class  func rightBarButton(_ imageName : NSString,controller : UIViewController) -> UIBarButtonItem {
        let button:UIButton = UIButton.init(type: UIButton.ButtonType.custom)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named: imageName as String), for: UIControl.State())
        button.addTarget(controller, action: #selector(rightBarButtonAction(_:)), for: UIControl.Event.touchUpInside)
        let leftBarButtonItem:UIBarButtonItem = UIBarButtonItem(customView: button)
        
        return leftBarButtonItem
    }
    
    class  func rightBarButtonTwo(_ imageName : NSString,controller : UIViewController) -> UIBarButtonItem {
        let button:UIButton = UIButton.init(type: UIButton.ButtonType.custom)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named: imageName as String), for: UIControl.State())
        button.addTarget(controller, action: #selector(rightBarButtonTwoAction(_:)), for: UIControl.Event.touchUpInside)
        let leftBarButtonItem:UIBarButtonItem = UIBarButtonItem(customView: button)
        
        return leftBarButtonItem
    }
    
    @objc   func leftBarButtonAction(_ button : UIButton) {
        
    }
    
    @objc   func rightBarButtonAction(_ button : UIButton) {
        
    }
    @objc   func rightBarButtonTwoAction(_ button : UIButton) {
        
    }
    
    
    
}


//MARK:- HexCode to UIColor
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
   
}


enum GradientType {
    case button
    case buttonBottom
    case view
    case halfButton
    case bottomView
    case upView
    case genderView
    case fanView
    case acceptView
    case adviceView


}


//

extension UIImageView {
    
    //MARK: - Set Image from URL
    func setImageFromUrl(_ url :String?, placeholder: UIImage? = UIImage()) {
        
        kf.indicatorType = .activity
        guard let urlTemp = URL(string: (url ?? "")) else {
            self.image = placeholder ;
            return
        }
        
        kf.setImage(with: urlTemp, placeholder: placeholder, options: nil, progressBlock: nil) { (image, error, cacheType, urls) in
            
            if(error != nil){
                self.kf.setImage(with: URL(string: url ?? ""), placeholder: UIImage(named: ""), options: nil)
            }else{
                print("not able to load image")
            }
        }
    }
}


//MARK:- Watermark method

func addWaterMarkImage(url: URL, completion: @escaping (_ url: URL?) -> Void) {
    if let item = MediaItem(url: url) {
        let logoImage = UIImage(named: "ic_watermark")
        let firstElement = MediaElement(image: logoImage!)
        firstElement.frame = CGRect(x: 10, y: 10, width: 120, height: 100)
        item.add(elements: [firstElement])
        let mediaProcessor = MediaProcessor()
        mediaProcessor.processElements(item: item) { (result, error) in
            DispatchQueue.main.async {
                completion(result.processedUrl)
            }
        }
    }
}
